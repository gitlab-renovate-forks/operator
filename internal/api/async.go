package api

import (
	"context"
	"sync"
)

/*
 * WARNING: Experimental code. Do not use in production.
 */

// Promise provides an async implementation of loading referenced resources.
//
// WARNING: Experimental code.
type Promise[T any] struct {
	sync.WaitGroup

	ref Reference[T]
	res T
	err error
}

func NewPromise[T any](ctx context.Context, reference Reference[T]) *Promise[T] {
	p := &Promise[T]{ref: reference}

	p.Add(1)
	go func(ctx context.Context) {
		p.res, p.err = p.ref.Resolve(ctx)
		p.Done()
	}(ctx)

	return p
}

func (p *Promise[T]) Then(ctx context.Context, resource Resource[T]) {
	go func(ctx context.Context) {
		p.Wait()
		if p.err != nil {
			panic(p.err)
		}
		resource.Consume(ctx, p.res)
	}(ctx)
}
