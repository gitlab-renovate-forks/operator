package templateutils

import (
	"fmt"

	"github.com/huandu/xstrings"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

func getRedisConfigFileName(redis v2alpha2.Redis) string {
	redisName := xstrings.ToSnakeCase(redis.Name)

	if redisName == "main" || redisName == "default" {
		redisName = "resque"
	}

	prefix := "redis."
	if redisName == "resque" || redisName == "cable" {
		prefix = ""
	}

	return fmt.Sprintf("%s%s.yml", prefix, redisName)
}

func mapRedisConfigFileNames(list []v2alpha2.Redis) map[string]any {
	fileNames := map[string]any{}

	for _, redis := range list {
		fileNames[redis.Name] = getRedisConfigFileName(redis)
	}

	return fileNames
}

var (
	redisSettingsMapper = mapper{
		keySet: []string{
			"configCommand",
			"connectTimeout",
			"readTimeout",
			"writeTimeout",
		},
		keyMapping: map[string]func(string) string{},
		valueMapping: map[string]func(string) any{
			"connectTimeout": toInt,
			"readTimeout":    toInt,
			"writeTimeout":   toInt,
		},
	}
)
