package applicationconfig

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func selectSecret(name string, key string) *corev1.SecretKeySelector {
	return &corev1.SecretKeySelector{
		LocalObjectReference: corev1.LocalObjectReference{
			Name: name,
		},
		Key: key,
	}
}

func refSecret(namespace, name string) *corev1.SecretReference {
	return &corev1.SecretReference{
		Namespace: namespace,
		Name:      name,
	}
}

func makeSecret(namespace, name string, kvs ...string) *corev1.Secret {
	dataMap := map[string][]byte{}
	for i := 0; i < len(kvs); i += 2 {
		dataMap[kvs[i]] = []byte(kvs[i+1])
	}

	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Data: dataMap,

		Type: corev1.SecretTypeOpaque,
	}
}
