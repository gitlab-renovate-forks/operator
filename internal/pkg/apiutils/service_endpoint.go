package apiutils

import (
	"fmt"
	"strings"

	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// ServiceEndpoint is a representation of an endpoint that is translated from
// a ServiceProvider specification.
type ServiceEndpoint struct {
	Scheme string
	Host   string
	Port   int32
	UseTLS bool
}

// ServiceEndpointType is a string representation of the type of service endpoint.
type ServiceEndpointType string

const (
	UnknownServiceEndpoint    ServiceEndpointType = ""
	PostgreSQLServiceEndpoint ServiceEndpointType = "PostgreSQL"
	RedisServiceEndpoint      ServiceEndpointType = "Redis"
	ClickHouseServiceEndpoint ServiceEndpointType = "ClickHouse"
	GitalyServiceEndpoint     ServiceEndpointType = "Gitaly"
	PrometheusServiceEndpoint ServiceEndpointType = "Prometheus"
	MattermostServiceEndpoint ServiceEndpointType = "Mattermost"
	SMTPServiceEndpoint       ServiceEndpointType = "SMTP"
	IMAPServiceEndpoint       ServiceEndpointType = "IMAP"
	HTTPServiceEndpoint       ServiceEndpointType = "HTTP"
)

// ResolveServiceProvider resolves a ServiceProvider into a ServiceEndpoint
// without dereferencing the provider Service reference.
//
// Depending on the specified endpoint type, the ServiceEndpoint is constructed
// differently.
//
// The function uses the following defaults:
//
// | Endpoint Type | Scheme     | TLS Scheme | Port | TLS Port |
// |---------------|------------|------------|------|----------|
// | PostgreSQL    | postgresql | postgresql | 5432 | 5432     |
// | Redis         | redis      | rediss     | 6379 | 6379     |
// | ClickHouse    | http       | https      | 8123 | 8443     |
// | Gitaly        | tcp        | tls        | 8075 | 8076     |
// | Prometheus    | http       | https      | 9090 | 443      |
// | Mattermost    | http       | https      | 8065 | 443      |
// | SMTP          | smtp       | smtp       | 25   | 465      |
// | IMAP          | imap       | imap       | 143  | 993      |
//
// The default ports are applied when the ServiceProvider does not specify a
// port or the port can not be deduced from the referenced Service.
func ResolveServiceProvider(rtCtx *framework.RuntimeContext, provider v2alpha2.ServiceEndpoint, endpointType ServiceEndpointType, opts ...ServiceProviderResolverOption) (ServiceEndpoint, error) {
	resolver := getProviderResolver(endpointType, opts...)

	return resolver.Resolve(rtCtx, provider)
}

// ProviderResolver is an interface for resolving ServiceProvider objects into
// ServiceEndpoints.  It is responsible for converting the ServiceProvider
// specification into a concrete ServiceEndpoint.
type ProviderResolver interface {
	Resolve(*framework.RuntimeContext, v2alpha2.ServiceEndpoint) (ServiceEndpoint, error)
}

// ServiceProviderResolverOption is used to configure a ProviderResolver. It
// allows for further customization of the resolver's behavior.
type ServiceProviderResolverOption func(ProviderResolver)

// TypeDereferencer is a function type for dereferencing Kubernetes object
// references. It uses an ObjectReference, and returns the dereferenced typed
// object.
type TypeDereferencer[T client.Object] func(rtCtx *framework.RuntimeContext, ref corev1.ObjectReference) (T, error)

// WithObjectDereferencer is a resolver option that allows setting a custom
// dereferencer for specific object types, for example Service objects.
//
// NOTE: Currently, we only support Service references.
func WithObjectDereferencer[T client.Object](d TypeDereferencer[T]) ServiceProviderResolverOption {
	return func(resolver ProviderResolver) {
		if r, ok := resolver.(*defaultProviderResolver); ok {
			if dSvc, ok := any(d).(TypeDereferencer[*corev1.Service]); ok {
				r.dereferenceService = dSvc
			}
		}
	}
}

// getProviderResolver returns a resolver that works best for the given endpoint
// type. Currently, we use a default resolver for all endpoint types. But this
// can change for individual endpoint types.
func getProviderResolver(endpointType ServiceEndpointType, opts ...ServiceProviderResolverOption) ProviderResolver {
	defaultResolver := &defaultProviderResolver{
		endpointType: endpointType,
		scheme:       defaultScheme[endpointType],
	}

	for _, opt := range opts {
		opt(defaultResolver)
	}

	return defaultResolver
}

// defaultProviderResolver is a one-size-fits-all solution for all endpoint
// types.
type defaultProviderResolver struct {
	endpointType       ServiceEndpointType
	scheme             string
	dereferenceService func(rtCtx *framework.RuntimeContext, ref corev1.ObjectReference) (*corev1.Service, error)
}

func (r *defaultProviderResolver) Resolve(rtCtx *framework.RuntimeContext, provider v2alpha2.ServiceEndpoint) (ServiceEndpoint, error) {
	result := ServiceEndpoint{
		Scheme: r.scheme,
		UseTLS: false,
	}

	if provider.External != nil {
		result.Host = provider.External.Host
		result.Port = provider.External.Port
	} else if provider.Service != nil {
		if r.dereferenceService != nil {
			svc, err := r.dereferenceService(rtCtx, *provider.Service.DeepCopy())
			if err != nil {
				return result, err
			}

			result.Host = fmt.Sprintf("%s.%s.svc.cluster.local", svc.Name, svc.Namespace)
			result.Port = r.findPortNumber(svc)
		} else {
			result.Host = fmt.Sprintf("%s.%s.svc.cluster.local", provider.Service.Name, provider.Service.Namespace)
		}
	} else {
		return result, fmt.Errorf("ambagious ServiceProvider for %s", r.endpointType)
	}

	if provider.TLS != nil {
		result.UseTLS = *provider.TLS
	}

	if result.Port == 0 {
		if result.UseTLS {
			result.Port = defaultTLSPorts[r.endpointType]
		} else {
			result.Port = defaultPorts[r.endpointType]
		}
	}

	result.Scheme = r.updateScheme(result.Scheme, result.UseTLS)

	return result, nil
}

// Returns the port number if port name or appProtocol fields of the Service
// resource contain the scheme or endpoint type.
func (r *defaultProviderResolver) findPortNumber(svc *corev1.Service) int32 {
	namesToMatch := []string{
		r.scheme, strings.ToLower(string(r.endpointType)),
	}

	for _, port := range svc.Spec.Ports {
		if containsIgnoreCase(port.Name, namesToMatch) {
			return port.Port
		}

		if port.AppProtocol != nil && containsIgnoreCase(*port.AppProtocol, namesToMatch) {
			return port.Port
		}
	}

	return 0
}

func (r *defaultProviderResolver) updateScheme(scheme string, tls bool) string {
	if !tls {
		return scheme
	}

	switch r.endpointType {
	case RedisServiceEndpoint:
		return "rediss"
	case ClickHouseServiceEndpoint, PrometheusServiceEndpoint, MattermostServiceEndpoint:
		return "https"
	case GitalyServiceEndpoint:
		return "tls"
	default:
		return scheme
	}
}

func containsIgnoreCase(s string, substr []string) bool {
	for _, sub := range substr {
		if strings.Contains(strings.ToLower(s), strings.ToLower(sub)) {
			return true
		}
	}

	return false
}

var (
	defaultScheme map[ServiceEndpointType]string = map[ServiceEndpointType]string{
		PostgreSQLServiceEndpoint: "postgresql",
		RedisServiceEndpoint:      "redis",
		ClickHouseServiceEndpoint: "http",
		GitalyServiceEndpoint:     "tcp",
		PrometheusServiceEndpoint: "http",
		MattermostServiceEndpoint: "http",
		SMTPServiceEndpoint:       "smtp",
		IMAPServiceEndpoint:       "imap",
	}

	defaultPorts map[ServiceEndpointType]int32 = map[ServiceEndpointType]int32{
		PostgreSQLServiceEndpoint: 5432,
		RedisServiceEndpoint:      6379,
		ClickHouseServiceEndpoint: 8123,
		GitalyServiceEndpoint:     8075,
		PrometheusServiceEndpoint: 9090,
		MattermostServiceEndpoint: 8065,
		SMTPServiceEndpoint:       25,
		IMAPServiceEndpoint:       143,
	}

	defaultTLSPorts map[ServiceEndpointType]int32 = map[ServiceEndpointType]int32{
		PostgreSQLServiceEndpoint: 5432,
		RedisServiceEndpoint:      6379,
		ClickHouseServiceEndpoint: 8443,
		GitalyServiceEndpoint:     8076,
		PrometheusServiceEndpoint: 443,
		MattermostServiceEndpoint: 443,
		SMTPServiceEndpoint:       465,
		IMAPServiceEndpoint:       993,
	}
)
