package controllerutils

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// LocalObjectReferenceToObjectReference converts a LocalObjectReference to an
// ObjectReference. It uses the given kind and API version to construct the
// ObjectReference. The optional source is used to determine the namespace of
// the ObjectReference.
func LocalObjectReferenceToObjectReference(localRef corev1.LocalObjectReference, source client.Object, kind, apiVersion string) corev1.ObjectReference {
	ref := corev1.ObjectReference{
		APIVersion: apiVersion,
		Kind:       kind,
		Name:       localRef.Name,
	}

	if source != nil {
		ref.Namespace = source.GetNamespace()
	}

	return ref
}

// SecretKeySelectorToObjectReference converts a SecretKeySelector to an ObjectReference.
// It use "v1" and "Secret" API version and kind to construct the ObjectReference.
// The optional source is used to determine the namespace of the ObjectReference.
func SecretKeySelectorToObjectReference(keySelector corev1.SecretKeySelector, source client.Object) corev1.ObjectReference {
	ref := corev1.ObjectReference{
		APIVersion: coreAPIVersion,
		Kind:       kindSecret,
		Name:       keySelector.Name,
		FieldPath:  fmt.Sprintf(".data.%q", keySelector.Key),
	}

	if source != nil {
		ref.Namespace = source.GetNamespace()
	}

	return ref
}

// SecretReferenceToObjectReference converts a SecretReference to an ObjectReference.
// It use "v1" and "Secret" API version and kind to construct the ObjectReference.
// The optional source is used to determine the namespace of the ObjectReference
// if not specified.
func SecretReferenceToObjectReference(secretRef corev1.SecretReference, source client.Object) corev1.ObjectReference {
	ref := corev1.ObjectReference{
		APIVersion: coreAPIVersion,
		Kind:       kindSecret,
		Name:       secretRef.Name,
		Namespace:  secretRef.Namespace,
	}

	if ref.Namespace == "" && source != nil {
		ref.Namespace = source.GetNamespace()
	}

	return ref
}

// Dereference returns the object referenced by the given reference. It returns
// an error if the reference is not resolvable. The reference must contain valid
// APIVersion and Kind that is mapped to a type in the runtime scheme.
func Dereference(rtCtx *framework.RuntimeContext, ref corev1.ObjectReference) (client.Object, error) {
	gvk := ref.GroupVersionKind()

	rtObj, err := framework.Scheme.New(gvk)
	if err != nil {
		return nil, fmt.Errorf("failed to create new object: %w", err)
	}

	obj, isClientObj := rtObj.(client.Object)
	if !isClientObj {
		return nil, fmt.Errorf("object is not a client.Object: %T", obj)
	}

	objMeta, err := meta.Accessor(obj)
	if err != nil {
		return nil, fmt.Errorf("failed to get object metadata: %w", err)
	}

	objMeta.SetName(ref.Name)
	objMeta.SetNamespace(ref.Namespace)

	err = framework.Client.Get(rtCtx.Context, client.ObjectKeyFromObject(obj), obj)
	if err != nil {
		return nil, fmt.Errorf("failed to get object from cluster: %w", err)
	}

	return obj, nil
}

// DereferenceHandler is a versatile utility to dereference various object
// references and service providers.
//
// It identifies missing references and returns them in the Missing field
// instead of breaking the process and returning an error. This is useful for
// try and wait loops.
type DereferenceHandler struct {
	tasks []dereferenceTask

	Missing []*corev1.ObjectReference
}

// NewDereferenceHandler returns a new DereferenceHandler.
func NewDereferenceHandler() *DereferenceHandler {
	return &DereferenceHandler{
		tasks:   []dereferenceTask{},
		Missing: []*corev1.ObjectReference{},
	}
}

// AddObjectReference adds an ObjectReference.
func (h *DereferenceHandler) AddObjectReference(ref corev1.ObjectReference, callback func(any)) {
	h.tasks = append(h.tasks, dereferenceTask{
		reference: &ref,
		callback:  callback,
	})
}

// AddSecretReference adds a SecretReference and converts it to an
// ObjectReference.
func (h *DereferenceHandler) AddSecretReference(source client.Object, ref corev1.SecretReference, callback func(any)) {
	h.AddObjectReference(SecretReferenceToObjectReference(ref, source), callback)
}

// AddSecretKeySelector adds a SecretKeySelector and converts it to an
// ObjectReference.
func (h *DereferenceHandler) AddSecretKeySelector(source client.Object, ref corev1.SecretKeySelector, callback func(any)) {
	h.AddObjectReference(SecretKeySelectorToObjectReference(ref, source), callback)
}

// AddServiceReference adds a ServiceReference and overrides it as a reference
// to a Service type.
func (h *DereferenceHandler) AddServiceReference(source client.Object, ref corev1.ObjectReference, callback func(any)) {
	if ref.APIVersion == "" {
		ref.APIVersion = "v1"
	}

	if ref.Kind == "" {
		ref.Kind = kindService
	}

	if ref.Namespace == "" {
		ref.Namespace = source.GetNamespace()
	}

	h.AddObjectReference(ref, callback)
}

// AddServiceProvider adds a ServiceProvider.
func (h *DereferenceHandler) AddServiceProvider(source client.Object, provider v2alpha2.ServiceEndpoint, endpointType apiutils.ServiceEndpointType, callback func(any)) {
	if provider.Service != nil && provider.Service.Namespace == "" {
		provider.Service.Namespace = source.GetNamespace()
	}

	h.tasks = append(h.tasks, dereferenceTask{
		provider:     &provider,
		endpointType: endpointType,
		callback:     callback,
	})
}

// Dereference attempts dereferencing all entries that are added and reports the
// missing resources. If any error other than NotFound is encountered, it is
// returned.
func (h *DereferenceHandler) Dereference(rtCtx *framework.RuntimeContext) error {
	for _, task := range h.tasks {
		err := task.resolve(rtCtx)
		if err != nil {
			if errors.IsNotFound(err) {
				if ref := task.getObjectReference(); ref != nil {
					h.Missing = append(h.Missing, ref)
				}

				continue
			}

			return err
		}
	}

	return nil
}

func (h *DereferenceHandler) Ready() bool {
	return len(h.Missing) == 0
}

type dereferenceTask struct {
	reference    *corev1.ObjectReference
	provider     *v2alpha2.ServiceEndpoint
	endpointType apiutils.ServiceEndpointType
	callback     func(any)
}

func (t *dereferenceTask) resolveObjectReference(rtCtx *framework.RuntimeContext) (bool, error) {
	if t.reference == nil {
		return false, nil
	}

	obj, err := Dereference(rtCtx, *t.reference)
	if err != nil {
		return true, err
	}

	if t.callback != nil {
		t.callback(obj)
	}

	return true, nil
}

func (t *dereferenceTask) resolveServiceProvider(rtCtx *framework.RuntimeContext) (bool, error) {
	if t.provider == nil {
		return false, nil
	}

	endpoint, err := DereferenceServiceProvider(rtCtx, *t.provider, t.endpointType)
	if err != nil {
		return true, err
	}

	if t.callback != nil {
		t.callback(endpoint)
	}

	return true, nil
}

func (t *dereferenceTask) resolve(rtCtx *framework.RuntimeContext) error {
	if ok, err := t.resolveObjectReference(rtCtx); err != nil {
		return err
	} else if ok {
		return nil
	}

	if _, err := t.resolveServiceProvider(rtCtx); err != nil {
		return err
	}

	return nil
}

func (t *dereferenceTask) getObjectReference() *corev1.ObjectReference {
	if t.reference != nil {
		return t.reference
	}

	if t.provider.Service != nil {
		ref := *t.provider.Service
		if ref.Kind == "" {
			ref.Kind = kindService
		}

		if ref.APIVersion == "" {
			ref.APIVersion = coreAPIVersion
		}

		return &ref
	}

	return nil
}

const (
	coreAPIVersion = "v1"
	kindConfigMap  = "ConfigMap"
	kindService    = "Service"
	kindSecret     = "Secret"
)
