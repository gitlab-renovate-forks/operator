# Internal API

**Date**: 2025-03-05

**Status**: Accepted

## Context

1. GitLab Kubernetes API in `api/` is a public API that is versioned, for
   example `v2alpha2`, and subject to change. To support GitLab custom resources
   the controllers form a strong bond to this API and its version.

1. The controller cannot use the public API without manipulating it first. For
   example, in the public API, for better user experience and security some
   resources are referenced and some configuration items are abridged.

## Decision

1. We introduce a new internal API in `internal/api/`. The internal API is NOT
   versioned. It serves as an intermediary between versioned public API types
   and the controller logic. While the internal API does not completely abstract
   public API types, it acts as a buffer that absorbs some changes in the public
   API, reducing the impact of such changes on controllers.

1. The internal API provides provides additional features for handling latent or
   referenced resources and de-referencing Kubernetes objects, so that the
   controller can focus on the core logic.

1. The internal API provides means to define shadow representations of public
   API types. Complex API types can be composed by putting together simpler
   building blocks and the additional features can be added to the shadow
   representations with minimal effort.

## References

- [Composite design pattern](https://refactoring.guru/design-patterns/composite).

## Consequences

1. Each GitLab custom resource will have a shadow representation in the internal
   API. The internal API may inherit the public API type and add the additional
   features.

1. The controllers can still use both public and internal API types, but in the
   controller logic, the internal API types are preferred.

1. When the public API changes, the internal API may change but depending on the
   change the controller logic may remain intact. For example, when the version
   of public API is changed, without any significant semantic changes, the
   internal API shadow has to use the new version, but the controller logic can
   remain the same.
