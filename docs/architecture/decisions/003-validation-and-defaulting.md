# API validation and defaulting

**Date**: 2024-11-12

**Status**: Accepted

## Context

The custom resources managed by the Operator need to be validated and need
to define sensible defaults.

The Kubernetes ecosystem supports different manners to archive this, including:

1. Schema-based validation and defaults, e.g. using [kubebuilder markers](https://book.kubebuilder.io/reference/markers/crd-validation).
1. [Validating and mutating admission webhooks](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#what-are-admission-webhooks).
1. [Common Expression Language (CEL) validation rules](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/#validation-rules),
   mainly for cross-field validation. CEL rules are also part of the CRD schema and
   are executed by the API server when a custom resource updated or created.
   The feature is stable since Kubernetes 1.29.

## Decision

### Validation

* Individual fields use [kubebuilder validation markers](https://book.kubebuilder.io/reference/markers/crd-validation)
* Fields that require cross-field validation use [CEL rules](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#validatingadmissionwebhook).
  CEL rules can be defined using `kubebuilder:validation:XValidation` markers.

### Defaulting

* The controller applies sensible defaults for optional fields at runtime.
  The defaults are not stored in the custom resource.
* The exception to this are fields that enable or disable workflows or components.
  These should me made transparent to users using `kubebuilder:default` markers.

## Consequences

* Non-breaking changes to validation rules result in changes to CRDs. As a result,
  users need to update their CRD for the same API version. Depending on their
  Operator installation method, this may require additional support or documentation.
  For example, [Helm does not support updating CRDs](https://helm.sh/docs/chart_best_practices/custom_resource_definitions/#some-caveats-and-explanations)
  and a workaround needs to be provided and communicated to the users that use Helm
  to install GitLab Operator.
* User-defined custom resources are validated at the API server level,
  to catch any errors as early as possible.
* Changes to defaults are propagated to all instances when the Operator is upgraded.
  This requires thoughtful changes to the defaults that could break an instance.
* Updates to validation rules and markers must be non-breaking, meaning that:
  * New required fields can not be introduced.
  * Existing fields can not removed.
  * Validation rules can not be updated to be more restrictive.
* Breaking changes to validation rules and default values, must be done in a
  major release or an new API version.
* Validation rules and defaults can be handled without webhooks.
* The Operator must be deployed on Kubernetes 1.29 or above for stable CEL
  support. At the time that this ADR is accepted, Kubernetes 1.28 is EOL and
  Kubernetes 1.29 is the minimum stable version.

## Notes

* A required field must not use the json `omitempty` tag because it
  [overrides the kubebuilder marker](https://github.com/kubernetes-sigs/controller-tools/issues/599#issuecomment-904960546).
* The default values of a custom resource must
  [must validate against the scheme](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/#defaulting).
* Optional fields with defaults may need to use [nested default markets](https://github.com/kubernetes-sigs/controller-tools/issues/622).
