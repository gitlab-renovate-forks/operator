package controllerutils

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func PatchStatus(rtCtx *framework.RuntimeContext, obj client.Object, mutator func() bool) error {
	origin := obj.DeepCopyObject().(client.Object)
	if mutator() {
		if err := framework.Client.Status().Patch(rtCtx, obj, client.MergeFrom(origin)); err != nil {
			return err
		}
	}

	return nil
}
