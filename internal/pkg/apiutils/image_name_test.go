package apiutils

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

var _ = Describe("Image Utils", func() {
	DescribeTable("GetImageName",
		func(source *v2alpha2.ImageSource, container, version string, editions []v2alpha2.Edition, expected string) {
			Expect(GetImageName(source, container, version, editions...)).To(Equal(expected))
		},
		Entry("default source, puma container, EE edition",
			nil,
			"puma",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:v17.8.0"),
		Entry("default source, workhorse container, CE edition",
			nil,
			"workhorse",
			"17.8.0",
			[]v2alpha2.Edition{"CE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-workhorse-ce:v17.8.0"),
		Entry("custom source, sidekiq container, EE edition",
			&v2alpha2.ImageSource{
				Registry:   "custom.registry.com",
				Repository: "custom-repo",
				Flavor:     v2alpha2.Debian,
			},
			"sidekiq",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"custom.registry.com/custom-repo/gitlab-sidekiq-ee:v17.8.0"),
		Entry("UBI flavor, migrations container, EE edition",
			&v2alpha2.ImageSource{
				Registry:   "registry.gitlab.com",
				Repository: "gitlab-org/build/cng",
				Flavor:     v2alpha2.UBI,
			},
			"migrations",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-toolbox-ee:v17.8.0-ubi"),
		Entry("FIPS flavor, puma container, EE edition",
			&v2alpha2.ImageSource{
				Flavor: v2alpha2.FIPS,
			},
			"puma",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:v17.8.0-fips"),
		Entry("non-semver version",
			nil,
			"puma",
			"latest",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:latest"),
		Entry("empty version",
			nil,
			"puma",
			"",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:master"),
		Entry("unmapped container",
			nil,
			"custom-container",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"registry.gitlab.com/gitlab-org/build/cng/custom-container:v17.8.0"),
		Entry("multiple editions, first one used",
			nil,
			"puma",
			"17.8.0",
			[]v2alpha2.Edition{"CE", "EE"},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ce:v17.8.0"),
		Entry("no editions provided, uses default edition",
			nil,
			"puma",
			"17.8.0",
			[]v2alpha2.Edition{},
			"registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ee:v17.8.0"),
		Entry("partial custom source, uses defaults for missing fields",
			&v2alpha2.ImageSource{
				Registry: "proxy.registry.com",
			},
			"puma",
			"17.8.0",
			[]v2alpha2.Edition{"EE"},
			"proxy.registry.com/gitlab-org/build/cng/gitlab-webservice-ee:v17.8.0"),
	)
})
