{{/*
Prints out the default security context for Pods.

Expects a context with the following keys:

  - UserID: Integer value for RunAsUser as expected in PodSecurityContext.
  - FSGroupID: Integer value for FSGroupID as expected in PodSecurityContext.

These are the default values that are defined in application-specific settings,
for example `.Settings.Rails`.
*/}}
securityContext:
  runAsUser: {{ .UserID }}
  fsGroup: {{ .FSGroupID }}
  runAsNonRoot: true