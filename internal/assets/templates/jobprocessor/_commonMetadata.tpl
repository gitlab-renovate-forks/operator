{{- $edition := .JobProcessor.Spec.Edition | default .Settings.Rails.Edition -}}
{{- $appName := printf "%s-%s" .JobProcessor.Namespace .JobProcessor.Name -}}
{{- $appInfo := dict
      "Name" $appName
      "Version" (printf "%s-%s" .JobProcessor.Spec.Version $edition | lower)
      "Component" "Sidekiq"
      "Role" "JobProcessor"
-}}

namespace: {{ .JobProcessor.Namespace }}
labels:
  {{- with .JobProcessor.Labels }}
  {{-   . | toYaml | nindent 2 }}
  {{- end }}
  {{- include "commons/_commonLabels.tpl" $appInfo | nindent 2 }}
  gitlab.io/job-processor: {{ $appName }}
  gitlab.io/edition: {{ $edition }}
annotations:
  {{- with .JobProcessor.Annotations }}
  {{-   . | toYaml | nindent 2 }}
  {{- end }}
