package applicationserver

import (
	"fmt"

	appsv1 "k8s.io/api/apps/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

func (c *Controller) isScalingEvent(deployment *appsv1.Deployment) bool {
	if c.appServer.Spec.Replicas == nil {
		return false
	}

	if deployment.Spec.Replicas == nil {
		return true
	}

	if c.appServer.Status.Replicas == *c.appServer.Spec.Replicas {
		return false
	}

	return c.appServer.Status.Replicas != deployment.Status.Replicas
}

func (c *Controller) scale(rtCtx *framework.RuntimeContext) error {
	deployment, err := c.getDeployment(rtCtx)
	if err != nil {
		if kerrors.IsNotFound(err) {
			// Deployment is not created yet. Ignore this error and move on.
			return nil
		}

		return fmt.Errorf("failed to get Deployment: %w", err)
	}

	if !c.isScalingEvent(deployment) {
		if err := c.clearStatusCondition(rtCtx, v2alpha2.ApplicationServerScalingCondition); err != nil {
			return err
		}

		return nil
	}

	rtCtx.Logger.Debug("Scaling application server",
		"from", c.appServer.Status.Replicas, "to", *c.appServer.Spec.Replicas)

	err = c.setStatusCondition(rtCtx, metav1.Condition{
		Type:   v2alpha2.ApplicationServerScalingCondition,
		Status: metav1.ConditionUnknown,
		Reason: v2alpha2.ScalingReplicasReason,
		Message: fmt.Sprintf("Scaling application replicas: from %d to %d",
			c.appServer.Status.Replicas, *c.appServer.Spec.Replicas),
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	scaled := deployment.DeepCopy()
	scaled.Spec.Replicas = utils.Ptr(*c.appServer.Spec.Replicas)

	err = framework.Client.Patch(rtCtx, scaled, client.MergeFrom(deployment))
	if err != nil {
		return fmt.Errorf("failed to scale Deployment: %w", err)
	}

	return nil
}

func (c *Controller) updateScaleStatus(rtCtx *framework.RuntimeContext) error {
	deployment, err := c.getDeployment(rtCtx)
	if err != nil {
		if kerrors.IsNotFound(err) {
			return nil
		}

		return fmt.Errorf("failed to get Deployment: %w", err)
	}

	if deployment.Spec.Replicas == nil {
		return nil
	}

	s, err := metav1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		return fmt.Errorf("failed to turn label selectors into an expression: %w", err)
	}

	replicas := deployment.Status.Replicas
	selector := s.String()

	err = controllerutils.PatchStatus(rtCtx, c.appServer, func() bool {
		changed := c.appServer.Status.Replicas != replicas || c.appServer.Status.Selector != selector

		c.appServer.Status.Replicas = replicas
		c.appServer.Status.Selector = selector

		return changed
	})
	if err != nil {
		return fmt.Errorf("failed to patch scale status: %w", err)
	}

	return nil
}
