{{- define "gvList" -}}
{{- $groupVersions := . -}}

---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!---
This document is auto generated using elastic/crd-ref-docs.

https://github.com/elastic/crd-ref-docs

Use the Makefile target `api-docs` to update this file.
Do not change the content manually!
-->

<!-- markdownlint-disable -->

# API Reference

## Packages
{{- range $groupVersions }}
- {{ markdownRenderGVLink . }}
{{- end }}

{{ range $groupVersions }}
{{ template "gvDetails" . }}
{{ end }}

<!-- markdownlint-enable -->

{{- end -}}
