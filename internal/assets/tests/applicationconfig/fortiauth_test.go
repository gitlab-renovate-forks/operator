package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _fortiauth.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/fortiauth.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleFortiAuth, "fortiauth/simple.yaml"),
	)
})

var (
	simpleFortiAuth = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				FortiAuthenticator: &v2alpha2.ApplicationFortiAuthenticatorSettings{
					Host: "forti.example.com",
					Credentials: corev1.SecretReference{
						Name: "forti-basic",
					},
				},
			},
		},
		"TwoFactorSecrets": map[string]*corev1.Secret{
			"FortiAuthCredentials": {
				Data: map[string][]byte{
					"username": []byte("user"),
					"password": []byte("token"),
				},
			},
		},
	}
)
