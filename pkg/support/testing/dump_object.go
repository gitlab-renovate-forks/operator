package testing

import (
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/json"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func defaultSerializer() *json.Serializer {
	return json.NewSerializerWithOptions(
		json.DefaultMetaFactory, framework.Scheme, framework.Scheme, json.SerializerOptions{Yaml: true, Pretty: true, Strict: true},
	)
}

func getGvk(obj client.Object) schema.GroupVersionKind {
	gvk := obj.GetObjectKind().GroupVersionKind()
	if gvk.Empty() {
		// try get gvk
		gvks, _, err := framework.Scheme.ObjectKinds(obj)
		if err == nil && len(gvks) > 0 {
			gvk = gvks[0]
		}
	}

	return gvk
}

func DumpObject(obj client.Object) string {
	if obj.GetObjectKind().GroupVersionKind().Kind == "" {
		// try get gvk
		gvks, _, err := framework.Scheme.ObjectKinds(obj)
		if err == nil && len(gvks) > 0 {
			obj.GetObjectKind().SetGroupVersionKind(gvks[0])
		}
	}

	title := "# ========== " + obj.GetObjectKind().GroupVersionKind().Kind + " " + obj.GetName() + " ==========\n"

	data, err := runtime.Encode(defaultSerializer(), obj)
	if err != nil {
		panic(err)
	}

	return ycat(title+string(data), false)
}
