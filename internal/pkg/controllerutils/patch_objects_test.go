package controllerutils

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("Patch", func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		configMap  *unstructured.Unstructured
		rtCtx      *framework.RuntimeContext
	)

	BeforeEach(func() {
		configMap = &unstructured.Unstructured{
			Object: map[string]interface{}{
				"apiVersion": "corev1",
				"kind":       "ConfigMap",
				"metadata": map[string]interface{}{
					"name":      "config",
					"namespace": "test",
				},
				"data": map[string]interface{}{
					"k": "v",
				},
			}}

		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			Build()

		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	It("Patches and registers the object", func() {
		objects, err := PatchObjects(rtCtx, framework.StaticValue[client.Object]{configMap},
			WithPatchOptions(
				client.PatchOptions{
					FieldManager: "test",
					// Use DryRun until apply patches are supported by client/fake:
					//   https://github.com/kubernetes-sigs/controller-runtime/blob/v0.19.0/pkg/client/fake/client.go#L1003
					DryRun: []string{"All"},
				}))

		Expect(err).ToNot(HaveOccurred())
		Expect(objects).To(HaveLen(1))
		Expect(objects[0]).To(BeIdenticalTo(configMap))
	})
})
