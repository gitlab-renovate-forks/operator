package controllerutils

import (
	"time"

	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func WaitForDeploymentReady(ctx *framework.RuntimeContext, deployment *appsv1.Deployment, delay time.Duration) error {
	// See: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#complete-deployment
	appDeploymentReady := DeploymentHasCondition(
		deployment, appsv1.DeploymentAvailable, metav1.ConditionTrue)

	if err := WaitFor(ctx, appDeploymentReady, WithDelay(delay)); err != nil {
		ctx.Logger.Debug("Deployment is not ready",
			"Deployment.Status.Conditions", deployment.Status.Conditions)

		return err
	}

	return nil
}
