package template

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/kubectl/pkg/scheme"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

var _ = Describe("Engine", func() {
	Describe("Render", func() {
		It("renders templates matching the glob pattern", func() {
			data := map[string]interface{}{
				"Name": "World",
			}

			rendered, err := testEngine.Render("*.txt", data)
			Expect(err).NotTo(HaveOccurred())
			Expect(rendered).To(HaveLen(1))
			Expect(rendered).To(HaveKey("hello.txt"))
			Expect(rendered["hello.txt"].String()).To(ContainSubstring("World"))
		})

		It("does not render partial templates", func() {
			rendered, err := testEngine.Render("_*.txt", nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(rendered).To(BeEmpty())
		})
	})

	Describe("Parse", func() {
		It("parses rendered templates into Kubernetes objects", func() {
			data := map[string]interface{}{
				"Name": "test",
				"Data": map[string]string{
					"key": "value",
				},
			}

			objects, err := testEngine.Parse("*.yaml", data)
			Expect(err).NotTo(HaveOccurred())
			Expect(objects).To(HaveLen(1))

			configMap, ok := objects[0].(*unstructured.Unstructured)
			Expect(ok).To(BeTrue())
			Expect(configMap).To(testing.HaveKind("ConfigMap"))
			Expect(configMap).To(testing.HaveName("test"))
		})

		It("parses rendered templates into Kubernetes objects with specified Decoder", func() {
			data := map[string]interface{}{
				"Name": "test",
				"Data": map[string]string{
					"key": "value",
				},
			}
			testEngine.Decoder = scheme.Codecs.UniversalDecoder(scheme.Scheme.PreferredVersionAllGroups()...)

			objects, err := testEngine.Parse("*.yaml", data)
			Expect(err).NotTo(HaveOccurred())
			Expect(objects).To(HaveLen(1))

			configMap, ok := objects[0].(*corev1.ConfigMap)
			Expect(ok).To(BeTrue())
			Expect(configMap).To(testing.HaveName("test"))
		})

		It("returns an empty list for unknown templates", func() {
			objects, err := testEngine.Parse("unknown.yaml", nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(objects).To(BeEmpty())
		})
	})
})
