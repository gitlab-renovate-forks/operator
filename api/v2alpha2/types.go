package v2alpha2

import (
	"encoding/json"

	corev1 "k8s.io/api/core/v1"
	policyv1 "k8s.io/api/policy/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
)

// +kubebuilder:validation:Enum=EE;CE
type Edition string

const (
	EE Edition = "EE"
	CE Edition = "CE"
)

// +kubebuilder:validation:Enum=Text;JSON;Structured
type LogFormat string

const (
	TextLogFormat       LogFormat = "Text"
	JSONLogFormat       LogFormat = "JSON"
	StructuredLogFormat LogFormat = "Structured"
)

// +kubebuilder:validation:Enum=Trace;Debug;Info;Warn;Error;Fatal
type LogLevel string

const (
	TraceLogLevel LogLevel = "Trace"
	DebugLogLevel LogLevel = "Debug"
	InfoLogLevel  LogLevel = "Info"
	WarnLogLevel  LogLevel = "Warn"
	ErrorLogLevel LogLevel = "Error"
	FatalLogLevel LogLevel = "Fatal"
)

// +structType=atomic
type CertificateBundle struct {
	// +required
	Credentials corev1.SecretReference `json:"credentials"`

	// +optional
	CACertificate *corev1.SecretKeySelector `json:"caCertificate,omitempty"`
}

type TLSClientSupport struct {
	// +optional
	// +kubebuilder:validation:Pattern="^TLSv1(\\.[0-3])?$"
	MinVersion *string `json:"minVersion,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^TLSv1(\\.[0-3])?$"
	MaxVersion *string `json:"maxVersion,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^(!?[\\w-]+)(?::!?[\\w-]+)*$"
	Ciphers *string `json:"ciphers,omitempty"`

	// +required
	CACertificate corev1.SecretKeySelector `json:"caCertificate,omitempty"`
}

type TLSServerSupport struct {
	// +optional
	// +kubebuilder:validation:Pattern="^TLSv1(\\.[0-3])?$"
	MinVersion *string `json:"minVersion,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^TLSv1(\\.[0-3])?$"
	MaxVersion *string `json:"maxVersion,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^(!?[\\w-]+)(?::!?[\\w-]+)*$"
	Ciphers *string `json:"ciphers,omitempty"`

	// +required
	Credentials corev1.SecretReference `json:"credentials"`

	// +optional
	CACertificate *corev1.SecretKeySelector `json:"caCertificate,omitempty"`
}

type MetricsSupport struct {
	// +optional
	Disabled bool `json:"disabled,omitempty"`

	// +optional
	TLS *TLSServerSupport `json:"tls,omitempty"`
}

type TracingSupport struct {
	// +required
	ConnectionString string `json:"connectionString"`

	// +optional
	URLTemplate string `json:"urlTemplate,omitempty"`
}

type GSSAPISupport struct {
	// +required
	Keytab corev1.SecretKeySelector `json:"keytab"`

	// +optional
	// +kubebuilder:validation:Format=email
	ServicePrincipalName string `json:"servicePrincipalName"`
}

type ServiceProvider struct {
	// +required
	Endpoint ServiceEndpoint `json:"endpoint"`

	// +required
	Authentication ServiceAuthentication `json:"authentication"`
}

type ServiceEndpoint struct {
	// Service is a reference to the provider service.
	// It could be a Controller, a Service.
	//
	// Currently we support:
	//  - Service
	//
	// The controller will derive the TLS configuration on a best-effort basis.
	// If the TLS configuration is not derived, please use the [TLS] field.
	//
	// +optional
	Service *corev1.ObjectReference `json:"service,omitempty"`

	// External is a reference to the external service.
	// Use [TLS] to enable the TLS settings.
	//
	// +optional
	External *ExternalServiceEndpoint `json:"external,omitempty"`

	// +optional
	TLS *bool `json:"tls,omitempty"`
}

type ServiceAuthentication struct {
	// +optional
	Basic *corev1.SecretReference `json:"basic,omitempty"`

	// +optional
	ClientTLS *corev1.SecretReference `json:"tls,omitempty"`

	// +optional
	Token *corev1.SecretKeySelector `json:"token,omitempty"`
}

// +structType=atomic
type ExternalServiceEndpoint struct {
	// +optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	ProviderName *string `json:"providerName,omitempty"`

	// +required
	// +kubebuilder:validation:Format=hostname
	Host string `json:"host"`

	// If unspecified, the default port for the service will be used.
	// +optional
	Port int32 `json:"port,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic) || has(self.authentication.tls)",message="either basic or tls authentication method is required"
type PostgreSQL struct {
	ServiceProvider `json:",inline"`

	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`

	// +optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Username string `json:"username,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	DatabaseName string `json:"databaseName"`

	// +optional
	// +mapType=atomic
	Settings map[string]string `json:"settings,omitempty"`

	// +optional
	TLS *TLSClientSupport `json:"tls,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic)",message="basic authentication method is required"
type ClickHouse struct {
	ServiceProvider `json:",inline"`

	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`

	// +optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	DatabaseName string `json:"databaseName,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic) || has(self.authentication.tls)",message="either basic or tls authentication method is required"
type Redis struct {
	ServiceProvider `json:",inline"`

	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`

	// +optional
	// +mapType=atomic
	Settings map[string]string `json:"settings,omitempty"`

	// +optional
	TLS *TLSClientSupport `json:"tls,omitempty"`
}

type ObjectStore struct {
	ObjectStoreProvider `json:",inline"`

	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`
}

type ObjectStoreDestination struct {
	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	StoreName string `json:"storeName"`

	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	BucketName string `json:"bucketName"`

	// +optional
	ProxyDownload *bool `json:"proxyDownload,omitempty"`

	// +optional
	// +mapType=atomic
	Options map[string]string `json:"options,omitempty"`
}

type ObjectStoreProvider struct {
	// +optional
	S3 *S3ObjectStoreConnection `json:"s3,omitempty"`

	// +optional
	GCS *GCSObjectStoreConnection `json:"gcs,omitempty"`
}

type S3ObjectStoreConnection struct {
	// +required
	AccessKeyId corev1.SecretKeySelector `json:"accessKeyId"`

	// +required
	AccessKeySecret corev1.SecretKeySelector `json:"accessKeySecret"`

	// +optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Region *string `json:"region,omitempty"`

	// +optional
	// +kubebuilder:validation:Enum=V4;V2
	SignatureVersion *string `json:"signatureVersion,omitempty"`

	// +optional
	// +kubebuilder:validation:Pattern="^(([a-zA-Z0-9][-a-zA-Z0-9]*\\.)+[a-zA-Z]{2,}|\\d{1,3}(\\.\\d{1,3}){3})(:\\d+)?$"
	// +kubebuilder:example="s3.example.com"
	// +kubebuilder:example="192.168.1.2:9000"
	Host *string `json:"host,omitempty"`

	// +optional
	// +kubebuilder:validation:Format=uri
	Endpoint *string `json:"endpoint,omitempty"`

	// +optional
	PathStyle bool `json:"pathStyle,omitempty"`
}

type GCSObjectStoreConnection struct {
	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Project string `json:"project"`

	// +required
	Credentials corev1.SecretKeySelector `json:"credentials"`
}

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic)",message="basic authentication method is required"
type SMTPServer struct {
	ServiceProvider `json:",inline"`

	// +optional
	TLS *TLSClientSupport `json:"tls,omitempty"`

	// +optional
	Pool *bool `json:"pool,omitempty"`

	// SMTP settings. Supported keys:
	//   - domain
	//   - autoStartTLS
	//   - opensslVerifyMode
	//   - openTimeout
	//   - readTimeout
	//   - authentication
	//
	// +optional
	Settings map[string]string `json:"settings,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic)",message="basic authentication method is required"
type IMAPServer struct {
	ServiceProvider `json:",inline"`

	// +optional
	TLS *TLSClientSupport `json:"tls,omitempty"`

	// SMTP settings. Supported keys:
	//   - startTLS
	//   - idleTimeout
	//
	// +optional
	Settings map[string]string `json:"settings,omitempty"`
}

type MicrosoftGraphMailer struct {
	// +required
	// +kubebuilder:validation:Format="uuid"
	Tenant string `json:"tenant"`

	// +required
	// +kubebuilder:validation:Format="uuid"
	UserId string `json:"userId"`

	// +required
	ClientId corev1.SecretKeySelector `json:"clientId"`

	// +required
	ClientSecret corev1.SecretKeySelector `json:"clientSecret"`

	// +optional
	// +kubebuilder:validation:Format=uri
	Endpoint *string `json:"endpoint,omitempty"`

	// +optional
	// +kubebuilder:validation:Format=uri
	AzureADEndpoint *string `json:"azureADEndpoint,omitempty"`
}

// +kubebuilder:validation:Enum=Sidekiq;Postback
type IncomingEmailDeliveryMethod string

const (
	SidekiqIncomingEmailDeliveryMethod  IncomingEmailDeliveryMethod = "Sidekiq"
	PostbackIncomingEmailDeliveryMethod IncomingEmailDeliveryMethod = "Postback"
)

// +kubebuilder:validation:Enum=Redis
type IncomingEmailArbitrationMethod string

const (
	RedisIncomingEmailArbitrationMethod IncomingEmailArbitrationMethod = "Redis"
)

// +structType=atomic
type JobSchedule struct {
	// +required
	// +kubebuilder:validation:Pattern="^[a-z]+(_[a-z]+)*$"
	Name string `json:"name"`

	// +required
	// +kubebuilder:validation:Pattern="^(((\\d+,)+\\d+|(\\d+(\\/|-)\\d+)|\\d+|\\*) ?){5,7}$"
	// +kubebuilder:example="15,30 * * * *"
	// +kubebuilder:example="0 2 * * 1-5"
	// +kubebuilder:example="0 0-23/2 * * *"
	Schedule string `json:"schedule"`
}

type SentrySupport struct {
	// +required
	DSN string `json:"dsn"`

	// +optional
	Environment string `json:"environment,omitempty"`

	// +optional
	ClientSideDSN string `json:"clientSideDSN,omitempty"`
}

// +structType=atomic
type SharedObjectReference struct {
	// +required
	Name string `json:"name"`

	// +optional
	Namespace string `json:"namespace,omitempty"`

	// +optional
	APIVersion string `json:"apiVersion,omitempty"`

	// +optional
	Kind string `json:"kind,omitempty"`

	// +required
	Usage string `json:"usage"`
}

// +k8s:deepcopy-gen=false
type Experimental struct {
	Values map[string]any `json:"-"`
}

func (u *Experimental) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.Values)
}

func (u *Experimental) UnmarshalJSON(data []byte) error {
	m := make(map[string]any)
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}

	u.Values = m

	return nil
}

func (u *Experimental) DeepCopyInto(out *Experimental) {
	out.Values = runtime.DeepCopyJSON(u.Values)
}

func (u *Experimental) DeepCopy() *Experimental {
	if u == nil {
		return nil
	}

	out := new(Experimental)
	u.DeepCopyInto(out)

	return out
}

type ImageFlavor string

const (
	Debian ImageFlavor = "Debian"
	FIPS   ImageFlavor = "FIPS"
	UBI    ImageFlavor = "UBI"
)

type ImageSource struct {
	// +optional
	Registry string `json:"registry,omitempty"`

	// +optional
	Repository string `json:"repository,omitempty"`

	// +optional
	Flavor ImageFlavor `json:"flavor,omitempty"`

	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	PullSecrets []corev1.LocalObjectReference `json:"pullSecrets,omitempty" patchStrategy:"merge" patchMergeKey:"name"`
}

type WorkloadSpec struct {
	// +optional
	ImageSource *ImageSource `json:"imageSource,omitempty"`

	// +optional
	PodTemplate *PodTemplate `json:"podTemplate,omitempty"`

	// +optional
	PodDisruptionBudget *PodDisruptionBudget `json:"podDisruptionBudget,omitempty"`

	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +optional
	ManagedContainers []ManagedContainerSpec `json:"managedContainers,omitempty" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`
}

type PodTemplate struct {
	// +optional
	Metadata *PodMetadata `json:"metadata,omitempty"`

	// +optional
	Spec *RestrictedPodSpec `json:"spec,omitempty"`
}

type PodDisruptionBudget struct {
	// +optional
	MinAvailable *intstr.IntOrString `json:"minAvailable,omitempty"`

	// +optional
	MaxUnavailable *intstr.IntOrString `json:"maxUnavailable,omitempty"`

	// +optional
	UnhealthyPodEvictionPolicy *policyv1.UnhealthyPodEvictionPolicyType `json:"unhealthyPodEvictionPolicy,omitempty"`
}

type PodMetadata struct {
	// +optional
	Labels map[string]string `json:"labels,omitempty"`

	// +optional
	Annotations map[string]string `json:"annotations,omitempty"`
}

// RestrictedPodSpec is a subset of the PodSpec that can be used to customize the pod.
// Provided fields will override the default ones populated by the operator.
// +kubebuilder:validation:XValidation:rule="has(self.extraVolumes) == has(self.extraVolumeMounts)",message="extraVolumeMounts does not match extraVolumes"
type RestrictedPodSpec struct {
	// ExtraVolumes is a list of additional volumes to be added to the pod.
	// Provided volumes will override the ones in the pod if they have the same name.
	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	ExtraVolumes []corev1.Volume `json:"extraVolumes,omitempty" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// ExtraVolumeMounts is a list of additional volume mounts to be added to the pod.
	// Same as volumes, provided volume mounts will override the ones in the pod if they have the same mount path.
	// +optional
	// +patchMergeKey=mountPath
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=mountPath
	ExtraVolumeMounts []corev1.VolumeMount `json:"extraVolumeMounts,omitempty" patchStrategy:"merge" patchMergeKey:"mountPath"`

	// ExtraInitContainers is a list of additional init containers to be added to the pod.
	// Provided init containers will override the ones in the pod if they have the same name.
	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	ExtraInitContainers []corev1.Container `json:"extraInitContainers,omitempty" patchStrategy:"merge" patchMergeKey:"name"`

	// ExtraContainers is a list of additional containers to be added to the pod.
	// Provided containers will override the ones in the pod if they have the same name.
	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	ExtraContainers []corev1.Container `json:"extraContainers" patchStrategy:"merge" patchMergeKey:"name"`

	// +optional
	RestartPolicy corev1.RestartPolicy `json:"restartPolicy,omitempty"`

	// +optional
	TerminationGracePeriodSeconds *int64 `json:"terminationGracePeriodSeconds,omitempty"`

	// +optional
	ActiveDeadlineSeconds *int64 `json:"activeDeadlineSeconds,omitempty"`

	// +optional
	DNSPolicy corev1.DNSPolicy `json:"dnsPolicy,omitempty"`

	// +optional
	// +mapType=atomic
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`

	// +optional
	ServiceAccountName string `json:"serviceAccountName,omitempty"`

	// +optional
	AutomountServiceAccountToken *bool `json:"automountServiceAccountToken,omitempty"`

	// +optional
	NodeName string `json:"nodeName,omitempty"`

	// +optional
	ShareProcessNamespace *bool `json:"shareProcessNamespace,omitempty"`

	// +optional
	SecurityContext *corev1.PodSecurityContext `json:"securityContext,omitempty"`

	// +optional
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// +optional
	SchedulerName string `json:"schedulerName,omitempty"`

	// +optional
	// +listType=atomic
	Tolerations []corev1.Toleration `json:"tolerations,omitempty"`

	// +optional
	// +patchMergeKey=ip
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=ip
	HostAliases []corev1.HostAlias `json:"hostAliases,omitempty" patchStrategy:"merge" patchMergeKey:"ip"`

	// +optional
	PriorityClassName string `json:"priorityClassName,omitempty"`

	// +optional
	DNSConfig *corev1.PodDNSConfig `json:"dnsConfig,omitempty"`

	// +optional
	RuntimeClassName *string `json:"runtimeClassName,omitempty"`

	// +optional
	Overhead corev1.ResourceList `json:"overhead,omitempty"`

	// +optional
	// +patchMergeKey=topologyKey
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=topologyKey
	// +listMapKey=whenUnsatisfiable
	TopologySpreadConstraints []corev1.TopologySpreadConstraint `json:"topologySpreadConstraints,omitempty" patchStrategy:"merge" patchMergeKey:"topologyKey"`

	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +optional
	ResourceClaims []corev1.PodResourceClaim `json:"resourceClaims,omitempty" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`
}

type ManagedContainerSpec struct {
	// +required
	Name string `json:"name"`

	// +optional
	Image string `json:"image,omitempty"`

	// +optional
	// +listType=atomic
	EnvFrom []corev1.EnvFromSource `json:"envFrom,omitempty"`

	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	Env []corev1.EnvVar `json:"env,omitempty" patchStrategy:"merge" patchMergeKey:"name"`

	// +optional
	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`

	// +optional
	LivenessProbe *corev1.Probe `json:"livenessProbe,omitempty"`

	// +optional
	ReadinessProbe *corev1.Probe `json:"readinessProbe,omitempty"`

	// +optional
	StartupProbe *corev1.Probe `json:"startupProbe,omitempty"`

	// +optional
	ImagePullPolicy *corev1.PullPolicy `json:"imagePullPolicy,omitempty"`

	// +optional
	SecurityContext *corev1.SecurityContext `json:"securityContext,omitempty"`
}
