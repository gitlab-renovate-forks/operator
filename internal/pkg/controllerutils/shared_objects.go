package controllerutils

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
)

// SetStatusSharedObject sets the shared object reference in object status.
func SetStatusSharedObject(rtCtx *framework.RuntimeContext, obj client.Object, sharedObjects *[]v2alpha2.SharedObjectReference, newSharedObject v2alpha2.SharedObjectReference) error {
	return PatchStatus(rtCtx, obj, func() bool {
		return apiutils.SetStatusSharedObject(sharedObjects, newSharedObject)
	})
}
