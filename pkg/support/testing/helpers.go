package testing

import (
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

type AddToScheme = func(*runtime.Scheme) error

func NewScheme(extra ...AddToScheme) (*runtime.Scheme, error) {
	scheme := runtime.NewScheme()

	if err := initializeScheme(scheme, extra); err != nil {
		return nil, err
	}

	return scheme, nil
}

func NewDecoder(scheme *runtime.Scheme) runtime.Decoder {
	codecs := serializer.NewCodecFactory(scheme)
	groupVersions := scheme.PreferredVersionAllGroups()

	return codecs.UniversalDecoder(groupVersions...)
}

func initializeScheme(scheme *runtime.Scheme, extra []AddToScheme) error {
	for _, s := range requiredSchemes {
		if err := s(scheme); err != nil {
			return err
		}
	}

	for _, s := range extra {
		if err := s(scheme); err != nil {
			return err
		}
	}

	return nil
}

var (
	requiredSchemes []AddToScheme = []AddToScheme{
		clientgoscheme.AddToScheme,
		v2alpha2.AddToScheme,
	}
)
