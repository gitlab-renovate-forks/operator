package controllerutils

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash"
	"sort"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

// Digest computes a hash checksum over a set of Secrets and ConfigMaps.
// Useful to set annotations to trigger rollout of workload after the
// content of a Secret or ConfigMap changed.
//
// Only the data and namespace reference of Secrets and ConfigMaps are hashed.
// Other types and fields are ignored.
func Digest(rtCtx *framework.RuntimeContext, objects []client.Object, opts ...TaskOption) (string, error) {
	t := &DigestTask{Objects: objects}
	t.ApplyOptions(opts...)

	return t.digest, t.Execute(rtCtx)
}

type DigestTask struct {
	Register framework.Register
	Objects  []client.Object
	Hash     hash.Hash

	digest string
}

func (t *DigestTask) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case digestHash:
			t.Hash = o.Hash
		}
	}
}

func (t *DigestTask) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *DigestTask) Handle(_ *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	hash := sha256.New()
	if t.Hash != nil {
		hash = t.Hash
	}

	entries, err := filterAndSortObjects(t.Objects)
	if err != nil {
		return "", err
	}

	for _, entry := range entries {
		entry.digest(hash)
	}

	t.digest = hex.EncodeToString(hash.Sum(nil))

	return t.digest, nil
}

// This ensures that regardless of the order of the input objects, the hash
// is always the same.
func filterAndSortObjects(objects []client.Object) ([]digestEntry, error) {
	entries := []digestEntry{}

	for _, obj := range objects {
		switch obj := obj.(type) {
		case *unstructured.Unstructured:
			switch obj.GetKind() {
			case kindConfigMap:
				configMap := &corev1.ConfigMap{}
				if err := kube.FromUnstructured(obj, configMap); err != nil {
					return nil, err
				}

				entries = append(entries, newEntry(configMap, configMap.Data))
			case kindSecret:
				secret := &corev1.Secret{}
				if err := kube.FromUnstructured(obj, secret); err != nil {
					return nil, err
				}

				entries = append(entries, newEntry(secret, secret.Data))
			}
		case *corev1.Secret:
			entries = append(entries, newEntry(obj, obj.Data))
		case *corev1.ConfigMap:
			entries = append(entries, newEntry(obj, obj.Data))
		}
	}

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].String() < entries[j].String()
	})

	return entries, nil
}

type digestEntry struct {
	gvk    schema.GroupVersionKind
	ref    types.NamespacedName
	keys   []string
	values []string
}

func newEntry[T string | []byte](obj client.Object, data map[string]T) digestEntry {
	keys := make([]string, 0, len(data))
	for k := range data {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	values := make([]string, 0, len(data))
	for _, k := range keys {
		values = append(values, string(data[k]))
	}

	return digestEntry{
		gvk:    obj.GetObjectKind().GroupVersionKind(),
		ref:    client.ObjectKeyFromObject(obj),
		keys:   keys,
		values: values,
	}
}

func (e digestEntry) String() string {
	return fmt.Sprintf("%s[%s]", e.gvk.Kind, e.ref)
}

func (e digestEntry) digest(hash hash.Hash) {
	hash.Write([]byte(e.gvk.Kind))
	hash.Write([]byte(e.ref.Name))
	hash.Write([]byte(e.ref.Namespace))

	for i := 0; i < len(e.keys); i++ {
		hash.Write([]byte(e.keys[i]))
		hash.Write([]byte(e.values[i]))
	}
}
