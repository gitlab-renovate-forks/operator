package main

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

var (
	yellowColor = color.New(color.Bold, color.FgYellow).SprintFunc()
	greenColor  = color.New(color.Bold, color.FgGreen).SprintFunc()
	redColor    = color.New(color.Bold, color.FgRed).SprintFunc()
)

func readUnstructuredObjects(path string) ([]*unstructured.Unstructured, error) {
	f, err := os.Open(filepath.Clean(path))
	if err != nil {
		return nil, fmt.Errorf("failed to open file %q: %w", path, err)
	}
	//nolint:errcheck
	defer f.Close()
	objs, err := kube.ReadObjects(f, unstructured.UnstructuredJSONScheme)

	if err != nil {
		panic(err)
	}

	results := make([]*unstructured.Unstructured, 0, len(objs))

	for _, obj := range objs {
		unstructuredObj, ok := obj.(*unstructured.Unstructured)
		if !ok {
			slog.Warn("Failed to convert object to unstructured", "object", obj.GetName(), "kind", obj.GetObjectKind().GroupVersionKind().String())
			continue
		}

		results = append(results, unstructuredObj)
	}

	return results, nil
}

func loadCRD(path string) ([]*apiextensions.CustomResourceDefinition, error) {
	slog.Debug("Loading CRD", "path", path)

	objs, err := readKustomize(path)
	if err != nil {
		return nil, err
	}

	var crds []*apiextensions.CustomResourceDefinition

	for _, obj := range objs {
		crd, ok := obj.(*apiextensionsv1.CustomResourceDefinition)
		if !ok {
			slog.Warn("failed to cast object to CustomResourceDefinition", "object", obj, "type", fmt.Sprintf("%T", obj))
			continue
		}

		slog.Debug("Loaded CRD", "name", crd.Name)

		internalCRD := &apiextensions.CustomResourceDefinition{}
		if err := scheme.Convert(crd, internalCRD, nil); err != nil {
			slog.Warn("failed to convert CRD to internal version", "name", crd.Name, "error", err)
			continue
		}

		crds = append(crds, internalCRD)
	}

	slog.Debug(fmt.Sprintf("Loaded %d CRDs", len(crds)))

	return crds, nil
}
