package framework

import (
	"errors"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var fakeCtx = &RuntimeContext{}

var _ = Describe("Tasks", func() {
	Describe("Task", func() {
		It("should execute a TaskFunc", func() {
			executed := false
			task := TaskFunc(func(ctx *RuntimeContext) error {
				executed = true
				return nil
			})

			err := task.Execute(fakeCtx)
			Expect(err).NotTo(HaveOccurred())
			Expect(executed).To(BeTrue())
		})
	})

	Describe("Workflow", func() {
		It("should execute Tasks in order", func() {
			executionOrder := []int{}
			workflow := Workflow{
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 1)
					return nil
				}),
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 2)
					return nil
				}),
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 3)
					return nil
				}),
			}

			err := workflow.Execute(fakeCtx)
			Expect(err).NotTo(HaveOccurred())
			Expect(executionOrder).To(Equal([]int{1, 2, 3}))
		})

		It("should stop execution on first error", func() {
			executionOrder := []int{}
			expectedError := errors.New("task 2 failed")
			workflow := Workflow{
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 1)
					return nil
				}),
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 2)
					return expectedError
				}),
				TaskFunc(func(ctx *RuntimeContext) error {
					executionOrder = append(executionOrder, 3)
					return nil
				}),
			}

			err := workflow.Execute(fakeCtx)
			Expect(err).To(MatchError(expectedError))
			Expect(executionOrder).To(Equal([]int{1, 2}))
		})
	})

	Describe("MiddlewareChain", func() {
		It("should execute Middlewares in order", func() {
			chain := MiddlewareChain{
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) + 1, nil
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) * 2, nil
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) - 3, nil
				}),
			}

			result, err := chain.Handle(fakeCtx, 5, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(Equal(9)) // (5 + 1) * 2 - 3 = 9
		})

		It("should stop execution on first error", func() {
			expectedError := errors.New("middleware 2 failed")
			chain := MiddlewareChain{
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) + 1, nil
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return nil, expectedError
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) - 3, nil
				}),
			}

			result, err := chain.Handle(fakeCtx, 5, nil)
			Expect(err).To(MatchError(expectedError))
			Expect(result).To(BeNil())
		})

		It("should handle ErrAbortTask", func() {
			chain := MiddlewareChain{
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) + 1, nil
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value, ErrAbortTask
				}),
				MiddlewareFunc(func(ctx *RuntimeContext, value any, err error) (any, error) {
					return value.(int) - 3, nil
				}),
			}

			result, err := chain.Handle(fakeCtx, 5, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(Equal(6)) // 5 + 1 = 6, then aborted
		})
	})
})
