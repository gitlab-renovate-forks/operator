package settings

import (
	"reflect"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

var (
	vp = viper.New()
)

func SetDefault(in any) {
	cfg, err := decode(in)
	if err != nil {
		panic(err)
	}

	mcfg, ok := cfg.(map[string]any)
	if !ok {
		panic("only support struct config")
	}

	for key, val := range mcfg {
		vp.SetDefault(key, val)
	}
}

func Unmarshal(data any) error {
	return vp.Unmarshal(data)
}

func WriteConfig(path string) error {
	return vp.WriteConfigAs(path)
}

// decode recursively decodes a struct or a slice of structs into a map[string]any.
func decode(in any) (_ any, err error) {
	if isStruct(in) {
		out := map[string]any{}
		if err = mapstructure.Decode(in, &out); err != nil {
			return nil, err
		}

		for k, v := range out {
			if isDecodable(v) {
				if out[k], err = decode(v); err != nil {
					return nil, err
				}
			}
		}

		return out, nil
	}

	if isArrayOfStruct(in) {
		val := reflect.ValueOf(in)
		out := make([]any, val.Len())

		for i := 0; i < val.Len(); i++ {
			if out[i], err = decode(val.Index(i).Interface()); err != nil {
				return nil, err
			}
		}

		return out, nil
	}

	if m, ok := in.(map[string]any); ok {
		for k, v := range m {
			if m[k], err = decode(v); err != nil {
				return nil, err
			}
		}

		return m, nil
	}

	return in, nil
}

func isDecodable(v any) bool {
	typ := reflect.TypeOf(v)
	if typ.Kind() == reflect.Slice {
		typ = typ.Elem()
	}

	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	return typ.Kind() == reflect.Struct || typ.Kind() == reflect.Map
}

func isStruct(v any) bool {
	typ := reflect.TypeOf(v)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	return typ.Kind() == reflect.Struct
}

func isArrayOfStruct(v any) bool {
	typ := reflect.TypeOf(v)
	if typ.Kind() == reflect.Slice {
		typ = typ.Elem()
		if typ.Kind() == reflect.Ptr {
			typ = typ.Elem()
		}

		return typ.Kind() == reflect.Struct
	}

	return false
}
