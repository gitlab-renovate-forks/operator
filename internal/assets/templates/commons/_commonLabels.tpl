{{/*
Prints out the recommended Kubernetes resource labels.

Expects a context with the following keys:
  - Name: Full name of the instances, in `namespace/name` format.
  - Version: Version string, containing semantic version and edition suffix where applicable.
  - Component: Name of the component, for example ApplicationServer.
  - Role: Role of the component in GitLab architecture.
*/}}
app.kubernetes.io/name: {{ .Component }}
app.kubernetes.io/instance: {{ .Name }}
app.kubernetes.io/version: {{ .Version }}
app.kubernetes.io/component: {{ .Component }}
app.kubernetes.io/role: {{ .Role | default .Component }}
app.kubernetes.io/part-of: GitLab
app.kubernetes.io/managed-by: gitlab-operator
