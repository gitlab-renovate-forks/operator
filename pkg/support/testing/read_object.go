package testing

import (
	"os"
	"path/filepath"

	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/runtime"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/kustomize/api/krusty"
	"sigs.k8s.io/kustomize/kyaml/filesys"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

func ReadObject(path, filename string) client.Object {
	f, err := os.Open(filepath.Clean(filepath.Join("testdata", path, filename)))
	Expect(err).NotTo(HaveOccurred())
	//nolint:errcheck
	defer f.Close()

	objs, err := kube.ReadObjects(f, defaultSerializer())
	Expect(err).NotTo(HaveOccurred())
	Expect(objs).To(HaveLen(1))

	return objs[0]
}

func ReadObjects(filePath string, decoder runtime.Decoder) []client.Object {
	//nolint:gosec
	f, err := os.Open(filePath)
	Expect(err).NotTo(HaveOccurred())

	objList, err := kube.ReadObjects(f, decoder)
	Expect(err).NotTo(HaveOccurred())

	return objList
}

func ReadKustomize(pathSegs ...string) []client.Object {
	var objects []client.Object

	decoder := defaultSerializer()
	fSys := filesys.MakeFsOnDisk()
	k := krusty.MakeKustomizer(krusty.MakeDefaultOptions())
	resMap, err := k.Run(fSys, filepath.Join(append([]string{"testdata"}, pathSegs...)...))
	Expect(err).NotTo(HaveOccurred())

	for _, resource := range resMap.Resources() {
		yaml, err := resource.AsYAML()
		Expect(err).NotTo(HaveOccurred())

		obj, _, err := decoder.Decode(yaml, nil, nil)
		Expect(err).NotTo(HaveOccurred())

		clientObj, ok := obj.(client.Object)
		Expect(ok).To(BeTrue())

		objects = append(objects, clientObj)
	}

	return objects
}
