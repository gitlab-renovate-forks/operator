#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    echo_stage "Installing PostgreSQL"

    gitlab_password=$(gen_random 64)

    kubectl -n $(namespace) create secret generic "postgres-secret" --from-literal="postgres-password"=$(gen_random 64) --from-literal="gitlab-password"="$gitlab_password"

    create_basic_auth_secret_raw "postgres-auth" "gitlab" "$gitlab_password"

    helm upgrade --install postgresql postgresql \
        --repo https://charts.bitnami.com/bitnami \
        -n $(namespace) \
        --create-namespace \
        --version 12.5.2 -f "$SCRIPT_DIR/manifests/values.psql.yaml"
}

uninstall() {
    echo_stage "Uninstalling PostgreSQL"

    helm uninstall postgresql -n $(namespace) || true

    kubectl -n $(namespace) delete secret postgres-secret --ignore-not-found=true
    kubectl -n $(namespace) delete secret postgres-auth --ignore-not-found=true
    kubectl -n $(namespace) delete pvc data-postgresql-0 --ignore-not-found=true
}

setup() {
    local _psql='PGPASSWORD=${POSTGRES_POSTGRES_PASSWORD} psql -U postgres'

    kubectl -n $(namespace) exec statefulsets/postgresql -c postgresql -- \
        bash -x -c "${_psql} \
            -c \"CREATE ROLE gitlab WITH CREATEDB LOGIN PASSWORD '\${POSTGRES_PASSWORD}'\" \
            -c 'CREATE DATABASE gitlabhq_production WITH OWNER gitlab TEMPLATE template1;'"
}

reset() {
    local _psql='PGPASSWORD=${POSTGRES_POSTGRES_PASSWORD} psql -U postgres'

    kubectl -n $(namespace) exec statefulsets/postgresql -c postgresql -- \
        bash -x -c "${_psql} \
            -c 'DROP DATABASE gitlabhq_production;' \
            -c 'CREATE DATABASE gitlabhq_production WITH OWNER gitlab TEMPLATE template1;'"
}

usage() {
    echo "Usage: $0 [install|uninstall|setup|reset]"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
setup)
    setup
    ;;
reset)
    reset
    ;;
*)
    usage
    ;;
esac
