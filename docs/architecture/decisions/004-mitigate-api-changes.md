# Mitigate API changes with controlled maps

**Date**: 2024-11-12

**Status**: Accepted

## Context

1. GitLab custom resources contain numerous application-specific configuration
   that can change frequently. These changes that are driven by the underlying
   application, necessitate changes to the API specification and in some cases
   requires releasing a new API version.

1. To maintain stability and predictability, we aim to implement a structured
   API change policy with defined versioning and release cycles. This approach
   is crucial for both developer productivity and user experience.

1. The API serves as a bridge, addressing the impedance mismatch between the
   need for API stability and the more frequent, asynchronous changes at the
   application level.

## Decision

1. To accommodate complex configuration requirements we introduce a schemaless
   attribute named "experimental" (Go type `map[string]any`) _when needed_.

   The "experimental" attribute serves as a staging area for new changes and
   features, allowing them to be introduced and tested before being incorporated
   into the stable API in the next release cycle.

   Its schemaless data type can handle complex structures, such as nested
   objects or arrays, which cannot be adequately represented using simple string
   key-value pairs.

   Validation of values in the experimental attribute is dependent on the
   controller, allowing for greater flexibility in testing new features. As
   these experimental features mature and prove their value, they can be
   gradually moved into the structured part of the API during subsequent release
   cycles, where they will undergo more rigorous validation.

   If a new feature can be represented with a simple string key-value pair,
   then resource annotations should be preferred.

## Consequences

This strategy strikes a balance between accommodating application-specific
changes and maintaining API stability, effectively mitigating the impedance
mismatch between application evolution and API versioning requirements.

While this strategy offers great flexibility, its usage should be carefully
managed to prevent overreliance on schemaless data, ensuring a smooth transition
to the stable API when appropriate.

The implementation of validation in the controller requires validating webhooks
for runtime structure and type checking of application settings and experimental
attributes to ensure the integrity of the application configuration.
