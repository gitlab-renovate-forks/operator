package secret

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"encoding/pem"

	"golang.org/x/crypto/ssh"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("SSHGenerator", func() {
	It("should generate valid SSH private key in PEM format", func() {
		generator := &SSHGenerator{
			Algorithm: RSA,
			Size:      2048,
		}

		payload, err := generator.Generate()

		Expect(err).NotTo(HaveOccurred())
		Expect(payload).To(HaveKey(SSHPrivateKey))
		Expect(payload).NotTo(HaveKey(SSHPublicKey))

		// Verify private key format
		block, _ := pem.Decode(payload[SSHPrivateKey])
		Expect(block).NotTo(BeNil())
		Expect(block.Type).To(Equal("OPENSSH PRIVATE KEY"))
		Expect(ssh.ParsePrivateKey(payload[SSHPrivateKey])).Error().NotTo(HaveOccurred())
	})

	It("should generate valid SSH public key in authorized keys format", func() {
		generator := &SSHGenerator{
			Algorithm:    RSA,
			Size:         2048,
			AddPublicKey: true,
		}

		payload, err := generator.Generate()

		Expect(err).NotTo(HaveOccurred())
		Expect(payload).To(HaveKey(SSHPrivateKey))
		Expect(payload).To(HaveKey(SSHPublicKey))

		// Verify public key format
		Expect(string(payload[SSHPublicKey])).To(HavePrefix("ssh-rsa "))
		Expect(ssh.ParseAuthorizedKey(payload[SSHPublicKey])).Error().NotTo(HaveOccurred())
	})

	DescribeTable("should work with different algorithms",
		func(algorithm Algorithm, size int, expectedPrefix string, keyType any) {
			generator := &SSHGenerator{
				Algorithm:    algorithm,
				Size:         size,
				AddPublicKey: true,
			}

			payload, err := generator.Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(payload[SSHPrivateKey]).NotTo(BeEmpty())
			Expect(payload[SSHPublicKey]).NotTo(BeEmpty())

			// Verify the generated key is in valid PEM format
			key, err := ssh.ParseRawPrivateKey(payload[SSHPrivateKey])
			Expect(err).NotTo(HaveOccurred())
			Expect(key).To(BeAssignableToTypeOf(keyType))

			// Verify public key format
			Expect(string(payload[SSHPublicKey])).To(HavePrefix(expectedPrefix))
			Expect(ssh.ParseAuthorizedKey(payload[SSHPublicKey])).Error().NotTo(HaveOccurred())
		},
		Entry("RSA", RSA, 2048, "ssh-rsa ", &rsa.PrivateKey{}),
		Entry("ECDSA", ECDSA, 256, "ecdsa-sha2-nistp256 ", &ecdsa.PrivateKey{}),
		Entry("ED25519", ED25519, 0, "ssh-ed25519 ", &ed25519.PrivateKey{}))
})
