<!-- Default MR template -->

/label ~"group::self managed" ~group::distribution::deploy ~devops::systems ~"section::core platform"
/assign me

<!--
Choose appropriate Work Type Labels.
See: https://handbook.gitlab.com/handbook/engineering/metrics/#work-type-classification
-->

## What does this MR do?

%{first_multiline_commit}

## Author's Checklist

For anything in this list which will not be completed, please provide a reason in the MR discussion.

### Required

- [ ] Ensure a release milestone is set.
- [ ] MR title and description are up to date, accurate, and descriptive.
- [ ] MR targeting the appropriate branch.
- [ ] MR has a green pipeline on GitLab.com.
- [ ] When ready for review, MR is labeled ~"workflow::ready for review" per the [MR workflow](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/merge_requests.html).

### Expected

- [ ] Test plan indicating conditions for success has been posted and passes.
- [ ] Documentation is created or updated.
- [ ] Tests are added.

## Related issues
