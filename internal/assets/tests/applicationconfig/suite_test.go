package applicationconfig

import (
	"fmt"
	"os"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gopkg.in/yaml.v3"
)

func TestApplicationConfigTemplates(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: ApplicationConfig Templates")
}

func readYAMLData(data []byte) map[string]any {
	m := map[string]any{}

	err := yaml.Unmarshal(data, &m)

	Expect(err).NotTo(HaveOccurred())

	return m
}

func readYAMLSource(source string) map[string]any {
	f, err := os.Open(fmt.Sprintf("testdata/%s", source))
	Expect(err).NotTo(HaveOccurred())

	m := map[string]any{}

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&m)

	Expect(err).NotTo(HaveOccurred())

	return m
}
