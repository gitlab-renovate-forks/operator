package databasemigration

import (
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var Settings = settings.Get()

type Controller struct {
	dbMigration        *v2alpha2.DatabaseMigration
	appConfig          *v2alpha2.ApplicationConfig
	railsSecrets       *corev1.Secret
	appConfigResources *appconfigutils.ApplicationConfigResources

	workflow framework.Workflow
}

func NewController(dbMigration *v2alpha2.DatabaseMigration) *Controller {
	c := &Controller{
		dbMigration: dbMigration,
	}

	c.workflow = framework.Workflow{
		framework.TaskFunc(c.generateInitialRootPassword),
		framework.TaskFunc(c.dereferenceAppConfig),
		framework.TaskFunc(c.dereferenceResources),
		framework.TaskFunc(c.provisionWorkload),
		framework.TaskFunc(c.checkReadCondition),
	}

	return c
}

func (c *Controller) Execute(ctx *framework.RuntimeContext) error {
	ctx.Logger.Info("Reconciling resource")

	err := framework.Workflow{
		c.workflow,
	}.Execute(ctx)
	if err != nil {
		return err
	}

	ctx.Logger.Info("Migration workflow completed successfully")

	return nil
}
