{{- $appName := printf "%s-%s" .ApplicationServer.Namespace .ApplicationServer.Name -}}

metadata:
  labels:
    {{- with .ApplicationServer.Labels }}
    {{-   . | toYaml | nindent 4 }}
    {{- end }}      
    gitlab.io/app-server: {{ $appName }}
  annotations:
    {{- with .ApplicationServer.Annotations }}
    {{-   . | toYaml | nindent 4 }}
    {{- end }}
    cluster-autoscaler.kubernetes.io/safe-to-evict: "true"
    gitlab.io/fingerprint: {{ .Fingerprint }}
spec:
  automountServiceAccountToken: false
  {{- include "commons/_defaultPodSecurityContext.tpl" .Settings.Rails | nindent 2 }}
  initContainers:
    - name: dependencies
      {{- include "application-server/_dependencies.tpl" . | nindent 6 }}
  containers:
    - name: puma
      {{- include "application-server/_puma.tpl" . | nindent 6 }}
    - name: workhorse
      {{- include "application-server/_workhorse.tpl" . | nindent 6 }}
  volumes:
    {{- include "application-server/_volumes.tpl" . | nindent 4 }}
