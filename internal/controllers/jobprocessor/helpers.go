package jobprocessor

import (
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

const jobProcessorSuffix = "-jobproc"

func (c *Controller) setStatusCondition(rtCtx *framework.RuntimeContext, condition metav1.Condition) error {
	return controllerutils.SetStatusCondition(rtCtx, c.jobProcessor, &c.jobProcessor.Status.Conditions, condition)
}

func (c *Controller) clearStatusCondition(rtCtx *framework.RuntimeContext, condType string) error {
	return controllerutils.ClearStatusCondition(rtCtx, c.jobProcessor, &c.jobProcessor.Status.Conditions, condType)
}

func (c *Controller) appConfigRef() (*types.NamespacedName, error) {
	return appconfigutils.GetApplicationConfigRef(c.jobProcessor.Spec.ApplicationConfigRef, c.jobProcessor)
}

func (c *Controller) getDeployment(rtCtx *framework.RuntimeContext) (*appsv1.Deployment, error) {
	key := types.NamespacedName{
		Namespace: c.jobProcessor.Namespace,
		Name:      c.jobProcessor.Name + jobProcessorSuffix,
	}
	deployment := &appsv1.Deployment{}

	if err := framework.Client.Get(rtCtx, key, deployment); err != nil {
		return nil, err
	}

	return deployment, nil
}

func (c *Controller) getReferencedResources(rtCtx *framework.RuntimeContext) error {
	c.appConfigResources = appconfigutils.NewApplicationConfigDereferencer(c.appConfig, appconfigutils.IncludeAllResources...)
	if err := c.appConfigResources.Dereference(rtCtx); err != nil {
		return err
	}

	return nil
}

func (c *Controller) allResourcesReady() framework.ConditionFunc {
	return func(rtCtx *framework.RuntimeContext) (bool, error) {
		return c.appConfigResources.Ready(), nil
	}
}
