package secret

import (
	"math"
)

func CalculateRandomSequenceEntropy(sequence string) float64 {
	charSet := map[rune]bool{}
	for _, char := range sequence {
		charSet[char] = true
	}

	poolSize := float64(len(charSet))
	entropy := math.Log2(math.Pow(poolSize, float64(len(sequence))))

	return entropy
}
