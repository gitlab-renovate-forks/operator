package settings

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/spf13/afero"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func init() {
	// Ignore unknown flags to avoid breaking the ginkgo test.
	pflag.CommandLine.ParseErrorsWhitelist.UnknownFlags = true
}

var _ = Describe("Config", func() {
	fs := afero.NewMemMapFs()

	BeforeEach(func() {
		fs = afero.NewMemMapFs()
		vp.SetFs(fs)
	})

	AfterEach(func() {
		vp = viper.New()
		vp.SetConfigType("yaml")
	})

	Context("when parsing configuration", func() {
		It("should parse the default configuration", func() {
			err := Parse()
			Expect(err).ToNot(HaveOccurred())

			cfg := Get()
			Expect(cfg.Runtime.MetricsAddr).To(Equal(":8443"))
			Expect(cfg.Runtime.MetricsSecure).To(BeTrue())
			Expect(cfg.Runtime.HealthProbeAddr).To(Equal(":8081"))
			Expect(cfg.Runtime.LeaderElection).To(BeFalse())
			Expect(cfg.Runtime.HTTP2).To(BeFalse())

			Expect(cfg.Log.Development).To(BeTrue())
			Expect(cfg.Log.Format).To(Equal("console"))
			Expect(cfg.Log.Level).To(Equal("debug"))
			Expect(cfg.Log.StacktraceLevel).To(Equal("error"))
			Expect(cfg.Log.TimeFormat).To(Equal("epoch"))
		})

		It("should parse configuration from file", func() {
			configContent := `
runtime:
  metrics-addr: ":9090"
  metrics-secure: false
  health-probe-addr: ":9091"
  leader-election: true
  http2: true
log:
  development: false
  format: "json"
  level: "info"
  stacktrace-level: "panic"
  time-format: "rfc3339"
`
			Expect(afero.WriteFile(fs, "/config.yaml", []byte(configContent), 0644)).To(Succeed())
			configFile = "/config.yaml"

			err := Parse()
			Expect(err).ToNot(HaveOccurred())

			cfg := Get()
			Expect(cfg.Runtime.MetricsAddr).To(Equal(":9090"))
			Expect(cfg.Runtime.MetricsSecure).To(BeFalse())
			Expect(cfg.Runtime.HealthProbeAddr).To(Equal(":9091"))
			Expect(cfg.Runtime.LeaderElection).To(BeTrue())
			Expect(cfg.Runtime.HTTP2).To(BeTrue())

			Expect(cfg.Log.Development).To(BeFalse())
			Expect(cfg.Log.Format).To(Equal("json"))
			Expect(cfg.Log.Level).To(Equal("info"))
			Expect(cfg.Log.StacktraceLevel).To(Equal("panic"))
			Expect(cfg.Log.TimeFormat).To(Equal("rfc3339"))
		})

		It("should return an error for invalid configuration", func() {
			configContent := `
runtime:
  metrics-addr: "invalid-addr"
`
			Expect(afero.WriteFile(fs, "/config.yaml", []byte(configContent), 0644)).To(Succeed())
			configFile = "/config.yaml"

			err := Parse()
			Expect(err).To(HaveOccurred())
		})
	})
})
