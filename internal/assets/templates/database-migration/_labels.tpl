{{- $spec := .DatabaseMigration.Spec -}}
{{- $edition := $spec.Edition | default .Settings.Rails.Edition -}}
{{- $appInfo := dict
      "Name" .DatabaseMigration.Name
      "Version" (printf "%s-%s" $spec.Version $edition | lower)
      "Component" "DatabaseMigration"
      "Role" "DatabaseMigration"
-}}

{{- include "commons/_commonLabels.tpl" $appInfo }}
gitlab.io/edition: {{ $edition }}
{{- with .DatabaseMigration.Labels }}
{{ toYaml . }}
{{- end }}