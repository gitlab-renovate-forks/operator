package manifests

import (
	"fmt"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/manifests/processors"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func ExampleManifestFactory() {
	params := struct {
		owner        client.Object
		workloadSpec *v2alpha2.WorkloadSpec
		ctx          framework.RuntimeContext
	}{
		owner: &appsv1.Deployment{},
		workloadSpec: &v2alpha2.WorkloadSpec{
			PodTemplate: &v2alpha2.PodTemplate{
				Spec: &v2alpha2.RestrictedPodSpec{
					ServiceAccountName: "dummy-sa",
				},
			},
		},
	}

	factory := ManifestFactory[any]{
		Factory(deployment),
		Factory(job),
		Factory(statefulSet),
		Factory(service, customizeService), // service specifc options here.
	}

	objs, err := factory.Build(
		params,
		processors.WithLabels(map[string]string{"app": "dummy-app"}),
		processors.WithSelectorLabels(map[string]string{"app": "dummy-app"}),
	)
	if err != nil {
		panic(err)
	}

	fmt.Println(len(objs))
	// Output: 4
}

func deployment(_ any) (*appsv1.Deployment, error) {
	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: "dummy-deployment",
		},
		Spec: appsv1.DeploymentSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "dummy-container",
							Image: "alpine:latest",
							Env: []corev1.EnvVar{
								{Name: "ENV", Value: "env_old_value"},
							},
						},
					},
				},
			},
		},
	}, nil
}

func job(_ any) (*batchv1.Job, error) {
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name: "dummy-deployment",
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "dummy-container",
							Image: "alpine:latest",
							Env: []corev1.EnvVar{
								{Name: "ENV", Value: "env_old_value"},
							},
						},
					},
				},
			},
		},
	}, nil
}

func statefulSet(_ any) (*appsv1.StatefulSet, error) {
	return &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name: "dummy-deployment",
		},
		Spec: appsv1.StatefulSetSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "dummy-container",
							Image: "alpine:latest",
							Env: []corev1.EnvVar{
								{Name: "ENV", Value: "env_old_value"},
							},
						},
					},
				},
			},
		},
	}, nil
}

func service(_ any) (*corev1.Service, error) {
	return &corev1.Service{}, nil
}

func customizeService(svc *corev1.Service) error {
	svc.Spec.Ports = []corev1.ServicePort{
		{
			Name: "http",
			Port: 80,
		},
	}

	return nil
}
