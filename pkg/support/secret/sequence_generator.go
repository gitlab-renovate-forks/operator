package secret

import (
	"crypto/rand"
	"strings"
)

// SequenceGenerator generates random sequences of characters.
type SequenceGenerator struct {
	CharacterSets []CharacterSet
	Encoder       SecretEncoder
	Length        int
	Names         []string
}

// Generate validates the parameters and generates a random sequence of
// characters for each key.
func (g *SequenceGenerator) Generate() (Payload, error) {
	if err := validateLength(g.Length); err != nil {
		return nil, err
	}

	var b strings.Builder

	for _, cs := range g.CharacterSets {
		b.WriteString(cs)
	}

	if err := validateCharacterSets(b.Len(), g.Length); err != nil {
		return nil, err
	}

	var (
		chars              = b.String()
		randomReadOverhead = 4
		mod                = byte(len(chars))
		reject             = 255 - (255 % mod)
		result             = Payload{}
	)

	for _, name := range g.Names {
		var (
			seq   = make([]byte, g.Length)
			bytes = make([]byte, g.Length*randomReadOverhead)
		)

		var i int = 0

		for i < g.Length {
			if _, err := rand.Read(bytes); err != nil {
				return nil, err
			}

			for _, b := range bytes {
				if i >= g.Length {
					break
				}

				if b < reject {
					seq[i] = chars[b%mod]
					i++
				}
			}
		}

		if g.Encoder != nil {
			seq = g.Encoder(seq)
		}

		result[name] = seq
	}

	return result, nil
}
