package controllerutils

import (
	corev1 "k8s.io/api/core/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
)

// CreateSecret creates the Kubernetes Secrets that the Inventory provides. It
// only creates Secrets when they do not exist.
func CreateSecret(rtCtx *framework.RuntimeContext, i inventory.Inventory[*corev1.Secret], opts ...TaskOption) ([]*corev1.Secret, error) {
	t := &CreateSecretTask{Inventory: i}
	t.ApplyOptions(opts...)

	return t.secrets, t.Execute(rtCtx)
}

type CreateSecretTask struct {
	Register    framework.Register
	Inventory   inventory.Inventory[*corev1.Secret]
	IgnoreError IgnoreErrorFunc

	secrets []*corev1.Secret
}

func (t *CreateSecretTask) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		}
	}
}

func (t *CreateSecretTask) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *CreateSecretTask) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	t.secrets, err = t.Inventory.Get(rtCtx)
	if err != nil {
		return nil, err
	}

	for _, secret := range t.secrets {
		err := framework.Client.Get(rtCtx, client.ObjectKeyFromObject(secret), secret)
		if err == nil {
			// Secret already exists.
			continue
		}

		if !errors.IsNotFound(err) {
			if t.IgnoreError == nil || !t.IgnoreError(err) {
				return nil, err
			}
		}

		if err := framework.Client.Create(rtCtx, secret); err != nil {
			return nil, err
		}
	}

	return t.secrets, nil
}
