{{/*
Binds the Secrets for TLS support into a projected volume.

Expects a context with the following keys:

  - TLS: An API type with TLS support that contains `Credentials` and `CACertificate`.
  - Path: The projection path.
*/}}
{{- $ := . -}}

{{- with .TLS }}
  {{- with digStruct . "Credentials" }}
- secret:
    name: {{ .Name }}
    items:
    - key: tls.key
      path: {{ $.Path }}/tls.key
    - key: tls.crt
      path: {{ $.Path }}/tls.crt
  {{- end }}
  {{- with digStruct . "CACertificate" }}
- secret:
    name: {{ .Name }}
    items:
    - key: {{ .Key }}
      path: {{ $.Path }}/ca.crt
  {{- end }}
{{- end }}
