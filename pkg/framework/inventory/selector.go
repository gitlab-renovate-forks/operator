package inventory

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// ObjectSelector is a function that returns true if the object should be
// selected. It should not modify the object. The function is allowed to fail
// returning an error.
type ObjectSelector func(client.Object) (bool, error)

// Select function wraps the original inventory and selects only the objects
// that match the provided selectors. If any selector fails, the function will
// return an error. All selectors are applied in the order they are provided and
// if any selector returns false, the object is not selected.
func Select[T client.Object](original Inventory[T], selectors ...ObjectSelector) Inventory[T] {
	return InventoryFunc[T](func(ctx *framework.RuntimeContext) ([]T, error) {
		objects, err := original.Get(ctx)
		if err != nil {
			return nil, err
		}

		result := make([]T, 0, len(objects))

		for _, obj := range objects {
			selected := true

			for _, selector := range selectors {
				selected, err = selector(obj)
				if err != nil {
					return nil, err
				}

				if !selected {
					break
				}
			}

			if selected {
				result = append(result, obj)
			}
		}

		return result, nil
	})
}

// NamespaceSelector creates an ObjectSelector that selects objects in the
// provided namespace.
func NamespaceSelector(namespace string) ObjectSelector {
	return func(obj client.Object) (bool, error) {
		return obj.GetNamespace() == namespace, nil
	}
}

// GVKSeletor creates an ObjectSelector that selects objects with the provided
// GroupVersionKind.
func GVKSelector(gvk schema.GroupVersionKind) ObjectSelector {
	return func(obj client.Object) (bool, error) {
		oGVK := obj.GetObjectKind().GroupVersionKind()

		return oGVK.Group == gvk.Group &&
			oGVK.Version == gvk.Version &&
			oGVK.Kind == gvk.Kind, nil
	}
}
