package templateutils

var (
	omniauthSettingsMapper = mapper{
		keySet: []string{
			"autoSignInWithProvider",
			"syncProfileFromProvider",
			"syncProfileAttributes",
			"allowSingleSignOn",
			"blockAutoCreatedUsers",
			"autoLinkLdapUser",
			"autoLinkSamlUser",
			"samlMessageMaxByteSize",
			"autoLinkUser",
			"externalProviders",
			"allowBypassTwoFactor",
		},
		keyMapping: map[string]func(string) string{},
		valueMapping: map[string]func(string) any{
			"autoSignInWithProvider":  toBoolWithFallback(toStringArray),
			"syncProfileFromProvider": toBoolWithFallback(toStringArray),
			"syncProfileAttributes":   toBoolWithFallback(toStringArray),
			"allowSingleSignOn":       toStringArray,
			"blockAutoCreatedUsers":   toBool,
			"autoLinkLdapUser":        toBool,
			"autoLinkSamlUser":        toBool,
			"samlMessageMaxByteSize":  toInt,
			"autoLinkUser":            toBoolWithFallback(toStringArray),
			"externalProviders":       toStringArray,
			"allowBypassTwoFactor":    toBoolWithFallback(toStringArray),
		},
	}
)
