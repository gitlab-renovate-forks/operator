package api

import (
	"context"
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("ObjectReference", func() {
	Context("Options", func() {
		var (
			owner = &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-pod",
					Namespace: "test-namespace",
				},
			}
		)

		It("creates reference from corev1.ObjectReference", func() {
			objRef := corev1.ObjectReference{
				Name: "test-secret",
			}

			ref := NewObjectReference[*corev1.Secret](
				WithOwner(owner),
				WithKind("Secret"),
				WithAPIVersion("v1"),
				FromObjectReference(objRef),
			)

			internalRef := ref.(*objectReference[*corev1.Secret])
			Expect(internalRef.Key.Name).To(Equal("test-secret"))
			Expect(internalRef.Key.Namespace).To(Equal("test-namespace"))
			Expect(internalRef.GVK.Kind).To(Equal("Secret"))
			Expect(internalRef.GVK.Version).To(Equal("v1"))
		})

		It("creates reference from corev1.SecretReference", func() {
			secretRef := corev1.SecretReference{
				Name: "test-secret",
			}

			ref := NewObjectReference[*corev1.Secret](
				WithOwner(owner),
				FromSecretReference(secretRef),
			)

			internalRef := ref.(*objectReference[*corev1.Secret])
			Expect(internalRef.Key.Name).To(Equal("test-secret"))
			Expect(internalRef.Key.Namespace).To(Equal("test-namespace"))
			Expect(internalRef.GVK.Kind).To(Equal("Secret"))
			Expect(internalRef.GVK.Version).To(Equal("v1"))
		})

		It("creates reference from corev1.SecretKeySelector", func() {
			secretRef := corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: "test-secret",
				},
				Key: "foo",
			}

			ref := NewObjectReference[*corev1.Secret](
				WithOwner(owner),
				FromSecretKeySelector(secretRef),
			)

			internalRef := ref.(*objectReference[*corev1.Secret])
			Expect(internalRef.Key.Name).To(Equal("test-secret"))
			Expect(internalRef.Key.Namespace).To(Equal("test-namespace"))
			Expect(internalRef.GVK.Kind).To(Equal("Secret"))
			Expect(internalRef.GVK.Version).To(Equal("v1"))
		})
	})

	Context("String representation", func() {
		It("formats the reference correctly", func() {
			ref := NewObjectReference[*corev1.Secret](
				WithName("test-secret"),
				WithNamespace("test-namespace"),
				WithKind("Secret"),
				WithAPIVersion("v1"),
			)

			Expect(fmt.Sprintf("%s", ref)).To(Equal("v1/Secret[test-namespace/test-secret]"))
		})
	})

	Context("Resolve", func() {
		var (
			fakeScheme *runtime.Scheme
			fakeClient client.WithWatch
			secret     *corev1.Secret
			reference  Reference[*corev1.Secret]
		)

		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
		utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

		BeforeEach(func() {
			secret = &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-secret",
					Namespace: "default",
				},
				Data: map[string][]byte{
					"key": []byte("value"),
				},
			}

			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(secret).
				Build()

			framework.Client = fakeClient
			framework.Scheme = fakeScheme

			reference = NewObjectReference[*corev1.Secret](
				WithName("test-secret"),
				WithNamespace("default"),
				WithKind("Secret"),
				WithAPIVersion("v1"),
			)
		})

		AfterEach(func() {
			framework.Scheme = nil
			framework.Client = nil
		})

		It("successfully resolves the Secret", func() {
			resolvedSecret, err := reference.Resolve(context.Background())

			Expect(err).NotTo(HaveOccurred())
			Expect(resolvedSecret).NotTo(BeNil())
			Expect(resolvedSecret.Name).To(Equal("test-secret"))
			Expect(resolvedSecret.Namespace).To(Equal("default"))
			Expect(resolvedSecret.Data).To(HaveKeyWithValue("key", []byte("value")))
		})

		It("returns an error when the Secret doesn't exist", func() {
			nonExistentRef := NewObjectReference[*corev1.Secret](
				WithName("non-existent"),
				WithNamespace("default"),
				WithKind("Secret"),
				WithAPIVersion("v1"),
			)

			_, err := nonExistentRef.Resolve(context.Background())

			Expect(err).To(HaveOccurred())
		})
	})
})
