package applicationconfig

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
)

type Controller struct {
	appConfig *v2alpha2.ApplicationConfig

	/* This allows us to inject various changes for unit testings */
	workflow framework.Workflow
}

func NewController(appConfig *v2alpha2.ApplicationConfig) *Controller {
	c := &Controller{
		appConfig: appConfig,
	}

	c.workflow = framework.Workflow{
		framework.TaskFunc(c.setPendingStatus),
		framework.TaskFunc(c.generateRailsSecrets),
		framework.TaskFunc(c.setReadyStatus),
	}

	return c
}

func (c *Controller) Execute(rtCtx *framework.RuntimeContext) error {
	return c.workflow.Execute(rtCtx)
}

func (c *Controller) setPendingStatus(rtCtx *framework.RuntimeContext) error {
	return c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationConfigReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingRailsSecretsReason,
		Message: "Preparing GitLab Rails secrets",
	})
}

func (c *Controller) generateRailsSecrets(rtCtx *framework.RuntimeContext) error {
	// Generate Rails Secrets.
	railsSecretsInventory := inventory.Decorate(railsSecretsGenerator,
		inventory.WithNamespace(c.appConfig.Namespace),
		inventory.WithName(c.appConfig.Name+railsSecretNameSuffix),
		inventory.WithLabelsFrom(c.appConfig),
		inventory.WithAnnotationsFrom(c.appConfig))

	secrets, err := controllerutils.CreateSecret(rtCtx, railsSecretsInventory)
	if err != nil {
		return err
	}

	// Advertise the Rails secrets for other resources.
	if len(secrets) != 1 {
		return fmt.Errorf("expected one and only Secret for Rails secrets, got %d", len(secrets))
	}

	return c.setSharedObject(rtCtx, v2alpha2.RailsSecretsObjectUsage, secrets[0])
}

func (c *Controller) setReadyStatus(rtCtx *framework.RuntimeContext) error {
	return c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationConfigReadyCondition,
		Status:  metav1.ConditionTrue,
		Reason:  v2alpha2.RailsSecretsAvailableReason,
		Message: "GitLab Rails secrets are available",
	})
}

func (c *Controller) setStatusCondition(rtCtx *framework.RuntimeContext, cond metav1.Condition) error {
	return controllerutils.SetStatusCondition(rtCtx, c.appConfig, &c.appConfig.Status.Conditions, cond)
}

func (c *Controller) setSharedObject(rtCtx *framework.RuntimeContext, usage string, target client.Object) error {
	sharedObject := apiutils.SharedObjectReferenceForObject(usage, target)

	return controllerutils.SetStatusSharedObject(rtCtx, c.appConfig, &c.appConfig.Status.SharedObjects, sharedObject)
}

const (
	railsSecretNameSuffix = "-rails-secrets"
)
