package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _redis.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/redis.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := parseFurther(readYAMLData([]byte(rendered)))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleRedis, "redis/simple.yaml"),
	)
})

// Each key in Redis template partial contains more YAML. We just parse them
// to make sure we do not catch trivial differences in string representation
// of data.
func parseFurther(d map[string]any) map[string]any {
	for k, v := range d {
		d[k] = readYAMLData([]byte(v.(string)))
	}

	return d
}

var (
	simpleRedis = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				Redis: []v2alpha2.Redis{
					{
						Name: "main",
						ServiceProvider: v2alpha2.ServiceProvider{
							Authentication: v2alpha2.ServiceAuthentication{
								Basic: &corev1.SecretReference{},
							},
						},
					},
				},
			}},
		"RedisSecrets": map[string]*corev1.Secret{
			"main": {
				Data: map[string][]byte{
					"username": []byte("username"),
					"password": []byte("password"),
				},
			},
		},
		"RedisEndpoints": map[string]apiutils.ServiceEndpoint{
			"main": {
				Scheme: "redis",
				Host:   "redis.example.com",
				Port:   6379,
			},
		},
	}
)
