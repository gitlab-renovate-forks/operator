package testing

import (
	"context"
	"regexp"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/interceptor"
)

type InterceptAction int

const (
	Get InterceptAction = 1 << (7 - iota)
	List
	Create
	Delete
	DeleteAllOf
	Update
	Patch

	Any   = Get | List | Create | Delete | DeleteAllOf | Update | Patch
	Apply = Create | Update | Patch
)

type InterceptStage int

const (
	Before InterceptStage = 1 << (1 - iota)
	After

	Surround = Before | After
)

type InterceptObjectSelector func(client.Object) bool
type InterceptFunction[T runtime.Object] func(T, error) error

type InterceptCallback[T runtime.Object] struct {
	Stage    InterceptStage
	Action   InterceptAction
	Selector InterceptObjectSelector
	Reactor  InterceptFunction[T]
}

type InterceptReactor interface {
	React(stage InterceptStage, action InterceptAction, obj runtime.Object, err error) error
}

type InterceptorCallbacks []InterceptReactor

func NameSelector(name string) InterceptObjectSelector {
	return func(obj client.Object) bool {
		return obj.GetName() == name
	}
}

func NamePatternSelector(pattern string) InterceptObjectSelector {
	return func(obj client.Object) bool {
		ok, _ := regexp.MatchString(pattern, obj.GetName())
		return ok
	}
}

func LabelSelector(label labels.Set) InterceptObjectSelector {
	return func(obj client.Object) bool {
		return label.AsSelector().Matches(labels.Set(obj.GetLabels()))
	}
}

func (c *InterceptCallback[T]) React(stage InterceptStage, action InterceptAction, obj runtime.Object, err error) error {
	if stage&c.Stage == 0 {
		return err
	}

	if action&c.Action == 0 {
		return err
	}

	t, isT := obj.(T)
	if !isT {
		return err
	}

	if clientObj, isClientObj := (obj).(client.Object); isClientObj && c.Selector != nil {
		if !c.Selector(clientObj) {
			return err
		}
	}

	if c.Reactor != nil {
		if ret := c.Reactor(t, err); ret != nil {
			return ret
		}
	}

	return err
}

func (c InterceptorCallbacks) React(stage InterceptStage, action InterceptAction, obj runtime.Object, err error) error {
	for _, callback := range c {
		if err := callback.React(stage, action, obj, err); err != nil {
			return err
		}
	}

	return err
}

func (c InterceptorCallbacks) Funcs() interceptor.Funcs {
	return interceptor.Funcs{
		Get:         c.get,
		List:        c.list,
		Create:      c.create,
		Delete:      c.delete,
		DeleteAllOf: c.deleteAllOf,
		Update:      c.update,
		Patch:       c.patch,
	}
}

func (c InterceptorCallbacks) get(ctx context.Context, client client.WithWatch, key client.ObjectKey, obj client.Object, opts ...client.GetOption) error {
	if err := c.React(Before, Get, obj, nil); err != nil {
		return err
	}

	err := client.Get(ctx, key, obj, opts...)

	return c.React(After, Get, obj, err)
}

func (c InterceptorCallbacks) list(ctx context.Context, client client.WithWatch, obj client.ObjectList, opts ...client.ListOption) error {
	if err := c.React(Before, List, obj, nil); err != nil {
		return err
	}

	err := client.List(ctx, obj, opts...)

	return c.React(After, List, obj, err)
}

func (c InterceptorCallbacks) create(ctx context.Context, client client.WithWatch, obj client.Object, opts ...client.CreateOption) error {
	if err := c.React(Before, Create, obj, nil); err != nil {
		return err
	}

	err := client.Create(ctx, obj, opts...)

	return c.React(After, Create, obj, err)
}

func (c InterceptorCallbacks) delete(ctx context.Context, client client.WithWatch, obj client.Object, opts ...client.DeleteOption) error {
	if err := c.React(Before, Delete, obj, nil); err != nil {
		return err
	}

	err := client.Delete(ctx, obj, opts...)

	return c.React(After, Delete, obj, err)
}

func (c InterceptorCallbacks) deleteAllOf(ctx context.Context, client client.WithWatch, obj client.Object, opts ...client.DeleteAllOfOption) error {
	if err := c.React(Before, DeleteAllOf, obj, nil); err != nil {
		return err
	}

	err := client.DeleteAllOf(ctx, obj, opts...)

	return c.React(After, DeleteAllOf, obj, err)
}

func (c InterceptorCallbacks) update(ctx context.Context, client client.WithWatch, obj client.Object, opts ...client.UpdateOption) error {
	if err := c.React(Before, Update, obj, nil); err != nil {
		return err
	}

	err := client.Update(ctx, obj, opts...)

	return c.React(After, Update, obj, err)
}

func (c InterceptorCallbacks) patch(ctx context.Context, client client.WithWatch, obj client.Object, patch client.Patch, opts ...client.PatchOption) error {
	if err := c.React(Before, Patch, obj, nil); err != nil {
		return err
	}

	err := client.Patch(ctx, obj, patch, opts...)

	return c.React(After, Patch, obj, err)
}

// PatchInterceptor is an interceptor to simulate a patch operation by updating the object if it exists, otherwise creating it.
// It does not fully simulate the patch operation, but it is useful for testing.
var PatchInterceptor = interceptor.Funcs{
	Patch: func(ctx context.Context, client client.WithWatch, obj client.Object, patch client.Patch, opts ...client.PatchOption) error {
		err := client.Update(ctx, obj)
		if apierrors.IsNotFound(err) {
			return client.Create(ctx, obj)
		}

		return err
	},
}

var (
	// SuccessfulDeploymentInterceptor is an interceptor to simulate successful deployments.
	// It will set all deployments to have a completly available status.
	SuccessfulDeploymentInterceptor = &InterceptCallback[*appsv1.Deployment]{
		Stage:  After,
		Action: Get,
		Reactor: func(obj *appsv1.Deployment, err error) error {
			if err != nil {
				return err
			}

			obj.Status.Conditions = []appsv1.DeploymentCondition{
				{
					Type:   appsv1.DeploymentProgressing,
					Status: corev1.ConditionTrue,
					Reason: "NewReplicaSetAvailable",
				},
				{
					Type:   appsv1.DeploymentAvailable,
					Status: corev1.ConditionTrue,
					Reason: "MinimumReplicasAvailable",
				},
			}
			return nil
		},
	}

	// SuccessfulJobInterceptor is an interceptor to simulate successful jobs.
	// It will set all jobs to have a completed status.
	SuccessfulJobInterceptor = &InterceptCallback[*batchv1.Job]{
		Stage:  After,
		Action: Get,
		Reactor: func(obj *batchv1.Job, err error) error {
			if err != nil {
				return err
			}

			obj.Status.Conditions = []batchv1.JobCondition{
				{
					Type:   batchv1.JobComplete,
					Status: corev1.ConditionTrue,
					Reason: "Completed",
				},
			}
			return nil
		},
	}

	FailedJobInterceptor = &InterceptCallback[*batchv1.Job]{
		Stage:  After,
		Action: Get,
		Reactor: func(obj *batchv1.Job, err error) error {
			if err != nil {
				return err
			}

			obj.Status.Conditions = []batchv1.JobCondition{
				{
					Type:   batchv1.JobComplete,
					Status: corev1.ConditionFalse,
					Reason: "Failed",
				},
				{
					Type:    batchv1.JobFailed,
					Status:  corev1.ConditionTrue,
					Reason:  "Failed",
					Message: "Job failed",
				},
			}
			return nil
		},
	}
)
