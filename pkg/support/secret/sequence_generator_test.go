package secret

import (
	"bytes"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("SequenceGenerator", func() {
	Context("with valid configurations", func() {
		It("should generate a valid sequence with single character set", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Hexadecimal},
				Length:        128,
				Names:         []string{"token"},
			}

			payload, err := generator.Generate()
			Expect(err).NotTo(HaveOccurred())
			Expect(payload).To(HaveLen(1))
			Expect(payload).To(HaveKeyWithValue("token", MatchRegexp(`^[0-9a-f]{128}$`)))
		})

		It("should generate valid sequences with multiple character sets", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Digits, LowerLetters, UpperLetters},
				Length:        32,
				Names:         []string{"key1", "key2"},
			}

			payload, err := generator.Generate()
			Expect(err).NotTo(HaveOccurred())
			Expect(payload).To(HaveLen(2))
			Expect(payload).To(HaveKey(BeElementOf("key1", "key2")))
			Expect(payload).To(HaveEach(MatchRegexp(`^[0-9a-zA-Z]{32}$`)))
		})

		It("should use encoder when provided", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Digits, UpperLetters},
				Length:        32,
				Names:         []string{"encoded"},
				Encoder:       func(b []byte) []byte { return bytes.ToLower(b) },
			}

			payload, err := generator.Generate()
			Expect(err).NotTo(HaveOccurred())
			Expect(payload).To(HaveKeyWithValue("encoded", MatchRegexp(`^[0-9a-z]{32}$`)))
		})
	})

	Context("with invalid configurations", func() {
		It("should return an error for zero length", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Digits},
				Length:        0,
				Names:         []string{"password"},
			}

			_, err := generator.Generate()
			Expect(err).To(MatchError(ErrInvalidLength))
		})

		It("should return an error for low entropy", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Digits},
				Length:        8,
				Names:         []string{"password"},
			}

			_, err := generator.Generate()
			Expect(err).To(MatchError(ErrInvalidCharacterSet))
		})
	})

	Context("quality of the generated sequences", func() {
		It("should generate unique sequences", func() {
			generator := &SequenceGenerator{
				CharacterSets: []CharacterSet{Digits, Letters},
				Length:        32,
				Names:         []string{"test"},
			}

			results := make([][]byte, 10)
			for i := 0; i < 10; i++ {
				payload, err := generator.Generate()
				Expect(err).NotTo(HaveOccurred())
				results[i] = payload["test"]
			}

			for i := 0; i < len(results); i++ {
				for j := i + 1; j < len(results); j++ {
					Expect(results[i]).NotTo(Equal(results[j]), "Generated identical sequences")
				}
			}
		})
	})
})
