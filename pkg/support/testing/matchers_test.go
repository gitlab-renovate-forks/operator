package testing

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("Matchers", func() {
	var configMap client.Object

	BeforeEach(func() {
		configMap = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-config",
				Namespace: "test-namespace",
				Annotations: map[string]string{
					"annotation1": "annotation1-value",
				},
				Labels: map[string]string{
					"label1": "label1-value",
				},
			},
			TypeMeta: metav1.TypeMeta{
				Kind:       "ConfigMap",
				APIVersion: "v1",
			},
			Data: map[string]string{
				"key1": "val1",
				"key2": "val2",
			},
		}
	})

	Describe("HaveKind", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveKind("ConfigMap"))
			Expect(configMap).To(HaveKind(MatchRegexp(".+Map")))
		})

		It("Does not match", func() {
			matcher := HaveKind("Secret")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			Expect(matcher.FailureMessage(configMap)).To(ContainSubstring("to have kind <string>: \"Secret\""))
		})
	})

	Describe("HaveName", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveName("test-config"))
			Expect(configMap).To(HaveName(MatchRegexp(".+-config")))
		})

		It("Does not match", func() {
			matcher := HaveName("test-secret")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			Expect(matcher.FailureMessage(configMap)).To(ContainSubstring("to have name <string>: \"test-secret\""))
		})
	})

	Describe("HaveGroupVersion", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveGroupVersion("v1"))
		})

		It("Does not match", func() {
			matcher := HaveGroupVersion("test/v1")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			Expect(matcher.FailureMessage(configMap)).To(ContainSubstring("to have group version <string>: \"test/v1\""))
		})
	})

	Describe("HaveNamespace", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveNamespace("test-namespace"))
			Expect(configMap).To(HaveNamespace(MatchRegexp("test-.+")))
		})

		It("Does not match", func() {
			matcher := HaveNamespace("default")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			Expect(matcher.FailureMessage(configMap)).To(ContainSubstring("to have namespace <string>: \"default\""))
		})
	})

	Describe("HaveAnnotation", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveAnnotation("annotation1", "annotation1-value"))
			Expect(configMap).To(HaveAnnotation(MatchRegexp("annotation."), MatchRegexp("annotation1-.+")))
		})

		It("Does not match", func() {
			matcher := HaveAnnotation("annotation2", "val")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			message := matcher.FailureMessage(configMap)
			Expect(message).To(SatisfyAll(
				ContainSubstring("to have annotation"),
				ContainSubstring("key: <string>: \"annotation2\""),
				ContainSubstring("value: <string>: \"val\""),
			))
		})
	})

	Describe("HaveLabel", func() {
		It("Matches", func() {
			Expect(configMap).To(HaveLabel("label1", "label1-value"))
			Expect(configMap).To(HaveLabel(MatchRegexp("label."), MatchRegexp("label1-.+")))
		})

		It("Does not match", func() {
			matcher := HaveLabel("label2", "val")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			message := matcher.FailureMessage(configMap)
			Expect(message).To(SatisfyAll(
				ContainSubstring("to have label"),
				ContainSubstring("key: <string>: \"label2\""),
				ContainSubstring("value: <string>: \"val\""),
			))
		})
	})

	Describe("HavePath", func() {
		It("Matches", func() {
			Expect(configMap).To(HavePath("data.key1", "val1"))
			Expect(configMap).To(HavePath("data.key1", HaveLen(4)))
			Expect(configMap).To(HavePath("data.key1", MatchRegexp("val.")))
			Expect(configMap).To(HavePath("apiVersion", MatchRegexp("v1")))
			Expect(configMap).To(HavePath("metadata.annotations.annotation1", "annotation1-value"))
			Expect(configMap).To(HavePath("i.do.not.exist", BeNil()))
		})

		It("Does not match", func() {
			matcher := HavePath("data.key1", "wrongVal")
			ok, err := matcher.Match(configMap)
			Expect(ok).To(BeFalse())
			Expect(err).ToNot(HaveOccurred())
			message := matcher.FailureMessage(configMap)
			Expect(message).To(
				ContainSubstring("to have <string>: \"wrongVal\" at path data.key1"),
			)
		})

		Context("On a unstructured object", func() {
			It("Matches", func() {
				un := &unstructured.Unstructured{
					Object: map[string]interface{}{
						"apiVersion": "v1",
						"kind":       "ConfigMap",
						"data":       map[string]string{"key": "val1"},
					}}

				Expect(un).To(HavePath("data.key", "val1"))
			})
		})
	})

	Describe("HaveElementSatisfying", func() {
		It("Matches", func() {
			Expect([]string{"a", "b", "c"}).To(HaveElementSatisfying(Equal("a")))
		})

		It("Does not match", func() {
			Expect([]string{"a", "b", "c"}).ToNot(HaveElementSatisfying(Equal("d")))
		})
	})

	Describe("MatchYAMLSubset", func() {
		It("Matches", func() {
			Expect(configMap).To(MatchYAMLSubset(`kind: ConfigMap
data:
  key1: val1
  key2: val2
`))
		})

		It("Does not match", func() {
			Expect(configMap).ToNot(MatchYAMLSubset(`kind: Secret`))
		})
	})
})
