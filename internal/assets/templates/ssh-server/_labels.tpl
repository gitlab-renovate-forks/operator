{{- $spec := .SSHServer.Spec -}}
{{- $appInfo := dict
      "Name" .SSHServer.Name
      "Version" (printf "%s" $spec.Version | lower)
      "Component" "SSHServer"
      "Role" "SSHServer"
-}}

{{- include "commons/_commonLabels.tpl" $appInfo }}
{{- with .SSHServer.Labels }}
{{ toYaml . }}
{{- end }}