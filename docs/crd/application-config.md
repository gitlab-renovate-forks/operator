---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# ApplicationConfig

`ApplicationConfig` contains GitLab Rails application configuration and secrets,
most of which are located in config directory of GitLab Rails application. It is
shared between Rails-based workloads including Puma and Sidekiq.

The controller of the `ApplicationConfig` custom resource is responsible for:

* Generating GitLab Rails secrets when they do not exist.
* Setting a status condition to indicate that the application configuration is
  ready to use.

## Installation

You can create your own `ApplicationConfig` custom resource by adapting
the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_applicationconfig.yaml).

For all available options check the [`ApplicationConfig` API reference](../generated/api-docs.md#applicationconfig).
