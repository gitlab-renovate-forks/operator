package templateutils

var (
	ldapSettingsMapper = mapper{
		keySet: []string{
			"base",
			"smartcardAuth",
			"activeDirectory",
			"smartcardADCertField",
			"smartcardADCertFormat",
			"allowUsernameOrEmailLogin",
			"blockAutoCreatedUsers",
			"userFilter",
			"groupBase",
			"adminGroup",
			"externalGroups",
			"syncSshKeys",
			"lowerCaseUsernames",
		},
		keyMapping: map[string]func(string) string{},
		valueMapping: map[string]func(string) any{
			"activeDirectory":           toBool,
			"allowUsernameOrEmailLogin": toBool,
			"blockAutoCreatedUsers":     toBool,
			"syncSshKeys":               toBool, // it is quoted bool in omnibus
			"lowerCaseUsernames":        toBool,
		},
	}

	ldapAttributesMapper = mapper{
		keySet: []string{
			"username",
			"email",
			"name",
			"firstName",
			"lastName",
		},
		keyMapping: map[string]func(string) string{},
		valueMapping: map[string]func(string) any{
			"username":  toStringArray,
			"email":     toStringArray,
			"name":      toStringArray,
			"firstName": toStringArray,
			"lastName":  toStringArray,
		},
	}
)
