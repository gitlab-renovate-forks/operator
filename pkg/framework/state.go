package framework

import (
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

// SharedStateVisitor is the callback for visiting SharedState entries. It is
// called for each entry and indicates if the visitor wants to continue
// visiting the rest of the entires by returning a boolean value.
type SharedStateVisitor = internal.SharedStateVisitor

// SharedState is the interface for interacting with the shared states of a
// RuntimeContext.
type SharedState interface {
	// Store adds an entry or updates an existing one.
	//
	// Entries are uniquely identified by their name. If an entry with the same
	// name already exists, it is overwritten. Entries also have a set of tags.
	// Multiple entries can have the same tags.
	//
	// As a convention, use CamelCase for entry names and lower-kebab-case for
	// tags.
	Store(string, any, ...string)

	// Get retrieves an entry with its name. It returns false if the entry does
	// not exist.
	Get(string) (any, bool)

	// Visit visits all the entries an calls the visitor callback for each of
	// them. If the visitor returns false, visiting is stopped.
	Visit(SharedStateVisitor)
}
