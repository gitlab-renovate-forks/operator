package controllerutils

import (
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
)

// CreateObject creates the Kubernetes objects that the Inventory provides. It
// only creates Objects when they do not exist.
func CreateObject[T client.Object](rtCtx *framework.RuntimeContext, i inventory.Inventory[T], opts ...TaskOption) ([]T, error) {
	t := &CreateObjectTask[T]{Inventory: i}
	t.ApplyOptions(opts...)

	return t.objects, t.Execute(rtCtx)
}

type CreateObjectTask[T client.Object] struct {
	Register    framework.Register
	Inventory   inventory.Inventory[T]
	IgnoreError IgnoreErrorFunc

	objects []T
}

func (t *CreateObjectTask[T]) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		}
	}
}

func (t *CreateObjectTask[T]) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *CreateObjectTask[T]) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	t.objects, err = t.Inventory.Get(rtCtx)
	if err != nil {
		return nil, err
	}

	for _, secret := range t.objects {
		err := framework.Client.Get(rtCtx, client.ObjectKeyFromObject(secret), secret)
		if err == nil {
			// Secret already exists.
			continue
		}

		if !errors.IsNotFound(err) {
			if t.IgnoreError == nil || !t.IgnoreError(err) {
				return nil, err
			}
		}

		if err := framework.Client.Create(rtCtx, secret); err != nil {
			return nil, err
		}
	}

	return t.objects, nil
}
