---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Instance

The `Instance` manages the lifecycle of a GitLab instance.

The controller can upgrade GitLab Rails components with
[zero-downtime upgrade workflow](https://docs.gitlab.com/ee/update/zero_downtime.html).

## Installation

To create your own `Instance` custom resource, you can adapt
the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_instance.yaml).

For all available options, see the [`Instance` API reference](../generated/api-docs.md#instance).

To link your other GitLab custom resources to the `Instance`, add
a `gitlab.io/instance=<instance name>` label to them. After
adding your resources, you can upgrade your GitLab instance by bumping
the version of the `Instance`.

If you have multiple `ApplicationServer`s, you must mark one of them as the
primary with `gitlab.io/primary: true` label. The primary's [`ApplicationConfig`](application-config.md)
is used to orchestrate the [`DatabaseMigration`](db-migration.md).

## Known issues

- The controller does not support upgrade paths. For more information,
  see [epic 85](https://gitlab.com/groups/gitlab-org/cloud-native/-/epics/85).
- The controller is not aware of the status of the background migrations.
