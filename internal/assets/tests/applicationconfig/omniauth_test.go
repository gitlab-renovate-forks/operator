package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _omniauth.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/omniauth.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleOmniAuth, "omniauth/simple.yaml"),
	)
})

var (
	simpleOmniAuth = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				OmniAuth: &v2alpha2.ApplicationOmniAuthSettings{
					Settings: map[string]string{
						"autoSignInWithProvider": "saml,cas3",
						"allowBypassTwoFactor":   "true",
					},

					Providers: []v2alpha2.ApplicationOmniAuthProvider{
						{
							Name:    "saml",
							Options: *selectSecret("saml", "config"),
						},
					},
				},
			},
		},
		"OmniAuthSecrets": map[string]*corev1.Secret{
			"saml": makeSecret("default", "saml",
				"config", "foobar: true",
			),
		},
	}
)
