package framework

import (
	"context"
	"errors"
	"log/slog"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

var _ = Describe("RuntimeContext", func() {
	It("should wrap the original context", func() {
		key := struct{ key string }{key: "test"}

		originalCtx := context.WithValue(context.Background(), key, "value")
		rtCtx, err := NewRuntimeContext(originalCtx)
		Expect(err).NotTo(HaveOccurred())
		Expect(rtCtx.Value(key)).To(Equal("value"))
	})

	It("should implement context.Context interface", func() {
		rtCtx, err := NewRuntimeContext(context.Background())
		Expect(err).NotTo(HaveOccurred())

		var _ context.Context = rtCtx // This will fail to compile if RuntimeContext doesn't implement context.Context
	})

	Describe("NewRuntimeContext", func() {
		It("should create a new RuntimeContext with default logger", func() {
			rtCtx, err := NewRuntimeContext(context.Background())
			Expect(err).NotTo(HaveOccurred())
			Expect(rtCtx).NotTo(BeNil())
			Expect(rtCtx.Logger).To(Equal(internal.DiscardLogger))
		})

		//nolint:staticcheck
		It("should return an error when context is nil", func() {
			rtCtx, err := NewRuntimeContext(nil)
			Expect(err).To(MatchError("cannot create context from nil source"))
			Expect(rtCtx).To(BeNil())
		})

		It("should create a new RuntimeContext with custom logger", func() {
			customLogger := slog.New(slog.NewTextHandler(GinkgoWriter, nil))
			rtCtx, err := NewRuntimeContext(context.Background(), WithLogger(customLogger))
			Expect(err).NotTo(HaveOccurred())
			Expect(rtCtx).NotTo(BeNil())
			Expect(rtCtx.Logger).To(Equal(customLogger))
		})

		It("should create a new RuntimeContext with controller logger", func() {
			controllerLogger := zap.New()
			rtCtx, err := NewRuntimeContext(context.Background(), WithLogrLogger(controllerLogger))
			Expect(err).NotTo(HaveOccurred())
			Expect(rtCtx).NotTo(BeNil())
			Expect(rtCtx.Logger).NotTo(BeNil())
		})

		It("should apply multiple options", func() {
			customLogger := slog.New(slog.NewTextHandler(GinkgoWriter, nil))
			controllerLogger := zap.New()

			rtCtx, err := NewRuntimeContext(context.Background(),
				WithLogger(customLogger),
				WithLogrLogger(controllerLogger))

			Expect(err).NotTo(HaveOccurred())
			Expect(rtCtx).NotTo(BeNil())
			Expect(rtCtx.Logger).NotTo(Equal(customLogger)) // The last option (WithControllerLogger) should override
		})
	})

	Describe("RuntimeContextOption", func() {
		It("WithLogger should set the provided logger", func() {
			customLogger := slog.New(slog.NewTextHandler(GinkgoWriter, nil))
			rtCtx := &RuntimeContext{}
			opt := WithLogger(customLogger)
			opt(rtCtx)

			Expect(rtCtx.Logger).To(Equal(customLogger))
		})

		It("WithLogrLogger should set the logger from controller logger", func() {
			zapLogger := zap.New()
			rtCtx := &RuntimeContext{}
			opt := WithLogrLogger(zapLogger)
			opt(rtCtx)

			Expect(rtCtx.Logger).NotTo(BeNil())
		})
	})
})

var _ = Describe("ContextualValue", func() {
	It("should be implemented by custom types", func() {
		var _ ContextualValue[string] = nil
	})

	It("should allow custom implementations to return values based on context", func() {
		ctxVal := &testContextualValue{}

		//nolint:staticcheck
		val1, err1 := ctxVal.Get(&RuntimeContext{
			Context: context.WithValue(context.TODO(), "test", "value1"),
		})

		//nolint:staticcheck
		val2, err2 := ctxVal.Get(&RuntimeContext{
			Context: context.WithValue(context.TODO(), "test", "value2"),
		})

		Expect(err1).NotTo(HaveOccurred())
		Expect(val1).To(Equal([]string{"value1"}))
		Expect(err2).NotTo(HaveOccurred())
		Expect(val2).To(Equal([]string{"value2"}))
	})

	Describe("StaticValue", func() {
		It("should implement ContextualValue interface", func() {
			var _ ContextualValue[int] = StaticValue[int]{}
		})

		It("should always return the same set of values", func() {
			values := StaticValue[int]{1, 2, 3}

			result1, err1 := values.Get(&RuntimeContext{})
			result2, err2 := values.Get(&RuntimeContext{})

			Expect(err1).NotTo(HaveOccurred())
			Expect(result1).To(Equal([]int{1, 2, 3}))
			Expect(err2).NotTo(HaveOccurred())
			Expect(result1).To(Equal(result2))
		})

		It("should return an empty slice for an empty StaticValue", func() {
			values := StaticValue[string]{}

			result, err := values.Get(&RuntimeContext{})

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(BeEmpty())
		})
	})
})

// Test contextual value.
type testContextualValue struct{}

func (c *testContextualValue) Get(rtCtx *RuntimeContext) ([]string, error) {
	val := rtCtx.Value("test")

	if val == nil {
		return []string{}, errors.New("no value in context")
	}

	if strVal, isString := val.(string); isString {
		return []string{strVal}, nil
	} else {
		return []string{}, errors.New("value is not a string")
	}
}
