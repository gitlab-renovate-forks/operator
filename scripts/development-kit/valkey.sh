#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    echo_stage "Installing Valkey"

    gitlab_password=$(gen_random 64)

    kubectl -n $(namespace) create secret generic "valkey-secret" --from-literal="password"="$gitlab_password"

    create_basic_auth_secret_raw "valkey-auth" "" "$gitlab_password"

    helm install valkey valkey \
        --repo https://charts.bitnami.com/bitnami \
        -n $(namespace) \
        --create-namespace \
        --version 0.3.13 -f "$SCRIPT_DIR/manifests/values.valkey.yaml"
}

uninstall() {
    echo_stage "Uninstalling Valkey"

    helm uninstall valkey -n $(namespace) || true

    kubectl -n $(namespace) delete secret valkey-secret --ignore-not-found=true
    kubectl -n $(namespace) delete secret valkey-auth --ignore-not-found=true
    kubectl -n $(namespace) delete pvc valkey-data-valkey-master-0 --ignore-not-found=true
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
