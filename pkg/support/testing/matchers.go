package testing

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/onsi/gomega"
	"github.com/onsi/gomega/format"
	"github.com/onsi/gomega/gcustom"
	"github.com/onsi/gomega/types"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

// haveElementSatisfyingMatcher implements the GomegaMatcher interface.
type haveElementSatisfyingMatcher struct {
	subMatcher types.GomegaMatcher

	// We'll store the matched element (if found) so that we can produce
	// more detailed error messages upon failure.
	matchedElement interface{}
}

// HaveElementSatisfying returns an instance of haveElementSatisfyingMatcher.
// You provide another matcher (e.g. BeNumerically(">", 5)) that each element is tested against.
func HaveElementSatisfying(subMatcher types.GomegaMatcher) types.GomegaMatcher {
	return &haveElementSatisfyingMatcher{
		subMatcher: subMatcher,
	}
}

// Match checks the actual value. It must be a slice or array; otherwise, it errors out.
func (m *haveElementSatisfyingMatcher) Match(actual interface{}) (bool, error) {
	// Make sure the actual value is a slice or array.
	val := reflect.ValueOf(actual)
	kind := val.Kind()

	if kind != reflect.Slice && kind != reflect.Array {
		return false, fmt.Errorf(
			"HaveElementSatisfying requires a slice or array. Got:\n%s",
			format.Object(actual, 1),
		)
	}

	// Iterate over the elements and attempt to match the sub-matcher.
	for i := 0; i < val.Len(); i++ {
		elem := val.Index(i).Interface()

		success, err := m.subMatcher.Match(elem)
		if err != nil {
			return false, err
		}

		if success {
			// We found an element that matches; store it for better error reporting.
			m.matchedElement = elem
			return true, nil
		}
	}

	// No element satisfied the sub-matcher.
	return false, nil
}

// FailureMessage is called when Match returns false in a positive expectation.
func (m *haveElementSatisfyingMatcher) FailureMessage(actual interface{}) string {
	// If matchedElement is nil, it means we never found a matching element.
	// We'll show the entire collection and the sub-matcher’s failure message
	// for clarity.
	return fmt.Sprintf(
		"Expected\n%s\nto have an element satisfying:\n%s",
		format.Object(actual, 1),
		m.subMatcher.FailureMessage(m.matchedElement),
	)
}

// NegatedFailureMessage is called when Match returns true in a negative expectation.
func (m *haveElementSatisfyingMatcher) NegatedFailureMessage(actual interface{}) string {
	return fmt.Sprintf(
		"Expected\n%s\nnot to have an element satisfying:\n%s",
		format.Object(actual, 1),
		m.subMatcher.NegatedFailureMessage(m.matchedElement),
	)
}

// HaveKind checks the kind of an object. By default the equal matcher is used.
func HaveKind(expected any) types.GomegaMatcher {
	return simpleObjectMatcher(func(o client.Object) any { return getGvk(o).Kind }, expected).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have kind {{format .Data}}", expected)
}

// HaveGroupVersion checks the string representation of an object's group version. By default the equal matcher is used.
func HaveGroupVersion(expected any) types.GomegaMatcher {
	return simpleObjectMatcher(func(o client.Object) any {
		return getGvk(o).GroupVersion().String()
	}, expected).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have group version {{format .Data}}", expected)
}

// HaveName checks the name of an object. By default the equal matcher is used.
func HaveName(expected any) types.GomegaMatcher {
	return simpleObjectMatcher(func(o client.Object) any { return o.GetName() }, expected).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have name {{format .Data}}", expected)
}

// HaveName checks the namespace of an object. By default the equal matcher is used.
func HaveNamespace(expected any) types.GomegaMatcher {
	return simpleObjectMatcher(func(o client.Object) any { return o.GetNamespace() }, expected).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have namespace {{format .Data}}", expected)
}

// HaveAnnotation checks if the object has a label.
// By default key and value use Equal() to perform the match, however custom
// matchers can be passed in instead.
func HaveLabel(key, value any) types.GomegaMatcher {
	return gcustom.MakeMatcher(func(obj client.Object) (bool, error) {
		return gomega.HaveKeyWithValue(key, value).Match(obj.GetLabels())
	}).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have label\n\tkey: {{index .Data 0 | format}}\n\tvalue: {{index .Data 1 | format}}", []any{key, value})
}

// HaveAnnotation checks if the object has a annotation.
// By default key and value use Equal() to perform the match, however custom
// matchers can be passed in instead.
func HaveAnnotation(key, value any) types.GomegaMatcher {
	return gcustom.MakeMatcher(func(obj client.Object) (bool, error) {
		return gomega.HaveKeyWithValue(key, value).Match(obj.GetAnnotations())
	}).
		WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have annotation\n\tkey: {{index .Data 0 | format}}\n\tvalue: {{index .Data 1 | format}}", []any{key, value})
}

// yamlSubsetMatcher struct for matching an object against a YAML.
type yamlSubsetMatcher struct {
	yamlContent string
	mismatches  []string
}

// MatchYAMLSubset creates a new instance of the matcher.
func MatchYAMLSubset(yamlContent string) types.GomegaMatcher {
	return &yamlSubsetMatcher{
		yamlContent: yamlContent,
	}
}

// Match checks if the object matches the YAML content.
func (m *yamlSubsetMatcher) Match(actual interface{}) (bool, error) {
	obj, ok := actual.(client.Object)
	if !ok {
		return false, fmt.Errorf("MatchYAMLForObject expects a client.Object, but got %T", actual)
	}

	// Convert YAML to a map.
	var yamlMap map[string]interface{}
	if err := yaml.Unmarshal([]byte(m.yamlContent), &yamlMap); err != nil {
		return false, fmt.Errorf("failed to parse YAML: %w", err)
	}

	// Convert object to unstructured map.
	unstructuredMap, err := runtime.DefaultUnstructuredConverter.ToUnstructured(obj)
	if err != nil {
		return false, fmt.Errorf("failed to convert object to map: %w", err)
	}

	// Perform recursive match and capture mismatches.
	m.mismatches = []string{}
	matches := recursiveMatchWithDetails(yamlMap, unstructuredMap, "", &m.mismatches)

	return matches, nil
}

// FailureMessage returns a failure message if the match fails.
func (m *yamlSubsetMatcher) FailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected\n%s\nto match YAML:\n%s\nMismatches:\n%s",
		actual,
		m.yamlContent,
		strings.Join(m.mismatches, "\n"),
	)
}

// NegatedFailureMessage returns a failure message if the negated match fails.
func (m *yamlSubsetMatcher) NegatedFailureMessage(actual interface{}) string {
	return fmt.Sprintf("Expected\n%s\nnot to match YAML:\n%s",
		actual,
		m.yamlContent,
	)
}

// recursiveMatchWithDetails checks if all key-value pairs in `subset` exist in `superset` and records mismatches.
//
//nolint:all
func recursiveMatchWithDetails(subset, superset map[string]interface{}, path string, mismatches *[]string) bool {
	allMatch := true
	for key, val := range subset {
		currentPath := key
		if path != "" {
			currentPath = fmt.Sprintf("%s.%s", path, key)
		}

		if supVal, found := superset[key]; found {
			// If the value is a map, recurse
			if subMap, ok := val.(map[string]interface{}); ok {
				if supMap, ok := supVal.(map[string]interface{}); ok {
					if !recursiveMatchWithDetails(subMap, supMap, currentPath, mismatches) {
						allMatch = false
					}
				} else {
					allMatch = false
					*mismatches = append(*mismatches, fmt.Sprintf("Field mismatch at '%s': expected map, got %T", currentPath, supVal))
				}
			} else if val != supVal {
				// Check for value equality
				allMatch = false
				*mismatches = append(*mismatches, fmt.Sprintf("Field mismatch at '%s': expected '%v', got '%v'", currentPath, val, supVal))
			}
		} else {
			allMatch = false
			*mismatches = append(*mismatches, fmt.Sprintf("Field missing at '%s': expected '%v'", currentPath, val))
		}
	}
	return allMatch
}

// HavePath checks for the value of a object using a jq-like dot-separated path.
// By default HavePath uses Equal() to perform the match, however a
// matcher can be passed in instead:
//
// Expect(corev1.ConfigMap{Data: map[string]string{"key", "val"}}).To(HavePath("data.key"), "val"))
// Expect(corev1.ConfigMap{Data: map[string]string{"key", "val"}}).To(HavePath("data.key"), HaveLen(4)))
//
//	configMap := &corev1.ConfigMap{
//	  TypeMeta: metav1.TypeMeta{Kind: "ConfigMap"},
//	   Data:     map[string]string{"key": "val"},
//	}
//
// Expect(configMap).To(HavePath("data.key", "val"))
// Expect(configMap).To(HavePath("data.key", HaveLen(3)))
//
// The object is marshaled to a unstructured instance.
func HavePath(path string, expected any) types.GomegaMatcher {
	return gcustom.MakeMatcher(func(obj client.Object) (bool, error) {
		matcher, isMatcher := expected.(gomega.OmegaMatcher)
		if !isMatcher {
			matcher = gomega.Equal(expected)
		}

		un := &unstructured.Unstructured{}

		if unObj, ok := obj.(*unstructured.Unstructured); ok {
			un = unObj
		} else {
			data, err := json.Marshal(obj)
			if err != nil {
				return false, err
			}

			err = un.UnmarshalJSON(data)

			if err != nil {
				return false, err
			}
		}

		var cursor any = un.Object

		for _, seg := range strings.Split(path, ".") {
			cursor = getPathSeg(seg, cursor)
			if cursor == nil {
				break
			}
		}

		return matcher.Match(cursor)
	}).WithTemplate("Expected:\n{{.FormattedActual}}\n{{.To}} have {{index .Data 0 | format}} at path {{index .Data 1}}", []any{expected, path})
}

type MatchObjectOption func(options *deepObjectMatcher)

// MatchObject is a custom Gomega matcher for comparing Kubernetes objects.
// It compares two objects for equality, with some flexibility in the comparison
// to handle special cases or ignore certain fields.
//
// It calculates a deep object "diff" and expects it to be empty. When the diff
// is not empty, the matcher fails and the diff is printed to help you to locate
// the mismatch.
//
// By default, the matcher ignores the following fields in metav1.ObjectMeta:
//   - SelfLink
//   - UID
//   - ResourceVersion
//   - Generation
//   - CreationTimestamp
//   - DeletionTimestamp
//
// Custom options can be provided to ignore additional fields or set a custom
// runtime.Scheme:
//
//	Expect(actual).To(MatchObject(expected,
//	    IgnoreField(SomeKubernetesType{}, "FieldToIgnore", "AnotherFieldToIgnore"),
//	    WithScheme(customScheme),
//	))
//
// The matcher supports comparison of unstructured.Unstructured objects by
// converting them to the appropriate client.Object type using the provided
// runtime.Scheme.
//
// Note: This matcher is specifically designed for client.Object types and will
// return an error if used with other types.
func MatchObject(expected client.Object, options ...MatchObjectOption) types.GomegaMatcher {
	matcher := &deepObjectMatcher{
		expected: expected,
		fieldsToIgnore: []fieldsDescriptor{
			{kind: metav1.ObjectMeta{}, fields: negligibleObjectMetaFields},
		},
		scheme: framework.Scheme,
	}

	for _, opt := range options {
		opt(matcher)
	}

	return matcher
}

// IgnoreField creates a MatchObjectOption that instructs the MatchObject matcher
// to ignore a specific field during object comparison.
func IgnoreField(kind any, fields ...string) MatchObjectOption {
	return func(matcher *deepObjectMatcher) {
		matcher.fieldsToIgnore = append(matcher.fieldsToIgnore, fieldsDescriptor{kind: kind, fields: fields})
	}
}

// WithScheme creates a MatchObjectOption that sets a custom runtime.Scheme
// for the MatchObject matcher to use during object comparisons.
func WithScheme(scheme *runtime.Scheme) MatchObjectOption {
	return func(matcher *deepObjectMatcher) {
		matcher.scheme = scheme
	}
}

func simpleObjectMatcher(objFn func(client.Object) any, expected any) gcustom.CustomGomegaMatcher {
	return gcustom.MakeMatcher(func(obj client.Object) (bool, error) {
		matcher, isMatcher := expected.(gomega.OmegaMatcher)
		if !isMatcher {
			matcher = gomega.Equal(expected)
		}

		return gomega.WithTransform(objFn, matcher).Match(obj)
	})
}

func getPathSeg(pathSeg string, obj any) any {
	cval := reflect.ValueOf(obj)

	if cval.Kind() != reflect.Map {
		return nil
	}

	mapI := cval.MapIndex(reflect.ValueOf(pathSeg))

	if !mapI.IsValid() {
		return nil
	}

	return mapI.Interface()
}

type deepObjectMatcher struct {
	expected       client.Object
	differences    string
	fieldsToIgnore []fieldsDescriptor
	scheme         *runtime.Scheme
}

type fieldsDescriptor struct {
	kind   any
	fields []string
}

func (m *deepObjectMatcher) Match(actual any) (bool, error) {
	cmpOptions := []cmp.Option{}
	for _, entry := range m.fieldsToIgnore {
		cmpOptions = append(cmpOptions, cmpopts.IgnoreFields(entry.kind, entry.fields...))
	}

	obj, isObject := actual.(client.Object)

	if !isObject {
		// NOTE: It is still possible to handle comparison for non-object types.
		//       For example for ApplyConfiguration values.
		//       One way around it is to marshal and unmarshal the object to and
		//       from JSON and treat it an a Unstructured type.
		return false, fmt.Errorf("object match can only handle client.Object types. %T is not supported", actual)
	}

	if u, isUnstructured := actual.(*unstructured.Unstructured); isUnstructured {
		var err error

		obj, err = kube.ConvertUnstructuredWithScheme(u, m.scheme)
		if err != nil {
			return false, fmt.Errorf("failed to convert unstructured.Unstructured object to client.Object: %w", err)
		}
	}

	m.differences = cmp.Diff(m.expected, obj, cmpOptions...)

	return cmp.Equal(m.expected, obj, cmpOptions...), nil
}

func (m *deepObjectMatcher) FailureMessage(actual any) (message string) {
	return fmt.Sprintf("Expected objects to match. Check the differences:\n\n"+
		"--- %T (expected)\n"+
		"+++ %T (actual)\n\n"+
		"%s", m.expected, actual, m.differences)
}

func (m *deepObjectMatcher) NegatedFailureMessage(actual any) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\nnot to match\n\t%#v", actual, m.expected)
}

var (
	negligibleObjectMetaFields = []string{
		"SelfLink",
		"UID",
		"ResourceVersion",
		"Generation",
		"CreationTimestamp",
		"DeletionTimestamp",
	}
)
