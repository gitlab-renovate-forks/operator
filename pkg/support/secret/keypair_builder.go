package secret

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
)

func GeneratePrivateKey(algorithm Algorithm, size int) (any, error) {
	var priv any

	var err error

	switch algorithm {
	case ED25519:
		_, priv, err = ed25519.GenerateKey(rand.Reader)
	case RSA:
		priv, err = rsa.GenerateKey(rand.Reader, size)
	case ECDSA:
		switch size {
		case 256:
			priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		case 384:
			priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
		case 521:
			priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
		}
	default:
		return nil, fmt.Errorf("algorithm: %s %w", algorithm, ErrInvalidAlgorithm)
	}

	if err != nil {
		return nil, err
	}

	if priv == nil {
		return nil, fmt.Errorf("algorithm/size: %s/%d %w", algorithm, size, ErrInvalidAlgorithmSize)
	}

	return priv, nil
}

func GetPublicKey(priv any) any {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	case ed25519.PrivateKey:
		return k.Public().(ed25519.PublicKey)
	default:
		return nil
	}
}
