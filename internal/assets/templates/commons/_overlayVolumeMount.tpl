{{/*
Overlays the entries of a volume over a mount point. It is useful for merging
the entries of a volume with the entries of an existing directory.

Expects a context with the following keys:

  - Volume: Name of the source volume that contains entries.
  - Path: The path to an existing directory.
  - Entries: Name of the entries of the volume. Mounted with the same name
    inside the directory.
*/}}
{{ $ := . }}

{{- range .Entries }}
- name: {{ $.Volume }}
  mountPath: {{ $.Path }}/{{ . }}
  subPath: {{ . }}
{{ end -}}
