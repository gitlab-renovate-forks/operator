package api

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	corev1 "k8s.io/api/core/v1"
)

// ServiceEndpointType is a string representation of the type of service endpoint.
type ServiceEndpointType string

const (
	UnknownServiceEndpoint    ServiceEndpointType = ""
	PostgreSQLServiceEndpoint ServiceEndpointType = "PostgreSQL"
	RedisServiceEndpoint      ServiceEndpointType = "Redis"
	ClickHouseServiceEndpoint ServiceEndpointType = "ClickHouse"
	GitalyServiceEndpoint     ServiceEndpointType = "Gitaly"
	PrometheusServiceEndpoint ServiceEndpointType = "Prometheus"
	MattermostServiceEndpoint ServiceEndpointType = "Mattermost"
	SMTPServiceEndpoint       ServiceEndpointType = "SMTP"
	IMAPServiceEndpoint       ServiceEndpointType = "IMAP"
	HTTPServiceEndpoint       ServiceEndpointType = "HTTP"
)

// ServiceEndpoint is a summary of the details of a v2alpha2.ServiceEndpoint.
//
// Depending on the specified endpoint type, the ServiceEndpoint is constructed
// differently.
//
// The function uses the following defaults:
//
// | Endpoint Type | Scheme     | TLS Scheme | Port | TLS Port |
// |---------------|------------|------------|------|----------|
// | PostgreSQL    | postgresql | postgresql | 5432 | 5432     |
// | Redis         | redis      | rediss     | 6379 | 6379     |
// | ClickHouse    | http       | https      | 8123 | 8443     |
// | Gitaly        | tcp        | tls        | 8075 | 8076     |
// | Prometheus    | http       | https      | 9090 | 443      |
// | Mattermost    | http       | https      | 8065 | 443      |
// | SMTP          | smtp       | smtp       | 25   | 465      |
// | IMAP          | imap       | imap       | 143  | 993      |
//
// The default ports are applied when the ServiceProvider does not specify a
// port or the port can not be deduced from the referenced Service.
type ServiceEndpoint struct {
	Type   ServiceEndpointType
	Scheme string
	Host   string
	Port   int32
	TLS    bool
}

// ServiceEndpointCarrier is an intermediary to attach additional application
// specific information to v2alpha2.ServiceEndpointCarrier after resolution.
type ServiceEndpointCarrier struct {
	*v2alpha2.ServiceEndpoint

	Service *corev1.Service
	Type    ServiceEndpointType
}

// NewServiceEndpointReference creates a reference from a v2alpha2.ServiceEndpoint.
// Depending on ServiceEndpoint provider (Service vs. External), the resulting
// reference may require resolution.
//
// When the provider is a Service, it resolves the ObjectReference to obtain the
// Service. In this case the provided ObjectReferenceOptions are used.
//
// When the provider is External, it simply uses the provided information without
// any further resolution.
//
// Note that for effective resolution and consumption, the endpoint type is
// required.
func NewServiceEndpointReference(endpoint *v2alpha2.ServiceEndpoint, endpointType ServiceEndpointType, options ...ObjectReferenceOption) Reference[*ServiceEndpointCarrier] {
	o := &ObjectReferenceOptions{}
	for _, option := range options {
		option(o)
	}

	return &endpointReference{
		endpoint:               endpoint,
		endpointType:           endpointType,
		objectReferenceOptions: o,
	}
}

func (e *ServiceEndpoint) Consume(ctx context.Context, endpoint *ServiceEndpointCarrier) error {
	if endpoint == nil {
		return nil
	}

	e.Type = endpoint.Type
	e.Scheme = defaultScheme[endpoint.Type]

	if endpoint.External != nil {
		e.Host = endpoint.External.Host
		e.Port = endpoint.External.Port
	} else if endpoint.Service != nil {
		e.Host = fmt.Sprintf("%s.%s.svc.cluster.local",
			endpoint.Service.Name, endpoint.Service.Namespace)
		e.Port = e.findPortNumber(endpoint.Service)
	}

	if endpoint.TLS != nil {
		e.TLS = *endpoint.TLS
	} else {
		e.TLS = defaultTLS[e.Type]
	}

	if e.Port == 0 {
		if e.TLS {
			e.Port = defaultTLSPorts[e.Type]
		} else {
			e.Port = defaultPorts[e.Type]
		}
	}

	e.Scheme = e.updateScheme(e.Scheme, e.TLS)

	return nil
}

func (e *ServiceEndpoint) findPortNumber(svc *corev1.Service) int32 {
	namesToMatch := []string{
		e.Scheme, strings.ToLower(string(e.Type)),
	}

	for _, port := range svc.Spec.Ports {
		if containsIgnoreCase(port.Name, namesToMatch) {
			return port.Port
		}

		if port.AppProtocol != nil && containsIgnoreCase(*port.AppProtocol, namesToMatch) {
			return port.Port
		}
	}

	return 0
}

func (e *ServiceEndpoint) updateScheme(scheme string, tls bool) string {
	if !tls {
		return scheme
	}

	switch e.Type {
	case RedisServiceEndpoint:
		return "rediss"
	case ClickHouseServiceEndpoint, PrometheusServiceEndpoint, MattermostServiceEndpoint:
		return "https"
	case GitalyServiceEndpoint:
		return "tls"
	default:
		return scheme
	}
}

func containsIgnoreCase(s string, substr []string) bool {
	for _, sub := range substr {
		if strings.Contains(strings.ToLower(s), strings.ToLower(sub)) {
			return true
		}
	}

	return false
}

type endpointReference struct {
	endpoint               *v2alpha2.ServiceEndpoint
	endpointType           ServiceEndpointType
	objectReferenceOptions *ObjectReferenceOptions
}

func (r *endpointReference) Resolve(ctx context.Context) (*ServiceEndpointCarrier, error) {
	e := &ServiceEndpointCarrier{
		ServiceEndpoint: r.endpoint,
		Type:            r.endpointType,
	}

	if r.endpoint.Service != nil {
		svcRef := NewObjectReference[*corev1.Service](
			WithObjectReferenceOptions(r.objectReferenceOptions),
			FromObjectReference(*r.endpoint.Service),
			WithAPIVersion("v1"),
			WithKind("Service"))

		if svc, err := svcRef.Resolve(ctx); err != nil {
			return e, err
		} else {
			e.Service = svc
		}
	} else if r.endpoint.External == nil {
		return e, fmt.Errorf("ambagious ServiceEndpoint for %s", r.endpointType)
	}

	return e, nil
}

func (r *endpointReference) String() string {
	if r.endpoint.External != nil {
		return fmt.Sprintf("ServiceEndpoint<%s>", string(r.endpointType))
	} else if r.endpoint.Service != nil {
		return fmt.Sprintf("v1/Service[%s/%s]",
			r.endpoint.Service.Namespace, r.endpoint.Service.Name)
	} else {
		return fmt.Sprintf("ambagious ServiceEndpoint: %s", r.endpointType)
	}
}

var (
	defaultScheme map[ServiceEndpointType]string = map[ServiceEndpointType]string{
		PostgreSQLServiceEndpoint: "postgresql",
		RedisServiceEndpoint:      "redis",
		ClickHouseServiceEndpoint: "http",
		GitalyServiceEndpoint:     "tcp",
		PrometheusServiceEndpoint: "http",
		MattermostServiceEndpoint: "http",
		SMTPServiceEndpoint:       "smtp",
		IMAPServiceEndpoint:       "imap",
	}

	defaultTLS map[ServiceEndpointType]bool = map[ServiceEndpointType]bool{
		PostgreSQLServiceEndpoint: false,
		RedisServiceEndpoint:      false,
		ClickHouseServiceEndpoint: false,
		GitalyServiceEndpoint:     false,
		PrometheusServiceEndpoint: false,
		MattermostServiceEndpoint: false,
		SMTPServiceEndpoint:       false,
		IMAPServiceEndpoint:       false,
	}

	defaultPorts map[ServiceEndpointType]int32 = map[ServiceEndpointType]int32{
		PostgreSQLServiceEndpoint: 5432,
		RedisServiceEndpoint:      6379,
		ClickHouseServiceEndpoint: 8123,
		GitalyServiceEndpoint:     8075,
		PrometheusServiceEndpoint: 9090,
		MattermostServiceEndpoint: 8065,
		SMTPServiceEndpoint:       25,
		IMAPServiceEndpoint:       143,
	}

	defaultTLSPorts map[ServiceEndpointType]int32 = map[ServiceEndpointType]int32{
		PostgreSQLServiceEndpoint: 5432,
		RedisServiceEndpoint:      6379,
		ClickHouseServiceEndpoint: 8443,
		GitalyServiceEndpoint:     8076,
		PrometheusServiceEndpoint: 443,
		MattermostServiceEndpoint: 443,
		SMTPServiceEndpoint:       465,
		IMAPServiceEndpoint:       993,
	}
)
