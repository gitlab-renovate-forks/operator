package applicationserver

import (
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	framework "gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
)

// The following secrets are owned by the application server:
//   - Workhorse: For verifying Workhorse access to Rails.
//   - Internal API: Shell and Gitaly use it to authenticate to internal API.
//   - Incoming Mail: Mailroom uses it to authenticate to webhook. Only used
//     when incoming mail delivery method is webhook.
//   - Service Desk: Mailroom uses it to authenticate to webhook. Only used
//     when service desk mail delivery method is webhook.
//   - KAS: KAS service uses it to authenticate to GitLab endpoint.
var appSecretsGenerator = &inventory.SecretGenerator{
	Generators: []secret.Generator{
		&secret.SequenceGenerator{
			Length:        32,
			CharacterSets: []secret.CharacterSet{secret.AlphaNumeric},
			Names: []string{
				"incoming-mail",
				"service-desk",
				"kas",
			},
			Encoder: secret.Base64Encoder,
		},
		&secret.SequenceGenerator{
			Length:        64,
			CharacterSets: []secret.CharacterSet{secret.AlphaNumeric},
			Names: []string{
				"internal-api",
			},
		},
	},
}

var workhorseSecretGenerator = &inventory.SecretGenerator{
	Generators: []secret.Generator{
		&secret.SequenceGenerator{
			Length:        32,
			CharacterSets: []secret.CharacterSet{secret.AlphaNumeric},
			Names: []string{
				"workhorse",
			},
			Encoder: secret.Base64Encoder,
		},
	},
}

func (c *Controller) appSecretsInventory() framework.Inventory[*corev1.Secret] {
	return framework.Decorate(
		framework.AggregateInventory[*corev1.Secret]{
			framework.Decorate(appSecretsGenerator,
				framework.WithName(c.appServer.Name+appSecretNameSuffix)),
			framework.Decorate(workhorseSecretGenerator,
				framework.WithName(c.appServer.Name+internalSecretsSuffix)),
		},
		framework.WithNamespace(c.appServer.Namespace),
		framework.WithLabelsFrom(c.appServer),
		framework.WithAnnotationsFrom(c.appServer))
}

func (c *Controller) appServiceInventory() framework.Inventory[client.Object] {
	return framework.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "application-server/service.yaml",
			Values: map[string]any{
				"ApplicationServer": c.appServer,
			},
		},
		framework.WithControllerReference(c.appServer))
}

func (c *Controller) configSecretsInventory() framework.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"ApplicationServer": c.appServer,
		"ApplicationConfig": c.appConfig,
	}, c.appConfigResources.Resources)

	return framework.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "application-server/config/*.yaml",
			Values:  values,
		},
		framework.WithControllerReference(c.appServer))
}

func (c *Controller) appWorkloadInventory(fingerprint string) framework.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"ApplicationServer": c.appServer,
		"ApplicationConfig": c.appConfig,
		"Fingerprint":       fingerprint,
	}, c.appConfigResources.Resources)

	return framework.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "application-server/*.yaml",
			Values:  values,
		},
		framework.WithControllerReference(c.appServer))
}
