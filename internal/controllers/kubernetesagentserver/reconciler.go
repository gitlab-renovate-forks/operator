package kubernetesagentserver

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

type Reconciler struct{}

//+kubebuilder:rbac:groups=gitlab.com,resources=kubernetesagentservers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=gitlab.com,resources=kubernetesagentservers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=gitlab.com,resources=kubernetesagentservers/finalizers,verbs=update

func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.KubernetesAgentServer) (ctrl.Result, error) {
	return framework.Expand(nil)
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.KubernetesAgentServer{}).
		Complete(reconcile.AsReconciler(mgr.GetClient(), &Reconciler{}))
}
