package api

import (
	"context"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

type ApplicationConfigOption int

const (
	ApplicationConfigPostgreSQL ApplicationConfigOption = iota
	ApplicationConfigPrometheus
	ApplicationConfigZoekt
)

type ApplicationConfig struct {
	Composite
	*v2alpha2.ApplicationConfig

	PostgreSQL       *NamedServiceProvider
	Prometheus       *ServiceEndpoint
	ZoektCredentials SecretData

	resources map[ApplicationConfigOption]declareApplicationConfigResource
}

func NewApplicationConfig(options ...ApplicationConfigOption) *ApplicationConfig {
	a := &ApplicationConfig{
		Composite: NewComposite(),
		resources: map[ApplicationConfigOption]declareApplicationConfigResource{},
	}

	a.declareResources(options)

	return a
}

var _ Resource[*v2alpha2.ApplicationConfig] = &ApplicationConfig{}

func (a *ApplicationConfig) Consume(ctx context.Context, appConfig *v2alpha2.ApplicationConfig) error {
	a.ApplicationConfig = appConfig

	for _, loader := range a.resources {
		loader(a, appConfig)
	}

	return a.Load(ctx)
}

func (a *ApplicationConfig) declareResources(options []ApplicationConfigOption) {
	for _, o := range options {
		if declaration, ok := knownApplicationConfigResources[o]; ok {
			a.resources[o] = declaration
		}
	}
}

type declareApplicationConfigResource func(*ApplicationConfig, *v2alpha2.ApplicationConfig)

var (
	knownApplicationConfigResources = map[ApplicationConfigOption]declareApplicationConfigResource{
		ApplicationConfigPostgreSQL: declareApplicationConfigPostgreSQL,
		ApplicationConfigPrometheus: declareApplicationConfigPrometheus,
		ApplicationConfigZoekt:      declareApplicationConfigZoektCredentials,
	}
)

func declareApplicationConfigPostgreSQL(a *ApplicationConfig, appConfig *v2alpha2.ApplicationConfig) {
	a.PostgreSQL = NewNamedServiceProvider(PostgreSQLServiceEndpoint, WithOwner(appConfig))

	AddResource(a.Composite,
		mapServiceProviderReferences(appConfig.Spec.PostgreSQL,
			func(v v2alpha2.PostgreSQL) string { return v.Name },
			func(v v2alpha2.PostgreSQL) *v2alpha2.ServiceProvider { return &v.ServiceProvider }),
		a.PostgreSQL)
}

func declareApplicationConfigPrometheus(a *ApplicationConfig, appConfig *v2alpha2.ApplicationConfig) {
	if appConfig.Spec.Prometheus == nil {
		return
	}

	a.Prometheus = &ServiceEndpoint{}

	AddReferencedResource(a.Composite,
		NewServiceEndpointReference(appConfig.Spec.Prometheus, PrometheusServiceEndpoint,
			WithOwner(appConfig)),
		a.Prometheus)
}

func declareApplicationConfigZoektCredentials(a *ApplicationConfig, appConfig *v2alpha2.ApplicationConfig) {
	if appConfig.Spec.ZoektCredentials == nil {
		return
	}

	a.ZoektCredentials = SecretData{}

	AddReferencedResource(a.Composite,
		NewObjectReference[*corev1.Secret](
			FromSecretReference(*appConfig.Spec.ZoektCredentials),
			WithOwner(appConfig)),
		a.ZoektCredentials)
}

func mapServiceProviderReferences[T any](all []T, name func(T) string, ref func(T) *v2alpha2.ServiceProvider) map[string]*v2alpha2.ServiceProvider {
	m := map[string]*v2alpha2.ServiceProvider{}

	for _, v := range all {
		m[name(v)] = ref(v)
	}

	return m
}
