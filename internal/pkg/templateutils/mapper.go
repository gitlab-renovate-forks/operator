package templateutils

import (
	"strconv"
	"strings"

	"github.com/huandu/xstrings"
)

type mapper struct {
	keySet       []string
	keyMapping   map[string]func(string) string
	valueMapping map[string]func(string) any
}

// Maps key-value pairs, enforcing the given key set. Uses the associated key
// and value mappers if specified.
//
// The default key mapper uses snake case and the default value mapper is to
// convert to string.
func (m *mapper) mapStrictly(in map[string]string) map[string]any {
	result := map[string]any{}

	for _, key := range m.keySet {
		value, hasValue := in[key]
		if !hasValue {
			continue
		}

		keyMapper, hasKeyMapper := m.keyMapping[key]
		if !hasKeyMapper {
			keyMapper = xstrings.ToSnakeCase
		}

		valueMapper, hasValueMapper := m.valueMapping[key]
		if !hasValueMapper {
			valueMapper = toString
		}

		result[keyMapper(key)] = valueMapper(value)
	}

	return result
}

// This is similar to mapStrictly, but it ignores value mapping, uses the
// original values, and only performs key mapping.
func (m *mapper) mapKeysStrictly(in map[string]any) map[string]any {
	result := map[string]any{}

	for _, key := range m.keySet {
		value, hasValue := in[key]
		if !hasValue {
			continue
		}

		keyMapper, hasKeyMapper := m.keyMapping[key]
		if !hasKeyMapper {
			keyMapper = xstrings.ToSnakeCase
		}

		result[keyMapper(key)] = value
	}

	return result
}

// As opposed to mapKeysStrictly, this method does not enforce the key set and
// maps all the input keys.
func (m *mapper) mapKeys(in map[string]any) map[string]any {
	result := map[string]any{}

	for key, value := range in {
		keyMapper, hasKeyMapper := m.keyMapping[key]
		if !hasKeyMapper {
			keyMapper = xstrings.ToSnakeCase
		}

		result[keyMapper(key)] = value
	}

	return result
}

func toString(s string) any {
	return s
}

func toRubyString(s string) any {
	return "'" + s + "'"
}

func toRubySymbol(s string) any {
	return ":" + s
}

func toBool(s string) any {
	v, _ := strconv.ParseBool(s)
	return v
}

func toInt(s string) any {
	v, _ := strconv.Atoi(s)
	return v
}

func toStringArray(s string) any {
	return strings.Split(s, ",")
}

func toBoolWithFallback(fallback func(string) any) func(s string) any {
	return func(s string) any {
		if v, err := strconv.ParseBool(s); err != nil {
			return fallback(s)
		} else {
			return v
		}
	}
}

func toIntWithFallback(d int) func(string) any {
	return func(s string) any {
		if v, err := strconv.Atoi(s); err != nil {
			return d
		} else {
			return v
		}
	}
}

func boolMapToAnyMap(in map[string]bool) map[string]any {
	out := make(map[string]any, len(in))
	for k, v := range in {
		out[k] = v
	}

	return out
}

func strMapToAnyMap(in map[string]string) map[string]any {
	out := make(map[string]any, len(in))
	for k, v := range in {
		out[k] = v
	}

	return out
}
