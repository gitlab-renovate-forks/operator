package databasemigration

import (
	"fmt"
	"strings"
	"time"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
)

func (c Controller) generateInitialRootPassword(ctx *framework.RuntimeContext) error {
	// If initial root password is already set or this is an upgrade, skip this step.
	if c.dbMigration.Spec.InitialRootPassword != nil || c.dbMigration.Spec.IsUpgrade {
		return nil
	}

	ctx.Logger.Debug("Generating initial root password")

	// Set status condition to Ready with False status and PendingApplicationSecrets reason.
	err := c.setStatusCondition(ctx, metav1.Condition{
		Type:    v2alpha2.DatabaseMigrationCompletedCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingRootPasswordReason,
		Message: "Preparing initial root password",
	})
	if err != nil {
		ctx.Logger.Debug("Failed to set status condition", "error", err)
		return err
	}

	secrets, err := controllerutils.CreateObject(ctx, c.rootPasswordInventory())
	if err != nil {
		ctx.Logger.Debug("Failed to create secret", "error", err)
		return err
	}

	if len(secrets) != 1 {
		err = fmt.Errorf("expected 1 secret, got %d", len(secrets))
		ctx.Logger.Debug("Unexpected number of secrets", "error", err)

		return err
	}

	return nil
}

// Dereference ApplicationConfig and wait for it to become ready to use.
func (c *Controller) dereferenceAppConfig(ctx *framework.RuntimeContext) error {
	ctx.Logger.Debug("Dereferencing ApplicationConfig resource")

	// Set status condition to Ready with False status and PendingApplicationConfig reason.
	err := c.setStatusCondition(ctx, metav1.Condition{
		Type:    v2alpha2.DatabaseMigrationCompletedCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.AwaitingApplicationConfigReason,
		Message: "Waiting for the referenced ApplicationConfig: " + c.dbMigration.Spec.ApplicationConfigRef.Name,
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Get ApplicationConfig.
	appConfigKey, err := c.appConfigRef()
	if err != nil {
		return fmt.Errorf("failed to get ApplicationConfig reference: %w", err)
	}

	appConfig, err := controllerutils.GetObjects(ctx, &v2alpha2.ApplicationConfig{},
		[]types.NamespacedName{*appConfigKey})
	if err != nil {
		return fmt.Errorf("failed to get ApplicationConfig: %w", err)
	}

	if len(appConfig) != 1 {
		return fmt.Errorf("expected one and only ApplicationConfig, got %d", len(appConfig))
	}

	c.appConfig = appConfig[0]

	// Wait for ApplicationConfig to become ready to use.
	err = controllerutils.WaitFor(ctx, c.appConfigReady(), controllerutils.WithDelay(10*time.Second))
	if err != nil {
		ctx.Logger.Debug("ApplicationConfig is not ready",
			"ApplicationConfig.Status.Conditions", c.appConfig.Status.Conditions)

		return err
	}

	// Dereference Rails secrets from ApplicationConfig. We need it to accurately
	// calculate the configuration digest.
	railsSecretKey, err := c.railsSecretRef()
	if err != nil {
		return fmt.Errorf("failed to retrieve Rails secrets reference: %w", err)
	}

	railsSecret, err := controllerutils.GetObjects(ctx, &corev1.Secret{},
		[]types.NamespacedName{*railsSecretKey})
	if err != nil {
		return fmt.Errorf("failed to get Rails secrets: %w", err)
	}

	if len(railsSecret) != 1 {
		return fmt.Errorf("expected one and only Secret for Rails secrets, got %d", len(railsSecret))
	}

	c.railsSecrets = railsSecret[0]

	return nil
}

// Dereference all referenced resources, including Secrets and Services.
func (c *Controller) dereferenceResources(ctx *framework.RuntimeContext) error {
	ctx.Logger.Debug("Dereferencing resources")

	// Set status condition to Ready with False status and AwaitingResources reason.
	err := c.setStatusCondition(ctx, metav1.Condition{
		Type:    v2alpha2.DatabaseMigrationCompletedCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.AwaitingResourcesReason,
		Message: "Awaiting referenced resources",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Dereference ALL referenced resources.
	err = c.getReferencedResources(ctx)
	if err != nil {
		return fmt.Errorf("failed to get referenced resources: %w", err)
	}

	// Update status condition message with name of unavailable resources.
	err = c.setStatusCondition(ctx, metav1.Condition{
		Type:   v2alpha2.DatabaseMigrationCompletedCondition,
		Status: metav1.ConditionFalse,
		Reason: v2alpha2.AwaitingResourcesReason,
		Message: "Awaiting referenced resources" + func() string {
			if len(c.appConfigResources.Missing) == 0 {
				return ""
			} else {
				return ": " + strings.Join(c.appConfigResources.Missing, ",")
			}
		}(),
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Wait for ALL referenced resources to be available.
	if err := controllerutils.WaitFor(ctx, c.allResourcesReady(),
		controllerutils.WithDelay(Settings.Rails.ResourceCheckDelay)); err != nil {
		ctx.Logger.Debug("Missing referenced resource", "Missing", c.appConfigResources.Missing)

		return err
	}

	return nil
}

// Deploy workload.
func (c *Controller) provisionWorkload(ctx *framework.RuntimeContext) error {
	ctx.Logger.Debug("Provisioning migration workload")

	// Set status condition to Ready with False status and PendingApplicationStartup.
	err := c.setStatusCondition(ctx, metav1.Condition{
		Type:    v2alpha2.DatabaseMigrationCompletedCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingStartReason,
		Message: "Pending migration Job startup",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Create configuration Secrets (database, clickhouse).
	_, err = controllerutils.CreateObject(ctx, c.configSecretsInventory())
	if err != nil {
		return fmt.Errorf("failed to create configuration Secrets: %w", err)
	}

	// Create migration Job resource.
	if _, err = controllerutils.CreateObject(ctx, c.workloadInventory()); err != nil {
		return fmt.Errorf("failed to create migration Job resource: %w", err)
	}

	return nil
}
