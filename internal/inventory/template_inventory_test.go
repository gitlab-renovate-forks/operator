package inventory

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/version"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

var _ = Describe("DefaultTemplateEngine", func() {
	It("should be able to render an embedded template", func() {
		result, err := DefaultTemplateEngine.Render("testdata/hello.txt", map[string]any{
			"Name": "World",
		})

		Expect(err).NotTo(HaveOccurred())
		Expect(result).To(HaveKey("testdata/hello.txt"))
		Expect(result["testdata/hello.txt"].String()).To(Equal("Hello World!"))
	})
})

var _ = Describe("TemplatedObjects", func() {
	var (
		actualServerVersion *version.Info
		actualAPIResources  []string
	)

	BeforeEach(func() {
		actualServerVersion = framework.ServerVersion
		actualAPIResources = framework.APIResources

		framework.ServerVersion = &version.Info{
			Major:      "1",
			Minor:      "30",
			GitVersion: "1.30.1",
		}
		framework.APIResources = []string{
			"foo.com/v1",
			"foo.com/v1/bar",
		}
	})

	AfterEach(func() {
		framework.ServerVersion = actualServerVersion
		framework.APIResources = actualAPIResources
	})

	It("should parse the embedded templated object", func() {
		rtCtx, _ := framework.NewRuntimeContext(context.TODO())
		rtCtx.State.Store("Resource", "Test")

		objects, err := (&TemplatedObjects{
			Pattern: "testdata/configmap.yaml",
			Values: map[string]any{
				"Name": "World",
			},
		}).Get(rtCtx)

		Expect(err).NotTo(HaveOccurred())
		Expect(objects).To(HaveLen(1))

		configMap := &corev1.ConfigMap{}

		err = kube.FromUnstructured(objects[0].(*unstructured.Unstructured), configMap)
		Expect(err).NotTo(HaveOccurred())

		Expect(configMap.Name).To(Equal("test"))
		Expect(configMap.Data).To(Equal(map[string]string{
			"context":     "Hello Test!",
			"kubeVersion": "1.30.1",
			"kubeAPIs":    "foo.com/v1,foo.com/v1/bar",
			"logLevel":    "debug",
			"values":      "Hello World!",
		}))
	})
})
