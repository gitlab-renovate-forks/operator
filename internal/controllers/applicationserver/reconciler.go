package applicationserver

import (
	"context"
	"fmt"
	"slices"

	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// +kubebuilder:rbac:groups=gitlab.com,resources=applicationservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationservers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationservers/finalizers,verbs=update
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete

type Reconciler struct{}

// Reconcile runs in the reconciliation loop for the ApplicationServer resource.
func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.ApplicationServer) (ctrl.Result, error) {
	logger := ctrl.Log.
		WithName("controllers.applicationserver").
		WithValues(
			"resource", fmt.Sprintf("%s/%s", obj.Namespace, obj.Name),
			"generation", obj.Generation,
			"resourceVersion", obj.ResourceVersion,
		)

	rtCtx, err := framework.NewRuntimeContext(ctx, framework.WithLogrLogger(logger))
	if err != nil {
		return framework.Expand(err)
	}

	err = NewController(obj.DeepCopy()).Execute(rtCtx)

	return framework.Expand(err)
}

func listApplicationServersForApplicationConfig(ctx context.Context, appConfig client.Object) []reconcile.Request {
	// NOTE:
	// This mapping is prone to the issue that is described in:
	//
	//   - https://github.com/kubernetes-sigs/controller-runtime/issues/1996.
	//
	// Since the handler.MapFunc does not return an error, any possible error
	// is ignored and does not lead to request requeue. As a result, the
	// controller fails to detect the changes in ApplicationConfig resources and
	// will not reconcile its changes.
	//
	// This is a known issue with controller-runtime and other controllers are
	// prone to it, for example:
	//
	//   - https://github.com/Kong/kubernetes-ingress-controller/blob/main/internal/controllers/gateway/gateway_controller.go#L280
	//
	// This needs to be revisited again once there is a viable solution for it
	// is available.
	result := []reconcile.Request{}

	appServers := &v2alpha2.ApplicationServerList{}
	listOpts := &client.ListOptions{Namespace: appConfig.GetNamespace()}

	if err := framework.Client.List(ctx, appServers, listOpts); err != nil {
		return result
	}

	for _, appServer := range appServers.Items {
		if appServer.Spec.ApplicationConfigRef.Name == appConfig.GetName() {
			result = append(result, reconcile.Request{
				NamespacedName: types.NamespacedName{
					Namespace: appServer.GetNamespace(),
					Name:      appServer.GetName(),
				}})
		}
	}

	return result
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	b := ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.ApplicationServer{}).
		Watches(
			&v2alpha2.ApplicationConfig{},
			handler.EnqueueRequestsFromMapFunc(listApplicationServersForApplicationConfig)).
		Owns(&corev1.Secret{}).
		Owns(&corev1.Service{}).
		Owns(&appsv1.Deployment{}).
		WithEventFilter(predicate.Or(predicate.GenerationChangedPredicate{}, predicate.LabelChangedPredicate{}, predicate.AnnotationChangedPredicate{}))

	if slices.Contains(framework.APIResources, "monitoring.coreos.com/v1/ServiceMonitor") {
		b.Owns(&monitoringv1.ServiceMonitor{})
	}

	return b.Complete(reconcile.AsReconciler(mgr.GetClient(), r))
}
