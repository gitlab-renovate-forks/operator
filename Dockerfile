ARG GO_VERSION=1.24

FROM scratch AS build-cache

COPY .go/pkg/mod /go/pkg/mod/
ARG GO_VERSION

FROM golang:${GO_VERSION} AS builder

ARG TARGETOS
ARG TARGETARCH
ARG VERSION

ENV GOPATH=/go

WORKDIR /workspace

RUN --mount=type=bind,target=. \
    --mount=type=cache,target=${GOPATH}/pkg/mod/,from=build-cache \
    go mod download -x && \
    CGO_ENABLED=0 GOOS=${TARGETOS:-linux} GOARCH=${TARGETARCH} \
    go build -ldflags "-X main.Version=${VERSION}" -o /.build/ ./cmd/manager

FROM registry.access.redhat.com/ubi9/ubi-micro:9.5

LABEL name=gitlab-operator-v2 \
      vendor='GitLab, Inc.' \
      description='Kubernetes Operator for managing lifecycle of GitLab components.' \
      summary='GitLab is the most comprehensive AI-powered DevSecOps Platform.' \
      source='https://gitlab.com/gitlab-org/cloud-native/operator/' \
      maintainer='GitLab Distribution Team'

WORKDIR /

COPY --from=builder /.build/manager /usr/bin/

ARG USER_UID=1001

USER ${USER_UID}

ENTRYPOINT ["/usr/bin/manager"]
