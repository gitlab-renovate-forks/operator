package secret

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("PrivateKeyGenerator", func() {
	Context("with invalid parameters", func() {
		It("should return error for unknown algorithm", func() {
			generator := &PrivateKeyGenerator{
				Algorithm: "Unknown",
				Size:      0,
				Names:     []string{"key"},
			}

			_, err := generator.Generate()

			Expect(err).To(MatchError(ErrInvalidAlgorithm))
		})

		DescribeTable("should return error for invalid algorithm key size",
			func(algorithm Algorithm, invalidSize int) {
				generator := &PrivateKeyGenerator{
					Algorithm: algorithm,
					Size:      invalidSize,
					Names:     []string{"key"},
				}

				_, err := generator.Generate()

				Expect(err).To(MatchError(ErrInvalidAlgorithmSize))
			},
			Entry("RSA", RSA, 1024),
			Entry("ECDSA", ECDSA, 224))
	})

	Context("with valid parameters", func() {
		It("should return empty payload for empty names", func() {
			generator := &PrivateKeyGenerator{
				Algorithm: RSA,
				Size:      2048,
				Names:     []string{},
			}

			payload, err := generator.Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(payload).To(BeEmpty())
		})

		DescribeTable("should generate valid private keys",
			func(algorithm Algorithm, size int, keyType any) {
				generator := &PrivateKeyGenerator{
					Algorithm: algorithm,
					Size:      size,
					Names:     []string{"key"},
				}

				payload, err := generator.Generate()

				Expect(err).NotTo(HaveOccurred())
				Expect(payload).To(HaveKey("key"))
				Expect(payload["key"]).NotTo(BeEmpty())

				// Verify the generated key is in valid PEM format
				block, _ := pem.Decode(payload["key"])
				Expect(block).NotTo(BeNil())
				Expect(block.Type).To(Equal("PRIVATE KEY"))

				key, err := x509.ParsePKCS8PrivateKey(block.Bytes)
				Expect(err).NotTo(HaveOccurred())
				Expect(key).To(BeAssignableToTypeOf(keyType))
			},
			Entry("RSA", RSA, 2048, &rsa.PrivateKey{}),
			Entry("ECDSA", ECDSA, 256, &ecdsa.PrivateKey{}),
			Entry("ED25519", ED25519, 0, ed25519.PrivateKey{}))

		It("should generate multiple keys when multiple names are provided", func() {
			generator := &PrivateKeyGenerator{
				Algorithm: RSA,
				Size:      2048,
				Names:     []string{"key1", "key2", "key3"},
			}

			payload, err := generator.Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(payload).To(HaveLen(3))
			Expect(payload).To(HaveKey("key1"))
			Expect(payload).To(HaveKey("key2"))
			Expect(payload).To(HaveKey("key3"))
		})
	})
})
