package templateutils

import (
	"text/template"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

func FuncMap() template.FuncMap {
	return funcMap
}

var (
	funcMap = template.FuncMap{
		/* Volumes */
		"populateTLSSupport": populateTLSSupport,

		/* PostgreSQL configuration for Rails applications */
		"mapPostgreSQLDatabases": mapPostgreSQLDatabases,
		"postgresqlSSLMode":      postgresqlSSLMode,
		"mapPostgreSQLSettings": func(in map[string]string) map[string]any {
			return postgresqlSettingsMapper.mapStrictly(in)
		},

		/* Redis configuration for Rails applications */
		"getRedisConfigFileName":  getRedisConfigFileName,
		"mapRedisConfigFileNames": mapRedisConfigFileNames,
		"mapRedisSettings": func(in map[string]string) map[string]any {
			return redisSettingsMapper.mapStrictly(in)
		},

		/* Rails configuration */
		"maxRequestDuration": maxRequestDuration,
		"mapCSPDirectives": func(in map[string]string) map[string]any {
			return cspDirectivesMapper.mapKeys(strMapToAnyMap(in))
		},
		"mapRailsSettings": func(in map[string]string) map[string]any {
			return railsSettingsMapper.mapStrictly(in)
		},
		"mapDefaultProjectFeatures": func(in map[string]bool) map[string]any {
			return defaultProjectFeaturesMapper.mapKeysStrictly(boolMapToAnyMap(in))
		},

		/* SMTP */
		"mapSMTPSettings": func(in map[string]string) map[string]any {
			return smtpSettingsMapper.mapStrictly(in)
		},

		/* IMAP */
		"mapIMAPSettings": func(in map[string]string) map[string]any {
			return imapSettingsMapper.mapStrictly(in)
		},

		/* Object Storage */
		"mapDestinationStores": func(in v2alpha2.ApplicationObjectStorage) map[v2alpha2.ObjectStoreUsage]v2alpha2.ObjectStore {
			return mapDestinationStores(in)
		},

		/* LDAP configuration */
		"mapLDAPSettings": func(in map[string]string) map[string]any {
			return ldapSettingsMapper.mapStrictly(in)
		},
		"mapLDAPAttributes": func(in map[string]string) map[string]any {
			return ldapAttributesMapper.mapStrictly(in)
		},

		/* OmniAuth configuration */
		"mapOmniAuthSettings": func(in map[string]string) map[string]any {
			return omniauthSettingsMapper.mapStrictly(in)
		},

		/* Workhorse */
		"getWorkhorseObjectStore": getWorkhorseObjectStore,
		"getWorkhorseRedis":       getWorkhorseRedis,
	}
)
