package manifests

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ framework.ContextualValue[client.Object] = &AugmentedInventory[client.Object]{}

// AugmentedInventory enhances its wrapped Inventory by applying a series of
// ObjectProcessors to each object retrieved from the wrapped Inventory.
//
// It implements the Inventory interface, allowing it to be used as a standard
// Inventory.
type AugmentedInventory[T client.Object] struct {
	Inventory        framework.ContextualValue[T]
	ObjectProcessors []ObjectProcessor[T]
}

func (i *AugmentedInventory[T]) Get(rtCtx *framework.RuntimeContext) ([]T, error) {
	objects, err := i.Inventory.Get(rtCtx)
	if err != nil {
		return nil, err
	}

	for _, obj := range objects {
		for _, proc := range i.ObjectProcessors {
			if err := proc(obj); err != nil {
				return nil, err
			}
		}
	}

	return objects, nil
}
