package framework

type Condition interface {
	Evaluate(*RuntimeContext) (bool, error)
}

type ConditionFunc func(*RuntimeContext) (bool, error)

func (f ConditionFunc) Evaluate(ctx *RuntimeContext) (bool, error) {
	return f(ctx)
}

var (
	Always ConditionFunc = func(_ *RuntimeContext) (bool, error) { return true, nil }
	Never  ConditionFunc = func(_ *RuntimeContext) (bool, error) { return false, nil }
)

// Guard is a Middleware that aborts a Task if it's Condition evaluated to false.
type Guard struct {
	Condition Condition
}

func (m *Guard) Handle(ctx *RuntimeContext, value any, _ error) (any, error) {
	if m.Condition == nil {
		return value, nil
	}

	outcome, err := m.Condition.Evaluate(ctx)

	if err != nil {
		return value, err
	}

	if !outcome {
		return value, ErrAbortTask
	}

	return value, nil
}

// Register is a Middleware that stores its input value in the runtime context
// using the given key and tags.
type Register struct {
	Name string
	Tags []string
}

func (m *Register) Handle(ctx *RuntimeContext, value any, _ error) (any, error) {
	if m.Name != "" {
		ctx.State.Store(m.Name, value, m.Tags...)
	}

	return value, nil
}
