package secret

import (
	"encoding/pem"

	"golang.org/x/crypto/ssh"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("SSHHostKeysGenerator", func() {
	It("should generate valid private host keys for all algorithms", func() {
		generator := &SSHHostKeysGenerator{
			Algorithms: []Algorithm{RSA, ECDSA, ED25519},
		}

		result, err := generator.Generate()
		Expect(err).NotTo(HaveOccurred())

		algorithms := []string{"rsa", "ecdsa", "ed25519"}
		for _, alg := range algorithms {
			sshPrivateKey := result[alg]
			sshPublicKey := result[alg+".pub"]

			Expect(sshPrivateKey).NotTo(BeEmpty())
			Expect(sshPublicKey).NotTo(BeEmpty())

			block, _ := pem.Decode(sshPrivateKey)
			Expect(block).NotTo(BeNil())
			Expect(block.Type).To(Equal("OPENSSH PRIVATE KEY"))

			privateKey, err := ssh.ParsePrivateKey(sshPrivateKey)
			Expect(err).NotTo(HaveOccurred())
			Expect(privateKey.PublicKey().Type()).To(ContainSubstring(alg))

			Expect(string(sshPublicKey)).To(ContainSubstring(alg))
			Expect(ssh.ParseAuthorizedKey(sshPublicKey)).Error().NotTo(HaveOccurred())
		}
	})
})
