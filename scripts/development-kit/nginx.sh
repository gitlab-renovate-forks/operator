#!/usr/bin/env bash

set -e

SCRIPT_DIR=$(dirname "$0")
EXTERNAL_SSH_PORT=${EXTERNAL_SSH_PORT:-32022}
LOAD_BALANCER_IP=${LOAD_BALANCER_IP:-}

source $SCRIPT_DIR/lib/helper.sh

check_kube_context() {
    current_context=$(kubectl config current-context)
    if [[ "$current_context" == kind-* ]]; then
        echo "kind"
    elif [[ "$current_context" == "orbstack" ]]; then
        echo "orbstack"
    elif [[ "$current_context" == gke_* ]]; then
        echo "gke"
    else
        echo "Unknown"
        exit 1
    fi
}

install() {
    kubectx=$(check_kube_context)

    echo_stage "Installing NGINX Ingress Controller for ${kubectx} cluster"

    case "$kubectx" in
    "kind")
        helm upgrade --install ingress-nginx ingress-nginx \
            --repo https://kubernetes.github.io/ingress-nginx \
            --namespace ingress-nginx --create-namespace \
            --set tcp\.${EXTERNAL_SSH_PORT}=$(namespace)/$(release)-gitlab-shell:22 \
            --set controller.hostPort.enabled=true \
            --set controller.service.type=NodePort
        ;;
    "orbstack")
        helm upgrade --install ingress-nginx ingress-nginx \
            --repo https://kubernetes.github.io/ingress-nginx \
            --namespace ingress-nginx --create-namespace \
            --set tcp\.${EXTERNAL_SSH_PORT}=$(namespace)/$(release)-gitlab-shell:22
        ;;
    "gke")
        if [[ "$LOAD_BALANCER_IP" != "" ]]; then
            echo_stage "using provided LOAD_BALANCER_IP: ${LOAD_BALANCER_IP}"
        else
            echo_stage "A random IP address will be assigned to the LoadBalancer service, please check the IP address after the installation"
        fi
        helm upgrade --install ingress-nginx ingress-nginx \
            --repo https://kubernetes.github.io/ingress-nginx \
            --namespace ingress-nginx --create-namespace \
            --set tcp\.${EXTERNAL_SSH_PORT}=$(namespace)/$(release)-gitlab-shell:22 \
            --set controller.service.loadBalancerIP=${LOAD_BALANCER_IP}
        ;;
    *)
        echo_stage "Unknown kubectl context"
        exit 1
        ;;
    esac

    echo_stage "${EXTERNAL_SSH_PORT} port will be redirected to $(namespace)/$(release)-gitlab-shell:22"

    echo_stage "Setting NGINX Ingress Controller as the default IngressClass"

    kubectl annotate ingressclasses nginx 'ingressclass.kubernetes.io/is-default-class'='true'
}

uninstall() {
    echo_stage "Uninstalling NGINX Ingress Controller"

    helm uninstall -n ingress-nginx ingress-nginx
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    echo "Environment variables:"
    echo "  EXTERNAL_SSH_PORT: External SSH port, defaults to 32022"
    echo "  LOAD_BALANCER_IP: LoadBalancer IP address for GKE cluster"
    echo "Note:"
    echo "  It supports kind, orbstack and GKE clusters"
    echo "  For GKE cluster, set LOAD_BALANCER_IP to the IP address of the LoadBalancer service"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
