package databasemigration

import (
	"context"
	"log/slog"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

var _ = Describe("DatabaseMigration Manifest", func() {
	DescribeTableSubtree("Object Templates", func(scenario string) {
		var data map[string]any
		ctx, _ := framework.NewRuntimeContext(context.TODO())
		var renderResults []client.Object
		var expected map[string]client.Object

		// prepare fixtures
		BeforeEach(func() {
			data = map[string]any{
				"DatabaseMigration": testing.ReadObject(scenario, "given/database-migration.yaml"),
				"ApplicationConfig": testing.ReadObject(scenario, "given/application-config.yaml"),
				// ?? we need a helper function to generate those.
				"RedisSecrets": map[string]any{
					"resque": testing.ReadObject(scenario, "given/sample-basic-auth-secret.yaml"),
					"cable":  testing.ReadObject(scenario, "given/sample-basic-auth-secret.yaml"),
				},
				"RedisEndpoints": redisServiceEndpoints,
				"PostgreSQLSecrets": map[string]any{
					"main": testing.ReadObject(scenario, "given/sample-basic-auth-secret.yaml"),
				},
				"PostgreSQLEndpoints": postgresServiceEndpoints,
				"RepositoryTokens": map[string]any{
					"gitlab": testing.ReadObject(scenario, "given/sample-token-secret.yaml"),
				},
				"RepositoryEndpoints": gitalyServiceEndpoints,
				"Fingerprint":         "TEST-FINGERPRINT",
				"Settings":            settings.Get(),
			}

			expected = map[string]client.Object{
				"Job":    testing.ReadObject(scenario, "expected/job.yaml"),
				"Secret": testing.ReadObject(scenario, "expected/secret.yaml"),
			}
		})

		// render objects
		JustBeforeEach(func() {
			var err error
			workloadInv := &inventory.TemplatedObjects{
				Pattern: "database-migration/*.yaml",
				Values:  data,
			}

			renderResults, err = workloadInv.Get(ctx)
			Expect(err).ToNot(HaveOccurred())

			configInv := &inventory.TemplatedObjects{
				Pattern: "database-migration/config/*.yaml",
				Values:  data,
			}
			configResults, err := configInv.Get(ctx)
			Expect(err).ToNot(HaveOccurred())

			renderResults = append(renderResults, configResults...)
		})

		It("Should render the Job, which matches the expected manifest", func() {
			By("Checking the rendered objects")
			for _, obj := range renderResults {
				slog.Debug(testing.DumpObject(obj))
			}
			// ?? improve the matcher
			Expect(renderResults).To(SatisfyAll(
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind(expected["Job"].GetObjectKind().GroupVersionKind().Kind),
					testing.HaveName(expected["Job"].GetName()),
				)),
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind(expected["Secret"].GetObjectKind().GroupVersionKind().Kind),
					testing.HaveName(expected["Secret"].GetName()),
				)),
			))
		})
	},
		Entry("bare minimum configuration", "simple"),
	)
})

var (
	redisServiceEndpoints = map[string]apiutils.ServiceEndpoint{
		"resque": {
			Scheme: "redis",
			Host:   "example.com",
			Port:   6379,
		},
		"cable": {
			Scheme: "redis",
			Host:   "example.com",
			Port:   6379,
		},
	}

	postgresServiceEndpoints = map[string]apiutils.ServiceEndpoint{
		"main": {
			Scheme: "postgres",
			Host:   "example.com",
			Port:   5432,
		},
	}

	gitalyServiceEndpoints = map[string]apiutils.ServiceEndpoint{
		"default": {
			Scheme: "tcp",
			Host:   "example.com",
			Port:   8075,
		},
	}
)
