package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type KubernetesAgentServerSpec struct {
	// Version is the target version of the KAS. This should usually match the GitLab version.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Pattern=^([1-9]\d?)\.(0|[1-9]\d?)\.(0|[1-9]\d?)$
	Version string `json:"version"`

	// ExternalURL is the URL KAS will be available as.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:XValidation:rule="isURL(self)",message="must be an valid URL"
	// +kubebuilder:example="https://kas.example.com"
	ExternalURL string `json:"externalUrl"`

	// +kubebuilder:validation:Required
	ApplicationServer KASApplicationServerProvider `json:"applicationServer"`

	// +kubebuilder:validation:Required
	Redis Redis `json:"redis"`

	// +optional
	Workload WorkloadSpec `json:"workload,omitempty"`

	// +optional
	// Number of desired pods. Defaults to 1 if not specified.
	Replicas *int32 `json:"replicas,omitempty"`

	// +optional
	// Paused will stop the rollout of new KAS Pods.
	Paused bool `json:"paused,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.service) != has(self.external)",message="exactly one of service or external provider must be set"
type KASApplicationServerProvider struct {
	//+ optional
	Service *corev1.ObjectReference `json:"service,omitempty"`

	//+ optional
	External KASExternalApplicationServer `json:"external,omitempty"`
}

type KASExternalApplicationServer struct {
	// ExternalURL is the external GitLab/ApplicationServer URL.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:XValidation:rule="isURL(self)",message="must be an valid URL"
	// +kubebuilder:example="https://gitlab.example.com"
	ExternalURL string `json:"externalUrl"`

	// InternalURL is the interhal GitLab/ApplicationServer URL pointing to workhorse.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:XValidation:rule="isURL(self)",message="must be an valid URL"
	// +kubebuilder:example="http://gitlab-application-server.svc.cluster.local:8181"
	InternalURL string `json:"internalUrl"`
}

type KubernetesAgentServerStatus struct {
	// +optional
	Replicas int32 `json:"replicas,omitempty"`

	// +optional
	Selector string `json:"selector,omitempty"`

	// +optional
	// +patchMergeKey=usage
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	SharedObjects []SharedObjectReference `json:"sharedObjects,omitempty" patchStrategy:"merge" patchMergeKey:"usage"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:resource:shortName=kas
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=`.spec.version`
// +kubebuilder:printcolumn:name="External URL",type=string,JSONPath=`.spec.externalUrl`
// +kubebuilder:printcolumn:name="Available",type=integer,JSONPath=`.status.replicas`
type KubernetesAgentServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   KubernetesAgentServerSpec   `json:"spec,omitempty"`
	Status KubernetesAgentServerStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type KubernetesAgentServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []KubernetesAgentServer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&KubernetesAgentServer{}, &KubernetesAgentServerList{})
}
