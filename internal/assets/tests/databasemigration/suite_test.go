package databasemigration

import (
	"log/slog"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	libtesting "gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

func TestDatabaseMigrationTemplates(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: DatabaseMigration Templates")
}

var _ = BeforeSuite(func() {
	slog.SetDefault(slog.New(&libtesting.GinkgoHandler{}))
	slog.SetLogLoggerLevel(slog.LevelDebug)

	fakeScheme, err := libtesting.NewScheme(monitoringv1.AddToScheme)
	Expect(err).ToNot(HaveOccurred())

	framework.Scheme = fakeScheme
})

var _ = AfterSuite(func() {
	framework.Scheme = nil
	framework.Client = nil
})
