{{- $spec := .JobProcessor.Spec -}}

image: {{ getImageName $spec.Workload.ImageSource "Sidekiq" $spec.Version $spec.Edition }}
ports:
  {{- if not $spec.Sidekiq.Metrics.Disabled }}
  - containerPort: {{ .Settings.Rails.Sidekiq.MetricsPort }}
    name: metrics
    protocol: "TCP"
  {{- end }}
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: ENABLE_BOOTSNAP
    value: {{ $spec.Sidekiq.Settings.EnableBootsnap | default .Settings.Rails.EnableBootsnap }}
  - name: SIDEKIQ_CONCURRENCY
    value: {{ $spec.Sidekiq.Settings.Concurrency | default .Settings.Rails.Sidekiq.Concurrency | quote }}
  - name: SIDEKIQ_TIMEOUT
    value: {{ $spec.Sidekiq.Settings.Timeout | default .Settings.Rails.Sidekiq.Timeout | toSeconds | quote }}
  - name: SIDEKIQ_QUEUES
    value: {{ $spec.Sidekiq.Settings.Queues | default (list) | join "," | quote }}
  {{- include "jobprocessor/_memorykiller.tpl" (dict "Spec" $spec.Sidekiq.MemoryKiller "Settings" .Settings.Rails.Sidekiq.MemoryKiller) | nindent 2 }}
  {{- include "commons/_tracingVariables.tpl" $spec | nindent 2 }}
volumeMounts:
  {{- include "jobprocessor/_railsVolumeMount.tpl" . | nindent 2 }}
lifecycle:
  preStop:
    exec:
      command: ["/bin/bash", "-c", "pkill -f 'sidekiq'"]
livenessProbe:
  exec:
    command: ["curl", "-sfS", "http://localhost:{{ .Settings.Rails.Sidekiq.HealthcheckPort }}/liveness"]
  initialDelaySeconds: 20
  periodSeconds: 60
  timeoutSeconds: 30
  successThreshold: 1
  failureThreshold: 3
readinessProbe:
  exec:
    command: ["curl", "-sfS", "http://localhost:{{ .Settings.Rails.Sidekiq.HealthcheckPort }}/readiness"]
  initialDelaySeconds: 0
  periodSeconds: 10
  timeoutSeconds: 2
  successThreshold: 1
  failureThreshold: 3

{{- include "commons/_defaultResources.tpl" .Settings.Rails.Sidekiq }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}
