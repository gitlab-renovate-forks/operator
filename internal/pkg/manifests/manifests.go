package manifests

import (
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ObjectFactory[P any, T client.Object] func(P) (T, error)
type ObjectProcessor[T client.Object] func(T) error

// Factory returns a function that creates a client.Object from the given parameters.
func Factory[P any, T client.Object](
	factory ObjectFactory[P, T],
	procs ...ObjectProcessor[T],
) func(P) (client.Object, error) {
	return func(params P) (client.Object, error) {
		obj, err := factory(params)
		if err != nil {
			return obj, err
		}

		for _, proc := range procs {
			if err := proc(obj); err != nil {
				return obj, err
			}
		}

		return obj, nil
	}
}

// ManifestFactory is a collection of [ObjectFactory] that build client.Objects from parameters.
type ManifestFactory[P any] []func(P) (client.Object, error)

func (f ManifestFactory[P]) Build(params P, procs ...ObjectProcessor[client.Object]) ([]client.Object, error) {
	manifests := []client.Object{}

	for _, builder := range f {
		obj, err := builder(params)
		if err != nil {
			return manifests, err
		}

		for _, proc := range procs {
			if err := proc(obj); err != nil {
				return manifests, err
			}
		}

		manifests = append(manifests, obj)
	}

	return manifests, nil
}
