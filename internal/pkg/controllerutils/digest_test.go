package controllerutils

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("Digest", func() {
	var rtCtx *framework.RuntimeContext
	var objects []client.Object

	ExpectDigestLength := func(digest string, length int) {
		Expect(digest).To(HaveLen(length / 4)) // sha uses a hexadecimal representation
	}

	BeforeEach(func() {
		rtCtx = &framework.RuntimeContext{}
	})

	When("typed objects are used", func() {
		BeforeEach(func() {
			objects = []client.Object{
				&corev1.ConfigMap{
					Data: map[string]string{"key": "config"},
				},
				&corev1.Secret{
					Data: map[string][]byte{"key": []byte("secret")},
				},
			}
		})

		It("Calculates a valid SHA256 sum", func() {
			digest, err := Digest(rtCtx, objects)

			Expect(err).ToNot(HaveOccurred())
			ExpectDigestLength(digest, 256)
		})

		It("Registers a valid SHA512 sum", func() {
			digest, err := Digest(rtCtx, objects, WithHash(sha512.New()))

			Expect(err).ToNot(HaveOccurred())
			ExpectDigestLength(digest, 512)
		})
	})

	When("Unstructured objects are used", func() {
		It("Calculates a valid SHA256 sum", func() {
			objects := []client.Object{
				&unstructured.Unstructured{
					Object: map[string]interface{}{
						"apiVersion": "corev1",
						"kind":       "ConfigMap",
						"data":       map[string]string{"key": "config"},
					}},
				&unstructured.Unstructured{
					Object: map[string]interface{}{
						"apiVersion": "corev1",
						"kind":       "Secret",
						"data":       map[string][]byte{"key": []byte("secret")},
					}},
			}

			digest, err := Digest(rtCtx, objects)
			Expect(err).ToNot(HaveOccurred())
			ExpectDigestLength(digest, 256)

			emptySha := hex.EncodeToString(sha256.New().Sum(nil))
			Expect(digest).ToNot(Equal(emptySha))
		})
	})
})
