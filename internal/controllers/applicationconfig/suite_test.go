package applicationconfig

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestApplicationConfigController(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: ApplicationConfig Controller")
}
