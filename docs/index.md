---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GitLab Operator V2

NOTE:
GitLab Operator V2 is in early development stage and is not yet suitable for
production use.

GitLab Operator V2 represents a significant rewrite and refactor of the existing
[GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator).
Its aim is to improve the overall user experience for provisioning and managing
cloud-native GitLab applications on Kubernetes and OpenShift.

## Installation

Check the available released on the [releases page](https://gitlab.com/gitlab-org/cloud-native/operator/-/releases)
and apply the manifest of the release you want to install:

```bash
VERSION=<Target Version>
kubectl apply -f "https://gitlab.com/api/v4/projects/56933558/packages/generic/manifests/${VERSION}/manifest.yaml"
```

## Installing stateful components

GitLab Operator V2 does not support installing the stateful components such as
PostgreSQL, Redis, Gitaly or a object storage backend.

Use a [Cloud native hybrid reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/#cloud-native-hybrid)
to setup these components.

For testing and proof of concept environments you can use the [the development kit](development/index.html)
to setup the component in the same namespace.

## Installing GitLab

After you installed the stateful components you can create the Operator-mananged
custom resources with [kustomize](https://github.com/kubernetes-sigs/kustomize).

1. Create a copy of [kustomization.yaml](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/cr/kustomization.yaml).
1. Change the `resources` section to use your Operator version as `ref` and change
   the overlay to one of:

   - `min` for a minimal GitLab instance,
   - `ref-3k` for a [3k CNH reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative),
   - `ref-5k` for a [5k CNH reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/5k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative),
   - `ref-10k` for a [10k CNH reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/10k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative),
   - `ref-25k` for a [25k CNH reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/25k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative),
   - `ref-50k` for a [50k CNH reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/50k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative).

1. Adapt the `ApplicationConfig` patch to use your external URLs and references to
   external resources.
1. Apply your kustomize resources:

   ```shell
   kubectl patch -k <KUSTOMIZE_DIRECTORY>
   ```

1. Remove the default `replacement` and `install` component from your kustomization.yaml.
