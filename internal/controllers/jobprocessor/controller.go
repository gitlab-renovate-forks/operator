package jobprocessor

import (
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var Settings = settings.Get()

type Controller struct {
	jobProcessor       *v2alpha2.JobProcessor
	appConfig          *v2alpha2.ApplicationConfig
	appConfigResources *appconfigutils.ApplicationConfigResources
	railsSecrets       *corev1.Secret

	provisionWorkflow framework.Workflow
	statusWorkflow    framework.Workflow
	scaleWorkflow     framework.Workflow
}

func NewController(jobProcessor *v2alpha2.JobProcessor) *Controller {
	c := &Controller{
		jobProcessor: jobProcessor,
	}

	c.provisionWorkflow = framework.Workflow{
		framework.TaskFunc(c.dereferenceAppConfig),
		framework.TaskFunc(c.dereferenceResources),
		framework.TaskFunc(c.provisionWorkload),
	}

	c.statusWorkflow = framework.Workflow{
		framework.TaskFunc(c.updateStatus),
		framework.TaskFunc(c.checkPausedCondition),
		framework.TaskFunc(c.checkReadyCondition),
	}

	c.scaleWorkflow = framework.Workflow{
		framework.TaskFunc(c.scale),
		framework.TaskFunc(c.updateScaleStatus),
	}

	return c
}

func (c *Controller) Execute(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("Reconciling resource")

	rtCtx.Logger.Debug("Running the main workflow")
	err := framework.Workflow{
		c.provisionWorkflow,
		c.statusWorkflow,
	}.Execute(rtCtx)

	// Try scale workflow regardless of previous errors.
	// This workflow is only attempted when the managed Deployment exists.
	rtCtx.Logger.Debug("Attempting scale workflow")
	scaleErr := c.scaleWorkflow.Execute(rtCtx)

	if err != nil {
		return err
	}

	return scaleErr
}
