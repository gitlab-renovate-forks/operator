{{- $ := . -}}
{{- $spec := .DatabaseMigration.Spec -}}
{{- $initRootPasswordSecretKey := empty $spec.InitialRootPassword | ternary "password" (digStruct $spec.InitialRootPassword "Key") -}}
image: {{ getImageName $spec.Workload.ImageSource "Migrations" $spec.Version $spec.Edition }}
args:
  - /scripts/wait-for-deps
  - /scripts/db-migrate
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: BYPASS_SCHEMA_VERSION
    value: "true"
  - name: ENABLE_BOOTSNAP
    value: "1"
  {{- if $spec.SkipPostMigrations }}
  - name: SKIP_POST_DEPLOYMENT_MIGRATIONS
    value: "true"
  {{- end }}

volumeMounts:
  - name: secrets
    mountPath: {{ .Settings.Secrets.MountPath }}
  {{- $cfgEntries := list "gitlab.yml" "database.yml" "INSTALLATION_TYPE" -}}

  {{- with .ApplicationConfig.Spec.Redis -}}
  {{- $cfgEntries = concat $cfgEntries (mapRedisConfigFileNames . | values) -}}
  {{- end -}}

  {{- if .ApplicationConfig.Spec.ClickHouse -}}
  {{- $cfgEntries = append $cfgEntries "click_house.yml" -}}
  {{- end -}}

  {{- range $cfgEntries  }}
  - name: migration-config
    mountPath: {{ $.Settings.Rails.ConfigDir }}/{{ . }}
    subPath: {{ . }}
  {{- end }}
  - mountPath: {{ $.Settings.Rails.ConfigDir }}/secrets.yml
    name: rails-secrets
    subPath: secrets.yml
  {{- if not $spec.IsUpgrade }}
  - mountPath: {{ $.Settings.Rails.ConfigDir }}/initial_root_password
    name: initial-password
    subPath: {{ $initRootPasswordSecretKey }}
  {{- end }}
  {{- if $spec.License }}
  - mountPath: {{ $.Settings.Rails.ConfigDir }}/enterprise_license
    name: license
    subPath: {{ $spec.License.Key }}
  {{- end }}

{{- include "commons/_defaultResources.tpl" .Settings.Rails }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}