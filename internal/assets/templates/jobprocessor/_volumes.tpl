{{- $spec := .JobProcessor.Spec -}}
{{- $appConfig := .ApplicationConfig.Spec -}}

{{- $railsSecretRef := findStatusSharedObjects .ApplicationConfig.Status.SharedObjects "RailsSecret" -}}
{{- $railsSecretName :=  empty $railsSecretRef | ternary (printf "%s-rails-secrets" .ApplicationConfig.Name) (digStruct $railsSecretRef "Name") -}}

{{/*
List of Volumes:

  - "rails-secrets": GitLab Rails secrets that ApplicationConfig controller
    generates. It contains `secret.yml` entry. It is referenced from the Status
    sub-resource of the associated ApplicationConfig. The fall back value of
    `<name>-rails-secrets` is for test purposes only.

  - "rails-config": A Secret that contains Rails application configuration, such
    as `gitlab.yml`, `database.yml``, etc. JobProcessor controller produces
    this Secret and its keys are predefined.

  - "secrets": An all-in-one volume for projecting various Secrets that the Rails
    application references as files (as opposed to incorporating
    the Secret values directly into the configuration). For more details, see
    the documentation below.

  - "rails-metrics": An empty memory-backed volume to maintain Rails metrics.

NOTE: 
  - The following volumes are not supported:
      1. SSH host keys, i.e. `/etc/ssh/ssh_host_*.pub`
      2. Kerberos config, i.e. `/etc/krb5.conf`

    User should be advised to use ExtraVolumes and ExtraVolumeMounts.
*/}}

- name: rails-secrets
  secret:
    secretName: {{ $railsSecretName }}
    defaultMode: 0400
- name: rails-config
  secret:
    secretName: {{ .JobProcessor.Name }}-rails-config-jobproc
    defaultMode: 0400
- name: rails-metrics
  emptyDir:
    medium: Memory

{{/*
The all-in-one projected `secrets` volume include secret tokens, TLS keys and
certificates, and other shared secrets. With a few exceptions, most of these
secrets are optional.

| Path                       | TLS Files                       | Description                                         |
|----------------------------|---------------------------------|-----------------------------------------------------|
| incoming-mail-secret       | Mailroom webhook secret file    | Mailroom uses it to authenticate to webhook. Only   |
|                            |                                 | used when incoming mail delivery method is webhook. |
| service-desk-secret        | Mailroom webhook secret file    | Mailroom uses it to authenticate to webhook. Only   |
|                            |                                 | used when service desk delivery method is webhook.  |
| kas-secret                 | KAS secret file                 | KAS uses it to authenticate. ApplicationServer owns |
|                            |                                 | the Secret.                                         |
| container-registry/        | Private key of registry         | Used to authenticate login requests. Container      |
|   auth.key                 |                                 | registry owns the Secret. ApplicationServer         |
|                            |                                 | references it.                                      |
| suggested-reviewers/secret | Suggested reviewers token file  | Used to authenticate to suggested reviewer service. |
| zoekt/{username,password}  | Zoekt basic auth files          | Used to authenticate to Zoekt service.              |
| kerberos/keytab            | Kerberos keytab file            | Mounted on a fixed location, `/etc/krb5.keytab`.    |
| ciscoauth/                   | Duo auth keys                 | Used to enable Cisco Duo authentication.            |
|   {integrationkey,secretkey} |                               |                                                     |

Note:
  [*] Required

This volume is mounted on `/run/secrets/gitlab.io`. Use the full path of the
secrets in configuration files, for example `/run/secrets/gitlab.io/workhorse-secret`.
*/}}

- name: secrets
  projected:
    defaultMode: 0400
    sources:
    {{- with $appConfig.IncomingEmail }}
      {{- if eq (.DeliveryMethod | toString) "Postback" }}
      - secret:
        - name: {{ .JobProcessor.Name }}-app-secrets
          items:
            - key: incoming-mail
              path: incoming-mail-secret
      {{- end }}
    {{- end }}
    {{- with $appConfig.ServiceDeskEmail }}
      {{- if eq (.DeliveryMethod | toString) "Postback" }}
      - secret:
        - name: {{ .JobProcessor.Name }}-app-secrets
          items:
            - key: service-desk
              path: service-desk-secret
      {{- end }}
    {{- end }}
    {{- with $appConfig.KAS }}
      - secret:
        - name: {{ .JobProcessor.Name }}-app-secrets
          items:
            - key: kas
              path: kas-secret
    {{- end }}
    {{- with $appConfig.ContainerRegistry }}
      - secret:
      {{- with .External }}
          name: {{ .Credentials.Name }}
          items:
            - key: {{ .Credentials.Key }}
              path: container-registry/auth.key
      {{- else }}
          {{/*  TODO: Pending ContainerRegistry resource. */}}
      {{- end }}
    {{- end }}
    {{- with $appConfig.SuggestedReviewersToken }}
      - secret:
          name: {{ .Name }}
          items:
            - key: {{ .Key }}
              path: suggested-reviewers/secret
    {{- end }}
    {{- with $appConfig.ZoektCredentials }}
      - secret:
          name: {{ .Name }}
          items:
            - key: username
              path: zoekt/username
            - key: password
              path: zoekt/password
    {{- end }}
    {{- with $appConfig.Kerberos }}
      - secret:
          name: {{ .Keytab.Name }}
          items:
            - key: {{ .Keytab.Key }}
              path: kerberos/keytab
    {{- end }}
    {{- with $appConfig.SmartCard }}
      - secret:
          name: {{ .CACertificate.Name }}
          items:
            - key: {{ .CACertificate.Key }}
              path: smartcard/ca.crt
    {{- end }}
    {{- with $appConfig.CiscoDuo }}
      {{- with .IntegrationKey }}
      - secret:
          name: {{ .Name }}
          items:
            - key: {{ .Key }}
              path: ciscoauth/integrationkey
      {{- end }}
      {{- with .SecretKey }}
      - secret:
          name: {{ .Name }}
          items:
            - key: {{ .Key }}
              path: ciscoauth/secretkey
      {{- end }}
    {{- end }}
