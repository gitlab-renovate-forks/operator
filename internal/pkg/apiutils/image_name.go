package apiutils

import (
	"fmt"
	"strings"

	"github.com/Masterminds/semver/v3"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

// GetImageName returns the image name of a container for the given version and
// edition. It uses the provided source, which contains the image flavor, or
// falls back to the default GitLab source and flavor.
//
// When the edition is not provided the default GitLab Rails edition is used.
func GetImageName(source *v2alpha2.ImageSource, container, version string, editions ...v2alpha2.Edition) string {
	r := source
	if r == nil {
		r = defaultSource.DeepCopy()
	}

	setFallbackValue(&r.Registry, defaultSource.Registry)
	setFallbackValue(&r.Repository, defaultSource.Repository)
	setFallbackValue(&r.Flavor, defaultFlavor)

	var edition *v2alpha2.Edition
	if len(editions) > 0 {
		edition = utils.Ptr(editions[0])
	}

	name := expandContainerName(strings.ToLower(container), edition)
	tag := mapVersion(version) + flavorToExt(r.Flavor)

	return fmt.Sprintf("%s/%s/%s:%s", r.Registry, r.Repository, name, tag)
}

func setFallbackValue[T string | v2alpha2.ImageFlavor](value *T, fallback T) {
	if string(*value) != "" {
		return
	}

	if string(fallback) != "" {
		*value = fallback
	}
}

func expandContainerName(container string, edition *v2alpha2.Edition) string {
	if mapped, ok := containerNameMapping[container]; ok {
		if strings.Contains(mapped, "%s") {
			editionToUse := defaultEdition
			if edition != nil && *edition != "" {
				editionToUse = *edition
			}

			return fmt.Sprintf(mapped, strings.ToLower(string(editionToUse)))
		}

		return mapped
	} else {
		return container
	}
}

func flavorToExt(flavor v2alpha2.ImageFlavor) string {
	switch flavor {
	case v2alpha2.UBI:
		return "-ubi"
	case v2alpha2.FIPS:
		return "-fips"
	default:
		return ""
	}
}

func mapVersion(version string) string {
	if _, err := semver.NewVersion(version); err == nil {
		return "v" + version
	}

	if version != "" {
		return version
	}

	return latestTag
}

const (
	latestTag = "master"
)

var (
	defaultEdition = v2alpha2.Edition(settings.Get().Rails.Edition)
	defaultFlavor  = v2alpha2.Debian
	defaultSource  = v2alpha2.ImageSource{
		Registry:   "registry.gitlab.com",
		Repository: "gitlab-org/build/cng",
		Flavor:     defaultFlavor,
	}

	containerNameMapping = map[string]string{
		"puma":       "gitlab-webservice-%s",
		"workhorse":  "gitlab-workhorse-%s",
		"sidekiq":    "gitlab-sidekiq-%s",
		"migrations": "gitlab-toolbox-%s",
	}
)
