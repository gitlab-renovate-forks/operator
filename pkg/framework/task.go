package framework

import (
	"errors"
)

// Task is the fundamental unit of work in the framework. Anything that accepts
// a RuntimeContext and performs one or more actions is a Task. A Task is not
// aware of its order of execution and its position in a sequence of Tasks must
// not change its behavior.
//
// Tasks do not return a value the same as functions do. They are inherently
// procedures. Any output that they produce must be stored in the mutable shared
// state that RuntimeContext holds. As such, Tasks use RuntimeContext to pass
// parameters to each other.
//
// Any function that accepts a RuntimeContext and returns an error is a Task.
// However, you can use Middlewares for Task composition. They provide reusable
// building blocks for Tasks.
type Task interface {
	// Execute runs the Task. It accepts a RuntimeContext and returns an error.
	// to signal that something went wrong.
	//
	// The task runner decides must decide how to handle an error.
	Execute(*RuntimeContext) error
}

// TaskFunc is a function that implements Task interface.
type TaskFunc func(*RuntimeContext) error

func (f TaskFunc) Execute(rtCtx *RuntimeContext) error {
	return f(rtCtx)
}

// Workflow is a sequence of Tasks that are executed in the specified order. It
// fails as soon as a Task fails. Workflow implements Task interface.
type Workflow []Task

// Execute sequentially runs the Tasks of the Workflow. If a Task fails it stops
// and returns the error.
func (w Workflow) Execute(ctx *RuntimeContext) error {
	for _, t := range w {
		if err := t.Execute(ctx); err != nil {
			return err
		}
	}

	return nil
}

// Middleware represents an atomic operation which either completes with an
// optional result or fails with an error.
//
// Being indivisible, Middlewares must carry out a single and very specific
// responsibility. This also makes them highly reusable. By chaining Middlewares
// we can make more complex Tasks or quickly add new features to existing Tasks.
//
// In a chain, the output of one Middleware is the input of the next. All
// Middlewares share the same RuntimeContext.
//
// This model is very similar to function composition and the pipeline pattern.
// See:
//
//   - https://en.wikipedia.org/wiki/Function_composition
//   - https://www.cise.ufl.edu/research/ParallelPatterns/PatternLanguage/AlgorithmStructure/Pipeline.htm
type Middleware interface {
	// Handle runs an operation for the Middleware. The operation must be atomic.
	// It either completes with an optional result or fails with an error.
	//
	// Middleware can return ErrAbortTask to abort the execution of the next
	// steps of a Task without failing it. Note that only next steps in the
	// are skipped. Any step that is sequenced before the middleware that aborts
	// is already executed.
	//
	// In a chain, the output of one Middleware is the input of the next and all
	// Middlewares share the same RuntimeContext.
	Handle(*RuntimeContext, any, error) (any, error)
}

// MiddlewareFunc is a function that implements Middleware interface.
type MiddlewareFunc func(*RuntimeContext, any, error) (any, error)

func (f MiddlewareFunc) Handle(rtCtx *RuntimeContext, value any, err error) (any, error) {
	return f(rtCtx, value, err)
}

// MiddlewareChain is a chain of Middlewares that are executed in the specified
// order with the same RuntimeContext. It implements Middleware interface.
type MiddlewareChain []Middleware

// Handle runs the Middlewares in the chain sequentially. The output of one
// Middleware is passed as the input of the next.
//
// When a Middleware fails by return non-nil error, the chain stops and returns
// the error. However, when the error is ErrAbortTask, it stops and returns the
// last known value and a nil error.
func (c MiddlewareChain) Handle(rtCtx *RuntimeContext, value any, err error) (any, error) {
	lastValue, lastError := value, err

	for _, m := range c {
		lastValue, lastError = m.Handle(rtCtx, lastValue, lastError)
		if lastError != nil {
			if lastError == ErrAbortTask {
				return lastValue, nil
			}

			return lastValue, lastError
		}
	}

	return lastValue, lastError
}

// ErrAbortTask is returned by a Middleware to abort the execution of the next
// steps of a Task without failing it.
var ErrAbortTask = errors.New("AbortTask")
