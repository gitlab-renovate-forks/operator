package databasemigration

import (
	"context"
	"fmt"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

type Reconciler struct{}

// +kubebuilder:rbac:groups=gitlab.com,resources=databasemigrations,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=databasemigrations/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=databasemigrations/finalizers,verbs=update
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationconfigs,verbs=get
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationconfigs/status,verbs=get
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get
// +kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.DatabaseMigration) (ctrl.Result, error) {
	logger := ctrl.Log.
		WithName("controllers.databasemigration").
		WithValues(
			"resource", fmt.Sprintf("%s/%s", obj.Namespace, obj.Name),
			"generation", obj.Generation,
			"resourceVersion", obj.ResourceVersion,
		)

	rtCtx, err := framework.NewRuntimeContext(ctx, framework.WithLogrLogger(logger))
	if err != nil {
		return framework.Expand(err)
	}

	err = NewController(obj).Execute(rtCtx)

	return framework.Expand(err)
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.DatabaseMigration{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(reconcile.AsReconciler(mgr.GetClient(), &Reconciler{}))
}
