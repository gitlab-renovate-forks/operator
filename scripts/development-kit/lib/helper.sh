#!/usr/bin/env bash

echo_stage() {
  if [[ -t 1 ]]; then
    GREEN='\033[0;32m'
    NO_COL='\033[0m' # No Color
    printf "${GREEN}$*${NO_COL}\n"
  else
    printf "$*\n"
  fi
}

gen_random() {
  head -c 4096 /dev/urandom | LC_CTYPE=C tr -cd '[:alnum:]' | head -c $1
}

# Function to read input with a default value
read_input_with_default() {
  local prompt="$1"
  local default_value="$2"
  local user_input

  if [[ -z "${CI}" ]]; then
    read -p "$prompt [$default_value]: " user_input
  fi

  # If user_input is empty, use the default value
  echo "${user_input:-$default_value}"
}

release() {
  echo -n "${RELEASE:-operator-v2-dev-kit}"
}

namespace() {
  echo -n "${NAMESPACE:-gitlab-operator-v2}"
}

get_secret_value() {
  local secret_name="${1}"
  local field_name="${2}"

  kubectl -n "$(namespace)" get secret "$(release)-${secret_name}" \
    -o jsonpath="{.data.${field_name}}" | base64 -d
}

create_basic_auth_secret() {
  local name="${1}"
  local username="${2}"
  local password="${3}"

  kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: $(release)-${name}
  namespace: $(namespace)
type: kubernetes.io/basic-auth
stringData:
  username: ${username}
  password: ${password}
EOF
}

create_basic_auth_secret_raw() {
  local name="${1}"
  local username="${2}"
  local password="${3}"

  kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: ${name}
  namespace: $(namespace)
type: kubernetes.io/basic-auth
stringData:
  username: ${username}
  password: ${password}
EOF
}

create_tls_secret() {
  local name="${1}"
  local crt="${2}"
  local key="${3}"

  kubectl create secret tls "$(release)-${name}" \
    --namespace "$(namespace)" \
    --key <(echo "$key") \
    --cert <(echo "$crt")
}
