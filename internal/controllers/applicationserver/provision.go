package applicationserver

import (
	"fmt"
	"strings"

	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
)

// Generate shared application secrets.
func (c *Controller) generateAppSecrets(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Generating application secrets")

	// Set status condition to Ready with False status and PendingApplicationSecrets reason.
	err := c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingApplicationSecretsReason,
		Message: "Preparing shared application secrets",
	})
	if err != nil {
		return err
	}

	// Generate shared application secrets.
	secrets, err := controllerutils.CreateSecret(rtCtx, c.appSecretsInventory())
	if err != nil {
		return err
	}

	for _, secret := range secrets {
		if strings.HasSuffix(secret.Name, appSecretNameSuffix) {
			c.appSecrets = secret
			break
		}
	}

	if c.appSecrets == nil {
		return fmt.Errorf("application secrets found")
	}

	return c.seSharedObject(rtCtx, v2alpha2.ApplicationSecretsObjectUsage, c.appSecrets)
}

// Create application Service resource.
func (c *Controller) createAppService(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Creating Service resource")

	//  Set status condition to Ready with False status and PendingApplicationService reason.
	err := c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingApplicationServiceReason,
		Message: "Preparing the Service resource for application server",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	appService, err := controllerutils.PatchObjects(rtCtx, c.appServiceInventory())
	if err != nil {
		return fmt.Errorf("failed to create Service resource: %w", err)
	}

	// Advertise the Service resource for other resources.
	if len(appService) != 1 {
		return fmt.Errorf("expected one and only Service for application, got %d", len(appService))
	}

	if err := c.seSharedObject(rtCtx, v2alpha2.ApplicationServiceObjectUsage, appService[0]); err != nil {
		return fmt.Errorf("failed to set shared object: %w", err)
	}

	return nil
}

// Dereference ApplicationConfig and wait for it to become ready to use.
func (c *Controller) dereferenceAppConfig(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Dereferencing ApplicationConfig resource")

	// Set status condition to Ready with False status and PendingApplicationConfig reason.
	err := c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.AwaitingApplicationConfigReason,
		Message: "Waiting for the referenced ApplicationConfig: " + c.appServer.Spec.ApplicationConfigRef.Name,
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Get ApplicationConfig.
	appConfigKey, err := c.appConfigRef()
	if err != nil {
		return fmt.Errorf("failed to get ApplicationConfig reference: %w", err)
	}

	appConfig, err := controllerutils.GetObjects(rtCtx, &v2alpha2.ApplicationConfig{},
		[]types.NamespacedName{*appConfigKey})
	if err != nil {
		return fmt.Errorf("failed to get ApplicationConfig: %w", err)
	}

	if len(appConfig) != 1 {
		return fmt.Errorf("expected one and only ApplicationConfig, got %d", len(appConfig))
	}

	c.appConfig = appConfig[0]

	// Wait for ApplicationConfig to become ready to use.
	err = controllerutils.WaitFor(rtCtx, appconfigutils.AppConfigReady(c.appConfig))
	if err != nil {
		rtCtx.Logger.Debug("ApplicationConfig is not ready",
			"ApplicationConfig.Status.Conditions", c.appConfig.Status.Conditions)

		return err
	}

	// Dereference Rails secrets from ApplicationConfig. We need it to accurately
	// calculate the configuration digest.
	railsSecretKey, err := appconfigutils.RailsSecretRef(c.appConfig)
	if err != nil {
		return fmt.Errorf("failed to retrieve Rails secrets reference: %w", err)
	}

	railsSecret, err := controllerutils.GetObjects(rtCtx, &corev1.Secret{},
		[]types.NamespacedName{*railsSecretKey})
	if err != nil {
		return fmt.Errorf("failed to get Rails secrets: %w", err)
	}

	if len(railsSecret) != 1 {
		return fmt.Errorf("expected one and only Secret for Rails secrets, got %d", len(railsSecret))
	}

	c.railsSecrets = railsSecret[0]

	return nil
}

// Dereference all referenced resources, including Secrets and Services.
func (c *Controller) dereferenceResources(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Dereferencing resources")

	// Set status condition to Ready with False status and AwaitingResources reason.
	err := c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.AwaitingResourcesReason,
		Message: "Awaiting referenced resources",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Dereference ALL referenced resources.
	err = c.getReferencedResources(rtCtx)
	if err != nil {
		return fmt.Errorf("failed to get referenced resources: %w", err)
	}

	// Update status condition message with name of unavailable resources.
	err = c.setStatusCondition(rtCtx, metav1.Condition{
		Type:   v2alpha2.ApplicationServerReadyCondition,
		Status: metav1.ConditionFalse,
		Reason: v2alpha2.AwaitingResourcesReason,
		Message: "Awaiting referenced resources" + func() string {
			if len(c.appConfigResources.Missing) == 0 {
				return ""
			} else {
				return ": " + strings.Join(c.appConfigResources.Missing, ",")
			}
		}(),
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Wait for ALL referenced resources to be available.
	if err := controllerutils.WaitFor(rtCtx, c.allResourcesReady(),
		controllerutils.WithDelay(Settings.Rails.ResourceCheckDelay)); err != nil {
		rtCtx.Logger.Debug("Missing referenced resource", "Missing", c.appConfigResources.Missing)

		return err
	}

	return nil
}

// Deploy workload.
func (c *Controller) provisionWorkload(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Provisioning workload")

	// Set status condition to Ready with False status and PendingApplicationStartup.
	err := c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionFalse,
		Reason:  v2alpha2.PendingStartReason,
		Message: "Pending application startup",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	// Create configuration Secrets (for Puma and Workhorse).
	cfgSecrets, err := controllerutils.PatchObjects(rtCtx, c.configSecretsInventory())
	if err != nil {
		return fmt.Errorf("failed to create configuration Secrets: %w", err)
	}

	// Calculate configuration fingerprint.
	fingerprint, err := controllerutils.Digest(rtCtx, append(cfgSecrets, c.railsSecrets, c.appSecrets))
	if err != nil {
		return fmt.Errorf("failed to calculate configuration fingerprint: %w", err)
	}

	// Create Deployment resource.
	if _, err = controllerutils.PatchObjects(rtCtx, c.appWorkloadInventory(fingerprint)); err != nil {
		return fmt.Errorf("failed to create Deployment resource: %w", err)
	}

	return nil
}

func (c *Controller) cleanupManagedResources(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Cleaning up managed resources")

	killList := []client.Object{}

	if c.appServer.Spec.Puma.Metrics.Disabled {
		killList = append(killList, &monitoringv1.ServiceMonitor{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: c.appServer.Namespace,
				Name:      c.appServer.Name + "-puma",
			},
		})
	}

	if c.appServer.Spec.Workhorse.Metrics.Disabled {
		killList = append(killList, &monitoringv1.ServiceMonitor{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: c.appServer.Namespace,
				Name:      c.appServer.Name + "-workhorse",
			},
		})
	}

	_, _ = controllerutils.DeleteObjects(rtCtx, killList, controllerutils.IgnoreAllErrors)

	return nil
}
