package databasemigration

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	frameworkinv "gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var initialRootPasswordGenerator = &inventory.SecretGenerator{
	SecretType:       corev1.SecretTypeOpaque,
	GenerateIfNeeded: true,
	Generators: []secret.Generator{
		&secret.SequenceGenerator{
			CharacterSets: []secret.CharacterSet{secret.AlphaNumeric},
			Length:        64,
			Names:         []string{"password"},
		},
	},
}

func (c *Controller) rootPasswordInventory() frameworkinv.Inventory[*corev1.Secret] {
	return frameworkinv.Decorate(
		initialRootPasswordGenerator,
		frameworkinv.WithName(fmt.Sprintf("%s-%s-root-password", c.dbMigration.Name, c.dbMigration.Spec.Version)),
		frameworkinv.WithNamespace(c.dbMigration.Namespace),
		frameworkinv.WithLabelsFrom(c.dbMigration),
		frameworkinv.WithAnnotationsFrom(c.dbMigration))
}

func (c *Controller) configSecretsInventory() frameworkinv.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"DatabaseMigration": c.dbMigration,
		"ApplicationConfig": c.appConfig,
	}, c.appConfigResources.Resources)

	return frameworkinv.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "database-migration/config/*.yaml",
			Values:  values,
		},
		frameworkinv.WithNamespace(c.dbMigration.Namespace),
		frameworkinv.WithLabelsFrom(c.dbMigration),
		frameworkinv.WithAnnotationsFrom(c.dbMigration),
		frameworkinv.WithControllerReference(c.dbMigration))
}

func (c *Controller) workloadInventory() frameworkinv.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"DatabaseMigration": c.dbMigration,
		"ApplicationConfig": c.appConfig,
	}, c.appConfigResources.Resources)

	return frameworkinv.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "database-migration/*.yaml",
			Values:  values,
		},
		frameworkinv.WithNamespace(c.dbMigration.Namespace),
		frameworkinv.WithLabelsFrom(c.dbMigration),
		frameworkinv.WithAnnotationsFrom(c.dbMigration),
		frameworkinv.WithControllerReference(c.dbMigration))
}
