{{- $spec := .ApplicationServer.Spec -}}
{{- $appName := printf "%s-%s" .ApplicationServer.Namespace .ApplicationServer.Name -}}

image: {{ getImageName $spec.Workload.ImageSource "Puma" $spec.Version $spec.Edition }}
args:
  - /scripts/wait-for-deps
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: WORKHORSE_ARCHIVE_CACHE_DISABLED
    value: true
  - name: ENABLE_BOOTSNAP
    value: {{ $spec.Puma.Settings.EnableBootsnap | default .Settings.Rails.EnableBootsnap }}
volumeMounts:
  {{- include "application-server/_sharedVolumeMount.tpl" . | nindent 2 }}
  {{- include "application-server/_railsVolumeMount.tpl" . | nindent 2 }}

{{- include "commons/_defaultResources.tpl" .Settings.Rails }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}
