package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"

	"github.com/huandu/xstrings"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/version"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

// The template is a tool for developing and testing object templates.
//
// It uses the embedded templates and can be configured to use an input data
// model to render the templates or parse Kubernetes objects from them.
//
// Usage:
//
//	go run tools/template COMMAND DATAMODEL PATTERN
//
//	COMMAND		Use `render` or `parse`. With `render` only the text template
//				is rendered while with `parse` Kubernetes objects are parsed
//				from the rendered templates.
//
//	DATAMODEL	The data model file. See below for its format and examples.
//
//	PATTERN		The glob pattern to select templates. This is evaluated in
//				`internal/assets/templates` directory.
//
// Data Model:
//
// The data model is a YAML file for defining the context of the templates. It
// uses the following format:
//
//	context:
//	  Key: Value
//	  Object: @object.yaml
//	  ObjectList:
//	    - @object-1.yaml
//	    - @object-2.yaml
//	  ObjectMap:
//	    A: @object-a.yaml
//	    B: @object-b.yaml
//	capabilities:
//	  apiResources: []
//	  version: {
//	    # See: k8s.io/apimachinery/pkg/version#Info
//	  }
//
// Values that start with "@" are interpreted as external types and read from
// the specified file.
//
// Examples:
//
//	go run ./tools/template parse datamodel.yaml 'application-server/deployment.yaml'
//
// Where `datamodel.yaml` contains:
//
//	context:
//	  ApplicationServer: "@ApplicationServer.yaml"
//	  ApplicationConfig: "@ApplicationConfig.yaml"
//	  Fingerprint: d8e28b8168f52cecbaaf51339da7d0c561b070a7adda0eaf433479b83d2fef6c
//	  PostgreSQLSecrets:
//	    Main: "@postgresql-basic-auth.Secret.yaml"
//	  PostgreSQLEndpoints:
//	    Main: "@postgresql.ServiceEndpoint.yaml"
//	  PostgreSQLCertificates: {}
//	  RedisSecrets:
//	    Main: "@redis-basic-auth.Secret.yaml"
//	  RedisEndpoints:
//	    Main: "@redis.ServiceEndpoint.yaml"
//	capabilities:
//	  apiResources: []
func main() {
	command := os.Args[1]
	source := os.Args[2]
	pattern := os.Args[3]

	e, err := newEngine(source)
	if err != nil {
		panic(err)
	}

	switch command {
	case "render":
		if err := e.render(pattern); err != nil {
			panic(err)
		}
	case "parse":
		if err := e.parse(pattern); err != nil {
			panic(err)
		}
	default:
		panic("unknown command")
	}
}

type engine struct {
	scheme  *runtime.Scheme
	decoder runtime.Decoder
	data    *dataModel
	ctx     *framework.RuntimeContext
	dir     string
}

type dataModel struct {
	Context      map[string]any   `json:"context"`
	Capabilities dataCapabilities `json:"capabilities"`
}

type dataCapabilities struct {
	Version      version.Info `json:"version"`
	APIResources []string     `json:"apiResources"`
}

func newEngine(source string) (*engine, error) {
	scheme, err := testing.NewScheme()
	if err != nil {
		return nil, err
	}

	decoder := testing.NewDecoder(scheme)

	rtCtx, err := framework.NewRuntimeContext(context.TODO())
	if err != nil {
		return nil, err
	}

	e := &engine{
		scheme:  scheme,
		decoder: decoder,
		data:    &dataModel{},
		dir:     filepath.Dir(source),
		ctx:     rtCtx,
	}

	if err := e.loadDataModel(source); err != nil {
		return nil, err
	}

	return e, nil
}

func (e *engine) parse(pattern string) error {
	tplObj := &inventory.TemplatedObjects{
		Pattern: pattern,
	}

	objList, err := tplObj.Get(e.ctx)
	if err != nil {
		return err
	}

	for _, obj := range objList {
		if err := printObject(obj); err != nil {
			return err
		}
	}

	return nil
}

func (e *engine) render(pattern string) error {
	data := map[string]any{}

	e.ctx.State.Visit(func(name string, value any, _ ...string) bool {
		data[name] = value

		return true
	})

	data["Capabilities"] = map[string]any{
		"ServerVersion": framework.ServerVersion,
		"APIResources":  framework.APIResources,
	}

	data["Settings"] = settings.Get()

	docs, err := inventory.RenderTemplate(pattern, data)
	if err != nil {
		return err
	}

	for name, doc := range docs {
		printDocument(name, doc)
	}

	return nil
}

func (e *engine) loadDataModel(source string) error {
	if buf, err := os.ReadFile(filepath.Clean(source)); err != nil {
		return err
	} else if err := yaml.Unmarshal(buf, e.data); err != nil {
		return err
	}

	framework.ServerVersion = &e.data.Capabilities.Version
	framework.APIResources = e.data.Capabilities.APIResources

	if err := e.loadSharedState(); err != nil {
		return err
	}

	return nil
}

func (e *engine) loadSharedState() error {
	for name, value := range e.data.Context {
		expandedValue, err := e.expandValue(value)
		if err != nil {
			return err
		}

		e.ctx.State.Store(name, expandedValue)
	}

	return nil
}

func (e *engine) expandValue(value any) (any, error) {
	//nolint:nestif
	if ok, path := isResource(value); ok {
		obj, err := e.getResources(path)
		if err != nil {
			return nil, err
		}

		return obj, nil
	} else if ok, paths := isResourceList(value); ok {
		objList := []any{}

		for _, p := range paths {
			obj, err := e.getResources(p)
			if err != nil {
				return nil, err
			}

			objList = append(objList, obj)
		}

		return objList, nil
	} else if ok, paths := isResourceMap(value); ok {
		objMap := map[string]any{}

		for k, p := range paths {
			obj, err := e.getResources(p)
			if err != nil {
				return nil, err
			}

			objMap[k] = obj
		}

		return objMap, nil
	} else {
		return value, nil
	}
}

func (e *engine) getResources(path string) (any, error) {
	absPath, err := e.getAbsolutePath(path)
	if err != nil {
		return nil, err
	}

	f, err := os.Open(filepath.Clean(absPath))
	if err != nil {
		return nil, err
	}

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	obj, err := e.readObjects(path, data)
	if err == nil {
		if len(obj) == 1 {
			return obj[0], nil
		} else {
			return obj, nil
		}
	}

	return dynamicUnmarshal(data)
}

func (e *engine) getAbsolutePath(relPath string) (string, error) {
	if absPath, err := filepath.Abs(filepath.Join(e.dir, relPath)); err != nil {
		return "", err
	} else {
		return absPath, nil
	}
}

func (e *engine) readObjects(source string, data []byte) ([]client.Object, error) {
	result, err := kube.ReadObjects(bytes.NewReader(data), e.decoder)
	if err != nil {
		return result, fmt.Errorf("error parsing %s: %w", source, err)
	}

	return result, nil
}

func isResource(v any) (bool, string) {
	s, isStr := v.(string)
	ok := isStr && len(s) > 1 && s[0] == '@'

	if ok {
		return true, s[1:]
	} else {
		return false, ""
	}
}

func isResourceList(v any) (bool, []string) {
	values, isArr := v.([]any)
	if !isArr {
		return false, []string{}
	}

	paths := []string{}

	for _, v := range values {
		if isPath, path := isResource(v); isPath {
			paths = append(paths, path)
		} else {
			return false, []string{}
		}
	}

	return true, paths
}

func isResourceMap(v any) (bool, map[string]string) {
	values, isMap := v.(map[string]any)
	if !isMap {
		return false, map[string]string{}
	}

	paths := map[string]string{}

	for k, v := range values {
		if isPath, path := isResource(v); isPath {
			paths[k] = path
		} else {
			return false, map[string]string{}
		}
	}

	return true, paths
}

func printObject(obj client.Object) error {
	buf, err := yaml.Marshal(obj)
	if err != nil {
		return err
	}

	fmt.Println("---")
	fmt.Println(string(buf))

	return nil
}

func printDocument(name string, buf *bytes.Buffer) {
	fmt.Printf("--- # %s \n", name)
	fmt.Println(buf.String())
}

func dynamicUnmarshal(data []byte) (any, error) {
	var tmp map[string]any

	err := yaml.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}

	fields := make([]reflect.StructField, 0, len(tmp))
	for key, value := range tmp {
		fields = append(fields, reflect.StructField{
			Name: xstrings.ToPascalCase(key),
			Type: reflect.TypeOf(value),
			Tag:  reflect.StructTag(fmt.Sprintf(`json:"%s"`, key)),
		})
	}

	dynamicType := reflect.StructOf(fields)
	dynamicValue := reflect.New(dynamicType).Interface()

	err = yaml.Unmarshal(data, dynamicValue)
	if err != nil {
		return nil, err
	}

	return dynamicValue, nil
}
