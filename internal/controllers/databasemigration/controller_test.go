package databasemigration

import (
	"context"
	"log/slog"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/client/interceptor"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

type testCase struct {
	name           string
	expectedError  error
	noRootPassword bool

	interceptorReactors testing.InterceptorCallbacks
}

var _ = DescribeTableSubtree("DatabaseMigrationController", func(scenario testCase) {
	var (
		fakeClient   client.WithWatch
		ctx          *framework.RuntimeContext
		dbMigration  *v2alpha2.DatabaseMigration
		reconcileErr error
	)

	BeforeEach(func() {
		ctx, _ = framework.NewRuntimeContext(context.TODO(), framework.WithLogger(slog.Default()))
		objects := testing.ReadKustomize(scenario.name, "given")
		fakeClient = fake.NewClientBuilder().
			WithScheme(framework.Scheme).
			WithObjects(objects...).
			WithStatusSubresource(objects...).
			WithInterceptorFuncs(testing.PatchInterceptor).
			Build()

		reactors := scenario.interceptorReactors
		if reactors == nil {
			reactors = successfulJob
		}

		fakeClient = interceptor.NewClient(fakeClient, reactors.Funcs())
		framework.Client = fakeClient
	})

	AfterEach(func() {
		framework.Client = nil
		ctx = nil
	})

	getDBMigration := func() *v2alpha2.DatabaseMigration {
		dbMigrationList := &v2alpha2.DatabaseMigrationList{}
		err := fakeClient.List(ctx, dbMigrationList, client.InNamespace("default"))
		Expect(err).ToNot(HaveOccurred())
		Expect(dbMigrationList.Items).To(HaveLen(1))
		return &dbMigrationList.Items[0]
	}

	JustBeforeEach(func() {
		dbMigration = getDBMigration()
		reconcileErr = NewController(dbMigration).Execute(ctx)
	})

	if scenario.expectedError != nil {
		It("Should run the reconciliation with an error", func() {
			Expect(reconcileErr).To(HaveOccurred())
			Expect(reconcileErr).To(MatchError(scenario.expectedError))
		})
		return
	}

	It("Should run the reconciliation successfully", func() {
		Expect(reconcileErr).ToNot(HaveOccurred())
		dbMigration = getDBMigration()
		slog.Debug(testing.DumpObject(dbMigration))

		By("Inspecting the DBMigration Object, it should have a completed condition")
		Expect(dbMigration.Status.Conditions).To(HaveLen(1))
		Expect(dbMigration.Status.Conditions[0].Type).To(Equal(v2alpha2.DatabaseMigrationCompletedCondition))
		Expect(dbMigration.Status.Conditions[0].Status).To(Equal(metav1.ConditionTrue))
		Expect(dbMigration.Status.Conditions[0].Reason).To(Equal(v2alpha2.DatabaseMigrationSucceededReason))

		secretList := &corev1.SecretList{}
		err := fakeClient.List(ctx, secretList, client.InNamespace(dbMigration.Namespace))
		Expect(err).NotTo(HaveOccurred())
		secrets := asSlice(secretList.Items)

		jobList := &batchv1.JobList{}
		err = fakeClient.List(ctx, jobList, client.InNamespace(dbMigration.Namespace))
		Expect(err).NotTo(HaveOccurred())
		jobs := asSlice(jobList.Items)

		By("Inspecting the migration job")
		Expect(jobs).To(HaveLen(1))
		slog.Debug(testing.DumpObject(jobs[0]))
		job := jobs[0]
		Expect(job).To(testing.HaveName("rails-sample-migration"))

		By("Inspecting the root password")
		secretMatcher := testing.HaveElementSatisfying(SatisfyAll(
			testing.HaveKind("Secret"),
			testing.HaveName("sample-17.6.1-root-password"),
		))

		if scenario.noRootPassword {
			By("Should not generate a root password with a given one")
			Expect(secrets).ToNot(secretMatcher)

		} else {
			By("Should generate a root password without a given one")
			Expect(secrets).To(secretMatcher)
		}
	})

},
	Entry("Simple input without existing root password", testCase{
		name: "simple",
	}),
	Entry("Simple input with existing root password", testCase{
		name:           "with-init-password",
		noRootPassword: true,
	}),
	Entry("Simple input with isUpgrade", testCase{
		name:           "with-is-upgrade",
		noRootPassword: true,
	}),
	Entry("Simple input with existing root password, migration job failed", testCase{
		name:                "with-init-password",
		expectedError:       reconcile.TerminalError(nil),
		interceptorReactors: failedJob,
	}),
)

var (
	successfulJob = testing.InterceptorCallbacks{
		testing.SuccessfulJobInterceptor,
	}

	failedJob = testing.InterceptorCallbacks{
		testing.FailedJobInterceptor,
	}
)

func asSlice[T any](objList []T) []*T {
	var result []*T

	for i := range objList {
		result = append(result, &objList[i])
	}

	return result
}
