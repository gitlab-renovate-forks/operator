package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _smartCard.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/smartCard.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleSmartCard, "smartcard/simple.yaml"),
	)
})

var (
	simpleSmartCard = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				SmartCard: &v2alpha2.ApplicationSmartCardSettings{
					Host: "smartcard.example.com:8080",
					CACertificate: corev1.SecretKeySelector{
						Key: "ca",
						LocalObjectReference: corev1.LocalObjectReference{
							Name: "smartcard-ca",
						},
					},
					RequiredForGitAccess: true,
				},
			},
		},
	}
)
