package internal

import (
	"context"
	"log/slog"
)

// DiscardLogHandler is a Handler which is always disabled and therefore logs nothing.
var DiscardLogHandler slog.Handler = discardHandler{}

// DiscardLogger is a Logger which logs nothing.
var DiscardLogger = slog.New(DiscardLogHandler)

type discardHandler struct{}

func (discardHandler) Enabled(context.Context, slog.Level) bool  { return false }
func (discardHandler) Handle(context.Context, slog.Record) error { return nil }
func (d discardHandler) WithAttrs([]slog.Attr) slog.Handler      { return d }
func (d discardHandler) WithGroup(string) slog.Handler           { return d }
