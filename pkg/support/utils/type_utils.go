package utils

import (
	"reflect"
)

// Ptr returns a pointer to the given value.
func Ptr[T any](v T) *T {
	return &v
}

// Zero will return the zero value of the given type.
// If the type is a pointer, it will return a pointer to the zero value.
// If the type is an interface, it will return nil-type nil.
func Zero[T any]() T {
	var val T
	defaultType := reflect.TypeOf(&val).Elem()

	if defaultType.Kind() == reflect.Ptr {
		newValue := reflect.New(defaultType.Elem())
		return newValue.Interface().(T)
	}

	return val
}

// IsZero returns true if the given value is the zero value of its type.
// If the type is a pointer, it will return true if the pointer is nil.
func IsZero[T any](v T) bool {
	return reflect.ValueOf(v).IsZero()
}
