# API Validation

## Overview

API validation is a crucial aspect of ensuring the integrity and correctness of
the data being processed by your Kubernetes Custom Resource Definitions (CRDs).
Based on [ADR003](/docs/architecture/decisions/003-validation-and-defaulting.md)
we utilize Kubebuilder markers and Common Expression Language (CEL) rules to
enforce validation constraints on our APIs. Additionally, we have developed a
CLI tool to facilitate the verification of these validation rules without the
need for an API server.

## Notes of Using CEL Rules

With the findings from <https://gitlab.com/gitlab-org/cloud-native/operator/-/merge_requests/186#note_2324454020>,
Kubernetes vendors may impose difference CEL budged limits.
We should be aware of this and try to keep the CEL expressions
as simple as possible.

## Validation CLI Tool

To assist with the verification of validation rules,
we have developed a CLI tool that can be found at [`tools/validation`](/tools/validation).
This tool allows you to validate your CRDs against the defined validation rules
without the need for an API server.
The documentation for the validation CLI tool is self-contained
in the tool itself.
You can run `validation -h` to see the available options and examples.

### Examples

- Validate a CRD file with debug log level:

```sh
./validation -c config/crd -l debug create custom-resource.yaml
```
