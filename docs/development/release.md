# Releases

## Create a new release

To create a release, you must create or push a tag in the following format: `vX.Y.Z`.
The `v` prefix is required by the [Go module versioning](https://go.dev/ref/mod#versions).

NOTE:
The project is in early development and currently releases `v0.Y.Z` versions.
[The releases carry no backward compatibility or stability guarantees.](https://go.dev/doc/modules/version-numbers#in-development)

The tag pipeline will create a [GitLab release](https://docs.gitlab.com/ee/user/project/releases/).
The release jobs are defined in `.gitlab/ci/release.gitlab-ci.yml` .
