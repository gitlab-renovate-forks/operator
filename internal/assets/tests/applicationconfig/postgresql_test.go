package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _postgresql.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/postgresql.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simplePostgreSQL, "postgresql/simple.yaml"),
	)
})

var (
	simplePostgreSQL = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				PostgreSQL: []v2alpha2.PostgreSQL{
					{
						Name: "main",
						ServiceProvider: v2alpha2.ServiceProvider{
							Authentication: v2alpha2.ServiceAuthentication{
								Basic: &corev1.SecretReference{},
							},
						},
					},
				},
			}},
		"PostgreSQLSecrets": map[string]*corev1.Secret{
			"main": {
				Data: map[string][]byte{
					"password": []byte("password"),
				},
			},
		},
		"PostgreSQLEndpoints": map[string]apiutils.ServiceEndpoint{
			"main": {Host: "postgresql.example.com", Port: 5432},
		},
	}
)
