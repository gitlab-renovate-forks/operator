package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	/* Condition Types */
	DatabaseMigrationCompletedCondition = "DatabaseMigrationCompleted"

	/* Condition Reasons */
	PendingRootPasswordReason        = "PendingRootPassword"
	DatabaseMigrationRunningReason   = "DatabaseMigrationRunning"
	DatabaseMigrationFailedReason    = "DatabaseMigrationFailed"
	DatabaseMigrationSucceededReason = "DatabaseMigrationSucceeded"
)

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:storageversion
// +kubebuilder:resource:shortName=dbmig
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=`.spec.version`
// +kubebuilder:printcolumn:name="Edition",type=string,JSONPath=`.spec.edition`
// +kubebuilder:printcolumn:name="Start",type=date,JSONPath=`.status.startTime`
// +kubebuilder:printcolumn:name="Completion",type=date,JSONPath=`.status.completionTime`
type DatabaseMigration struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DatabaseMigrationSpec   `json:"spec,omitempty"`
	Status DatabaseMigrationStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true
type DatabaseMigrationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []DatabaseMigration `json:"items"`
}

// +kubebuilder:validation:XValidation:rule="self == oldSelf",message="DatabaseMigrationSpec is immutable"
type DatabaseMigrationSpec struct {
	// Version is the target GitLab version the database will be upgraded to.
	// +kubebuilder:validation:Required
	Version string `json:"version"`

	// Edition specifies if the enterprise or community edition of GitLab is used.
	// Defaults to enterprise edition (EE).
	// +optional
	Edition Edition `json:"edition,omitempty"`

	// ApplicationConfigRef references the ApplicationConfigRef which points to the database to be migrated.
	// +kubebuilder:validation:Required
	ApplicationConfigRef corev1.ObjectReference `json:"applicationConfigRef"`

	// IsUpgrade specifies if the database migration is an upgrade.
	// If so, it will skip root password provisioning.
	// +optional
	IsUpgrade bool `json:"isUpgrade,omitempty"`

	// SkipPostMigrations allow to skip post deployment migrations for zero downtime upgrades:
	//   https://docs.gitlab.com/ee/update/zero_downtime.html#requirements-and-considerations.
	// +optional
	SkipPostMigrations bool `json:"skipPostMigrations,omitempty"`

	// InitialRootPassword is a reference to the Secret with the initial root password.
	// This will only be applied if the database is being initialized the first time and will be ignored otherwise.
	// +optional
	InitialRootPassword *corev1.SecretKeySelector `json:"initialRootPassword,omitempty"`

	// License is a reference to the Secret of the GitLab license.
	// This will only be applied if the database is being initialized the first time and will be ignored otherwise.
	// +optional
	License *corev1.SecretKeySelector `json:"license,omitempty"`

	// ActiveDeadline is the duration in which the database migration should complete.
	// Defaults to 1 hour.
	// +optional
	ActiveDeadline *metav1.Duration `json:"activeDeadline,omitempty"`

	// BackoffLimit is the number of retries before considering the database migration as failed.
	// Defaults to 6.
	// +optional
	// +kubebuilder:validation:Minimum=0
	BackoffLimit int32 `json:"backoffLimit,omitempty"`

	// +optional
	Workload WorkloadSpec `json:"workload,omitempty"`
}

type DatabaseMigrationStatus struct {
	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`

	// StartTime is the time the controller started the database migrations.
	// +optional
	StartTime *metav1.Time `json:"startTime,omitempty"`

	// CompletionTime is the time the migrations completed.
	// The completion time is set when the migrations finish successfully.
	// +optional
	CompletionTime *metav1.Time `json:"completionTime,omitempty"`
}

func init() {
	SchemeBuilder.Register(&DatabaseMigration{}, &DatabaseMigrationList{})
}
