package main

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/spf13/cobra"
)

var createCmd = &cobra.Command{
	Use:     "create custom-resource.yaml",
	Short:   "Run validation on a create operation",
	Example: "  validation -c config/crd -l debug create custom-resource.yaml",
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if err := validateCreate(args[0]); err != nil {
			os.Exit(1)
		}
	},
}

func validateCreate(crPath string) error {
	vc, err := NewValidatorCollection(crdPath)
	if err != nil {
		slog.Error("Failed to create validator collection", "error", err)
		return err
	}

	cr, err := readUnstructuredObjects(crPath)
	if err != nil {
		slog.Error("Failed to read resource", "error", err)
		return err
	}

	for objIdx, obj := range cr {
		errs := vc.ValidateCustomResource(obj)
		header := fmt.Sprintf("%s#%d:", crPath, objIdx)

		if len(errs) == 0 {
			fmt.Printf("%s %s %s:\n", greenColor(header), obj.GroupVersionKind().String(), obj.GetName())
			fmt.Printf("\t%s %s\n", "✅", greenColor("PASS"))
		} else {
			fmt.Printf("%s %s %s:\n", redColor(header), obj.GroupVersionKind().String(), obj.GetName())
		}

		for errIdx, err := range errs {
			fmt.Printf("\t%s %s #%d: %s\n", "❌", redColor("ERROR"), errIdx, err.Error())
		}
	}

	return nil
}
