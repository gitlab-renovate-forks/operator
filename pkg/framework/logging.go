package framework

import (
	"context"
	"log/slog"

	"github.com/go-logr/logr"
)

// the debug level of slog is -4, logr is -1, we need to correctly remap it.
type sloggerDebugFix struct {
	slog.Handler
}

func toSlogHandler(logger logr.Logger) slog.Handler {
	return &sloggerDebugFix{
		Handler: logr.ToSlogHandler(logger),
	}
}

func (s *sloggerDebugFix) Enabled(ctx context.Context, level slog.Level) bool {
	if level == slog.LevelDebug {
		level = -1
	}

	return s.Handler.Enabled(ctx, level)
}

func (s *sloggerDebugFix) Handle(ctx context.Context, record slog.Record) error {
	if record.Level == slog.LevelDebug {
		record.Level = -1
	}

	return s.Handler.Handle(ctx, record)
}
