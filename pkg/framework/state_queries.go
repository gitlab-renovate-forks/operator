package framework

import (
	"fmt"
	"reflect"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

// SummonByType is used for retrieving values of type T from the context.
type SummonByType[T any] struct{}

// Get returns all values of type T. If no value is found, an empty slice and
// ErrNotFound are returned.
func (q *SummonByType[T]) Get(rtCtx *RuntimeContext) ([]T, error) {
	result := []T{}

	rtCtx.State.Visit(func(_ string, value any, tags ...string) bool {
		if vals, ok := strictConvertGenerics[T](value); ok {
			result = append(result, vals...)
		}

		return true
	})

	if len(result) == 0 {
		var t T
		return result, fmt.Errorf("type %T: %w", t, ErrNotFound)
	}

	return result, nil
}

// SummonByName is used for retrieving a single value of type T with a known
// name from the context.
type SummonByName[T any] struct {
	Name string
}

// Get returns a value of type T that is associated to the given key. If the
// value is not found, an empty slice and ErrNotFound are returned. If a value
// is found but is not of type T, an error is returned.
//
// When found, the returned list contains a single element.
func (q *SummonByName[T]) Get(rtCtx *RuntimeContext) ([]T, error) {
	value, found := rtCtx.State.Get(q.Name)
	if !found {
		return []T{}, fmt.Errorf("name %s: %w", q.Name, ErrNotFound)
	}

	result, ok := strictConvertGenerics[T](value)
	if ok {
		return result, nil
	}

	var (
		t  T
		tA []T
	)

	return []T{}, fmt.Errorf("expected value of type %T or %T, got %T: %s",
		t, tA, value, q.Name)
}

// SummonByTags is used for retrieving a list of values of type T that have
// all of the given tags from the context.
type SummonByTags[T any] struct {
	Tags []string
}

// Get returns a list of values of type T that have all of the given tags. If
// no value is not found, an empty slice and ErrNotFound are returned. The tags
// are only examined when the value is of type T.
//
// This implementation safeguards against empty query. When the query tag list
// is empty, an empty result and ErrNotFound are returned.
func (q *SummonByTags[T]) Get(rtCtx *RuntimeContext) ([]T, error) {
	result := []T{}

	qTags := internal.Unique(q.Tags)

	if len(qTags) == 0 {
		return result, fmt.Errorf("empty query tags: %w", ErrNotFound)
	}

	rtCtx.State.Visit(func(_ string, value any, tags ...string) bool {
		vals, ok := strictConvertGenerics[T](value)
		if !ok {
			return true
		}

		if internal.Subset(tags, qTags) {
			result = append(result, vals...)
		}

		return true
	})

	if len(result) == 0 {
		return result, fmt.Errorf("tags %v: %w", qTags, ErrNotFound)
	}

	return result, nil
}

// lenientConvertGenerics attempts to convert input values to the target generic
// type T. It handles both single values and slices, converting as many elements
// as possible.
//
// It returns a slice of successfully converted values of type T and the number
// of input values before conversion attempts.
//
// This function will convert as many values as it can, skipping those it cannot.
// It does not guarantee that all input values will be converted.
func lenientConvertGenerics[T any](val any) ([]T, int) {
	var values []any

	rVal := reflect.ValueOf(val)
	if rVal.Kind() == reflect.Slice {
		values = make([]any, rVal.Len())
		for i := 0; i < rVal.Len(); i++ {
			values[i] = rVal.Index(i).Interface()
		}
	} else {
		values = []any{val}
	}

	result := []T{}
	valCnt := len(values)

	var t T
	tType := reflect.TypeOf(&t).Elem()

	for _, v := range values {
		vType := reflect.TypeOf(v)
		if vType.ConvertibleTo(tType) {
			if newVal, ok := v.(T); ok {
				result = append(result, newVal)
			}
		}
	}

	return result, valCnt
}

// strictConvertGenerics attempts to convert all input values to the target
// generic type T. It only succeeds if all values can be converted.
func strictConvertGenerics[T any](val any) ([]T, bool) {
	result, numOfValues := lenientConvertGenerics[T](val)

	return result, len(result) == numOfValues
}
