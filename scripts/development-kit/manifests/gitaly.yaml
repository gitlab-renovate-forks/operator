# NOTE:
# need to replace ##NAMESPACE## and ##APPLICATION_NAME## with the actual values
apiVersion: v1
data:
  config.toml.tpl: |
    # The directory where Gitaly's executables are stored
    bin_dir = "/usr/local/bin"

    # listen on a TCP socket. This is insecure (no authentication)
    listen_addr = "0.0.0.0:8075"

    # Directory where internal sockets reside
    # note: no value will result in a `/tmp/gitlab-internal-*` path
    # internal_socket_dir = "/home/git"

    # If metrics collection is enabled, inform gitaly about that
    prometheus_listen_addr = "0.0.0.0:9236"

    {% $storages := coll.Slice  "default" %}

    {% $index := index (.Env.HOSTNAME | strings.Split "-" | coll.Reverse) 0 | conv.ToInt64 %}
    {% if len $storages | lt $index %}
    [[storage]]
    name = "{% index $storages $index %}"
    path = "/home/git/repositories"
    {% else %}
    {% printf "Storage for node %d is not present in the storageNames array. Did you use kubectl to scale up? You need to solely use helm for this purpose." $index | fail %}
    {% end %}

    [logging]
    format = "json"
    dir = "/var/log/gitaly"

    [auth]
    token = {% file.Read "/etc/gitlab-secrets/gitaly/gitaly_token" | strings.TrimSpace | data.ToJSON %}

    [git]
    use_bundled_binaries = true
    ignore_gitconfig = true

    [gitlab-shell]
    # The directory where gitlab-shell is installed
    dir = "/srv/gitlab-shell"

    [gitlab]
    # location of shared secret for GitLab Shell / API interaction
    secret_file = "/etc/gitlab-secrets/shell/.gitlab_shell_secret"
    # URL of API
    url = "http://##APPLICATION_NAME##-app-server.##NAMESPACE##.svc.cluster.local:8181/"

    [gitlab.http-settings]
    # read_timeout = 300
    # user = someone
    # password = somepass
    # ca_file = /etc/ssl/cert.pem
    # ca_path = /etc/pki/tls/certs

    [hooks]
    # directory containing custom hooks
    custom_hooks_dir = "/home/git/custom_hooks"
kind: ConfigMap
metadata:
  labels:
    app: gitaly
  name: gitaly-config
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: gitaly
  name: gitaly
spec:
  clusterIP: None
  clusterIPs:
    - None
  internalTrafficPolicy: Cluster
  ipFamilies:
    - IPv4
  ipFamilyPolicy: SingleStack
  ports:
    - name: tcp-gitaly
      port: 8075
      protocol: TCP
      targetPort: grpc-gitaly
    - name: http-metrics
      port: 9236
      protocol: TCP
      targetPort: http-metrics
  selector:
    app: gitaly
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: gitaly
  name: gitaly
spec:
  persistentVolumeClaimRetentionPolicy:
    whenDeleted: Retain
    whenScaled: Retain
  podManagementPolicy: Parallel
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: gitaly
  serviceName: gitaly
  template:
    metadata:
      annotations:
        gitlab.com/prometheus_path: /metrics
        gitlab.com/prometheus_port: "9236"
        gitlab.com/prometheus_scrape: "true"
        prometheus.io/path: /metrics
        prometheus.io/port: "9236"
        prometheus.io/scrape: "true"
      labels:
        app: gitaly
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app: gitaly
                topologyKey: kubernetes.io/hostname
              weight: 1
      automountServiceAccountToken: false
      containers:
        - env:
            - name: CONFIG_TEMPLATE_DIRECTORY
              value: /etc/gitaly/templates
            - name: CONFIG_DIRECTORY
              value: /etc/gitaly
            - name: GITALY_CONFIG_FILE
              value: /etc/gitaly/config.toml
            - name: SSL_CERT_DIR
              value: /etc/ssl/certs
          image: registry.gitlab.com/gitlab-org/build/cng/gitaly:v17.1.1
          imagePullPolicy: IfNotPresent
          livenessProbe:
            exec:
              command:
                - /scripts/healthcheck
            failureThreshold: 3
            initialDelaySeconds: 30
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 3
          name: gitaly
          ports:
            - containerPort: 8075
              name: grpc-gitaly
              protocol: TCP
            - containerPort: 9236
              name: http-metrics
              protocol: TCP
          readinessProbe:
            exec:
              command:
                - /scripts/healthcheck
            failureThreshold: 3
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 3
          resources:
            requests:
              cpu: 500m
              memory: 1000Mi
          securityContext:
            runAsUser: 1000
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
            - mountPath: /etc/gitaly/templates
              name: gitaly-config
            - mountPath: /etc/gitlab-secrets
              name: gitaly-secrets
              readOnly: true
            - mountPath: /home/git/repositories
              name: repo-data
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext:
        fsGroup: 1000
        runAsUser: 1000
      terminationGracePeriodSeconds: 30
      volumes:
        - configMap:
            defaultMode: 420
            name: gitaly-config
          name: gitaly-config
        - name: gitaly-secrets
          projected:
            defaultMode: 288
            sources:
              - secret:
                  items:
                    - key: token
                      path: gitaly/gitaly_token
                  name: gitaly-secret
              - secret:
                  items:
                    - key: internal-api
                      path: shell/.gitlab_shell_secret
                  name: "##APPLICATION_NAME##-app-secrets"
  updateStrategy:
    rollingUpdate:
      partition: 0
    type: RollingUpdate
  volumeClaimTemplates:
    - apiVersion: v1
      kind: PersistentVolumeClaim
      metadata:
        labels:
          app: gitaly
        name: repo-data
      spec:
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Gi
        volumeMode: Filesystem
      status:
        phase: Pending
