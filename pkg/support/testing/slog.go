package testing

import (
	"bytes"
	"context"
	"fmt"
	"log/slog"

	"github.com/onsi/ginkgo/v2"
)

type GinkgoHandler struct{}

func (h *GinkgoHandler) Enabled(_ context.Context, _ slog.Level) bool {
	// Enable all log levels during tests
	return true
}

func (h *GinkgoHandler) Handle(_ context.Context, record slog.Record) error {
	var buf bytes.Buffer
	// Format the log message
	message := record.Message
	if record.Level != slog.LevelInfo {
		message = "[" + record.Level.String() + "] " + message
	}

	buf.WriteString(message)

	// Include additional attributes
	record.Attrs(func(attr slog.Attr) bool {
		buf.WriteString(fmt.Sprintf(" %s=%v", attr.Key, attr.Value))
		return true
	})

	// Write the formatted log to GinkgoWriter
	ginkgo.GinkgoWriter.Println(buf.String())

	return nil
}

func (h *GinkgoHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	// Return a new handler with additional attributes
	return h
}

func (h *GinkgoHandler) WithGroup(name string) slog.Handler {
	// Return a new handler with a group name
	return h
}
