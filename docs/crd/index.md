---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Supported Custom Resources

The operator supports the following custom resources:

- Lifecycle resources:
  - [`Instance`](instance.md)
- Rails resources:
  - [`ApplicationServer`](applicationserver.md)
  - [`JobProcessor`](jobprocessor.md)
  - [`ApplicationConfig`](applicationconfig.md)
  - [`DatabaseMigrations`](db-migration.md)
- Supporting resources:
  - [`SSHServer`](sshserver.md)

## Resource Relationships

The components are linked using object references in the resource
specification and labels. These relationships are maintained automatically,
if you use our [recommended install tooling](../index.md#installing-gitlab).

```mermaid
classDiagram
  namespace LifecycleResources {
     class Instance {
        GitLab lifecycle
     }
  }

  namespace RailsResources {
    class ApplicationServer {
        Puma
        Workhorse
    }
    class ApplicationConfig { 
        Rails Configuration 
        Rails Secrets
    }
    class JobProcessor { Sidekiq }
    class DatabaseMigration { PostgreSQL Migrations }
  }

  namespace SupportingResources {
    class SSHServer {
      OpenSSH Daemon
      GitLab SSH Daemon
    }

    class KubernetesAgentServer { GitLab KAS }
  }

  ApplicationServer --> Instance
  JobProcessor --> Instance
  DatabaseMigration --> Instance
  SSHServer --> Instance
  KubernetesAgentServer --> Instance

  ApplicationServer ..> ApplicationConfig
  JobProcessor ..> ApplicationConfig
  DatabaseMigration ..> ApplicationConfig
```

One GitLab instance may have multiple custom resources of one type.
For example, at high scale, it can make sense to deploy multiple
`JobProcessor`s for different queues or different `ApplicationServer`s
for different web paths.

If you have multiple resources, make sure to setup your
[`Instance`](../instance.md#installation) appropriately.
