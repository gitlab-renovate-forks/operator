{{- $appName := printf "%s-%s" .JobProcessor.Namespace .JobProcessor.Name -}}

metadata:
  labels:
    {{- with .JobProcessor.Labels }}
    {{-   . | toYaml | nindent 4 }}
    {{- end }}      
    gitlab.io/job-processor: {{ $appName }}
  annotations:
    {{- with .JobProcessor.Annotations }}
    {{-   . | toYaml | nindent 4 }}
    {{- end }}
    cluster-autoscaler.kubernetes.io/safe-to-evict: "false"
    gitlab.io/fingerprint: {{ .Fingerprint }}
spec:
  automountServiceAccountToken: false
  {{- include "commons/_defaultPodSecurityContext.tpl" .Settings.Rails | nindent 2 }}
  initContainers:
    - name: dependencies
      {{- include "jobprocessor/_dependencies.tpl" . | nindent 6 }}
  containers:
    - name: sidekiq
      {{- include "jobprocessor/_sidekiq.tpl" . | nindent 6 }}
  volumes:
    {{- include "jobprocessor/_volumes.tpl" . | nindent 4 }}
