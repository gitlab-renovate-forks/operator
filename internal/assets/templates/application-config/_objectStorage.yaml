{{/* 
Renders the object storage configuration for Rails.

Expects a context with the following keys:
  - ApplicationConfig: The ApplicationConfig custom resource.
  - S3AccessKeyIds: A map of Secret objects, containing S3 AccessKeyIDs, with
    name of the S3 object store as its key.
  - S3AccessKeySecrets: A map of Secret objects, containing S3 AccessKeySecrets,
    with name of the S3 object store as its key.
  - GCSCredentials: A map of Secret objects, containing GCP credentials, with
    with name of the GCS object store as its key.
*/}}

{{- $ := . -}}
{{- $stores := mapDestinationStores .ApplicationConfig.Spec.ObjectStorage -}}

{{- range $destination := .ApplicationConfig.Spec.ObjectStorage.Objects }}
{{- $store := index $stores $destination.Usage }}
{{- if empty $store }}
{{-   continue }}
{{- end }}
{{ $destination.Usage | toString | snakecase }}:
  enabled: true
  object_store:
    enabled: true
    remote_directory: {{ $destination.BucketName }}
    proxy_download: {{ $destination.ProxyDownload | default true }}
    connection:
    {{- with $store.S3 }}
      {{- $idSecret := index $.S3AccessKeyIds $store.Name }}
      {{- $keySecret := index $.S3AccessKeySecrets $store.Name }}
      provider: AWS
      aws_access_key_id: {{ getSecretValue $idSecret .AccessKeyId.Key "aws_access_key_id" }}
      aws_secret_access_key: {{ getSecretValue $keySecret .AccessKeySecret.Key "aws_secret_access_key" }}
      {{- with .Region }}
      region: {{ . }}
      {{- end }}
      {{- with .SignatureVersion -}}
      aws_signature_version: {{ . }}
      {{- end }}
      {{- with .Host }}
      host: {{ . }}
      {{- end }}
      {{- with .Endpoint }}
      endpoint: {{ . }}
      {{- end }}
      path_style: {{ .PathStyle }}
      # ~S3
    {{- end }}
    {{- with $store.GCS }}
      {{- $credSecret := index $.GCSCredentials $store.Name }}
      provider: Google
      google_project: {{ .Project }}
      google_json_key_string: |
        {{ getSecretValue $credSecret .Credentials.Key "google_json_key_string" | nindent 8 }}
      # ~GCS
    {{- end }}
{{- end }}