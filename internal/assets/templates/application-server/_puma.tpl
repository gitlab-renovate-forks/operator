{{- $spec := .ApplicationServer.Spec -}}

image: {{ getImageName $spec.Workload.ImageSource "Puma" $spec.Version $spec.Edition }}
ports:
  - name: puma
    containerPort: {{ empty $spec.Puma.TLS | ternary .Settings.Rails.Puma.Port .Settings.Rails.Puma.TLSPort }}
  {{- if not $spec.Puma.Metrics.Disabled }}
  - name: puma-metrics
    containerPort: {{ .Settings.Rails.Puma.MetricsPort }}
  {{- end }}
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: WORKHORSE_ARCHIVE_CACHE_DISABLED
    value: true
  - name: ENABLE_BOOTSNAP
    value: {{ $spec.Puma.Settings.EnableBootsnap | default .Settings.Rails.EnableBootsnap }}
  - name: WORKER_PROCESSES
    value: {{ $spec.Puma.Settings.WorkerProcesses | default .Settings.Rails.Puma.WorkerProcesses }}
  - name: WORKER_TIMEOUT
    value: {{ $spec.Puma.Settings.WorkerTimeout | default .Settings.Rails.Puma.WorkerTimeout | toSeconds }}
  - name: PUMA_WORKER_MAX_MEMORY
    value: {{ $spec.Puma.Settings.WorkerMaxMemory | default .Settings.Rails.Puma.WorkerMaxMemory | toMegaBytes }}
  - name: PUMA_THREADS_MIN
    value: {{ $spec.Puma.Settings.MinThreads | default .Settings.Rails.Puma.MinThreads }}
  - name: PUMA_THREADS_MAX
    value: {{ $spec.Puma.Settings.MaxThreads | default .Settings.Rails.Puma.MaxThreads }}
  - name: DISABLE_PUMA_WORKER_KILLER
    value: {{ $spec.Puma.Settings.DisableWorkerKiller | default .Settings.Rails.Puma.DisableWorkerKiller }}
  {{- if $spec.Puma.TLS }}
  - name: SSL_INTERNAL_PORT
    value: {{ .Settings.Rails.Puma.TLSPort }}
  - name: PUMA_SSL_KEY
    value: {{ .Settings.Secrets.MountPath }}/puma/tls.key
  - name: PUMA_SSL_CERT
    value: {{ .Settings.Secrets.MountPath }}/puma/tls.crt
    {{- if $spec.Puma.TLS.CACertificate }}
  - name: PUMA_SSL_CLIENT_CERT
    value: {{ .Settings.Secrets.MountPath }}/puma/ca.crt
    {{- end }}
  {{- else }}
  - name: INTERNAL_PORT
    value: {{ .Settings.Rails.Puma.Port }}
  {{- end }}
  {{- include "commons/_tracingVariables.tpl" $spec | nindent 2 }}
  - name: prometheus_multiproc_dir
    value: {{ .Settings.Rails.MetricsDir }}
volumeMounts:
  {{- include "application-server/_sharedVolumeMount.tpl" . | nindent 2 }}
  {{- include "application-server/_railsVolumeMount.tpl" . | nindent 2 }}  
lifecycle:
  preStop:
    exec:
      command: ["/bin/bash", "-c", "pkill -SIGINT -o ruby"]
livenessProbe:
  exec:
    command: ["curl", "-sfS", "http://localhost:{{ .Settings.Rails.Puma.Port }}/-/liveness"]
  initialDelaySeconds: 20
  periodSeconds: 60
  timeoutSeconds: 30
  successThreshold: 1
  failureThreshold: 3
readinessProbe:
  exec:
    command: ["curl", "-sfS", "http://localhost:{{ .Settings.Rails.Puma.Port }}/-/readiness"]
  initialDelaySeconds: 0
  periodSeconds: 5
  timeoutSeconds: 2
  successThreshold: 1
  failureThreshold: 2

{{- include "commons/_defaultResources.tpl" .Settings.Rails }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}