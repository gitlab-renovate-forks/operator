#!/usr/bin/env bash

# This script checks if all generated files are checked in and up to date.

check_updated() {
  local target="${1}"
  local updated="$(git status --porcelain)"

  if [ -n "$updated" ]; then
    echo "`make ${target}` updated files:"
    echo "$updated"
    exit 1
  fi
}

make generate
check_updated "generate"

make manifests
check_updated "manifests"

make api-docs
check_updated "api-docs"
