---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# JobProcessor

The `JobProcessor` manages [Sidekiq](https://docs.gitlab.com/ee/development/sidekiq/),
which is the background job processor of GitLab.

The `JobProcessor` must:

- Reference an [`ApplicationConfig`](application-config.md).
- Be optionally linked to an [`Instance`](instance.md).

The controller of the `JobProccesor` custom resource is responsible for:

- Generating a GitLab Rails configuration file based on the `ApplicationConfig`.
- Managing the Sidekiq Deployment.

## Installation

To create your own `JobProcessor` custom resource, you can adapt
the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_jobprocessor.yaml).

For all available options, see the [`JobProcessor` API reference](../generated/api-docs.md#jobprocessor).
