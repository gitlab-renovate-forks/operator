package appconfigutils

import (
	"fmt"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
)

type ApplicationConfigResourceOption string

const (
	RepositoryResources    ApplicationConfigResourceOption = "Repository"
	PostgreSQLResources    ApplicationConfigResourceOption = "PostgreSQL"
	RedisResources         ApplicationConfigResourceOption = "Redis"
	ClickHouseResources    ApplicationConfigResourceOption = "ClickHouse"
	ObjectStoreResources   ApplicationConfigResourceOption = "ObjectStore"
	TwoFactorAuthResources ApplicationConfigResourceOption = "2FA"
)

var (
	IncludeAllResources []ApplicationConfigResourceOption = []ApplicationConfigResourceOption{
		RepositoryResources,
		PostgreSQLResources,
		RedisResources,
		ClickHouseResources,
		ObjectStoreResources,
		TwoFactorAuthResources,
	}
)

const (
	RepositoryEndpointsBucket = "RepositoryEndpoints"
	RepositoryTokensBucket    = "RepositoryTokens"
	PostgreSQLSecretsBucket   = "PostgreSQLSecrets" //nolint:gosec
	PostgreSQLEndpointsBucket = "PostgreSQLEndpoints"
	RedisSecretsBucket        = "RedisSecrets"
	RedisEndpointsBucket      = "RedisEndpoints"
	ClickHouseSecretsBucket   = "ClickHouseSecrets"
	ClickHouseEndpointsBucket = "ClickHouseEndpoints"
	S3AccessKeyIdsBucket      = "S3AccessKeyIds"
	S3AccessKeySecretsBucket  = "S3AccessKeySecrets" //nolint:gosec
	GCSCredentialsBucket      = "GCSCredentials"
	TwoFactorAuthBucket       = "TwoFactorSecrets"
)

type ApplicationConfigResources struct {
	dereferencer *controllerutils.DereferenceHandler
	appConfig    *v2alpha2.ApplicationConfig
	options      map[ApplicationConfigResourceOption]bool

	Missing   []string
	Resources map[string]any
}

func NewApplicationConfigDereferencer(appConfig *v2alpha2.ApplicationConfig, options ...ApplicationConfigResourceOption) *ApplicationConfigResources {
	r := &ApplicationConfigResources{
		appConfig:    appConfig,
		dereferencer: controllerutils.NewDereferenceHandler(),
		options:      map[ApplicationConfigResourceOption]bool{},

		Missing:   []string{},
		Resources: map[string]any{},
	}

	r.setOptions(options...)
	r.populate()

	return r
}

func (r *ApplicationConfigResources) Dereference(rtCtx *framework.RuntimeContext) error {
	err := r.dereferencer.Dereference(rtCtx)

	r.Missing = make([]string, len(r.dereferencer.Missing))

	for i, m := range r.dereferencer.Missing {
		r.Missing[i] = fmt.Sprintf("%s/%s[%s/%s]", m.APIVersion, m.Kind, m.Namespace, m.Name)
	}

	return err
}

func (r *ApplicationConfigResources) Ready() bool {
	return r.dereferencer.Ready()
}

func (r *ApplicationConfigResources) setOptions(option ...ApplicationConfigResourceOption) {
	for _, o := range option {
		r.options[o] = true
	}
}

func (r *ApplicationConfigResources) hasOption(option ApplicationConfigResourceOption) bool {
	return r.options[option]
}

func (r *ApplicationConfigResources) populate() {
	if r.hasOption(RepositoryResources) {
		r.populateRepositories()
	}

	if r.hasOption(PostgreSQLResources) {
		r.populatePostgreSQL()
	}

	if r.hasOption(PostgreSQLResources) {
		r.populateRedis()
	}

	if r.hasOption(ClickHouseResources) {
		r.populateClickHouse()
	}

	if r.hasOption(ObjectStoreResources) {
		r.populateObjectStores()
	}

	if r.hasOption(TwoFactorAuthResources) {
		r.populateCiscoDuoAuth()
		r.populateFortiAuth()
	}
}

func (r *ApplicationConfigResources) populateRepositories() {
	for _, repository := range r.appConfig.Spec.Repositories {
		r.dereferencer.AddServiceProvider(
			r.appConfig,
			repository.Endpoint,
			apiutils.GitalyServiceEndpoint,
			func(endpoint any) {
				r.addMappedResource(RepositoryEndpointsBucket, repository.Name, endpoint)
			})
		r.dereferencer.AddSecretKeySelector(
			r.appConfig,
			*repository.Authentication.Token,
			func(secret any) { r.addMappedResource(RepositoryTokensBucket, repository.Name, secret) })
	}
}

func (r *ApplicationConfigResources) populatePostgreSQL() {
	for _, postgresql := range r.appConfig.Spec.PostgreSQL {
		r.dereferencer.AddServiceProvider(
			r.appConfig,
			postgresql.Endpoint,
			apiutils.PostgreSQLServiceEndpoint,
			func(endpoint any) {
				r.addMappedResource(PostgreSQLEndpointsBucket, postgresql.Name, endpoint)
			})

		if postgresql.TLS != nil {
			r.dereferencer.AddSecretKeySelector(
				r.appConfig,
				postgresql.TLS.CACertificate,
				func(_ any) { /* We don't really use them. Just need to make sure they exist */ })
		}

		if postgresql.Authentication.Basic != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*postgresql.Authentication.Basic,
				func(secret any) { r.addMappedResource(PostgreSQLSecretsBucket, postgresql.Name, secret) })
		}

		if postgresql.Authentication.ClientTLS != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*postgresql.Authentication.ClientTLS,
				func(_ any) { /* We don't really use them. Just need to make sure they exist */ })
		}
	}
}

func (r *ApplicationConfigResources) populateRedis() {
	for _, redis := range r.appConfig.Spec.Redis {
		r.dereferencer.AddServiceProvider(
			r.appConfig,
			redis.Endpoint,
			apiutils.RedisServiceEndpoint,
			func(endpoint any) {
				r.addMappedResource(RedisEndpointsBucket, redis.Name, endpoint)
			})

		if redis.TLS != nil {
			r.dereferencer.AddSecretKeySelector(
				r.appConfig,
				redis.TLS.CACertificate,
				func(_ any) {})
		}

		if redis.Authentication.Basic != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*redis.Authentication.Basic,
				func(secret any) { r.addMappedResource(RedisSecretsBucket, redis.Name, secret) })
		}

		if redis.Authentication.ClientTLS != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*redis.Authentication.ClientTLS,
				func(_ any) {})
		}
	}
}

func (r *ApplicationConfigResources) populateClickHouse() {
	for _, clickhouse := range r.appConfig.Spec.ClickHouse {
		r.dereferencer.AddServiceProvider(
			r.appConfig,
			clickhouse.Endpoint,
			apiutils.PostgreSQLServiceEndpoint,
			func(endpoint any) {
				r.addMappedResource(ClickHouseEndpointsBucket, clickhouse.Name, endpoint)
			})

		if clickhouse.Authentication.Basic != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*clickhouse.Authentication.Basic,
				func(secret any) { r.addMappedResource(ClickHouseEndpointsBucket, clickhouse.Name, secret) })
		}

		if clickhouse.Authentication.ClientTLS != nil {
			r.dereferencer.AddSecretReference(
				r.appConfig,
				*clickhouse.Authentication.ClientTLS,
				func(_ any) { /* We don't really use them. Just need to make sure they exist */ })
		}
	}
}

func (r *ApplicationConfigResources) populateObjectStores() {
	for _, store := range r.appConfig.Spec.ObjectStorage.Stores {
		if store.S3 != nil {
			r.dereferencer.AddSecretKeySelector(
				r.appConfig,
				store.S3.AccessKeyId,
				func(secret any) { r.addMappedResource(S3AccessKeyIdsBucket, store.Name, secret) })

			r.dereferencer.AddSecretKeySelector(
				r.appConfig,
				store.S3.AccessKeySecret,
				func(secret any) { r.addMappedResource(S3AccessKeySecretsBucket, store.Name, secret) })

			continue
		}

		if store.GCS != nil {
			r.dereferencer.AddSecretKeySelector(
				r.appConfig,
				store.GCS.Credentials,
				func(secret any) { r.addMappedResource(GCSCredentialsBucket, store.Name, secret) })
		}
	}
}

func (r *ApplicationConfigResources) populateCiscoDuoAuth() {
	if r.appConfig.Spec.CiscoDuo == nil {
		return
	}

	r.dereferencer.AddSecretKeySelector(r.appConfig, r.appConfig.Spec.CiscoDuo.SecretKey, func(secret any) {
		r.addMappedResource(TwoFactorAuthBucket, "DuoSecretKey", secret)
	})

	r.dereferencer.AddSecretKeySelector(r.appConfig, r.appConfig.Spec.CiscoDuo.IntegrationKey, func(secret any) {
		r.addMappedResource(TwoFactorAuthBucket, "DuoIntegrationKey", secret)
	})
}

func (r *ApplicationConfigResources) populateFortiAuth() {
	if r.appConfig.Spec.FortiAuthenticator == nil {
		return
	}

	r.dereferencer.AddSecretReference(r.appConfig, r.appConfig.Spec.FortiAuthenticator.Credentials, func(secret any) {
		r.addMappedResource(TwoFactorAuthBucket, "FortiAuthCredentials", secret)
	})
}

func (r *ApplicationConfigResources) addMappedResource(category, key string, resource any) {
	v := r.Resources[category]

	if v == nil {
		r.Resources[category] = map[string]any{
			key: resource,
		}
	} else if b, ok := v.(map[string]any); ok {
		b[key] = resource
	}
}
