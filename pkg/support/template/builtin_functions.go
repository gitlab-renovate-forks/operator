package template

import (
	"fmt"
	"net"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"text/template"
	"time"

	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

var (
	builtinFuncMap = template.FuncMap{
		"toYaml":            toYAML,
		"fromYaml":          fromYAML,
		"toDuration":        toDuration,
		"toSeconds":         toSeconds,
		"toMilliseconds":    toMilliseconds,
		"toQuantity":        toQuantity,
		"toUnit":            toUnit,
		"toKilo":            toKilo,
		"toMega":            toMega,
		"toBytes":           toUnit,
		"toKiloBytes":       toKilo,
		"toMegaBytes":       toMega,
		"digStruct":         digStruct,
		"asList":            asList,
		"parseUrl":          parseUrl,
		"parseHostnamePort": parseHostnamePort,
		"depointer":         depointer,
	}
)

func generateInclude(t *template.Template, tracker map[string]any) func(string, any) (string, error) {
	return func(name string, data any) (string, error) {
		if data == nil {
			return "", fmt.Errorf("include requires a non-nil context: %#q", name)
		}

		if _, ok := tracker[name]; ok {
			return "", fmt.Errorf("recursive call to include %#q", name)
		}

		var buf strings.Builder

		tracker[name] = true
		err := t.ExecuteTemplate(&buf, name, data)
		delete(tracker, name)

		return buf.String(), err
	}
}

func toYAML(v interface{}) (string, error) {
	out, err := yaml.Marshal(v)
	if err != nil {
		return "", err
	}

	return strings.TrimSuffix(string(out), "\n"), nil
}

func fromYAML(s string) (map[string]any, error) {
	out := map[string]any{}

	err := yaml.Unmarshal([]byte(s), &out)
	if err != nil {
		return out, err
	}

	return out, nil
}

func toDuration(v any, opts ...string) (time.Duration, error) {
	unit := "s"
	if len(opts) > 0 {
		unit = opts[0]
	}

	v = depointer(v)
	if v == nil {
		return 0, nil
	}

	switch v := v.(type) {
	case string:
		return time.ParseDuration(v)
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return time.ParseDuration(fmt.Sprintf("%d%s", v, unit))
	case float32, float64:
		return time.ParseDuration(fmt.Sprintf("%.0f%s", v, unit))
	case metav1.Duration:
		return v.Duration, nil
	case time.Duration:
		return v, nil
	default:
		return 0, fmt.Errorf("cannot convert %T to Duration type", v)
	}
}

func toSeconds(v any, opts ...string) (int64, error) {
	d, err := toDuration(v, opts...)
	if err != nil {
		return 0, err
	}

	return int64(d.Seconds()), nil
}

func toMilliseconds(v any, opts ...string) (int64, error) {
	d, err := toDuration(v, opts...)
	if err != nil {
		return 0, err
	}

	return d.Milliseconds(), nil
}

func toQuantity(v any, opts ...string) (resource.Quantity, error) {
	unit := ""
	if len(opts) > 0 {
		unit = opts[0]
	}

	v = depointer(v)
	if v == nil {
		return resource.Quantity{}, nil
	}

	switch v := v.(type) {
	case string:
		return resource.ParseQuantity(v)
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return resource.ParseQuantity(fmt.Sprintf("%d%s", v, unit))
	case float32, float64:
		return resource.ParseQuantity(fmt.Sprintf("%.0f%s", v, unit))
	case resource.Quantity:
		return v, nil
	default:
		return resource.Quantity{}, fmt.Errorf("cannot convert %T to Quantity type", v)
	}
}

func toUnit(v any, opts ...string) (int64, error) {
	q, err := toQuantity(v, opts...)
	if err != nil {
		return 0, err
	}

	return q.Value(), nil
}

func toKilo(v any, opts ...string) (int64, error) {
	q, err := toQuantity(v, opts...)
	if err != nil {
		return 0, err
	}

	return q.ScaledValue(resource.Kilo), nil
}

func toMega(v any, opts ...string) (int64, error) {
	q, err := toQuantity(v, opts...)
	if err != nil {
		return 0, err
	}

	return q.ScaledValue(resource.Mega), nil
}

func digStruct(v any, f string) any {
	fieldNames := strings.Split(f, ".")
	value := reflect.ValueOf(v)

	for _, fieldName := range fieldNames {
		if value.Kind() == reflect.Ptr {
			value = value.Elem()
		}

		if value.Kind() != reflect.Struct {
			return nil
		}

		field := value.FieldByName(fieldName)
		if !field.IsValid() || !field.CanInterface() {
			return nil
		}

		value = field
	}

	return value.Interface()
}

func asList(v any) any {
	value := reflect.ValueOf(v)

	if value.Kind() == reflect.Slice {
		return v
	}

	sType := reflect.SliceOf(value.Type())
	s := reflect.MakeSlice(sType, 1, 1)
	s.Index(0).Set(value)

	return s.Interface()
}

// Similar to urlParse but returns port number as well.
func parseUrl(v string) (map[string]any, error) {
	result := map[string]any{}

	parsedURL, err := url.Parse(v)
	if err != nil {
		return result, err
	}

	result["scheme"] = parsedURL.Scheme
	result["host"] = parsedURL.Host
	result["hostname"] = parsedURL.Hostname()
	result["port"] = parsedURL.Port()
	result["path"] = parsedURL.Path
	result["query"] = parsedURL.RawQuery
	result["opaque"] = parsedURL.Opaque
	result["fragment"] = parsedURL.Fragment

	if parsedURL.User != nil {
		result["userInfo"] = parsedURL.User.String()
	} else {
		result["userInfo"] = ""
	}

	return result, nil
}

func parseHostnamePort(v string, defaultPort int32) (map[string]any, error) {
	hostname, portStr, err := net.SplitHostPort(v)

	if err != nil {
		hostname, portStr, err = net.SplitHostPort(fmt.Sprintf("%s:%d", v, defaultPort))
		if err != nil {
			return nil, err
		}
	}

	port, err := strconv.Atoi(portStr)

	if err != nil {
		return nil, err
	}

	return map[string]any{
		"hostname": hostname,
		"port":     port,
	}, nil
}

func depointer(v any) any {
	value := reflect.ValueOf(v)

	if value.Kind() != reflect.Ptr {
		return v
	}

	if value.IsNil() {
		return reflect.Zero(value.Type().Elem()).Interface()
	}

	return value.Elem().Interface()
}
