---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# ApplicationServer

The `ApplicationServer` manages the GitLab webserver, which consists of:

- [Puma](https://docs.gitlab.com/ee/administration/operations/puma.html)
- [Workhorse](https://docs.gitlab.com/ee/development/workhorse/)

The `ApplicationServer` must:

- Reference an [`ApplicationConfig`](application-config.md).
- Be optionally linked to an [`Instance`](instance.md).

The controller of the `ApplicationServer` custom resource is responsible for:

- Generating a GitLab Rails configuration file based on the `ApplicationConfig`.
- Managing the Puma and Workhorse Deployment.

## Installation

To create your own `ApplicationServer` custom resource, you can adapt the
[sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_applicationserver.yaml).

For all available options, see the [`ApplicationServer` API reference](../generated/api-docs.md#applicationserver).
