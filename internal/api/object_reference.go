package api

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

// NewObjectReference creates a reference to a Kubernetes object. Use the
// provided options to customize the reference:
//
//   - WithName
//   - WithNamespace
//   - WithKind
//   - WithAPIVersion
//   - WithOwner
//   - FromObjectReference
//   - FromSecretReference
//   - FromSecretKeySelector
//
// The resulting object reference uses the cluster API to locate and retrieve
// the referenced Kubernetes object.
func NewObjectReference[T client.Object](options ...ObjectReferenceOption) Reference[T] {
	o := &ObjectReferenceOptions{}
	for _, option := range options {
		option(o)
	}

	return &objectReference[T]{o}
}

// WithName sets the name of the reference to Kubernetes object.
func WithName(name string) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		o.Key.Name = name
	})
}

// WithNamespace sets the namespace of the reference to Kubernetes object.
func WithNamespace(namespace string) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		o.Key.Namespace = namespace
	})
}

// WithKind sets the kind of the reference to Kubernetes object.
func WithKind(kind string) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		o.GVK.Kind = kind
	})
}

// WithAPIVersion sets the API version of the reference to Kubernetes object.
func WithAPIVersion(apiVersion string) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		if gv, err := schema.ParseGroupVersion(apiVersion); err == nil {
			o.GVK.Group = gv.Group
			o.GVK.Version = gv.Version
		}
	})
}

// WithOwner sets the owner of the referenced object. The owner's namespace is
// used as the namespace of the referenced object.
func WithOwner(owner client.Object) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		if owner.GetNamespace() != "" {
			o.Key.Namespace = owner.GetNamespace()
		}
	})
}

// FromObjectReference uses the provided corev1.ObjectReference to create the
// reference to Kubernetes object.
func FromObjectReference(ref corev1.ObjectReference) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		if ref.Kind != "" {
			o.GVK.Kind = ref.Kind
		}

		if ref.APIVersion != "" {
			if gv, err := schema.ParseGroupVersion(ref.APIVersion); err == nil {
				o.GVK.Group = gv.Group
				o.GVK.Version = gv.Version
			}
		}

		if ref.Namespace != "" {
			o.Key.Namespace = ref.Namespace
		}

		o.Key.Name = ref.Name
	})
}

// FromSecretKeySelector uses the provided corev1.SecretKeySelector to create a
// reference to a Kubernetes object.
func FromSecretReference(ref corev1.SecretReference) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		o.GVK = schema.GroupVersionKind{
			Group:   "",
			Version: "v1",
			Kind:    "Secret",
		}

		if ref.Namespace != "" {
			o.Key.Namespace = ref.Namespace
		}

		o.Key.Name = ref.Name
	})
}

// FromSecretKeySelector uses the provided corev1.SecretKeySelector to create a
// reference to a Kubernetes object.
func FromSecretKeySelector(ref corev1.SecretKeySelector) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		o.GVK = schema.GroupVersionKind{
			Group:   "",
			Version: "v1",
			Kind:    "Secret",
		}

		o.Key.Name = ref.Name
	})
}

func WithObjectReferenceOptions(options *ObjectReferenceOptions) ObjectReferenceOption {
	return ObjectReferenceOption(func(o *ObjectReferenceOptions) {
		*o = *options
	})
}

type NameObjectReference[T client.Object] map[string]Reference[T]

// ObjectReferenceOption is a function that customizes the options for a
// reference to a Kubernetes object.
type ObjectReferenceOption func(*ObjectReferenceOptions)

// ObjectReferenceOptions contains the options for a reference to a Kubernetes
// object.
type ObjectReferenceOptions struct {
	GVK schema.GroupVersionKind
	Key types.NamespacedName
}

type objectReference[T client.Object] struct {
	*ObjectReferenceOptions
}

func (r *objectReference[T]) Resolve(ctx context.Context) (T, error) {
	obj, err := framework.Scheme.New(r.GVK)
	if err != nil {
		return utils.Zero[T](), err
	}

	t, isT := obj.(T)
	if !isT {
		return utils.Zero[T](), fmt.Errorf("expected %T, got %T", t, obj)
	}

	if err := framework.Client.Get(ctx, r.Key, t); err != nil {
		return t, err
	}

	return t, nil
}

func (r *objectReference[T]) String() string {
	return fmt.Sprintf("%s/%s[%s/%s]",
		r.GVK.GroupVersion(), r.GVK.Kind,
		r.Key.Namespace, r.Key.Name)
}
