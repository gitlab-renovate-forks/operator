// Package v2alpha2 contains API Schema definitions for the v2alpha2 API group

// +kubebuilder:object:generate=true
// +groupName=gitlab.com
package v2alpha2
