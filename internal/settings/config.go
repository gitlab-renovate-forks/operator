package settings

import (
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/pflag"
	"k8s.io/apimachinery/pkg/api/resource"

	"sigs.k8s.io/controller-runtime/pkg/log/zap"
)

// Config defines the global configuration of GitLab Operator. It is recommended
// to add new fields to this struct to extend the configuration.
//
// We use Viper library to read the configuration from command-line arguments,
// environment variables and config files. Viper uses `mapstructure` tags to
// read the configuration from various sources into the struct.
//
// The  struct tags are used by the viper library to read the configuration from
// environment variables and config files.
type Config struct {
	Secrets Secrets   `mapstructure:"secrets"`
	Rails   Rails     `mapstructure:"rails"`
	Runtime Runtime   `mapstructure:"runtime"`
	Log     LogConfig `mapstructure:"log"`
}

type Secrets struct {
	EnforceEntropyControl bool   `mapstructure:"enforce-entropy-control"`
	MountPath             string `mapstructure:"mount-path"`
}

type Rails struct {
	Environment              string            `mapstructure:"environment"`
	Edition                  string            `mapstructure:"edition" validate:"oneof=CE EE"`
	PostgreSQLUsername       string            `mapstructure:"postgresql-username"`
	PostgreSQLDatabase       string            `mapstructure:"postgresql-database"`
	ClickHouseDatabase       string            `mapstructure:"clickhouse-database"`
	UserID                   int64             `mapstructure:"user-id"`
	FSGroupID                int64             `mapstructure:"fs-group-id"`
	ResourceRequests         map[string]string `mapstructure:"resource-requests"`
	ResourceLimits           map[string]string `mapstructure:"resource-limits"`
	HomeDir                  string            `mapstructure:"home-dir"`
	ConfigDir                string            `mapstructure:"config-dir"`
	MetricsDir               string            `mapstructure:"metrics-dir"`
	UploadDir                string            `mapstructure:"upload-dir"`
	TempDir                  string            `mapstructure:"temp-dir"`
	EnableBootsnap           bool              `mapstructure:"enable-bootsnap"`
	ResourceCheckDelay       time.Duration     `mapstructure:"resource-check-delay"`
	Puma                     Puma              `mapstructure:"puma"`
	Workhorse                Workhorse         `mapstructure:"workhorse"`
	Sidekiq                  Sidekiq           `mapstructure:"sidekiq"`
	Migrations               Migrations        `mapstructure:"migrations"`
	WebhookTimeout           time.Duration     `mapstructure:"webhook-timeout"`
	GraphQLTimeout           time.Duration     `mapstructure:"graphql-timeout"`
	ApplicationSettingsCache time.Duration     `mapstructure:"application-settings-cache"`
}

type Puma struct {
	Port                int32             `mapstructure:"port"`
	TLSPort             int32             `mapstructure:"tls-port"`
	MetricsPort         int32             `mapstructure:"metrics-port"`
	WorkerProcesses     int32             `mapstructure:"worker-processes"`
	WorkerTimeout       time.Duration     `mapstructure:"worker-timeout"`
	WorkerMaxMemory     resource.Quantity `mapstructure:"worker-max-memory"`
	MinThreads          int32             `mapstructure:"min-threads"`
	MaxThreads          int32             `mapstructure:"max-threads"`
	DisableWorkerKiller bool              `mapstructure:"disable-worker-killer"`
	StatusCheckDelay    time.Duration     `mapstructure:"status-check-delay"`
}

type Workhorse struct {
	Port               int32             `mapstructure:"port"`
	MetricsPort        int32             `mapstructure:"metrics-port"`
	LogFormat          string            `mapstructure:"log-format"`
	ShutdownTimeout    time.Duration     `mapstructure:"shutdown-timeout"`
	ShutdownBlackout   time.Duration     `mapstructure:"shutdown-blackout"`
	ScalerMaxProcesses int32             `mapstructure:"scaler-max-processes"`
	ScalerMaxFileSize  resource.Quantity `mapstructure:"scaler-max-file-size"`
}

type Sidekiq struct {
	MetricsPort      int32               `mapstructure:"metrics-port"`
	HealthcheckPort  int32               `mapstructure:"healthcheck-port"`
	LogFormat        string              `mapstructure:"log-format"`
	Concurrency      int32               `mapstructure:"concurrency"`
	Timeout          time.Duration       `mapstructure:"timeout"`
	ResourceRequests map[string]string   `mapstructure:"resource-requests"`
	ResourceLimits   map[string]string   `mapstructure:"resource-limits"`
	MemoryKiller     SidekiqMemoryKiller `mapstructure:"memory-killer"`
	StatusCheckDelay time.Duration       `mapstructure:"status-check-delay"`
}

type Migrations struct {
	StatusCheckDelay time.Duration `mapstructure:"status-check-delay"`
}

type SidekiqMemoryKiller struct {
	CheckInterval time.Duration     `mapstructure:"check-interval"`
	ShutdownWait  time.Duration     `mapstructure:"shutdown-wait"`
	GraceTime     time.Duration     `mapstructure:"grace-time"`
	MaxRSS        resource.Quantity `mapstructure:"maximum-rss"`
	HardLimitRSS  resource.Quantity `mapstructure:"hard-limit-rss"`
}

type Runtime struct {
	MetricsAddr      string `mapstructure:"metrics-addr" validate:"hostname_port"`
	MetricsSecure    bool   `mapstructure:"metrics-secure"`
	HealthProbeAddr  string `mapstructure:"health-probe-addr" validate:"hostname_port"`
	LeaderElection   bool   `mapstructure:"leader-election"`
	LeaderElectionID string `mapstructure:"leader-election-id"`
	HTTP2            bool   `mapstructure:"http2"`
}

type LogConfig struct {
	Development     bool   `mapstructure:"development"`
	Format          string `mapstructure:"format" validate:"oneof=json console"`                                       // json, console
	Level           string `mapstructure:"level" validate:"oneof=debug info error"`                                    // debug, info, error
	StacktraceLevel string `mapstructure:"stacktrace-level" validate:"oneof=info error panic"`                         // info, error, panic
	TimeFormat      string `mapstructure:"time-format" validate:"oneof=epoch millis nano iso8601 rfc3339 rfc3339nano"` // epoch, millis, nano, iso8601, rfc3339, rfc3339nano
}

func (l LogConfig) ZapOptions() (*zap.Options, error) {
	encoder, err := parseZapEncoder(l.Format, l.Development)
	if err != nil {
		return nil, err
	}

	level, err := parseZapLevel(l.Level)
	if err != nil {
		return nil, err
	}

	stacktraceLevel, err := parseZapLevel(l.StacktraceLevel)
	if err != nil {
		return nil, err
	}

	timeEncoder, err := parseTimeFormat(l.TimeFormat)
	if err != nil {
		return nil, err
	}

	return &zap.Options{
		Development:     l.Development,
		Encoder:         encoder,
		Level:           level,
		StacktraceLevel: stacktraceLevel,
		TimeEncoder:     timeEncoder,
	}, nil
}

var (
	defaultConfig = Config{
		Secrets: Secrets{
			EnforceEntropyControl: false,
			MountPath:             "/run/secrets/gitlab.io",
		},
		Rails: Rails{
			Environment: "production",
			Edition:     "EE",
			UserID:      1000,
			FSGroupID:   1000,
			ResourceRequests: map[string]string{
				"cpu": "50m",
			},
			ResourceLimits:     map[string]string{},
			HomeDir:            "/srv/gitlab",
			ConfigDir:          "/srv/gitlab/config",
			UploadDir:          "/srv/gitlab/public/uploads/tmp",
			TempDir:            "/tmp",
			MetricsDir:         "/metrics",
			EnableBootsnap:     true,
			ResourceCheckDelay: 3 * time.Second,
			Puma: Puma{
				Port:                8080,
				TLSPort:             8443,
				MetricsPort:         9228,
				WorkerProcesses:     2,
				WorkerTimeout:       60 * time.Second,
				WorkerMaxMemory:     resource.MustParse("1Gi"),
				MinThreads:          4,
				MaxThreads:          4,
				DisableWorkerKiller: true,
				StatusCheckDelay:    10 * time.Second,
			},
			Workhorse: Workhorse{
				Port:               8181,
				MetricsPort:        9229,
				LogFormat:          "JSON",
				ShutdownTimeout:    10 * time.Second,
				ShutdownBlackout:   10 * time.Second,
				ScalerMaxProcesses: 2,
				ScalerMaxFileSize:  resource.MustParse("250k"),
			},
			Sidekiq: Sidekiq{
				LogFormat:       "JSON",
				MetricsPort:     3807,
				HealthcheckPort: 3808,
				Concurrency:     25,
				Timeout:         25 * time.Second,
				ResourceRequests: map[string]string{
					"cpu":    "1000m",
					"memory": "1G",
				},
				MemoryKiller: SidekiqMemoryKiller{
					CheckInterval: 3 * time.Second,
					MaxRSS:        resource.MustParse("2Mi"),
					GraceTime:     15 * time.Minute,
					ShutdownWait:  30 * time.Second,
				},
				StatusCheckDelay: 10 * time.Second,
			},
			Migrations: Migrations{
				StatusCheckDelay: 3 * time.Second,
			},
			WebhookTimeout:           10 * time.Second,
			GraphQLTimeout:           30 * time.Second,
			ApplicationSettingsCache: 60 * time.Second,
			PostgreSQLUsername:       "gitlab",
			PostgreSQLDatabase:       "gitlabhq_production",
			ClickHouseDatabase:       "gitlab_clickhouse_production",
		},
		Runtime: Runtime{
			MetricsAddr:      ":8443",
			MetricsSecure:    true,
			HealthProbeAddr:  ":8081",
			LeaderElection:   false,
			LeaderElectionID: "92d21895.gitlab.com",
			HTTP2:            false,
		},
		Log: LogConfig{
			Development:     true,
			Format:          "console",
			Level:           "debug",
			StacktraceLevel: "error",
			TimeFormat:      "epoch",
		},
	}

	configFile string
	config     = defaultConfig
	validate   = validator.New(validator.WithRequiredStructEnabled())
)

func Get() Config {
	return config
}

// entrypoint for setting default values and flags.
func init() {
	SetDefault(defaultConfig)

	pflag.StringVarP(&configFile, "config", "c", "", "Path to the config file")

	pflag.String("runtime.metrics-addr", defaultConfig.Runtime.MetricsAddr, "The address the metric endpoint binds to")
	pflag.Bool("runtime.metrics-secure", defaultConfig.Runtime.MetricsSecure, "If set the metrics endpoint is served securely")
	pflag.String("runtime.health-probe-addr", defaultConfig.Runtime.HealthProbeAddr, "The address the probe endpoint binds to")
	pflag.Bool("runtime.leader-election", defaultConfig.Runtime.LeaderElection, "Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager")
	pflag.String("runtime.leader-election-id", defaultConfig.Runtime.LeaderElectionID, "Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager")
	pflag.Bool("runtime.http2", defaultConfig.Runtime.HTTP2, "If set, HTTP/2 will be enabled for the metrics and webhook servers")

	pflag.Bool("log.devel", defaultConfig.Log.Development, "Enable development mode")
	pflag.String("log.format", defaultConfig.Log.Format, "Log format: json, console")
	pflag.String("log.level", defaultConfig.Log.Level, "Log level: debug, info, error")
	pflag.String("log.stacktrace-level", defaultConfig.Log.StacktraceLevel, "Stacktrace level: info, error, panic")
	pflag.String("log.time-format", defaultConfig.Log.TimeFormat, "Time format: epoch, millis, nano, iso8601, rfc3339, rfc3339nano")

	vp.SetConfigType("yaml")
}

// Parse reads the configuration from the environment and config file.
// Just like flag.Parse(), it should be called after all flags are defined and before using the configuration.
//
// It uses viper under the hood to read the configuration from the environment and config file.
// Viper uses the following precedence order. Each item takes precedence over the item below it:
//   - explicit call to Set
//   - flag
//   - env
//   - config
//   - key/value store
//   - default
func Parse() error {
	pflag.Parse()

	if err := vp.BindPFlags(pflag.CommandLine); err != nil {
		return err
	}

	if configFile != "" {
		vp.SetConfigFile(configFile)

		if err := vp.ReadInConfig(); err != nil {
			return err
		}
	}

	if err := Unmarshal(&config); err != nil {
		return err
	}

	if err := validate.Struct(&config); err != nil {
		return err
	}

	return nil
}
