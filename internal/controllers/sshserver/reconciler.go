package sshserver

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

type Reconciler struct{}

// +kubebuilder:rbac:groups=gitlab.com,resources=sshservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=sshservers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=sshservers/finalizers,verbs=update

func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.SSHServer) (ctrl.Result, error) {
	return framework.Expand(nil)
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.SSHServer{}).
		Complete(reconcile.AsReconciler(mgr.GetClient(), &Reconciler{}))
}
