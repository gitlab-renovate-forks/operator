package inventory

import (
	"bytes"
	"fmt"
	"io/fs"
	gtpl "text/template"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/template"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/assets"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/templateutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

// TemplatedObjects renders YAML-formatted resource templates and decodes raw
// Kubernetes objects from them.
//
// The templates have access to the following values and functions:
//
//   - Builtin template engine functions, including "sprig" functions.
//   - Additional Operator-specific functions.
//   - The shared states of the runtime context are available as variables.
//   - Operator settings are accessible with `.Settings` variable.
//   - Cluster capabilities are available with `.Capabilities` variable.
//   - Any additional values provided by the caller using Values field.
//
// NOTE:
//
//   - Some of the variables are mutable. Do not change them in the templates.
//   - By default, the templated objects use Unstructured data type. It is safe
//     to use them as-is for server-side apply.
type TemplatedObjects struct {
	Pattern string
	Values  map[string]any
	Engine  *template.Engine
}

func (i *TemplatedObjects) Get(rtCtx *framework.RuntimeContext) ([]client.Object, error) {
	data := map[string]any{}

	rtCtx.State.Visit(func(name string, value any, _ ...string) bool {
		data[name] = value

		return true
	})

	for k, v := range i.Values {
		data[k] = v
	}

	data["Capabilities"] = map[string]any{
		"ServerVersion": framework.ServerVersion,
		"APIResources":  framework.APIResources,
	}

	data["Settings"] = settings.Get()

	engineToUse := DefaultTemplateEngine
	if i.Engine != nil {
		engineToUse = i.Engine
	}

	return engineToUse.Parse(i.Pattern, data)
}

var (
	// DefaultTemplateEngine is the globally available default template engine.
	// It is initialized with the templates that are embedded in `assets/templates`
	// directory. It ensures that the Operator-specific template functions are
	// accessible to these templates.
	//
	// If the template engine cannot be initialized, the program will panic.
	// This includes any template errors.
	DefaultTemplateEngine *template.Engine
)

// RenderTemplate is a proxy for the RenderTemplate method of the default template engine.
func RenderTemplate(pattern string, data map[string]any) (map[string]*bytes.Buffer, error) {
	return DefaultTemplateEngine.Render(pattern, data)
}

func init() {
	var err error

	tplFS, err := fs.Sub(assets.Templates, "templates")
	if err != nil {
		panic(fmt.Errorf("failed to load templates: %v", err))
	}

	funcMap := gtpl.FuncMap{}
	funcMap = utils.MergeMap(funcMap, apiutils.FuncMap())
	funcMap = utils.MergeMap(funcMap, templateutils.FuncMap())

	DefaultTemplateEngine, err = template.Load(tplFS, template.WithFunctions(funcMap))

	if err != nil {
		panic(fmt.Errorf("failed to load templates: %v", err))
	}
}
