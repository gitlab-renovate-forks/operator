package api

import (
	"context"
	"maps"

	corev1 "k8s.io/api/core/v1"
)

// SecretData represents payload of a Kubernetes Secret.
type SecretData map[string]string

func (d SecretData) Consume(_ context.Context, secret *corev1.Secret) error {
	if secret == nil {
		return nil
	}

	for k, v := range secret.Data {
		d[k] = string(v)
	}

	// NOTE: This does not work with live Secrets. It is only used for testing.
	maps.Copy(d, secret.StringData)

	return nil
}
