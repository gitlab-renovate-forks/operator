package instance

import (
	"errors"
	"fmt"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var Settings = settings.Get()

type Controller struct {
	instance          *v2alpha2.Instance
	adapter           *Adapter
	planner           UpgradePlanner
	newUpgradeHandler UpgradeHandlerFactory
}

func NewController(instance *v2alpha2.Instance) *Controller {
	c := &Controller{
		instance:          instance,
		planner:           dumbPlanner,
		newUpgradeHandler: NewZeroDowntimeUpgrade,
	}

	return c
}

func (c *Controller) Execute(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("upgrading the instance")

	if err := c.initialize(rtCtx); err != nil {
		return err
	}

	if needsUpgrade, err := c.needsUpgrade(rtCtx); err != nil {
		return err
	} else if !needsUpgrade {
		return nil
	}

	nextVersion, edition, err := c.getNextUpgradeTarget(rtCtx)
	if err != nil {
		return err
	}

	rtCtx.Logger.Info("upgrading to the next version",
		"version", nextVersion,
		"edition", edition)

	err = c.doUpgrade(rtCtx, nextVersion, edition)
	if err != nil {
		return err
	}

	needsFurtherUpgrade, err := c.needsFurtherUpgrade(rtCtx, nextVersion, edition)
	if err != nil {
		return err
	}

	if needsFurtherUpgrade {
		return framework.Requeue()
	}

	return nil
}

func (c *Controller) initialize(rtCtx *framework.RuntimeContext) error {
	var err error

	rtCtx.Logger.Debug("initializing instance adapter and upgrade handler")

	c.adapter, err = NewAdapter(rtCtx, c.instance)
	if err != nil {
		rtCtx.Logger.Error("upgrade initialization failed", "reason", err)

		_ = c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.UpgradeCompletedCondition,
			Status:  metav1.ConditionFalse,
			Reason:  v2alpha2.FailedUpgradeInitializationReason,
			Message: err.Error(),
		})

		return err
	}

	if err = c.adapter.Verify(); err != nil {
		return err
	}

	err = c.updateStatus(rtCtx, c.adapter.CurrentVersion, c.adapter.CurrentEdition)
	if err != nil {
		rtCtx.Logger.Error("failed to reliably set version details", "reason", err)

		return framework.Terminate(err)
	}

	return nil
}

func (c *Controller) needsUpgrade(rtCtx *framework.RuntimeContext) (needs bool, err error) {
	if c.adapter.NeedsUpgrade() {
		needs = true

		return
	}

	rtCtx.Logger.Info("upgrade is not required")

	err = c.setStatusCondition(rtCtx,
		metav1.Condition{
			Type:   v2alpha2.UpgradeCompletedCondition,
			Status: metav1.ConditionTrue,
			Reason: v2alpha2.UpgradeTargetReachedReason,
			Message: fmt.Sprintf("Already have the expected version and edition: %s/%s",
				c.adapter.CurrentVersion, c.adapter.CurrentEdition),
		})

	return
}

func (c *Controller) getNextUpgradeTarget(rtCtx *framework.RuntimeContext) (version string, edition v2alpha2.Edition, err error) {
	edition = getEdition(c.instance.Spec.Edition)
	version = c.planner(c.adapter.CurrentVersion, c.instance.Spec.Version, edition)

	err = c.setStatusCondition(rtCtx, metav1.Condition{
		Type:   v2alpha2.UpgradeCompletedCondition,
		Status: metav1.ConditionUnknown,
		Reason: v2alpha2.UpgradeProgressingReason,
		Message: fmt.Sprintf("Upgrading to the next available version and edition: %s/%s",
			version, edition),
	})

	return
}

func (c *Controller) doUpgrade(rtCtx *framework.RuntimeContext, version string, edition v2alpha2.Edition) error {
	lastState := c.getUpgradeState(rtCtx)
	nextState, err := c.newUpgradeHandler(c.adapter, version, edition).Upgrade(rtCtx, lastState)

	if framework.IsTerminal(err) {
		rtCtx.Logger.Error("upgrade failed and cannot proceed further", "reason", err)

		_ = c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.UpgradeCompletedCondition,
			Status:  metav1.ConditionFalse,
			Reason:  v2alpha2.UpgradeFailedReason,
			Message: errors.Unwrap(err).Error(),
		})

		_ = c.clearStatusCondition(rtCtx, v2alpha2.UpgradeProgressingCondition)

		return err
	}

	_ = c.setUpgradeState(rtCtx, nextState)

	if err != nil {
		rtCtx.Logger.Debug("upgrade is pending, retrying to update its status")

		return err
	}

	rtCtx.Logger.Info("upgrade target reached",
		"version", version,
		"edition", edition)

	return c.updateStatus(rtCtx, version, edition)
}

func (c *Controller) needsFurtherUpgrade(rtCtx *framework.RuntimeContext, version string, edition v2alpha2.Edition) (needs bool, err error) {
	needs = version != c.instance.Spec.Version

	if needs {
		rtCtx.Logger.Info("intermediate upgrade target reached",
			"version", version,
			"edition", edition)

		err = c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.UpgradeCompletedCondition,
			Status:  metav1.ConditionFalse,
			Reason:  v2alpha2.IntermediateUpgradeTargetReachedReason,
			Message: fmt.Sprintf("Intermediate upgrade completed: %s/%s", version, edition),
		})
	} else {
		rtCtx.Logger.Info("final upgrade target reached",
			"version", version,
			"edition", edition)

		err = c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.UpgradeCompletedCondition,
			Status:  metav1.ConditionTrue,
			Reason:  v2alpha2.UpgradeTargetReachedReason,
			Message: fmt.Sprintf("Upgrade completed: %s/%s", version, edition),
		})
	}

	return
}

func (c *Controller) getUpgradeState(_ *framework.RuntimeContext) UpgradeState {
	cond := meta.FindStatusCondition(c.instance.Status.Conditions, v2alpha2.UpgradeProgressingCondition)

	if cond == nil {
		return UpgradeStateStart
	}

	return UpgradeState(cond.Reason)
}

func (c *Controller) setUpgradeState(rtCtx *framework.RuntimeContext, state UpgradeState) error {
	if state == UpgradeStateCompleted {
		return c.clearStatusCondition(rtCtx, v2alpha2.UpgradeProgressingCondition)
	} else {
		return c.setStatusCondition(rtCtx, metav1.Condition{
			Type:   v2alpha2.UpgradeProgressingCondition,
			Status: metav1.ConditionTrue,
			Reason: string(state),
		})
	}
}

func (c *Controller) updateStatus(rtCtx *framework.RuntimeContext, version string, edition v2alpha2.Edition) error {
	err := controllerutils.PatchStatus(rtCtx, c.instance, func() bool {
		changed := c.instance.Status.Version != version ||
			c.instance.Status.Edition != edition

		c.instance.Status.Version = version
		c.instance.Status.Edition = edition

		return changed
	})
	if err != nil {
		return fmt.Errorf("failed to set version and edition status: %w", err)
	}

	return nil
}

func (c *Controller) setStatusCondition(rtCtx *framework.RuntimeContext, condition metav1.Condition) error {
	err := controllerutils.SetStatusCondition(rtCtx, c.instance, &c.instance.Status.Conditions, condition)
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	return nil
}

func (c *Controller) clearStatusCondition(rtCtx *framework.RuntimeContext, condType string) error {
	err := controllerutils.ClearStatusCondition(rtCtx, c.instance, &c.instance.Status.Conditions, condType)
	if err != nil {
		return fmt.Errorf("failed to clear status condition: %w", err)
	}

	return nil
}
