package api

import (
	"context"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

// ServiceProvider is a composite resource that expands v2alpha2.ServiceProvider.
// It retrieves its endpoint and authentication secrets.
type ServiceProvider struct {
	Composite

	Type      ServiceEndpointType
	Endpoint  *ServiceEndpoint
	Basic     SecretData
	ClientTLS SecretData
	Token     SecretData

	objectReferenceOptions *ObjectReferenceOptions
}

// NewServiceProvider creates a new ServiceProvider for the desired endpoint
// type. It uses the provided options to configure how to resolve ObjectReferences,
// including referenced Kubernetes Services and Secrets.
//
// Depending on the defined authentication method the secrets are loaded into
// respective fields.
func NewServiceProvider(endpointType ServiceEndpointType, options ...ObjectReferenceOption) *ServiceProvider {
	o := &ObjectReferenceOptions{}
	for _, option := range options {
		option(o)
	}

	return &ServiceProvider{
		Composite:              NewComposite(),
		Type:                   endpointType,
		objectReferenceOptions: o,
	}
}

func (p *ServiceProvider) Consume(ctx context.Context, provider *v2alpha2.ServiceProvider) error {
	p.Endpoint = &ServiceEndpoint{}

	AddReferencedResource(p.Composite,
		NewServiceEndpointReference(&provider.Endpoint, p.Type, WithObjectReferenceOptions(p.objectReferenceOptions)),
		p.Endpoint)

	if provider.Authentication.ClientTLS != nil {
		p.ClientTLS = SecretData{}

		AddReferencedResource(p.Composite,
			NewObjectReference[*corev1.Secret](
				WithObjectReferenceOptions(p.objectReferenceOptions),
				FromSecretReference(*provider.Authentication.ClientTLS)),
			p.ClientTLS)
	}

	if provider.Authentication.Basic != nil {
		p.Basic = SecretData{}

		AddReferencedResource(p.Composite,
			NewObjectReference[*corev1.Secret](
				WithObjectReferenceOptions(p.objectReferenceOptions),
				FromSecretReference(*provider.Authentication.Basic)),
			p.Basic)
	}

	if provider.Authentication.Token != nil {
		p.Token = SecretData{}

		AddReferencedResource(p.Composite,
			NewObjectReference[*corev1.Secret](
				WithObjectReferenceOptions(p.objectReferenceOptions),
				FromSecretKeySelector(*provider.Authentication.Token)),
			p.Token)
	}

	return p.Load(ctx)
}

// NamedServiceProvider is a collection resource that expands and consumes a set
// of named v2alpha2.ServiceProvider.
type NamedServiceProvider = NamedResources[*v2alpha2.ServiceProvider, *ServiceProvider]

// NewNamedServiceProvider creates a new NamedServiceProvider for the desired
// endpoint type. It uses the provided options to configure how to resolve
// ObjectReferences, including referenced Kubernetes Services and Secrets.
func NewNamedServiceProvider(endpointType ServiceEndpointType, options ...ObjectReferenceOption) *NamedServiceProvider {
	return NewNamedResources(func(ctx context.Context) *ServiceProvider {
		return NewServiceProvider(endpointType, options...)
	})
}
