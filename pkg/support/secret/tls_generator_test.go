package secret

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"net"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("TLSGenerator", func() {
	var (
		fixedTime time.Time
	)

	BeforeEach(func() {
		fixedTime = time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
		nowFunc = func() time.Time { return fixedTime }
	})

	AfterEach(func() {
		nowFunc = time.Now
	})

	It("should generate valid self-signed TLS certificate and private key", func() {
		generator := &TLSGenerator{
			Algorithm:  RSA,
			Size:       2048,
			Lifespan:   24 * time.Hour,
			CommonName: "test.example.com",
			Hosts:      []string{"test.example.com", "192.168.1.1"},
		}

		payload, err := generator.Generate()

		Expect(err).NotTo(HaveOccurred())
		Expect(payload[TLSCertificate]).NotTo(BeEmpty())
		Expect(payload[TLSPrivateKey]).NotTo(BeEmpty())

		// Verify certificate
		block, _ := pem.Decode(payload[TLSCertificate])
		Expect(block).NotTo(BeNil())
		Expect(block.Type).To(Equal("CERTIFICATE"))

		cert, err := x509.ParseCertificate(block.Bytes)
		Expect(err).NotTo(HaveOccurred())
		Expect(cert.Subject.CommonName).To(Equal("test.example.com"))
		Expect(cert.Issuer.CommonName).To(Equal("test.example.com"))
		Expect(cert.NotBefore).To(Equal(fixedTime))
		Expect(cert.NotAfter).To(Equal(fixedTime.Add(24 * time.Hour)))
		Expect(cert.IsCA).To(BeTrue())
		Expect(cert.KeyUsage & x509.KeyUsageDigitalSignature).NotTo(BeZero())
		Expect(cert.KeyUsage & x509.KeyUsageCertSign).NotTo(BeZero())
		Expect(cert.KeyUsage & x509.KeyUsageKeyEncipherment).NotTo(BeZero())
		Expect(cert.ExtKeyUsage).To(ContainElement(x509.ExtKeyUsageServerAuth))
		Expect(cert.DNSNames).To(ContainElement("test.example.com"))
		Expect(cert.IPAddresses).To(ContainElement(net.ParseIP("192.168.1.1").To4()))
	})

	It("should handle multiple host names and IPs", func() {
		generator := &TLSGenerator{
			Algorithm:  RSA,
			Size:       2048,
			Lifespan:   24 * time.Hour,
			CommonName: "test.example.com",
			Hosts: []string{
				"test1.example.com",
				"test2.example.com",
				"192.168.1.1",
				"192.168.1.2",
			},
		}

		payload, err := generator.Generate()
		Expect(err).NotTo(HaveOccurred())

		block, _ := pem.Decode(payload[TLSCertificate])
		Expect(block).NotTo(BeNil())

		cert, err := x509.ParseCertificate(block.Bytes)
		Expect(err).NotTo(HaveOccurred())
		Expect(cert.DNSNames).To(ConsistOf("test1.example.com", "test2.example.com"))
		Expect(cert.IPAddresses).To(ConsistOf(
			net.ParseIP("192.168.1.1").To4(),
			net.ParseIP("192.168.1.2").To4(),
		))
	})

	DescribeTable("should work with different algorithms",
		func(algo Algorithm, size int, keyType any) {
			generator := &TLSGenerator{
				Algorithm:  algo,
				Size:       size,
				Lifespan:   24 * time.Hour,
				CommonName: "test.example.com",
			}

			payload, err := generator.Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(payload[TLSPrivateKey]).NotTo(BeEmpty())
			Expect(payload[TLSCertificate]).NotTo(BeEmpty())

			block, _ := pem.Decode(payload[TLSPrivateKey])
			Expect(block).NotTo(BeNil())
			Expect(block.Type).To(Equal("PRIVATE KEY"))

			key, err := x509.ParsePKCS8PrivateKey(block.Bytes)
			Expect(err).NotTo(HaveOccurred())
			Expect(key).To(BeAssignableToTypeOf(keyType))
		},
		Entry("RSA", RSA, 2048, &rsa.PrivateKey{}),
		Entry("ECDSA", ECDSA, 256, &ecdsa.PrivateKey{}),
		Entry("ED25519", ED25519, 0, ed25519.PrivateKey{}))
})
