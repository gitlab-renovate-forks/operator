package controllerutils

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

// PatchObjects performs a patch operation on Objects with the given PatchObjects algorithm
// and Options. By default it uses Apply patch which is suitable for server-side
// apply (SSA).
//
// Note that not all object types are safe for SSA. Only Unstructured types can
// be used for SSA in their original form.
func PatchObjects(rtCtx *framework.RuntimeContext, objects inventory.Inventory[client.Object], opts ...TaskOption) ([]client.Object, error) {
	t := &PatchObjectsTask{
		Objects: objects,
	}
	t.ApplyOptions(opts...)

	return t.objects, t.Execute(rtCtx)
}

type PatchObjectsTask struct {
	Register    framework.Register
	Objects     inventory.Inventory[client.Object]
	Patch       client.Patch
	Options     *client.PatchOptions
	IgnoreError func(error) bool

	objects []client.Object
}

func (t *PatchObjectsTask) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		case patchAlgorithm:
			t.Patch = o
		case patchOptions:
			t.Options = &o.PatchOptions
		}
	}
}

func (t *PatchObjectsTask) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *PatchObjectsTask) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	patchToUse := client.Apply
	if t.Patch != nil {
		patchToUse = t.Patch
	}

	optionsToUse := &defaultPatchOptions
	if t.Options != nil {
		optionsToUse = t.Options
	}

	t.objects, err = t.Objects.Get(rtCtx)
	if err != nil {
		return nil, err
	}

	for _, o := range t.objects {
		if err := framework.Client.Patch(rtCtx, o, patchToUse, optionsToUse); err != nil {
			if t.IgnoreError != nil && !t.IgnoreError(err) {
				continue
			}

			return nil, err
		}
	}

	return t.objects, nil
}

var (
	defaultPatchOptions = client.PatchOptions{
		FieldManager: fieldManager,
		Force:        utils.Ptr(true),
	}
)

const (
	fieldManager = "gitlab-operator"
)
