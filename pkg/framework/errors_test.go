package framework

import (
	"errors"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Errors", func() {
	Describe("Requeue", func() {
		It("returns a requeue error with no delay", func() {
			err := Requeue()
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal("requeue request after 0s"))
		})
	})

	Describe("RequeueWithDelay", func() {
		It("returns a requeueError with the specified delay", func() {
			err := RequeueWithDelay(5 * time.Second)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal("requeue request after 5s"))
		})
	})

	Describe("Terminate", func() {
		It("returns a terminal error", func() {
			err := Terminate(errors.New("test error"))
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal("terminal error: test error"))
		})
	})

	Describe("Expand", func() {
		Context("when given a nil error", func() {
			It("returns an empty Result and nil error", func() {
				result, err := Expand(nil)
				Expect(result.IsZero()).To(BeTrue())
				Expect(err).To(BeNil())
			})
		})

		Context("when given a regular error", func() {
			It("returns an empty Result and the original error", func() {
				testErr := errors.New("test error")
				result, err := Expand(testErr)
				Expect(result.IsZero()).To(BeTrue())
				Expect(err).To(BeIdenticalTo(testErr))
			})
		})

		Context("when given an error from Requeue", func() {
			It("returns a Result with Requeue true and nil error", func() {
				result, err := Expand(Requeue())
				Expect(result).To(Equal(reconcile.Result{Requeue: true}))
				Expect(err).To(BeNil())
			})
		})

		Context("when given an error from RequeueWithDelay", func() {
			It("returns a Result with RequeueAfter set and nil error", func() {
				delay := 5 * time.Second
				result, err := Expand(RequeueWithDelay(delay))
				Expect(result).To(Equal(reconcile.Result{RequeueAfter: delay}))
				Expect(err).To(BeNil())
			})
		})
	})
})
