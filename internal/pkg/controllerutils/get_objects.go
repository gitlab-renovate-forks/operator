package controllerutils

import (
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// GetObjects retrieves objects of type T with the given names.
func GetObjects[T client.Object](rtCtx *framework.RuntimeContext, kind T, names []types.NamespacedName, opts ...TaskOption) ([]T, error) {
	t := &GetObjectsTask[T]{
		Names: names,
		Kind:  kind,
	}
	t.ApplyOptions(opts...)

	return t.objects, t.Execute(rtCtx)
}

type GetObjectsTask[T client.Object] struct {
	Register    framework.Register
	Names       []types.NamespacedName
	Kind        client.Object
	IgnoreError IgnoreErrorFunc

	objects []T
}

func (t *GetObjectsTask[T]) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		}
	}
}

func (t *GetObjectsTask[T]) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *GetObjectsTask[T]) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	t.objects = make([]T, 0, len(t.Names))

	for _, key := range t.Names {
		obj := t.Kind.DeepCopyObject().(T)
		if err := framework.Client.Get(rtCtx, key, obj); err != nil {
			if t.IgnoreError != nil && t.IgnoreError(err) {
				continue
			}

			return nil, err
		}

		t.objects = append(t.objects, obj)
	}

	return t.objects, nil
}
