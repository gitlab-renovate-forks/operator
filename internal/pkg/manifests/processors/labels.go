package processors

import (
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

// WithLabels sets the common labels for a workload object.
//
// It will also set the annotations on the pod template for:
//   - Deployment
//   - StatefulSet
//   - Job
func WithLabels(labels map[string]string) func(client.Object) error {
	return func(obj client.Object) error {
		existing := obj.GetLabels()
		if existing == nil {
			existing = make(map[string]string)
		}

		for k, v := range labels {
			existing[k] = v
		}

		obj.SetLabels(existing)

		switch x := obj.(type) {
		case *appsv1.Deployment:
			x.Spec.Template.Labels = utils.MergeMap(x.Spec.Template.Labels, labels)
		case *appsv1.StatefulSet:
			x.Spec.Template.Labels = utils.MergeMap(x.Spec.Template.Labels, labels)
		case *batchv1.Job:
			x.Spec.Template.Labels = utils.MergeMap(x.Spec.Template.Labels, labels)
		}

		return nil
	}
}

// WithAnnotations sets the common annotations for a workload object.
//
// It will also set the annotations on the pod template for:
//   - Deployment
//   - StatefulSet
//   - Job
func WithAnnotations(annotations map[string]string) func(client.Object) error {
	return func(obj client.Object) error {
		existing := obj.GetAnnotations()
		if existing == nil {
			existing = make(map[string]string)
		}

		for k, v := range annotations {
			existing[k] = v
		}

		obj.SetAnnotations(existing)

		switch x := obj.(type) {
		case *appsv1.Deployment:
			x.Spec.Template.Annotations = utils.MergeMap(x.Spec.Template.Annotations, annotations)
		case *appsv1.StatefulSet:
			x.Spec.Template.Annotations = utils.MergeMap(x.Spec.Template.Annotations, annotations)
		case *batchv1.Job:
			x.Spec.Template.Annotations = utils.MergeMap(x.Spec.Template.Annotations, annotations)
		}

		return nil
	}
}

// WithSelectorLabels sets the selector labels for:
//   - Deployment
//   - StatefulSet
//   - Job
//   - Service
//
// It is a no-op for other objects.
func WithSelectorLabels(selectorLabels map[string]string) func(client.Object) error {
	return func(workload client.Object) error {
		switch x := workload.(type) {
		case *appsv1.Deployment:
			x.Spec.Selector = setSelector(x.Spec.Selector, selectorLabels)
		case *appsv1.StatefulSet:
			x.Spec.Selector = setSelector(x.Spec.Selector, selectorLabels)
		case *batchv1.Job:
			x.Spec.Selector = setSelector(x.Spec.Selector, selectorLabels)
		case *corev1.Service:
			x.Spec.Selector = utils.MergeMap(x.Spec.Selector, selectorLabels)
		}

		return nil
	}
}

func setSelector(sel *metav1.LabelSelector, labels map[string]string) *metav1.LabelSelector {
	if sel == nil {
		sel = &metav1.LabelSelector{}
	}

	for k, v := range labels {
		metav1.AddLabelToSelector(sel, k, v)
	}

	return sel
}
