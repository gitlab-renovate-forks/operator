package inventory

import (
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// ObjectDecorator is a context-aware decorator for Kubernetes objects. It
// applies a predefined mutation on any given object with provided context
// and/or additional parameters.
type ObjectDecorator func(*framework.RuntimeContext, client.Object) error

// Decorate function wraps the original inventory with the additional decorators.
// Note that the decorators are applied in the order they are provided. Once
// used the objects in the original Inventory are modified. As such it is very
// important that for the decorators to be idempotent.
func Decorate[T client.Object](original Inventory[T], decorators ...ObjectDecorator) Inventory[T] {
	return InventoryFunc[T](func(ctx *framework.RuntimeContext) ([]T, error) {
		objects, err := original.Get(ctx)
		if err != nil {
			return nil, err
		}

		for i, obj := range objects {
			for _, decorator := range decorators {
				if err := decorator(ctx, obj); err != nil {
					return nil, err
				}
			}

			objects[i] = obj
		}

		return objects, nil
	})
}

// WithNamespace creates an ObjectDecorator that sets the namespace of the
// object.
func WithNamespace(namespace string) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		obj.SetNamespace(namespace)

		return nil
	}
}

// WithName creates an ObjectDecorator that sets the name of the object.
func WithName(name string) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		obj.SetName(name)

		return nil
	}
}

// WithNamespacedName creates an ObjectDecorator that sets the namespace and
// name of the object.
func WithNamespacedName(key types.NamespacedName) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		obj.SetName(key.Name)
		obj.SetNamespace(key.Namespace)

		return nil
	}
}

// WithLabelsFrom creates an ObjectDecorator that copies labels from a source
// object to the target object.
func WithLabelsFrom(src client.Object) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		labels := src.GetLabels()
		if labels == nil {
			return nil
		}

		existing := obj.GetLabels()
		if existing == nil {
			existing = make(map[string]string)
		}

		for k, v := range labels {
			existing[k] = v
		}

		obj.SetLabels(existing)

		return nil
	}
}

// WithAnnotationsFrom creates an ObjectDecorator that copies annotations from a
// source object to the target object.
func WithAnnotationsFrom(src client.Object) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		annotations := src.GetAnnotations()
		if annotations == nil {
			return nil
		}

		existing := obj.GetAnnotations()
		if existing == nil {
			existing = make(map[string]string)
		}

		for k, v := range annotations {
			existing[k] = v
		}

		obj.SetAnnotations(existing)

		return nil
	}
}

// WithControllerReference creates an ObjectDecorator that sets the owner
// reference of the target object to the provided owner object.
func WithOwnerReference(owner client.Object) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		return controllerutil.SetOwnerReference(owner, obj, framework.Scheme)
	}
}

// WithControllerReference creates an ObjectDecorator that sets the owner
// reference of the target object to the provided owner object, making it a
// controller reference.
func WithControllerReference(owner client.Object) ObjectDecorator {
	return func(rtCtx *framework.RuntimeContext, obj client.Object) error {
		return controllerutil.SetControllerReference(owner, obj, framework.Scheme)
	}
}
