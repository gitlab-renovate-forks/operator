package applicationconfig

import (
	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var railsSecretsGenerator = &inventory.SecretGenerator{
	SecretType: corev1.SecretTypeOpaque,
	Generators: []secret.Generator{
		shapeRailsSecrets(
			[]secret.Generator{
				&secret.PrivateKeyGenerator{
					Algorithm: secret.RSA,
					Size:      2048,
					Names: []string{
						"openid_connect_signing_key",
					},
				},
				&secret.SequenceGenerator{
					Length:        128,
					CharacterSets: []secret.CharacterSet{secret.Hexadecimal},
					Names: []string{
						"secret_key_base",
						"otp_key_base",
						"db_key_base",
						"encrypted_settings_key_base",
					},
				},
				&secret.SequenceGenerator{
					Length:        32,
					CharacterSets: []secret.CharacterSet{secret.AlphaNumeric},
					Names: []string{
						"active_record_encryption_primary_key",
						"active_record_encryption_deterministic_key",
						"active_record_encryption_key_derivation_salt",
					},
				},
			},
			railsSecretYamlName,
			settings.Get().Rails.Environment,
		),
	},
	GenerateIfNeeded: true,
}

const (
	railsSecretYamlName = "secrets.yml"
)
