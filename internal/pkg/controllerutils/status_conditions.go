package controllerutils

import (
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// SetStatusCondition sets the status condition in object status.
func SetStatusCondition(rtCtx *framework.RuntimeContext, obj client.Object, conditions *[]metav1.Condition, newCondition metav1.Condition) error {
	newCondition.ObservedGeneration = obj.GetGeneration()

	return PatchStatus(rtCtx, obj, func() bool {
		return meta.SetStatusCondition(conditions, newCondition)
	})
}

// ClearStatusCondition clears the status condition in object status.
func ClearStatusCondition(rtCtx *framework.RuntimeContext, obj client.Object, conditions *[]metav1.Condition, contType string) error {
	return PatchStatus(rtCtx, obj, func() bool {
		return meta.RemoveStatusCondition(conditions, contType)
	})
}
