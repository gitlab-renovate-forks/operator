package controllerutils

import (
	"fmt"
	"slices"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// DeploymentHasCondition reads the specified Deployment resource and checks if
// its status Condition has the specified condition type and status.
func DeploymentHasCondition(deployment *appsv1.Deployment, condType appsv1.DeploymentConditionType, condStatus metav1.ConditionStatus) framework.Condition {
	return framework.ConditionFunc(func(rtCtx *framework.RuntimeContext) (bool, error) {
		cond := findDeploymentCondition(deployment, condType)
		if cond == nil {
			return false, nil
		}

		return string(cond.Status) == string(condStatus), nil
	})
}

// JobCompleted checks if a Job has completed successfully.
// See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.28/#jobstatus-v1-batch
func JobCompleted(job *batchv1.Job) framework.Condition {
	return framework.ConditionFunc(func(rtCtx *framework.RuntimeContext) (bool, error) {
		return condJobComplete(job)
	})
}

func findDeploymentCondition(deployment *appsv1.Deployment, condType appsv1.DeploymentConditionType) *appsv1.DeploymentCondition {
	if deployment.Status.Conditions == nil {
		return nil
	}

	for _, cond := range deployment.Status.Conditions {
		if cond.Type == condType {
			return &cond
		}
	}

	return nil
}

func condJobComplete(job *batchv1.Job) (bool, error) {
	if slices.ContainsFunc(
		job.Status.Conditions,
		func(c batchv1.JobCondition) bool {
			return c.Type == batchv1.JobComplete && c.Status == corev1.ConditionTrue
		},
	) { //nolint:whitespace
		return true, nil
	}

	if idx := slices.IndexFunc(
		job.Status.Conditions,
		func(c batchv1.JobCondition) bool {
			return c.Type == batchv1.JobFailed && c.Status == corev1.ConditionTrue
		},
	); idx >= 0 {
		return false, reconcile.TerminalError(fmt.Errorf("job %s/%s %s: %s", job.Namespace, job.Name, job.Status.Conditions[idx].Reason, job.Status.Conditions[idx].Message))
	}

	return false, nil
}
