---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# SSHServer

The `SSHServer` manages an SSH server configured to access Git repositories
by using SSH. It supports `openssh` and `gitlab-sshd` daemons.

The controller of the `SSHServer` is not implemented. Support for the
controller of the `SSHServer` is tracked in [issue 110](https://gitlab.com/gitlab-org/cloud-native/operator/-/issues/110).

## Installation

To create your own `SSHServer` custom resource, you can adapt
the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_sshserver.yaml).

For all available options, see the [`SSHServer` API reference](../generated/api-docs.md#sshserver).
