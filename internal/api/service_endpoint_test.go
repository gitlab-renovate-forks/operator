package api

import (
	"context"
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var _ = Describe("ServiceEndpoint", func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		service    *corev1.Service
	)

	fakeScheme = runtime.NewScheme()
	utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

	Context("NewServiceEndpointReference", func() {
		It("creates a reference for a service-backed endpoint", func() {
			endpoint := &v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-service",
					Namespace: "default",
				},
			}

			ref := NewServiceEndpointReference(endpoint, PostgreSQLServiceEndpoint)

			Expect(ref).NotTo(BeNil())
			Expect(fmt.Sprintf("%s", ref)).To(Equal("v1/Service[default/test-service]"))
		})

		It("creates a reference for an external endpoint", func() {
			endpoint := &v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "example.com",
					Port: 5432,
				},
			}

			ref := NewServiceEndpointReference(endpoint, PostgreSQLServiceEndpoint)

			Expect(ref).NotTo(BeNil())
			Expect(fmt.Sprintf("%s", ref)).To(Equal("ServiceEndpoint<PostgreSQL>"))
		})
	})

	Context("Resolve", func() {
		BeforeEach(func() {
			service = &corev1.Service{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-service",
					Namespace: "default",
				},
				Spec: corev1.ServiceSpec{
					Ports: []corev1.ServicePort{
						{Name: "postgresql", Port: 5432},
						{Name: "redis", Port: 6379},
						{Name: "clickhouse", Port: 8123},
					},
				},
			}

			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(service).
				Build()

			framework.Client = fakeClient
			framework.Scheme = fakeScheme
		})

		AfterEach(func() {
			framework.Scheme = nil
			framework.Client = nil
		})

		It("successfully resolves the service-backed endpoint", func() {
			endpoint := &v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-service",
					Namespace: "default",
				},
			}

			ref := NewServiceEndpointReference(endpoint, PostgreSQLServiceEndpoint)
			resolved, err := ref.Resolve(context.Background())

			Expect(err).NotTo(HaveOccurred())
			Expect(resolved).NotTo(BeNil())
			Expect(resolved.Service).NotTo(BeNil())
			Expect(resolved.Service.Name).To(Equal("test-service"))
			Expect(resolved.Type).To(Equal(PostgreSQLServiceEndpoint))
		})

		It("returns an error for ambiguous ServiceEndpoint", func() {
			endpoint := &v2alpha2.ServiceEndpoint{}

			ref := NewServiceEndpointReference(endpoint, PostgreSQLServiceEndpoint)
			_, err := ref.Resolve(context.Background())

			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(ContainSubstring("ambagious ServiceEndpoint"))
		})
	})

	Context("Consume", func() {
		It("consumes a service-backed PostgreSQL endpoint", func() {
			carrier := &ServiceEndpointCarrier{
				ServiceEndpoint: &v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "test-service",
						Namespace: "default",
					},
				},
				Service: service,
				Type:    PostgreSQLServiceEndpoint,
			}

			endpoint := &ServiceEndpoint{}
			err := endpoint.Consume(context.Background(), carrier)

			Expect(err).NotTo(HaveOccurred())
			Expect(endpoint.Type).To(Equal(PostgreSQLServiceEndpoint))
			Expect(endpoint.Scheme).To(Equal("postgresql"))
			Expect(endpoint.Host).To(Equal("test-service.default.svc.cluster.local"))
			Expect(endpoint.Port).To(Equal(int32(5432)))
			Expect(endpoint.TLS).To(BeFalse())
		})

		It("consumes an external Redis endpoint with TLS", func() {
			carrier := &ServiceEndpointCarrier{
				ServiceEndpoint: &v2alpha2.ServiceEndpoint{
					External: &v2alpha2.ExternalServiceEndpoint{
						Host: "redis.example.com",
						Port: 6380,
					},
					TLS: utils.Ptr(true),
				},
				Type: RedisServiceEndpoint,
			}

			endpoint := &ServiceEndpoint{}
			err := endpoint.Consume(context.Background(), carrier)

			Expect(err).NotTo(HaveOccurred())
			Expect(endpoint.Type).To(Equal(RedisServiceEndpoint))
			Expect(endpoint.Scheme).To(Equal("rediss"))
			Expect(endpoint.Host).To(Equal("redis.example.com"))
			Expect(endpoint.Port).To(Equal(int32(6380)))
			Expect(endpoint.TLS).To(BeTrue())
		})

		It("uses default port when not specified in Service", func() {
			carrier := &ServiceEndpointCarrier{
				ServiceEndpoint: &v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "test-service",
						Namespace: "default",
					},
				},
				Service: service,
				Type:    GitalyServiceEndpoint,
			}

			endpoint := &ServiceEndpoint{}
			err := endpoint.Consume(context.Background(), carrier)

			Expect(err).NotTo(HaveOccurred())
			Expect(endpoint.Type).To(Equal(GitalyServiceEndpoint))
			Expect(endpoint.Scheme).To(Equal("tcp"))
			Expect(endpoint.Host).To(Equal("test-service.default.svc.cluster.local"))
			Expect(endpoint.Port).To(Equal(int32(8075))) // Default port for Gitaly
			Expect(endpoint.TLS).To(BeFalse())
		})
	})
})
