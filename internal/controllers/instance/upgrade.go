package instance

import (
	"errors"
	"fmt"
	"strings"

	corev1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// Calculates the next version to upgrade to, in order to upgrade from
// current version to the desired version.
type UpgradePlanner = func(current, desired string, edition v2alpha2.Edition) string

type UpgradeHandlerFactory = func(adapter *Adapter, version string, edition v2alpha2.Edition) UpgradeHandler

type UpgradeState string

// UpgradeHandler is a function that performs an upgrade. It starts from a given
// state and transitions the upgrade to a new state or fails.
type UpgradeHandler interface {
	Upgrade(*framework.RuntimeContext, UpgradeState) (UpgradeState, error)
}

// ZeroDowntimeUpgrade is a UpgradeHandler that performs a zero-downtime upgrade.
// for GitLab Rails application. It uses the following procedure:
//
//  1. Pause workloads and set the desired version details
//  2. Run database migrations but skip post-migrations. This is referred to as
//     "pre" migrations (not to confused with pre-migrations).
//  3. Wait for the database migrations to finish.
//  4. When database migrations fail to complete successfully, rollback the
//     workload to the current version details, but keep them paused so it
//     does not inadvertently cause an outage.
//  5. Resume workloads and instruct them to skip database scheme version
//     checking. The database schema is not fully upgraded yet.
//  6. Wait for workloads, that now run on the desired version, to become
//     available.
//  7. Run full database migrations.
//  8. Wait for the database migrations to finish successfully.
//  9. Instruct workloads to resume database scheme version checking.
//  10. Wait for workloads to become available.
//
// Important assumptions:
//
//  1. Workload specification does not change while the upgrade is in progress.
//     Instance controller by itself has can do very little to disallow it.
//  2. Workload does not disassociate from an Instance and re-associate to a
//     different Instance while the upgrade is in progress.
//
// Known limitations:
//  1. After "pre" migrations controller can not rollback a failed upgrade. It
//     simply panics and freezes. Resuming the upgrade requires manual
//     intervention, such as deleting and recreating DatabaseMigrations.
//  2. Currently the Instance does not cleanup the DatabaseMigrations that it
//     creates.
type ZeroDowntimeUpgrade struct {
	adapter *Adapter
	version string
	edition v2alpha2.Edition
}

// Zero downtime upgrade states.
const (
	UpgradeStateStart                   = "Start"
	UpgradeStatePreMigrationsStarted    = "PreUpgradeMigrationsStarted"
	UpgradeStatePreMigrationsCompleted  = "PreUpgradeMigrationsCompleted"
	UpgradeStateNewVersionRolling       = "NewVersionRolling"
	UpgradeStatePostMigrationsStarted   = "PostUpgradeMigrationsStarted"
	UpgradeStatePostMigrationsCompleted = "PostUpgradeMigrationsCompleted"
	UpgradeStateFinalizingNewVersion    = "FinalizingNewVersion"
	UpgradeStateCompleted               = "Completed"
)

func NewZeroDowntimeUpgrade(adapter *Adapter, version string, edition v2alpha2.Edition) UpgradeHandler {
	return &ZeroDowntimeUpgrade{
		adapter: adapter,
		version: version,
		edition: getEdition(edition),
	}
}

func (h *ZeroDowntimeUpgrade) Upgrade(rtCtx *framework.RuntimeContext, lastState UpgradeState) (UpgradeState, error) {
	state := lastState

	if state == UpgradeStateStart {
		if err := h.startMigration(rtCtx, preUpgradeMigration); err != nil {
			return state, err
		}

		state = UpgradeStatePreMigrationsStarted
	}

	if state == UpgradeStatePreMigrationsStarted {
		if err := h.checkMigration(rtCtx, preUpgradeMigration); err != nil {
			return state, err
		}

		state = UpgradeStatePreMigrationsCompleted
	}

	if state == UpgradeStatePreMigrationsCompleted {
		if err := h.rolloutNewVersion(rtCtx); err != nil {
			return state, err
		}

		state = UpgradeStateNewVersionRolling
	}

	if state == UpgradeStateNewVersionRolling {
		if err := h.startMigration(rtCtx, allMigrations); err != nil {
			return state, err
		}

		state = UpgradeStatePostMigrationsStarted
	}

	if state == UpgradeStatePostMigrationsStarted {
		if err := h.checkMigration(rtCtx, allMigrations); err != nil {
			return state, err
		}

		state = UpgradeStatePostMigrationsCompleted
	}

	if state == UpgradeStatePostMigrationsCompleted {
		if err := h.finalizeNewVersion(rtCtx); err != nil {
			return state, err
		}

		state = UpgradeStateFinalizingNewVersion
	}

	if state == UpgradeStateFinalizingNewVersion {
		rtCtx.Logger.Info("upgrade is almost done, waiting for workload to be ready",
			"version", h.version, "edition", h.edition)

		// NOTE: For complete support of upgrade path, this final transition
		//       must factor in the state of background migrations.
		if err := h.isWorkloadReady(rtCtx); err != nil {
			return state, err
		}

		rtCtx.Logger.Info("upgrade is completed",
			"version", h.version, "edition", h.edition)

		state = UpgradeStateCompleted
	}

	return state, nil
}

func (h *ZeroDowntimeUpgrade) startMigration(rtCtx *framework.RuntimeContext, kind dbMigrationKind) error {
	dbm, err := h.getDatabaseMigration(rtCtx, kind)
	if err != nil {
		return err
	}

	if dbm != nil {
		rtCtx.Logger.Info("database migration exists, checking if it is completed",
			"migrationType", kind.String(),
			"databaseMigrationName", dbm.Name)

		return nil
	}

	if err := h.isWorkloadReady(rtCtx); err != nil {
		return err
	}

	if kind == preUpgradeMigration {
		if err := h.pauseAndSetVersionDetails(rtCtx); err != nil {
			return err
		}
	}

	rtCtx.Logger.Info("starting database migration:",
		"migrationType", kind.String(),
		"databaseMigrationName", h.dbMigrationName(kind))

	if _, err = h.runDatabaseMigration(rtCtx, kind); err != nil {
		return err
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) checkMigration(rtCtx *framework.RuntimeContext, kind dbMigrationKind) error {
	dbm, err := h.getDatabaseMigration(rtCtx, kind)
	if err != nil {
		return err
	}

	if dbm == nil {
		return framework.Terminate(
			fmt.Errorf("missing database migration: %s", h.dbMigrationName(kind)))
	}

	if h.isMigrationFailed(dbm) {
		rtCtx.Logger.Info("database migration failed",
			"migrationType", kind.String(),
			"databaseMigrationName", dbm.Name)

		if kind == preUpgradeMigration {
			return h.revertAndTerminate(rtCtx, dbm.Name)
		}

		return framework.Terminate(
			fmt.Errorf("database migration failed, upgrade cannot be reverted: %s", dbm.Name))
	}

	if !h.isMigrationCompleted(dbm) {
		rtCtx.Logger.Info("database migration is still running, waiting for it to complete",
			"migrationType", kind.String(),
			"databaseMigrationName", dbm.Name)

		return framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) rolloutNewVersion(rtCtx *framework.RuntimeContext) error {
	if err := h.isWorkloadReady(rtCtx); err != nil {
		return err
	}

	return h.resumeAndIgnoreSchemaVersion(rtCtx)
}

func (h *ZeroDowntimeUpgrade) finalizeNewVersion(rtCtx *framework.RuntimeContext) error {
	if err := h.isWorkloadReady(rtCtx); err != nil {
		return err
	}

	return h.enableSchemaVersionCheck(rtCtx)
}

func (h *ZeroDowntimeUpgrade) isWorkloadReady(rtCtx *framework.RuntimeContext) error {
	if err := h.areApplicationServersReady(rtCtx); err != nil {
		return err
	}

	if err := h.areJobProcessorsReady(rtCtx); err != nil {
		return err
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) pauseAndSetVersionDetails(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("pausing workload and updating version details",
		"version", h.version,
		"edition", h.edition)

	return h.patchWorkload(rtCtx, "pause and set version details", h.doPauseAndSetVersionDetails)
}

func (h *ZeroDowntimeUpgrade) revertAndTerminate(rtCtx *framework.RuntimeContext, dbmName string) error {
	rtCtx.Logger.Info("database migration failed", "databaseMigrationName", dbmName)

	if err := h.patchWorkload(rtCtx, "revert version details", h.doRevertVersionDetails); err != nil {
		rtCtx.Logger.Error("failed to revert version details", "error", err)
	}

	return framework.Terminate(
		fmt.Errorf("database migration failed, upgrade cannot proceed: %s", dbmName))
}

func (h *ZeroDowntimeUpgrade) resumeAndIgnoreSchemaVersion(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("resuming workload and starting it on the new version",
		"version", h.version,
		"edition", h.edition)

	return h.patchWorkload(rtCtx, "resume and ignore schema version", h.doResumeAndIgnoreSchemaVersion)
}

// NOTE: Controller does not eagerly refresh the state of the workload. It makes
//       the following decisions based on the last observed state of the
//       workload, i.e. ApplicationServers and JobProcessors, at the time of
//       the initialization (see NewAdapter).
//
//       It may well be the case that while it was going through the upgrade
//       process, the workload was progressed and has become ready. It does not
//       cause any harm, but this makes the upgrade process slightly slower but
//       less complex.

func (h *ZeroDowntimeUpgrade) areApplicationServersReady(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("waiting for ApplicationServers workload to be ready")

	readyCount := 0

	for _, appServer := range h.adapter.ApplicationServers {
		if err := framework.Client.Get(rtCtx, client.ObjectKeyFromObject(appServer), appServer); err != nil {
			return err
		}

		cond := meta.FindStatusCondition(appServer.Status.Conditions, v2alpha2.ApplicationServerReadyCondition)
		if cond == nil {
			break
		}

		if cond.Status != metav1.ConditionTrue {
			break
		}

		readyCount++
	}

	if readyCount < len(h.adapter.ApplicationServers) {
		rtCtx.Logger.Info("workloads for ApplicationServers are not ready, waiting for them to become available",
			"readyCount", readyCount,
			"count", len(h.adapter.ApplicationServers))

		return framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)
	} else {
		rtCtx.Logger.Info("workloads for ApplicationServers are ready")
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) areJobProcessorsReady(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("waiting for JobProcessors workload to be ready")

	readyCount := 0

	for _, jobProcessor := range h.adapter.JobProcessors {
		if err := framework.Client.Get(rtCtx, client.ObjectKeyFromObject(jobProcessor), jobProcessor); err != nil {
			return err
		}

		cond := meta.FindStatusCondition(jobProcessor.Status.Conditions, v2alpha2.JobProcessorReadyCondition)
		if cond == nil {
			break
		}

		if cond.Status != metav1.ConditionTrue {
			break
		}

		readyCount++
	}

	if readyCount < len(h.adapter.JobProcessors) {
		rtCtx.Logger.Info("workloads for JobProcessors are not ready, waiting for them to become available",
			"readyCount", readyCount,
			"count", len(h.adapter.JobProcessors))

		return framework.RequeueWithDelay(Settings.Rails.Sidekiq.StatusCheckDelay)
	} else {
		rtCtx.Logger.Info("workloads for JobProcessors are ready")
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) enableSchemaVersionCheck(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Info("rolling out upgraded workload", "version", h.version, "edition", h.edition)

	return h.patchWorkload(rtCtx, "enable schema version check", h.doEnableSchemaVersionCheck)
}

func (h *ZeroDowntimeUpgrade) patchWorkload(rtCtx *framework.RuntimeContext, operation string, mutator func(*framework.RuntimeContext, client.Object) error) error {
	var errs []error

	for _, appServer := range h.adapter.ApplicationServers {
		err := mutator(rtCtx, appServer)
		if err != nil {
			errs = append(errs,
				fmt.Errorf("failed to %s for ApplicationServer %s/%s: %w",
					operation, appServer.Namespace, appServer.Name, err))
		}
	}

	for _, jobProcessor := range h.adapter.JobProcessors {
		err := mutator(rtCtx, jobProcessor)
		if err != nil {
			errs = append(errs,
				fmt.Errorf("failed to %s for JobProcessor %s/%s: %w",
					operation, jobProcessor.Namespace, jobProcessor.Name, err))
		}
	}

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil
}

func (h *ZeroDowntimeUpgrade) doPauseAndSetVersionDetails(rtCtx *framework.RuntimeContext, workload client.Object) error {
	rtCtx.Logger.Debug("pausing workload and setting its version to target version",
		"kind", workload.GetObjectKind().GroupVersionKind().Kind,
		"name", fmt.Sprintf("%s/%s", workload.GetNamespace(), workload.GetName()),
		"targetVersion", h.version,
		"targetEdition", h.edition)

	patch := fmt.Sprintf("{%q: {%q:%t, %q:%q, %q:%q}}",
		"spec",
		"paused", true,
		"version", h.version,
		"edition", h.edition)

	return framework.Client.Patch(rtCtx, workload, client.RawPatch(
		types.MergePatchType, []byte(patch)))
}

func (h *ZeroDowntimeUpgrade) doRevertVersionDetails(rtCtx *framework.RuntimeContext, workload client.Object) error {
	rtCtx.Logger.Debug("reverting workload and to original version",
		"kind", workload.GetObjectKind().GroupVersionKind().Kind,
		"name", fmt.Sprintf("%s/%s", workload.GetNamespace(), workload.GetName()),
		"originalVersion", h.version,
		"originalEdition", h.edition)

	patch := fmt.Sprintf("{%q: {%q:%t, %q:%q, %q:%q}}",
		"spec",
		"paused", true,
		"version", h.adapter.CurrentVersion,
		"edition", h.adapter.CurrentEdition)

	return framework.Client.Patch(rtCtx, workload, client.RawPatch(
		types.MergePatchType, []byte(patch)))
}

func (h *ZeroDowntimeUpgrade) doResumeAndIgnoreSchemaVersion(rtCtx *framework.RuntimeContext, workload client.Object) error {
	rtCtx.Logger.Debug("resuming workload and ignoring schema version check",
		"kind", workload.GetObjectKind().GroupVersionKind().Kind,
		"name", fmt.Sprintf("%s/%s", workload.GetNamespace(), workload.GetName()))

	patch := fmt.Sprintf("{%q: {%q:%t, %q: {%q: [{%q: %q, %q: [{%q: %q, %q: %q}]}]}}}",
		"spec",
		"paused", false,
		"workload", "managedContainers",
		"name", "dependencies",
		"env",
		"name", "BYPASS_SCHEMA_VERSION",
		"value", "true")

	// CAVEAT: The following operation uses MergePatch which can override existing
	//         managed containers. This must be changed to Apply patch.
	return framework.Client.Patch(rtCtx, workload, client.RawPatch(
		types.MergePatchType, []byte(patch)))
}

func (h *ZeroDowntimeUpgrade) doEnableSchemaVersionCheck(rtCtx *framework.RuntimeContext, workload client.Object) error {
	rtCtx.Logger.Debug("re-enabling schema version check for workload",
		"kind", workload.GetObjectKind().GroupVersionKind().Kind,
		"name", fmt.Sprintf("%s/%s", workload.GetNamespace(), workload.GetName()))

	patch := fmt.Sprintf("{%q: {%q: {%q: [{%q: %q, %q: [{%q: %q, %q: %q}]}]}}}",
		"spec", "workload", "managedContainers",
		"name", "dependencies",
		"env",
		"name", "BYPASS_SCHEMA_VERSION",
		"value", "false")

	// CAVEAT: The following operation uses MergePatch which can override existing
	//         managed containers. This must be changed to Apply patch.
	return framework.Client.Patch(rtCtx, workload, client.RawPatch(
		types.MergePatchType, []byte(patch)))
}

func (h *ZeroDowntimeUpgrade) getDatabaseMigration(rtCtx *framework.RuntimeContext, kind dbMigrationKind) (*v2alpha2.DatabaseMigration, error) {
	dbm := &v2alpha2.DatabaseMigration{}
	ref := client.ObjectKey{
		Name:      h.dbMigrationName(kind),
		Namespace: h.adapter.Namespace,
	}

	if err := framework.Client.Get(rtCtx, ref, dbm); err != nil {
		if kerrors.IsNotFound(err) {
			return nil, nil
		}

		return nil, err
	}

	return dbm, nil
}

func (h *ZeroDowntimeUpgrade) runDatabaseMigration(rtCtx *framework.RuntimeContext, kind dbMigrationKind) (*v2alpha2.DatabaseMigration, error) {
	dbm := &v2alpha2.DatabaseMigration{
		ObjectMeta: metav1.ObjectMeta{
			Name:      h.dbMigrationName(kind),
			Namespace: h.adapter.Namespace,
			Labels: map[string]string{
				instanceMarker:      h.adapter.Name,
				migrationKindMarker: kind.String(),
			},
		},
		Spec: v2alpha2.DatabaseMigrationSpec{
			Version: h.version,
			Edition: h.edition,
			ApplicationConfigRef: corev1.ObjectReference{
				Name:      h.adapter.ApplicationConfigName,
				Namespace: h.adapter.Namespace,
			},
			IsUpgrade:          true,
			SkipPostMigrations: kind == preUpgradeMigration,
		},
	}

	if err := controllerutil.SetControllerReference(h.adapter.source, dbm, framework.Scheme); err != nil {
		return nil, err
	}

	return dbm, framework.Client.Create(rtCtx, dbm)
}

func (h *ZeroDowntimeUpgrade) isMigrationCompleted(dbm *v2alpha2.DatabaseMigration) bool {
	if dbm == nil {
		return false
	}

	cond := meta.FindStatusCondition(dbm.Status.Conditions, v2alpha2.DatabaseMigrationCompletedCondition)
	if cond == nil {
		return false
	}

	return cond.Status == metav1.ConditionTrue
}

func (h *ZeroDowntimeUpgrade) isMigrationFailed(dbm *v2alpha2.DatabaseMigration) bool {
	if dbm == nil {
		return false
	}

	cond := meta.FindStatusCondition(dbm.Status.Conditions, v2alpha2.DatabaseMigrationCompletedCondition)
	if cond == nil {
		return false
	}

	return cond.Status == metav1.ConditionFalse && cond.Reason == v2alpha2.DatabaseMigrationFailedReason
}

func (h *ZeroDowntimeUpgrade) dbMigrationName(kind dbMigrationKind) string {
	suffix := ""
	if kind == preUpgradeMigration {
		suffix = "-pre"
	}

	return fmt.Sprintf("%s-%s-%s%s", h.adapter.Name, h.version,
		strings.ToLower(string(h.edition)), suffix)
}

// The dumb plan jumps straight to the desired version. The smart way to do
// it is to incorporate GitLab upgrade path.
//
// See: https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/
var dumbPlanner UpgradePlanner = func(_, desired string, _ v2alpha2.Edition) string {
	return desired
}

type dbMigrationKind int

func (k dbMigrationKind) String() string {
	switch k {
	case preUpgradeMigration:
		return "pre"
	case allMigrations:
		return "all"
	default:
		return "unknown"
	}
}

const (
	preUpgradeMigration dbMigrationKind = iota
	allMigrations
)
