#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    echo_stage "Installing Redis"

    gitlab_password=$(gen_random 64)

    kubectl -n $(namespace) create secret generic "redis-secret" --from-literal="password"="$gitlab_password"

    create_basic_auth_secret_raw "redis-auth" "" "$gitlab_password"

    helm install redis redis \
        --repo https://charts.bitnami.com/bitnami \
        -n $(namespace) \
        --create-namespace \
        --version 16.13.2 -f "$SCRIPT_DIR/manifests/values.redis.yaml"
}

uninstall() {
    echo_stage "Uninstalling Redis"

    helm uninstall redis -n $(namespace) || true

    kubectl -n $(namespace) delete secret redis-secret --ignore-not-found=true
    kubectl -n $(namespace) delete secret redis-auth --ignore-not-found=true
    kubectl -n $(namespace) delete pvc redis-data-redis-master-0 --ignore-not-found=true
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
