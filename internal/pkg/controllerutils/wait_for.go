package controllerutils

import (
	"errors"
	"time"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var ErrBreakpoint = errors.New("condition has reached its breaking point")

// WaitFor checks the condition and returns a requeue error if it is not met.
func WaitFor(rtCtx *framework.RuntimeContext, condition framework.Condition, opts ...TaskOption) error {
	t := &WaitForTask{
		Condition: condition,
	}
	t.ApplyOptions(opts...)

	return t.Execute(rtCtx)
}

type WaitForTask struct {
	Register    framework.Register
	Condition   framework.Condition
	Delay       time.Duration
	IgnoreError IgnoreErrorFunc
}

func (t *WaitForTask) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		case waitDelay:
			t.Delay = o.Duration
		}
	}
}

func (t *WaitForTask) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *WaitForTask) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	var checkBreakpoint = func(err error) bool {
		return err != ErrBreakpoint
	}

	if t.IgnoreError != nil {
		checkBreakpoint = t.IgnoreError
	}

	ok, err := t.Condition.Evaluate(rtCtx)

	if err != nil {
		if checkBreakpoint(err) {
			return nil, err
		} else {
			return nil, framework.Terminate(err)
		}
	}

	if ok {
		return nil, nil
	} else {
		return nil, framework.RequeueWithDelay(t.Delay)
	}
}
