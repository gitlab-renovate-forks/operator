package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	/* Condition Types */
	ApplicationServerReadyCondition   = "ApplicationServerReady"
	ApplicationServerPausedCondition  = "ApplicationServerPaused"
	ApplicationServerScalingCondition = "ApplicationServerScaling"

	/* Condition Reasons */
	PendingApplicationSecretsReason = "PendingApplicationSecrets"
	PendingApplicationServiceReason = "PendingApplicationService"

	AwaitingApplicationConfigReason = "AwaitingApplicationConfig"
	AwaitingResourcesReason         = "AwaitingResources"
	PendingStartReason              = "PendingStartup"
	ScalingReplicasReason           = "ScalingReplicas"
	WorkloadPausedReason            = "WorkloadPaused"
	WorkloadResumedReason           = "WorkloadResumed"
	WorkloadAvailableReason         = "WorkloadAvailable"
	WorkloadFailedReason            = "WorkloadFailed"

	/* Shared Object Usages */
	ApplicationSecretsObjectUsage = "ApplicationSecrets"
	ApplicationServiceObjectUsage = "ApplicationService"
)

// +kubebuilder:object:root=true
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:resource:shortName=appsrv
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=`.spec.version`
// +kubebuilder:printcolumn:name="Edition",type=string,JSONPath=`.spec.edition`
// +kubebuilder:printcolumn:name="Available",type="string",JSONPath=".status.replicas"
type ApplicationServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApplicationServerSpec   `json:"spec,omitempty"`
	Status ApplicationServerStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true
type ApplicationServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []ApplicationServer `json:"items"`
}

type ApplicationServerSpec struct {
	// +required
	// +kubebuilder:validation:Pattern=^([1-9]\d?)\.(0|[1-9]\d?)\.(0|[1-9]\d?)$
	Version string `json:"version"`

	// +optional
	Edition Edition `json:"edition,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	Replicas *int32 `json:"replicas,omitempty"`

	// +optional
	Paused bool `json:"paused,omitempty"`

	// +required
	ApplicationConfigRef corev1.ObjectReference `json:"applicationConfigRef"`

	// +optional
	Puma PumaSpec `json:"puma,omitempty"`

	// +optional
	Workhorse WorkhorseSpec `json:"workhorse,omitempty"`

	// +optional
	UploadVolume *corev1.EmptyDirVolumeSource `json:"uploadVolume,omitempty"`

	// +optional
	Tracing *TracingSupport `json:"tracing,omitempty"`

	// +optional
	Workload WorkloadSpec `json:"workload,omitempty"`
}

type ApplicationServerStatus struct {
	// +optional
	Version string `json:"version,omitempty"`

	// +optional
	Edition Edition `json:"edition,omitempty"`

	// +optional
	Replicas int32 `json:"replicas,omitempty"`

	// +optional
	Selector string `json:"selector,omitempty"`

	// +optional
	// +patchMergeKey=usage
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	SharedObjects []SharedObjectReference `json:"sharedObjects,omitempty" patchStrategy:"merge" patchMergeKey:"usage"`

	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

type PumaSpec struct {
	// +optional
	Metrics MetricsSupport `json:"metrics"`

	// +optional
	Settings PumaSettings `json:"settings"`

	// +optional
	TLS *TLSServerSupport `json:"tls,omitempty"`
}

type WorkhorseSpec struct {
	// +optional
	Metrics MetricsSupport `json:"metrics"`

	// +optional
	Settings WorkhorseSettings `json:"settings"`

	// +optional
	RedisName string `json:"redisName,omitempty"`

	// +optional
	ObjectStoreName string `json:"objectStoreName,omitempty"`

	// +optional
	TLS *TLSServerSupport `json:"tls,omitempty"`
}

type PumaSettings struct {
	// +optional
	EnableBootsnap *bool `json:"enableBootsnap,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	MinThreads *int32 `json:"minThreads,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	MaxThreads *int32 `json:"maxThreads,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	WorkerProcesses *int32 `json:"workerProcesses,omitempty"`

	// +optional
	WorkerTimeout *metav1.Duration `json:"workerTimeout,omitempty"`

	// +optional
	WorkerMaxMemory *resource.Quantity `json:"workerMaxMemory,omitempty"`

	// +optional
	DisableWorkerKiller *bool `json:"disableWorkerKiller,omitempty"`
}

type WorkhorseSettings struct {
	// +optional
	LogFormat LogFormat `json:"logFormat,omitempty"`

	// +optional
	// +kubebuilder:validation:Format=uri
	SentryDSN string `json:"sentryDSN,omitempty"`

	// +optional
	ShutdownTimeout *metav1.Duration `json:"shutdownTimeout,omitempty"`

	// +optional
	ShutdownBlackout *metav1.Duration `json:"shutdownBlackout,omitempty"`

	// +optional
	TrustedCIDRsForPropagation []string `json:"trustedCIDRsForPropagation,omitempty"`

	// +optional
	TrustedCIDRsForXForwardedFor []string `json:"trustedCIDRsForXForwardedFor,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	ScalerMaxProcesses *int32 `json:"scalerMaxProcesses,omitempty"`

	// +optional
	ScalerMaxFileSize *resource.Quantity `json:"scalerMaxFileSize,omitempty"`
}

func init() {
	SchemeBuilder.Register(&ApplicationServer{}, &ApplicationServerList{})
}
