package applicationserver

import (
	"context"
	"log/slog"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/client/interceptor"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

var _ = Describe("ApplicationServerController", Ordered, func() {
	var (
		fakeClient        client.WithWatch
		railsSecrets      *corev1.Secret
		appConfig         *v2alpha2.ApplicationConfig
		appServer         *v2alpha2.ApplicationServer
		referencedObjects []client.Object
		rtCtx             *framework.RuntimeContext
	)

	BeforeEach(func() {
		railsSecrets = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-rails-secrets",
				Namespace: "default",
			},
			Type: corev1.SecretTypeOpaque,
		}

		appConfig = &v2alpha2.ApplicationConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test",
				Namespace: "default",
				Annotations: map[string]string{
					"gitlab.io/test": "test",
				},
				Labels: map[string]string{
					"gitlab.io/instance": "test",
				},
			},

			Spec: v2alpha2.ApplicationConfigSpec{
				PostgreSQL: []v2alpha2.PostgreSQL{
					{
						Name: "main",
						ServiceProvider: v2alpha2.ServiceProvider{
							Endpoint: v2alpha2.ServiceEndpoint{
								External: &v2alpha2.ExternalServiceEndpoint{
									Host: "test-postgresql",
								},
							},
							Authentication: v2alpha2.ServiceAuthentication{
								Basic: &corev1.SecretReference{
									Name: "test-postgresql-basic-auth",
								},
							},
						},
					},
				},
				Redis: []v2alpha2.Redis{
					{
						Name: "main",
						ServiceProvider: v2alpha2.ServiceProvider{
							Endpoint: v2alpha2.ServiceEndpoint{
								External: &v2alpha2.ExternalServiceEndpoint{
									Host: "test-redis",
								},
							},
							Authentication: v2alpha2.ServiceAuthentication{
								Basic: &corev1.SecretReference{
									Name: "test-redis-basic-auth",
								},
							},
						},
					},
				},
				ObjectStorage: v2alpha2.ApplicationObjectStorage{
					Stores: []v2alpha2.ObjectStore{
						{
							Name: "main",
							ObjectStoreProvider: v2alpha2.ObjectStoreProvider{
								S3: &v2alpha2.S3ObjectStoreConnection{
									AccessKeyId: corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "test-s3-auth",
										},
									},
									AccessKeySecret: corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "test-s3-auth",
										},
									},
								},
							},
						},
					},
				},
				CiscoDuo: &v2alpha2.ApplicationCiscoDuoSettings{
					Host: "duo.example.com",
					IntegrationKey: corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: "test-duo-integrationkey"},
					},
					SecretKey: corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: "test-duo-secretkey"},
					},
				},
				FortiAuthenticator: &v2alpha2.ApplicationFortiAuthenticatorSettings{
					Host:        "forti.example.com",
					Credentials: corev1.SecretReference{Name: "test-fortiauth"},
				},
			},

			Status: v2alpha2.ApplicationConfigStatus{
				Conditions: []metav1.Condition{
					{
						Type:   v2alpha2.ApplicationConfigReadyCondition,
						Status: metav1.ConditionTrue,
						Reason: v2alpha2.RailsSecretsAvailableReason,
					},
				},
				SharedObjects: []v2alpha2.SharedObjectReference{
					sharedRefFromObject(railsSecrets, v2alpha2.RailsSecretsObjectUsage),
				},
			},
		}

		appServer = &v2alpha2.ApplicationServer{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test",
				Namespace: "default",
				Annotations: map[string]string{
					"gitlab.io/test": "test",
				},
				Labels: map[string]string{
					"gitlab.io/instance": "test",
				},
			},
			Spec: v2alpha2.ApplicationServerSpec{
				Version:              "1.0.0",
				ApplicationConfigRef: refFromObject(appConfig),
			},
		}

		referencedObjects = []client.Object{
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-postgresql-basic-auth",
					Namespace: "default",
				},
			},
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-redis-basic-auth",
					Namespace: "default",
				},
			},
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-s3-auth",
					Namespace: "default",
				},
			},
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-duo-integrationkey",
					Namespace: "default",
				},
			},
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-duo-secretkey",
					Namespace: "default",
				},
			},
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-fortiauth",
					Namespace: "default",
				},
			},
		}

		rtCtx, _ = framework.NewRuntimeContext(context.Background(), framework.WithLogger(slog.Default()))
	})

	AfterEach(func() {
		framework.Client = nil
	})

	Context("With a simple configuration", func() {
		BeforeEach(func() {
			objects := append(referencedObjects, appConfig, appServer, railsSecrets)

			fakeClient = fake.NewClientBuilder().
				WithScheme(framework.Scheme).
				WithObjects(objects...).
				WithStatusSubresource(appConfig, appServer).
				WithInterceptorFuncs(patchInterceptor).
				Build()

			fakeClient = interceptor.NewClient(fakeClient, passDeployment.Funcs())

			framework.Client = fakeClient
		})

		It("should create ApplicationServer resources", func() {
			err := NewController(appServer).Execute(rtCtx)
			Expect(err).NotTo(HaveOccurred())

			dumpIfVerbose(appServer)

			secretList := &corev1.SecretList{}
			err = fakeClient.List(rtCtx, secretList, client.InNamespace(appServer.Namespace))
			Expect(err).NotTo(HaveOccurred())

			for _, secret := range secretList.Items {
				obj := &secret
				dumpIfVerbose(obj)
			}

			// have all the following elements
			Expect(asSlice(secretList.Items)).To(SatisfyAll(
				// have test-app-secrets
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind("Secret"),
					testing.HaveName("test-app-secrets"),
				)),
				// have test-rails-config
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind("Secret"),
					testing.HaveName("test-rails-config-app-server"),
				)),
				// have test-workhorse-config
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind("Secret"),
					testing.HaveName("test-workhorse-config"),
				)),
			))

			configList := &corev1.ConfigMapList{}
			err = fakeClient.List(rtCtx, configList, client.InNamespace(appServer.Namespace))
			Expect(err).NotTo(HaveOccurred())

			for _, config := range configList.Items {
				obj := &config
				dumpIfVerbose(obj)
			}

			Expect(configList.Items).To(HaveLen(0))

			serviceList := &corev1.ServiceList{}
			err = fakeClient.List(rtCtx, serviceList, client.InNamespace(appServer.Namespace))
			Expect(err).NotTo(HaveOccurred())

			for _, service := range serviceList.Items {
				obj := &service
				dumpIfVerbose(obj)
			}

			Expect(asSlice(serviceList.Items)).To(SatisfyAll(
				// have test-app-server
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind("Service"),
					testing.HaveName("test-app-server"),
				)),
			))

			deploymentList := &appsv1.DeploymentList{}
			err = fakeClient.List(rtCtx, deploymentList, client.InNamespace(appServer.Namespace))
			Expect(err).NotTo(HaveOccurred())

			for _, deployment := range deploymentList.Items {
				obj := &deployment
				dumpIfVerbose(obj)
			}

			Expect(asSlice(deploymentList.Items)).To(SatisfyAll(
				// have test-app-server
				testing.HaveElementSatisfying(SatisfyAll(
					testing.HaveKind("Deployment"),
					testing.HaveName("test-app-server"),
				)),
			))

			serviceMonitorList := &monitoringv1.ServiceMonitorList{}
			err = fakeClient.List(rtCtx, serviceMonitorList, client.InNamespace(appServer.Namespace))

			for _, serviceMonitor := range serviceMonitorList.Items {
				obj := &serviceMonitor
				dumpIfVerbose(obj)
			}

			Expect(err).NotTo(HaveOccurred())
			Expect(serviceMonitorList.Items).To(HaveLen(0))
		})
	})
})

var (
	patchInterceptor = interceptor.Funcs{
		Patch: func(ctx context.Context, client client.WithWatch, obj client.Object, patch client.Patch, opts ...client.PatchOption) error {
			err := client.Update(ctx, obj)
			if apierrors.IsNotFound(err) {
				return client.Create(ctx, obj)
			}

			return err
		},
	}

	passDeployment = testing.InterceptorCallbacks{
		deploymentInterceptor,
	}

	deploymentInterceptor = &testing.InterceptCallback[*appsv1.Deployment]{
		Stage:   testing.After,
		Action:  testing.Get,
		Reactor: successfulDeployment,
	}

	successfulDeployment = func(obj *appsv1.Deployment, err error) error {
		if err != nil {
			return err
		}

		obj.Status.Conditions = []appsv1.DeploymentCondition{
			{
				Type:   appsv1.DeploymentProgressing,
				Status: corev1.ConditionTrue,
				Reason: "NewReplicaSetAvailable",
			},
			{
				Type:   appsv1.DeploymentAvailable,
				Status: corev1.ConditionTrue,
				Reason: "MinimumReplicasAvailable",
			},
		}
		return nil
	}
)

func asSlice[T any](objList []T) []any {
	var result []any

	for i := range objList {
		result = append(result, &objList[i])
	}

	return result
}

func dumpIfVerbose(obj client.Object) {
	_, reporterCfg := GinkgoConfiguration()
	if reporterCfg.Verbose {
		GinkgoWriter.Println(testing.DumpObject(obj))
	}
}
