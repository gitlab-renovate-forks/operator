package templateutils

import (
	"errors"
	"fmt"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

func getWorkhorseObjectStore(appConfig *v2alpha2.ApplicationConfig, objectStoreName string) (*v2alpha2.ObjectStore, error) {
	if objectStoreName == "" {
		if len(appConfig.Spec.ObjectStorage.Stores) == 0 {
			return nil, errors.New("cannot find object store for Workhorse: " +
				"empty .spec.objectStorage.Stores")
		}

		return &appConfig.Spec.ObjectStorage.Stores[0], nil
	}

	for _, objectStore := range appConfig.Spec.ObjectStorage.Stores {
		if objectStore.Name == objectStoreName {
			return &objectStore, nil
		}
	}

	return nil, fmt.Errorf("cannot find object store for Workhorse: %q", objectStoreName)
}

func getWorkhorseRedis(appConfig *v2alpha2.ApplicationConfig, redisName string) (*v2alpha2.Redis, error) {
	if redisName == "" {
		if len(appConfig.Spec.Redis) == 0 {
			return nil, errors.New("cannot find Redis for Workhorse: " +
				"empty .spec.redis")
		}

		return &appConfig.Spec.Redis[0], nil
	}

	for _, redis := range appConfig.Spec.Redis {
		if redis.Name == redisName {
			return &redis, nil
		}
	}

	return nil, fmt.Errorf("cannot find Redis for Workhorse: %q", redisName)
}
