package utils

import (
	"bytes"
	"strings"
	"text/template"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// ExpandStrings expands the given strings using the given context. It uses the
// shared state to expand the strings using Go template syntax.
func ExpandStrings(rtCtx *framework.RuntimeContext, strings ...string) ([]string, error) {
	data := map[string]any{}

	rtCtx.State.Visit(func(name string, value any, _ ...string) bool {
		data[name] = value

		return true
	})

	result := make([]string, len(strings))

	for i, s := range strings {
		s, err := expandString(s, data)
		if err != nil {
			return result, err
		}

		result[i] = s
	}

	return result, nil
}

func expandString(s string, data map[string]any) (string, error) {
	if !strings.Contains(s, "{{") {
		return s, nil
	}

	tpl, err := template.New("").Parse(s)
	if err != nil {
		return s, err
	}

	var buf bytes.Buffer

	if err = tpl.Execute(&buf, data); err != nil {
		return s, err
	}

	return buf.String(), nil
}
