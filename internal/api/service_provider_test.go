package api

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var _ = Describe("ServiceProvider", func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		service    *corev1.Service
		secret     *corev1.Secret
	)

	fakeScheme = runtime.NewScheme()
	utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

	BeforeEach(func() {
		service = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-service",
				Namespace: "default",
			},
			Spec: corev1.ServiceSpec{
				Ports: []corev1.ServicePort{
					{Name: "postgresql", Port: 5432},
					{Name: "redis", Port: 6379},
					{Name: "clickhouse", Port: 8123},
				},
			},
		}

		secret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-secret",
				Namespace: "default",
			},
			Data: map[string][]byte{
				"username": []byte("test-user"),
				"password": []byte("test-password"),
			},
		}

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(service, secret).
			Build()

		framework.Client = fakeClient
		framework.Scheme = fakeScheme
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	Context("NewServiceProvider", func() {
		It("creates a new ServiceProvider with the specified endpoint type", func() {
			provider := NewServiceProvider(PostgreSQLServiceEndpoint)

			Expect(provider).NotTo(BeNil())
			Expect(provider.Type).To(Equal(PostgreSQLServiceEndpoint))
		})

		It("applies ObjectReferenceOptions", func() {
			owner := &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-pod",
					Namespace: "test-namespace",
				},
			}
			provider := NewServiceProvider(PostgreSQLServiceEndpoint, WithOwner(owner))

			Expect(provider).NotTo(BeNil())
			Expect(provider.objectReferenceOptions).To(Equal(&ObjectReferenceOptions{
				Key: client.ObjectKey{Namespace: "test-namespace"},
			}))
		})
	})

	Context("ServiceProvider.Consume", func() {
		It("consumes a ServiceProvider with a service-backed endpoint and basic authentication", func() {
			serviceProvider := &v2alpha2.ServiceProvider{
				Endpoint: v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "test-service",
						Namespace: "default",
					},
				},
				Authentication: v2alpha2.ServiceAuthentication{
					Basic: &corev1.SecretReference{
						Name:      "test-secret",
						Namespace: "default",
					},
				},
			}

			provider := NewServiceProvider(PostgreSQLServiceEndpoint)
			err := provider.Consume(context.Background(), serviceProvider)

			Expect(err).NotTo(HaveOccurred())
			Expect(provider.Ready()).To(BeTrue())
			Expect(provider.Endpoint).NotTo(BeNil())
			Expect(provider.Endpoint.Type).To(Equal(PostgreSQLServiceEndpoint))
			Expect(provider.Endpoint.Scheme).To(Equal("postgresql"))
			Expect(provider.Endpoint.Host).To(Equal("test-service.default.svc.cluster.local"))
			Expect(provider.Endpoint.Port).To(Equal(int32(5432)))
			Expect(provider.Basic).NotTo(BeNil())
			Expect(provider.Basic["username"]).To(Equal("test-user"))
			Expect(provider.Basic["password"]).To(Equal("test-password"))
			Expect(provider.ClientTLS).To(BeNil())
			Expect(provider.Token).To(BeNil())
		})

		It("consumes a ServiceProvider with an external endpoint and token authentication", func() {
			serviceProvider := &v2alpha2.ServiceProvider{
				Endpoint: v2alpha2.ServiceEndpoint{
					External: &v2alpha2.ExternalServiceEndpoint{
						Host: "example.com",
					},
					TLS: utils.Ptr(true),
				},
				Authentication: v2alpha2.ServiceAuthentication{
					Token: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: "test-secret",
						},
						Key: "password",
					},
				},
			}

			provider := NewServiceProvider(GitalyServiceEndpoint, WithNamespace(secret.Namespace))
			err := provider.Consume(context.Background(), serviceProvider)

			Expect(err).NotTo(HaveOccurred())
			Expect(provider.Ready()).To(BeTrue())
			Expect(provider.Endpoint).NotTo(BeNil())
			Expect(provider.Endpoint.Type).To(Equal(GitalyServiceEndpoint))
			Expect(provider.Endpoint.Scheme).To(Equal("tls"))
			Expect(provider.Endpoint.Host).To(Equal("example.com"))
			Expect(provider.Endpoint.Port).To(Equal(int32(8076)))
			Expect(provider.Endpoint.TLS).To(BeTrue())
			Expect(provider.Token).NotTo(BeNil())
			Expect(provider.Token["password"]).To(Equal("test-password"))
			Expect(provider.Basic).To(BeNil())
			Expect(provider.ClientTLS).To(BeNil())
		})

		It("consumes a ServiceProvider with a service-backed endpoint client TLS authentication", func() {
			serviceProvider := &v2alpha2.ServiceProvider{
				Endpoint: v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "test-service",
						Namespace: "default",
					},
					TLS: utils.Ptr(true),
				},
				Authentication: v2alpha2.ServiceAuthentication{
					ClientTLS: &corev1.SecretReference{
						Name:      "test-secret",
						Namespace: "default",
					},
				},
			}

			provider := NewServiceProvider(RedisServiceEndpoint)
			err := provider.Consume(context.Background(), serviceProvider)

			Expect(err).NotTo(HaveOccurred())
			Expect(provider.Ready()).To(BeTrue())
			Expect(provider.Endpoint).NotTo(BeNil())
			Expect(provider.Endpoint.Type).To(Equal(RedisServiceEndpoint))
			Expect(provider.Endpoint.Scheme).To(Equal("rediss"))
			Expect(provider.Endpoint.Host).To(Equal("test-service.default.svc.cluster.local"))
			Expect(provider.Endpoint.Port).To(Equal(int32(6379)))
			Expect(provider.ClientTLS).NotTo(BeNil())
			Expect(provider.ClientTLS["username"]).To(Equal("test-user"))
			Expect(provider.ClientTLS["password"]).To(Equal("test-password"))
			Expect(provider.Basic).To(BeNil())
			Expect(provider.Token).To(BeNil())
		})

		It("should not be ready when the Service of service-backed endpoint is missing", func() {
			serviceProvider := &v2alpha2.ServiceProvider{
				Endpoint: v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "missing-service",
						Namespace: "default",
					},
				},
			}

			provider := NewServiceProvider(PostgreSQLServiceEndpoint)
			err := provider.Consume(context.Background(), serviceProvider)

			Expect(err).NotTo(HaveOccurred())
			Expect(provider.Ready()).To(BeFalse())
			Expect(provider.Missing()).To(ContainElements(
				"v1/Service[default/missing-service]",
			))
		})

		It("should report all missing resources", func() {
			serviceProvider := &v2alpha2.ServiceProvider{
				Endpoint: v2alpha2.ServiceEndpoint{
					Service: &corev1.ObjectReference{
						Name:      "missing-service",
						Namespace: "default",
					},
				},
				Authentication: v2alpha2.ServiceAuthentication{
					Basic: &corev1.SecretReference{
						Name:      "missing-secret",
						Namespace: "default",
					},
				},
			}

			provider := NewServiceProvider(PostgreSQLServiceEndpoint)
			err := provider.Consume(context.Background(), serviceProvider)

			Expect(err).NotTo(HaveOccurred())
			Expect(provider.Ready()).To(BeFalse())
			Expect(provider.Missing()).To(ContainElements(
				"v1/Service[default/missing-service]",
				"v1/Secret[default/missing-secret]",
			))
		})
	})

	Context("NewNamedServiceProvider", func() {
		It("creates a new NamedServiceProvider with the specified endpoint type", func() {
			namedProvider := NewNamedServiceProvider(PostgreSQLServiceEndpoint)

			Expect(namedProvider).NotTo(BeNil())
			Expect(namedProvider.All).To(BeEmpty())
			Expect(namedProvider.builder).NotTo(BeNil())
		})

		It("applies ObjectReferenceOptions to generated providers", func() {
			owner := &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-pod",
					Namespace: "test-namespace",
				},
			}
			namedProvider := NewNamedServiceProvider(PostgreSQLServiceEndpoint, WithOwner(owner))

			serviceProvider := namedProvider.builder(context.Background())

			Expect(serviceProvider.objectReferenceOptions).To(Equal(&ObjectReferenceOptions{
				Key: client.ObjectKey{Namespace: "test-namespace"},
			}))
		})
	})

	Context("NamedServiceProvide.Consume", func() {
		It("consumes multiple named service providers", func() {
			serviceProviders := map[string]*v2alpha2.ServiceProvider{
				"db1": {
					Endpoint: v2alpha2.ServiceEndpoint{
						Service: &corev1.ObjectReference{
							Name:      "test-service",
							Namespace: "default",
						},
					},
					Authentication: v2alpha2.ServiceAuthentication{
						Basic: &corev1.SecretReference{
							Name:      "test-secret",
							Namespace: "default",
						},
					},
				},
				"db2": {
					Endpoint: v2alpha2.ServiceEndpoint{
						External: &v2alpha2.ExternalServiceEndpoint{
							Host: "example.com",
						},
					},
					Authentication: v2alpha2.ServiceAuthentication{
						ClientTLS: &corev1.SecretReference{
							Name:      "test-secret",
							Namespace: "default",
						},
					},
				},
			}

			namedProvider := NewNamedServiceProvider(PostgreSQLServiceEndpoint)
			err := namedProvider.Consume(context.Background(), serviceProviders)

			Expect(err).NotTo(HaveOccurred())
			Expect(namedProvider.Ready()).To(BeTrue())
			Expect(namedProvider.All).To(HaveLen(2))

			Expect(namedProvider.All["db1"].Endpoint.Host).To(Equal("test-service.default.svc.cluster.local"))
			Expect(namedProvider.All["db1"].Endpoint.Port).To(Equal(int32(5432)))
			Expect(namedProvider.All["db1"].Endpoint.Scheme).To(Equal("postgresql"))
			Expect(namedProvider.All["db1"].Basic["username"]).To(Equal("test-user"))
			Expect(namedProvider.All["db1"].Basic["password"]).To(Equal("test-password"))
			Expect(namedProvider.All["db1"].ClientTLS).To(BeNil())
			Expect(namedProvider.All["db1"].Token).To(BeNil())

			Expect(namedProvider.All["db2"].Endpoint.Host).To(Equal("example.com"))
			Expect(namedProvider.All["db2"].Endpoint.Port).To(Equal(int32(5432)))
			Expect(namedProvider.All["db2"].Endpoint.Scheme).To(Equal("postgresql"))
			Expect(namedProvider.All["db2"].ClientTLS["username"]).To(Equal("test-user"))
			Expect(namedProvider.All["db2"].ClientTLS["password"]).To(Equal("test-password"))
			Expect(namedProvider.All["db2"].Basic).To(BeNil())
			Expect(namedProvider.All["db2"].Token).To(BeNil())
		})

		It("reuses existing providers for the same name", func() {
			serviceProviders := map[string]*v2alpha2.ServiceProvider{
				"db1": {
					Endpoint: v2alpha2.ServiceEndpoint{
						Service: &corev1.ObjectReference{
							Name:      "test-service",
							Namespace: "default",
						},
					},
				},
			}

			namedProvider := NewNamedServiceProvider(PostgreSQLServiceEndpoint)
			err := namedProvider.Consume(context.Background(), serviceProviders)
			Expect(err).NotTo(HaveOccurred())

			firstProvider := namedProvider.All["db1"]

			err = namedProvider.Consume(context.Background(), serviceProviders)
			Expect(err).NotTo(HaveOccurred())

			Expect(namedProvider.All["db1"]).To(BeIdenticalTo(firstProvider))
		})

		It("should report all missing resources", func() {
			serviceProviders := map[string]*v2alpha2.ServiceProvider{
				"db1": {
					Endpoint: v2alpha2.ServiceEndpoint{
						Service: &corev1.ObjectReference{
							Name:      "missing-service",
							Namespace: "default",
						},
					},
					Authentication: v2alpha2.ServiceAuthentication{
						Basic: &corev1.SecretReference{
							Name:      "test-secret",
							Namespace: "default",
						},
					},
				},
				"db2": {
					Endpoint: v2alpha2.ServiceEndpoint{
						External: &v2alpha2.ExternalServiceEndpoint{
							Host: "example.com",
						},
					},
					Authentication: v2alpha2.ServiceAuthentication{
						ClientTLS: &corev1.SecretReference{
							Name:      "missing-secret",
							Namespace: "default",
						},
					},
				},
			}

			namedProvider := NewNamedServiceProvider(PostgreSQLServiceEndpoint)
			err := namedProvider.Consume(context.Background(), serviceProviders)

			Expect(err).NotTo(HaveOccurred())
			Expect(namedProvider.Ready()).To(BeFalse())
			Expect(namedProvider.Missing()).To(ContainElements(
				"v1/Service[default/missing-service]",
				"v1/Secret[default/missing-secret]",
			))
			Expect(namedProvider.All).To(HaveLen(2))
		})
	})
})
