package jobprocessor

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/runtime"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/assets/tests"
	internalinventory "gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
)

var _ = Describe("JobProcessor Manifest", func() {
	Context("Minimal JobProcessor setup", func() {
		context, _ := framework.NewRuntimeContext(context.TODO())
		scheme, _ := testing.NewScheme()
		decoder := testing.NewDecoder(scheme)

		framework.Scheme = scheme

		template := internalinventory.TemplatedObjects{
			Pattern: "jobprocessor/*.yaml",
			Values:  getValues("minimal", decoder),
		}

		It("Renders the JobProcessor Deployment", func() {
			objects, err := template.Get(context)
			Expect(err).ToNot(HaveOccurred())
			Expect(objects).To(HaveLen(1))

			expected := tests.ReadObject("minimal", "expected/deployment.yaml", decoder)
			Expect(objects[0]).To(testing.MatchObject(expected))
		})
	})
})

func getValues(scenario string, decoder runtime.Decoder) map[string]any {
	jobProcessor := tests.ReadObject(scenario, "given/jobprocessor.yaml", decoder)
	appConfig := tests.ReadObject(scenario, "given/application-config.yaml", decoder)

	return map[string]any{
		"JobProcessor":      jobProcessor,
		"ApplicationConfig": appConfig,
		"Fingerprint":       "TEST-FINGERPRINT",
	}
}
