package templateutils

import (
	"github.com/huandu/xstrings"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

const (
	mainPostgreSQLName = "main"
	ciPostgreSQLName   = "ci"
)

func postgresqlSSLMode(postgresql v2alpha2.PostgreSQL) string {
	if postgresql.Authentication.ClientTLS != nil {
		return "verify-ca"
	}

	if postgresql.Endpoint.TLS == nil {
		return "prefer"
	}

	if *postgresql.Endpoint.TLS {
		return "require"
	} else {
		return "allow"
	}
}

type databaseConfig struct {
	v2alpha2.PostgreSQL
	Key string
}

func mapPostgreSQLDatabases(list []v2alpha2.PostgreSQL) []databaseConfig {
	if len(list) == 0 {
		return []databaseConfig{}
	}

	mapping := map[string]v2alpha2.PostgreSQL{}
	ciDBConfigured := false
	mainPostgreSQL := list[0]

	for _, postgresql := range list {
		postgresqlName := xstrings.ToSnakeCase(postgresql.Name)

		if postgresqlName == "default" {
			postgresqlName = mainPostgreSQLName
		}

		if postgresqlName == mainPostgreSQLName {
			mainPostgreSQL = postgresql
		}

		if postgresqlName == ciPostgreSQLName {
			ciDBConfigured = true
		}

		mapping[postgresqlName] = postgresql
	}

	if !ciDBConfigured {
		mapping[ciPostgreSQLName] = mainPostgreSQL
	}

	result := []databaseConfig{
		{
			PostgreSQL: mainPostgreSQL,
			Key:        mainPostgreSQLName,
		},
	}

	for key, postgresql := range mapping {
		if key != mainPostgreSQLName {
			result = append(result, databaseConfig{
				PostgreSQL: postgresql,
				Key:        key,
			})
		}
	}

	return result
}

var (
	postgresqlSettingsMapper = mapper{
		keySet: []string{
			"encoding",
			"applicationName",
			"connectTimeout",
			"keepAlive",
		},
		keyMapping: map[string]func(string) string{
			"keepAlive": func(string) string {
				return "keepalives"
			},
		},
		valueMapping: map[string]func(string) any{
			"connectTimeout": toInt,
			"keepAlive":      toInt,
		},
	}
)
