package applicationserver

import (
	"context"
	"strings"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/assets/tests"
	internalinventory "gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
)

var _ = Describe("ApplicationServer Manifest", func() {
	var (
		scheme  *runtime.Scheme
		decoder runtime.Decoder
	)

	BeforeEach(func() {
		scheme, _ = testing.NewScheme(monitoringv1.AddToScheme)
		decoder = testing.NewDecoder(scheme)

		framework.Scheme = scheme
		framework.APIResources = append(framework.APIResources, "monitoring.coreos.com/v1/ServiceMonitor")
	})

	DescribeTable("Object Templates",
		func(scenario string) {
			src := &internalinventory.TemplatedObjects{
				Pattern: "application-server/*.yaml",
				Values:  getScenarioValues(scenario, decoder),
			}

			expected := manifestForScenario(scenario, decoder)
			observed := manifestForInventory(src)

			Expect(observed.deployment).To(testing.MatchObject(expected.deployment))
			Expect(observed.service).To(testing.MatchObject(expected.service))
			Expect(observed.pumaServiceMonitor).To(testing.MatchObject(expected.pumaServiceMonitor))
			Expect(observed.workhorseServiceMonitor).To(testing.MatchObject(expected.workhorseServiceMonitor))
		},
		Entry("bare minimum configuration", "minimal"),
	)
})

var appConfigResourceSamples = map[string]appconfigutils.ApplicationConfigResources{}

type manifest struct {
	deployment              client.Object
	service                 client.Object
	pumaServiceMonitor      client.Object
	workhorseServiceMonitor client.Object
}

func manifestForScenario(scenario string, decoder runtime.Decoder) *manifest {
	return &manifest{
		deployment:              tests.ReadObject(scenario, "expected/deployment.yaml", decoder),
		service:                 tests.ReadObject(scenario, "expected/service.yaml", decoder),
		pumaServiceMonitor:      tests.ReadObject(scenario, "expected/serviceMonitor-puma.yaml", decoder),
		workhorseServiceMonitor: tests.ReadObject(scenario, "expected/serviceMonitor-workhorse.yaml", decoder),
	}
}

func manifestForInventory(i inventory.Inventory[client.Object]) *manifest {
	rtCtx, _ := framework.NewRuntimeContext(context.TODO())

	objList, err := i.Get(rtCtx)
	Expect(err).NotTo(HaveOccurred())

	m := &manifest{}

	for _, obj := range objList {
		switch obj.GetObjectKind().GroupVersionKind().Kind {
		case "Deployment":
			if m.deployment != nil {
				Fail("unexpected Deployment")
			}

			m.deployment = obj
		case "Service":
			if m.service != nil {
				Fail("unexpected Service")
			}

			m.service = obj

		case "ServiceMonitor":
			if strings.HasSuffix(obj.GetName(), "puma") {
				if m.pumaServiceMonitor != nil {
					Fail("unexpected puma ServiceMonitor")
				}

				m.pumaServiceMonitor = obj
			} else if strings.HasSuffix(obj.GetName(), "workhorse") {
				if m.workhorseServiceMonitor != nil {
					Fail("unexpected workhorse ServiceMonitor")
				}

				m.workhorseServiceMonitor = obj
			} else {
				Fail("unexpected ServiceMonitor")
			}
		}
	}

	return m
}

func getScenarioValues(scenario string, decoder runtime.Decoder) map[string]any {
	appServer := tests.ReadObject(scenario, "given/application-server.yaml", decoder)
	appConfig := tests.ReadObject(scenario, "given/application-config.yaml", decoder)

	appConfiResources := appConfigResourceSamples[scenario]

	values := utils.MergeMap(map[string]any{
		"ApplicationServer": appServer,
		"ApplicationConfig": appConfig,
		"Fingerprint":       "TEST-FINGERPRINT",
	}, appConfiResources.Resources)

	return values
}
