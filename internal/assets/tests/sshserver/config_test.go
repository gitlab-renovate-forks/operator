package gitlabshell

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

const (
	testConfigTemplate = "ssh-server/testdata/gitlabShellConfig.yaml"
	testSSHDTemplate   = "ssh-server/testdata/sshdConfig"
)

var _ = Describe("Partial: _gitlabShellConfig.yaml", func() {
	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testConfigTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testConfigTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleGitLabShell, "simple_gitlab_ssh_config.yaml"),
		Entry("with simple OpenSSH configuration", simpleOpenSSH, "simple_openssh_config.yaml"),
	)
})

var _ = Describe("Partial: _sshdConfig", func() {
	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testSSHDTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := normalizeString(data[testSSHDTemplate].String())
			expected := normalizeString(readLiteralSource(output))

			Expect(rendered).To(Equal(expected))
		},
		Entry("with simple OpenSSH configuration", simpleOpenSSH, "simple_sshd_config"),
	)
})

var (
	simpleGitLabShell = map[string]any{
		"SSHServer": v2alpha2.SSHServer{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "gitlab-shell",
				Namespace: "default",
			},
			Spec: v2alpha2.SSHServerSpec{
				LogFormat: v2alpha2.JSONLogFormat,
				Daemon: v2alpha2.SSHServerDaemon{
					GitLabSSHD: &v2alpha2.GitLabSSHDConfig{},
				},
			},
		},
		"ApplicationServerEndpoint": apiutils.ServiceEndpoint{
			Scheme: "https",
			Host:   "gitlab.example.com",
			Port:   443,
			UseTLS: true,
		},
	}

	simpleOpenSSH = map[string]any{
		"SSHServer": v2alpha2.SSHServer{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "gitlab-shell",
				Namespace: "default",
			},
			Spec: v2alpha2.SSHServerSpec{
				LogFormat: v2alpha2.JSONLogFormat,
				Daemon: v2alpha2.SSHServerDaemon{
					OpenSSH: &v2alpha2.OpenSSHConfig{
						ExtraConfig: selectConfigMap("openssh", "sshd_config"),
					},
				},
			},
		},
		"ApplicationServerEndpoint": apiutils.ServiceEndpoint{
			Scheme: "https",
			Host:   "gitlab.example.com",
			Port:   443,
			UseTLS: true,
		},
		"OpenSSHConfigMap": makeConfigMap("default", "openssh",
			"sshd_config", "foo bar"),
	}
)
