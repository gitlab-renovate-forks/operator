package api

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestGitLabOperatorAPIUtils(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: Internal API")
}
