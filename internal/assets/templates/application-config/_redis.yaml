{{/*
Renders the Redis configuration for Rails.

Expects a context with the following keys:
  - ApplicationConfig: The ApplicationConfig custom resource.
  - RedisSecrets: A map of Secret objects, with name of the Redis entry as its
    key and the Secret object of the authentication as its value. Only basic
    authentication Secrets are used to render the template.
  - RedisEndpoints: A map of ServiceEndpoints, with name of the Redis entry as
    its key.

The template produces a list of keyed YAML entries, for each Redis that is
specified in ApplicationConfig. The key is the file name of the configuration.
*/}}

{{- $ := . -}}
{{- $fileNames := mapRedisConfigFileNames .ApplicationConfig.Spec.Redis  -}}

{{- range .ApplicationConfig.Spec.Redis }}
  {{- $name := .Name }}
  {{- $authSecret := index $.RedisSecrets $name }}
  {{- $endpoint := index $.RedisEndpoints $name }}
  {{- $userInfo := "" -}}
  {{- with .Authentication.Basic }}
    {{- $userInfo = printf "%s:%s@" (getSecretValue $authSecret "username") (getSecretValue $authSecret "password") }}
  {{- end }}
{{ index $fileNames $name }}: |
  {{ $.Settings.Rails.Environment }}:
    url: {{ printf "%s://%s%s:%d" $endpoint.Scheme $userInfo $endpoint.Host $endpoint.Port | quote }}
    {{- with .Authentication.ClientTLS }}
    ssl_params:
      cert_file: {{ printf "%s/redis/%s/client/tls.crt" $.Settings.Secrets.MountPath ($name | lower) }}
      key_file: {{ printf "%s/redis/%s/client/tls.key" $.Settings.Secrets.MountPath ($name | lower) }}
      {{- with .CACertificate }}
      ca_file: {{ printf "%s/redis/%s/client/ca.crt" $.Settings.Secrets.MountPath ($name | lower) }}
      {{- end }}
    {{- else with .TLS }}
    ssl_params:
      ca_path: {{ printf "%s/redis/%s/" $.Settings.Secrets.MountPath ($name | lower) }}
      {{- if .CACertificate -}}
      ca_file: {{ printf "%s/redis/%s/ca.crt" $.Settings.Secrets.MountPath ($name | lower) }}
      {{- end }}
    {{- end }}
    {{- with .Settings }}
    {{- mapRedisSettings . | toYaml | nindent 4 }}
    {{- end }}
{{- end }}