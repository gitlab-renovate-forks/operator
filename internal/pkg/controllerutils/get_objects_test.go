package controllerutils

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("Get", func() {
	var (
		fakeScheme    *runtime.Scheme
		fakeClient    client.WithWatch
		mockConfigMap *corev1.ConfigMap
		rtCtx         *framework.RuntimeContext
	)

	BeforeEach(func() {
		mockConfigMap = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-cm",
				Namespace: "test",
			},
			Data: map[string]string{
				"key": "data",
			},
		}

		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(mockConfigMap).
			Build()

		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	It("Gets and registers the object", func() {
		configMaps, err := GetObjects[*corev1.ConfigMap](rtCtx, &corev1.ConfigMap{}, []types.NamespacedName{
			{
				Name:      "test-cm",
				Namespace: "test",
			},
		})

		Expect(err).ToNot(HaveOccurred())
		Expect(configMaps).To(HaveLen(1))
		Expect(configMaps[0].Name).To(Equal("test-cm"))
	})
})
