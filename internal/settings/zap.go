package settings

import (
	"fmt"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func parseZapEncoder(text string, development bool) (zapcore.Encoder, error) {
	var encoderCfg zapcore.EncoderConfig
	if development {
		encoderCfg = zap.NewDevelopmentEncoderConfig()
	} else {
		encoderCfg = zap.NewProductionEncoderConfig()
	}

	switch strings.ToLower(text) {
	case "json":
		return zapcore.NewJSONEncoder(encoderCfg), nil
	case "console":
		return zapcore.NewConsoleEncoder(encoderCfg), nil
	default:
		return nil, fmt.Errorf("invalid encoder value %q", text)
	}
}

func parseZapLevel(text string) (zapcore.LevelEnabler, error) {
	lvl, err := zap.ParseAtomicLevel(text)
	if err != nil {
		return nil, err
	}

	return lvl, err
}

func parseTimeFormat(text string) (zapcore.TimeEncoder, error) {
	switch strings.ToLower(text) {
	case "rfc3339nano":
		return zapcore.RFC3339NanoTimeEncoder, nil
	case "rfc3339":
		return zapcore.RFC3339TimeEncoder, nil
	case "iso8601":
		return zapcore.ISO8601TimeEncoder, nil
	case "millis":
		return zapcore.EpochMillisTimeEncoder, nil
	case "nanos":
		return zapcore.EpochNanosTimeEncoder, nil
	case "epoch":
		return zapcore.EpochTimeEncoder, nil
	default:
		return nil, fmt.Errorf("invalid time-encoding value %q", text)
	}
}
