package controllerutils

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("CreateObject", Ordered, func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		mockSecret *corev1.Secret
		rtCtx      *framework.RuntimeContext
	)

	BeforeAll(func() {
		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	})

	BeforeEach(func() {
		mockSecret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-secret",
				Namespace: "default",
			},
			Type: corev1.SecretTypeOpaque,
			Data: map[string][]byte{
				"key": []byte("test-secret-value"),
			},
		}

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(mockSecret).
			Build()

		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	It("creates new Secrets when they do not exist", func() {
		inventory := &mockInventory{
			secrets: []*corev1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test-secret-1",
						Namespace: "default",
					},
				},
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test-secret-2",
						Namespace: "default",
					},
				},
			},
		}

		secrets, err := CreateObject(rtCtx, inventory)
		Expect(err).NotTo(HaveOccurred())
		Expect(secrets).To(HaveLen(2))

		actualList := &corev1.SecretList{}
		Expect(fakeClient.List(rtCtx, actualList)).To(Succeed())
		Expect(actualList.Items).To(HaveLen(3))
	})

	It("skips creating existing Secrets and returns their current state", func() {
		inventory := &mockInventory{
			secrets: []*corev1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      mockSecret.Name,
						Namespace: mockSecret.Namespace,
					},
				},
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test-secret-2",
						Namespace: "default",
					},
				},
			},
		}

		secrets, err := CreateObject(rtCtx, inventory)
		Expect(err).NotTo(HaveOccurred())
		Expect(secrets).To(HaveLen(2))
		Expect(secrets[0].Name).To(Equal(mockSecret.Name))
		Expect(secrets[0].Type).To(Equal(mockSecret.Type))
		Expect(secrets[0].Data).To(Equal(mockSecret.Data))

		actualList := &corev1.SecretList{}
		Expect(fakeClient.List(rtCtx, actualList)).To(Succeed())
		Expect(actualList.Items).To(HaveLen(2))
	})

	It("handles inventory errors", func() {
		inventory := &mockInventory{
			shouldFail: true,
		}
		_, err := CreateObject(rtCtx, inventory)
		Expect(err).To(MatchError("mock error"))
	})
})
