package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	/* Condition Types */
	JobProcessorReadyCondition   = "JobProcessorReady"
	JobProcessorPausedCondition  = "JobProcessorPaused"
	JobProcessorScalingCondition = "JobProcessorScaling"
)

// +kubebuilder:object:root=true
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:resource:shortName=jobproc
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=`.spec.version`
// +kubebuilder:printcolumn:name="Edition",type=string,JSONPath=`.spec.edition`
// +kubebuilder:printcolumn:name="Available",type="string",JSONPath=".status.replicas"
type JobProcessor struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JobProcessorSpec   `json:"spec,omitempty"`
	Status JobProcessorStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true
type JobProcessorList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []JobProcessor `json:"items"`
}

type JobProcessorSpec struct {
	// +required
	// +kubebuilder:validation:Pattern=^([1-9]\d?)\.(0|[1-9]\d?)\.(0|[1-9]\d?)$
	Version string `json:"version"`

	// +optional
	Edition Edition `json:"edition,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	Replicas *int32 `json:"replicas,omitempty"`

	// +optional
	Paused bool `json:"paused,omitempty"`

	// +required
	ApplicationConfigRef corev1.ObjectReference `json:"applicationConfigRef"`

	// +optional
	Sidekiq SidekiqSpec `json:"puma,omitempty"`

	// +optional
	Tracing *TracingSupport `json:"tracing,omitempty"`

	// +optional
	Workload WorkloadSpec `json:"workload,omitempty"`
}

type JobProcessorStatus struct {
	// +optional
	Version string `json:"version,omitempty"`

	// +optional
	Edition Edition `json:"edition,omitempty"`

	// +optional
	Replicas int32 `json:"replicas,omitempty"`

	// +optional
	Selector string `json:"selector,omitempty"`

	// +optional
	// +patchMergeKey=usage
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	SharedObjects []SharedObjectReference `json:"sharedObjects,omitempty" patchStrategy:"merge" patchMergeKey:"usage"`

	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

type SidekiqSpec struct {
	// +optional
	Metrics MetricsSupport `json:"metrics"`

	// +optional
	Settings SidekiqSettings `json:"settings"`

	// +optional
	MemoryKiller SidekiqMemoryKiller `json:"memoryKiller,omitempty"`

	// +optional
	RoutingRules []SidekiqRoutingRule `json:"routingRules,omitempty"`
}

type SidekiqSettings struct {
	// +optional
	EnableBootsnap *bool `json:"enableBootsnap,omitempty"`

	// +optional
	// +kubebuilder:validation:Minimum=0
	Concurrency *int32 `json:"concurrency,omitempty"`

	// +optional
	Timeout *metav1.Duration `json:"timeout,omitempty"`

	// +optional
	LogFormat LogFormat `json:"logFormat,omitempty"`

	// +optional
	Queues []string `json:"queues,omitempty"`
}

type SidekiqMemoryKiller struct {
	// +optional
	DaemonMode *bool `json:"daemonMode,omitempty"`

	// +optional
	CheckInterval *metav1.Duration `json:"checkInterval,omitempty"`

	// +optional
	MaxRSS *resource.Quantity `json:"maxRSS,omitempty"`

	// +optional
	HardLimitRSS *resource.Quantity `json:"hardLimitRSS,omitempty"`

	// +optional
	ShutdownWait *metav1.Duration `json:"shutdownWait,omitempty"`

	// +optional
	GraceTime *metav1.Duration `json:"graceTime,omitempty"`
}

// +structType=atomic
type SidekiqRoutingRule struct {
	// +required
	Query string `json:"query"`

	// +required
	Queue string `json:"queue"`
}

func init() {
	SchemeBuilder.Register(&JobProcessor{}, &JobProcessorList{})
}
