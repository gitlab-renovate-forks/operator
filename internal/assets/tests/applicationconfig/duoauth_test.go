package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _duoauth.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/duoauth.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleDuoAuth, "duoauth/simple.yaml"),
	)
})

var (
	simpleDuoAuth = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				CiscoDuo: &v2alpha2.ApplicationCiscoDuoSettings{
					IntegrationKey: corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: "foo"},
						Key:                  "bar",
					},
					SecretKey: corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{Name: "foo"},
						Key:                  "bar",
					},
					Host: "duo.example.com:8443",
				},
			},
		},
		"TwoFactorSecrets": map[string]*corev1.Secret{
			"DuoIntegrationKey": {
				Data: map[string][]byte{"bar": []byte("integrationkey")},
			},
			"DuoSecretKey": {
				Data: map[string][]byte{"bar": []byte("secretkey")},
			},
		},
	}
)
