package framework

import (
	"context"
	"errors"
	"fmt"
	"log/slog"

	"github.com/go-logr/logr"
	"sigs.k8s.io/controller-runtime/pkg/manager"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

// RuntimeContext encapsulates the request-scoped information and a shared state
// between the Tasks that handle a reconciler request, i.e. reconcile.Request.
//
// It wraps the original context that is passed to the Reconciler and attaches
// additional information to it.
//
// Always use NewRuntimeContext and various RuntimeContextOptions to create a
// new RuntimeContext.
//
// Note that the RuntimeContext is not thread-safe and must be scoped to a
// single request. Subsequent retries of a request are considered new requests
// and require a new RuntimeContext.
type RuntimeContext struct {
	context.Context

	Logger *slog.Logger
	State  SharedState
}

// RuntimeContextOption is a function that can be used to configure a RuntimeContext.
type RuntimeContextOption = func(*RuntimeContext)

// WithManager configures the RuntimeContext to use the given manager.
var WithManager = func(mgr manager.Manager) RuntimeContextOption {
	return func(rtCtx *RuntimeContext) {
		rtCtx.Logger = slog.New(toSlogHandler(mgr.GetLogger()))
	}
}

// WithLogger configures the RuntimeContext to use the given logger.
var WithLogger = func(logger *slog.Logger) RuntimeContextOption {
	return func(rtCtx *RuntimeContext) {
		rtCtx.Logger = logger
	}
}

// WithLogrLogger configures the RuntimeContext to use the given logger
// that controller provides.
var WithLogrLogger = func(logger logr.Logger) RuntimeContextOption {
	return func(rtCtx *RuntimeContext) {
		rtCtx.Logger = slog.New(toSlogHandler(logger))
	}
}

// NewRuntimeContext creates a new RuntimeContext using the associated context
// and the provided options.
//
// It returns error when it can not create a functioning context. Optional
// fields that are not set are initialized with default values.
func NewRuntimeContext(ctx context.Context, opts ...RuntimeContextOption) (*RuntimeContext, error) {
	if ctx == nil {
		return nil, fmt.Errorf("cannot create context from nil source")
	}

	rtCtx := &RuntimeContext{
		Context: ctx,
	}

	for _, opt := range opts {
		opt(rtCtx)
	}

	if rtCtx.Logger == nil {
		rtCtx.Logger = internal.DiscardLogger
	}

	rtCtx.State = internal.StateRegistry{}

	return rtCtx, nil
}

// ContextualValue is an arbitrary value that is evaluated within the given
// RuntimeContext and is retrieved when needed.
//
// It provides an access point to the shared state that is stored in the context.
// This is the main interface for passing parameters between Tasks.
type ContextualValue[T any] interface {
	// Get returns a list of concrete values that match the expected criteria
	// that are defined by an implementation.
	//
	// The implementation must return an ErrNotFound error when none of the
	// values in the shared state match its expected criteria. When there is an
	// error, an empty list is returned. It is the responsibility of the caller
	// to handle the error and decide how to use the empty list.
	Get(*RuntimeContext) ([]T, error)
}

// ContextualValueFunc is a function that implements ContextualValue interface.
type ContextualValueFunc[T any] func(*RuntimeContext) ([]T, error)

// Get implements ContextualValue interface. It calls the function.
func (f ContextualValueFunc[T]) Get(rtCtx *RuntimeContext) ([]T, error) {
	return f(rtCtx)
}

// StaticValue is context-free value that adheres to ContextualValue interface.
// It always returns the same value regardless of the given context.
type StaticValue[T any] []T

// Get always returns the same set of the values.
func (v StaticValue[T]) Get(_ *RuntimeContext) ([]T, error) {
	return v, nil
}

var (
	// ErrNotFound is returned when no value matches the expected criteria of a
	// ContextualValue implementation.
	ErrNotFound = errors.New("not found")
)
