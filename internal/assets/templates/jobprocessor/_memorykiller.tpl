{{/*
Renders the environment varibales to configure the Sidekiq memorykiller.

Expects a context with the following keys:
  - Spec: The memory killer settings from the JobProcessor CR.
  - Settings: The memory killer settings from the global settings.
*/}}
- name: SIDEKIQ_MEMORY_KILLER_CHECK_INTERVAL
  value: {{ .Spec.CheckInterval | default .Settings.CheckInterval | toSeconds | quote }}
- name: SIDEKIQ_MEMORY_KILLER_MAX_RSS
  value: {{ .Spec.MaxRSS | default .Settings.MaxRSS | toBytes | quote }}
- name: SIDEKIQ_MEMORY_KILLER_GRACE_TIME
  value: {{ .Spec.GraceTime | default .Settings.GraceTime | toSeconds | quote }}
- name: SIDEKIQ_MEMORY_KILLER_SHUTDOWN_WAIT
  value: {{ .Spec.ShutdownWait | default .Settings.ShutdownWait | toSeconds | quote }}
- name: SIDEKIQ_MEMORY_KILLER_HARD_LIMIT_RSS
  value: {{ .Spec.HardLimitRSS | default .Settings.HardLimitRSS | toBytes | quote }}
