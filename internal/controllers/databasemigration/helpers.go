package databasemigration

import (
	"fmt"

	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func (c *Controller) setStatusCondition(ctx *framework.RuntimeContext, condition metav1.Condition) error {
	return controllerutils.SetStatusCondition(ctx, c.dbMigration, &c.dbMigration.Status.Conditions, condition)
}

func (c *Controller) appConfigRef() (*types.NamespacedName, error) {
	ref := c.dbMigration.Spec.ApplicationConfigRef

	if ref.APIVersion != "" && ref.APIVersion != v2alpha2.GroupVersion.String() {
		return nil, fmt.Errorf("unsupported group and version for ApplicationConfig reference: %s", ref.APIVersion)
	}

	if ref.Kind != "" && ref.Kind != "ApplicationConfig" {
		return nil, fmt.Errorf("unsupported kind for ApplicationConfig reference: %s", ref.Kind)
	}

	if ref.Namespace != "" && ref.Namespace != c.dbMigration.Namespace {
		return nil, fmt.Errorf("expected ApplicationConfig to be in the same namespace: %s", ref.Namespace)
	}

	name := types.NamespacedName{
		Name:      ref.Name,
		Namespace: c.dbMigration.Namespace,
	}

	return &name, nil
}

func (c *Controller) railsSecretRef() (*types.NamespacedName, error) {
	sharedObjRef := apiutils.FindStatusSharedObject(c.appConfig.Status.SharedObjects, v2alpha2.RailsSecretsObjectUsage)
	if sharedObjRef == nil {
		return nil, fmt.Errorf("rails secrets is not specified: %s", client.ObjectKeyFromObject(c.appConfig))
	}

	return &types.NamespacedName{
		Name:      sharedObjRef.Name,
		Namespace: sharedObjRef.Namespace,
	}, nil
}

func (c *Controller) appConfigReady() framework.ConditionFunc {
	return func(ctx *framework.RuntimeContext) (bool, error) {
		readyCondition := meta.FindStatusCondition(c.appConfig.Status.Conditions, v2alpha2.ApplicationConfigReadyCondition)

		return readyCondition != nil && readyCondition.Status == metav1.ConditionTrue, nil
	}
}

func (c *Controller) getReferencedResources(ctx *framework.RuntimeContext) error {
	c.appConfigResources = appconfigutils.NewApplicationConfigDereferencer(c.appConfig,
		appconfigutils.RepositoryResources,
		appconfigutils.PostgreSQLResources,
		appconfigutils.RedisResources,
		appconfigutils.ClickHouseResources,
	)
	if err := c.appConfigResources.Dereference(ctx); err != nil {
		return err
	}

	return nil
}

func (c *Controller) allResourcesReady() framework.ConditionFunc {
	return func(ctx *framework.RuntimeContext) (bool, error) {
		return c.appConfigResources.Ready(), nil
	}
}

func (c *Controller) getJob(ctx *framework.RuntimeContext) (*batchv1.Job, error) {
	key := types.NamespacedName{
		Namespace: c.dbMigration.Namespace,
		Name:      "rails-" + c.dbMigration.Name + "-migration",
	}
	job := &batchv1.Job{}

	if err := framework.Client.Get(ctx, key, job); err != nil {
		return nil, err
	}

	return job, nil
}
