package apiutils

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

var _ = Describe("Shared Objects", func() {
	Context("SharedObjectReferenceForObject", func() {
		It("creates a reference from a client.Object", func() {
			pod := &corev1.Pod{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-pod",
					Namespace: "test-namespace",
				},
				TypeMeta: metav1.TypeMeta{
					APIVersion: "v1",
					Kind:       "Pod",
				},
			}

			ref := SharedObjectReferenceForObject("test-usage", pod)

			Expect(ref).To(Equal(v2alpha2.SharedObjectReference{
				Usage:      "test-usage",
				Name:       "test-pod",
				Namespace:  "test-namespace",
				APIVersion: "v1",
				Kind:       "Pod",
			}))
		})
	})

	Context("SharedObjectReferenceForObjectKey", func() {
		It("creates a reference from a client.ObjectKey", func() {
			key := client.ObjectKey{
				Name:      "test-object",
				Namespace: "test-namespace",
			}

			ref := SharedObjectReferenceForObjectKey("test-usage", key)

			Expect(ref).To(Equal(v2alpha2.SharedObjectReference{
				Usage:     "test-usage",
				Name:      "test-object",
				Namespace: "test-namespace",
			}))
		})
	})

	Context("SharedObjectReferenceForObjectRef", func() {
		It("creates a reference from a corev1.ObjectReference", func() {
			objRef := corev1.ObjectReference{
				Name:       "test-ref",
				Namespace:  "test-namespace",
				APIVersion: "v1",
				Kind:       "Secret",
			}

			ref := SharedObjectReferenceForObjectRef("test-usage", objRef)

			Expect(ref).To(Equal(v2alpha2.SharedObjectReference{
				Usage:      "test-usage",
				Name:       "test-ref",
				Namespace:  "test-namespace",
				APIVersion: "v1",
				Kind:       "Secret",
			}))
		})
	})

	Context("SetStatusSharedObject", func() {
		var sharedObjects []v2alpha2.SharedObjectReference

		BeforeEach(func() {
			sharedObjects = []v2alpha2.SharedObjectReference{
				{
					Usage:     "existing-usage",
					Name:      "old-name",
					Namespace: "old-namespace",
				},
			}
		})

		It("adds a new shared object reference", func() {
			newRef := v2alpha2.SharedObjectReference{
				Usage:     "new-usage",
				Name:      "new-name",
				Namespace: "new-namespace",
			}

			changed := SetStatusSharedObject(&sharedObjects, newRef)

			Expect(changed).To(BeTrue())
			Expect(sharedObjects).To(HaveLen(2))
			Expect(sharedObjects).To(ContainElement(newRef))
		})

		It("updates an existing shared object reference", func() {
			updatedRef := v2alpha2.SharedObjectReference{
				Usage:     "existing-usage",
				Name:      "new-name",
				Namespace: "new-namespace",
			}

			changed := SetStatusSharedObject(&sharedObjects, updatedRef)

			Expect(changed).To(BeTrue())
			Expect(sharedObjects).To(HaveLen(1))
			Expect(sharedObjects[0]).To(Equal(updatedRef))
		})

		It("returns false when no changes are made", func() {
			sameRef := v2alpha2.SharedObjectReference{
				Usage:     "existing-usage",
				Name:      "old-name",
				Namespace: "old-namespace",
			}

			changed := SetStatusSharedObject(&sharedObjects, sameRef)

			Expect(changed).To(BeFalse())
		})

		It("returns false when sharedObjects is nil", func() {
			var nilSharedObjects *[]v2alpha2.SharedObjectReference
			changed := SetStatusSharedObject(nilSharedObjects, v2alpha2.SharedObjectReference{})

			Expect(changed).To(BeFalse())
		})
	})

	Context("RemoveStatusSharedObject", func() {
		var sharedObjects []v2alpha2.SharedObjectReference

		BeforeEach(func() {
			sharedObjects = []v2alpha2.SharedObjectReference{
				{
					Usage: "usage-1",
					Name:  "name-1",
				}, {
					Usage: "usage-2",
					Name:  "name-2",
				},
			}
		})

		It("removes an existing shared object reference", func() {
			changed := RemoveStatusSharedObject(&sharedObjects, "usage-1")

			Expect(changed).To(BeTrue())
			Expect(sharedObjects).To(HaveLen(1))
			Expect(sharedObjects[0].Usage).To(Equal("usage-2"))
		})

		It("returns false when usage doesn't exist", func() {
			changed := RemoveStatusSharedObject(&sharedObjects, "non-existent")

			Expect(changed).To(BeFalse())
			Expect(sharedObjects).To(HaveLen(2))
		})

		It("returns false when sharedObjects is nil", func() {
			var nilSharedObjects *[]v2alpha2.SharedObjectReference
			changed := RemoveStatusSharedObject(nilSharedObjects, "usage")

			Expect(changed).To(BeFalse())
		})

		It("returns false when sharedObjects is empty", func() {
			emptySharedObjects := []v2alpha2.SharedObjectReference{}
			changed := RemoveStatusSharedObject(&emptySharedObjects, "usage")

			Expect(changed).To(BeFalse())
		})
	})

	Context("FindStatusSharedObject", func() {
		var sharedObjects []v2alpha2.SharedObjectReference

		BeforeEach(func() {
			sharedObjects = []v2alpha2.SharedObjectReference{
				{
					Usage: "usage-1",
					Name:  "name-1",
				}, {
					Usage: "usage-2",
					Name:  "name-2",
				},
			}
		})

		It("finds an existing shared object reference", func() {
			found := FindStatusSharedObject(sharedObjects, "usage-1")

			Expect(found).NotTo(BeNil())
			Expect(found.Name).To(Equal("name-1"))
		})

		It("returns nil when usage doesn't exist", func() {
			found := FindStatusSharedObject(sharedObjects, "non-existent")

			Expect(found).To(BeNil())
		})

		It("returns nil for empty shared objects list", func() {
			found := FindStatusSharedObject([]v2alpha2.SharedObjectReference{}, "usage")

			Expect(found).To(BeNil())
		})
	})
})
