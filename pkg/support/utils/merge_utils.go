package utils

import "sort"

// MergeMap merges two maps of items.
// If a key exists in both maps, the value from `updated` will be used.
func MergeMap[K comparable, V any](original, updated map[K]V) map[K]V {
	result := map[K]V{}
	for k, v := range original {
		result[k] = v
	}

	for k, v := range updated {
		result[k] = v
	}

	return result
}

// MergeSlices merges two slices of items based on the specified key.
// `keyFunc` extracts the key from an item to determine uniqueness.
func MergeSlices[T any](original, updated []T, keyFunc func(T) string) []T {
	// Create a map to store merged items by their key
	mergedMap := make(map[string]T)

	// Add all items from the original slice
	for _, item := range original {
		key := keyFunc(item)
		mergedMap[key] = item
	}

	// Add/overwrite items from the updated slice
	for _, item := range updated {
		key := keyFunc(item)
		mergedMap[key] = item
	}

	// Convert the map back to a slice
	mergedSlice := make([]T, 0, len(mergedMap))
	for _, item := range mergedMap {
		mergedSlice = append(mergedSlice, item)
	}

	// sort the slice
	sort.Slice(mergedSlice, func(i, j int) bool {
		return keyFunc(mergedSlice[i]) < keyFunc(mergedSlice[j])
	})

	return mergedSlice
}
