package controllerutils

import (
	"hash"
	"time"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// TaskOption is a marker interface and provides a generic way to apply
// various options to a Task in its procedural form.
type TaskOption interface{}

func WithHash(h hash.Hash) TaskOption {
	return digestHash{h}
}

func WithPatchMethod(p client.Patch) TaskOption {
	return patchAlgorithm{p}
}

func WithPatchOptions(o client.PatchOptions) TaskOption {
	return patchOptions{o}
}

func WithDelay(d time.Duration) TaskOption {
	return waitDelay{d}
}

type IgnoreErrorFunc func(error) bool

var (
	IgnoreNotFound IgnoreErrorFunc = func(err error) bool {
		return kerrors.IsNotFound(err)
	}

	IgnoreAllErrors IgnoreErrorFunc = func(_ error) bool {
		return true
	}
)

//nolint:unused
func (f IgnoreErrorFunc) isTaskOption() {}

type digestHash struct{ hash.Hash }

//nolint:unused
func (h digestHash) isTaskOption() {}

type patchAlgorithm struct{ client.Patch }

//nolint:unused
func (p patchAlgorithm) isTaskOption() {}

type patchOptions struct{ client.PatchOptions }

//nolint:unused
func (o patchOptions) isTaskOption() {}

type waitDelay struct{ time.Duration }

//nolint:unused
func (o waitDelay) isTaskOption() {}
