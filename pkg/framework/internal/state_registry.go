package internal

// SharedStateVisitor is a callback function that is called for each state entry.
type SharedStateVisitor func(string, any, ...string) bool

// StateRegistry is a map-based implementation of SharedState interface.
type StateRegistry map[string]*stateEntry

type stateEntry struct {
	name  string
	value any
	tags  []string
}

// Implementation of SharedState interface.
func (s StateRegistry) Store(name string, value any, tags ...string) {
	s[name] = &stateEntry{
		name:  name,
		value: value,
		tags:  Unique(tags),
	}
}

// Implementation of SharedState interface.
func (s StateRegistry) Get(name string) (any, bool) {
	if e, ok := s[name]; !ok {
		return nil, false
	} else {
		return e.value, true
	}
}

// Implementation of SharedState interface.
func (s StateRegistry) Visit(f SharedStateVisitor) {
	for _, entry := range s {
		if !f(entry.name, entry.value, entry.tags...) {
			break
		}
	}
}
