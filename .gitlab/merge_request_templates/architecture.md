<!-- Architecture MR template -->

/label ~"Architecture decision" ~documentation ~type::feature ~docs::feature
/label ~group::distribution ~group::distribution::deploy ~devops::systems ~"section::core platform"
/assign me

## What does this MR do?

This MR records the following Architectural Decision:

<!-- 
- **Number of ADR: Title of ADR document**
-->

## Author's checklist

- [ ] Ensure the branch name starts with `docs-`/`adr-` or ends with `-docs`/`-adr`, so only the relevant CI jobs are included.
- [ ] Ensure a release milestone is set.
- [ ] Follow the [recommendations](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions) for documenting Architectural Decisions
- [ ] Ensure that the document has the following sections:
  - [ ] Context
  - [ ] Decision
  - [ ] Consequences
- [ ] Follow the:
  - [Documentation process](https://docs.gitlab.com/ee/development/documentation/workflow.html).
  - [Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/).
  - [Style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
- [ ] MR title and description are up to date, accurate, and descriptive.
- [ ] MR targeting the appropriate branch.
- [ ] MR has a green pipeline on GitLab.com.
- [ ] When ready for review, MR is labeled ~"workflow::ready for review" per the [MR workflow](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/merge_requests.html).
- [ ] If you are a GitLab team member, [request a review](https://docs.gitlab.com/ee/development/code_review.html#dogfooding-the-reviewers-feature) based on:
  - [ ] The documentation page's [metadata](https://docs.gitlab.com/ee/development/documentation/metadata.html).
  - [ ] The [associated Technical Writer](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments).

If you are only adding documentation, do NOT add any of the following labels:

- ~frontend
- ~backend
- ~type::bug

These labels cause the MR to be added to code verification QA issues.

## Reviewer's checklist

Documentation MRs should be reviewed by a Technical Writer for a non-blocking review, based on [documentation guidelines](https://docs.gitlab.com/ee/development/documentation/) and the [style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).

If you aren't sure which Technical Writer to ask, use [roulette](https://gitlab-org.gitlab.io/gitlab-roulette/?sortKey=stats.avg30&order=-1&hourFormat24=true&visible=maintainer%7Cdocs) or ask in the [#docs](https://gitlab.slack.com/archives/C16HYA2P5):lock: Slack channel.

- [ ] Ensure that the document Status is `Approved` before merge.
- [ ] Ensure that the document Date is updated before merge.
- [ ] If the content requires it, ensure the information is reviewed by a subject matter expert.
- [ ] Technical writer review items:
  - [ ] Ensure document metadata is present and up-to-date.
  - [ ] Ensure the appropriate [labels](https://handbook.gitlab.com/handbook/product/ux/technical-writing/workflow/#labels) are added to this MR.
  - [ ] If relevant to this MR, ensure [content topic type](https://docs.gitlab.com/ee/development/documentation/topic_types/) principles are in use, including:
    - [ ] The headings should be something you'd do a Google search for. Instead of `Default behavior`, say something like `Default behavior when you close an issue`.
    - [ ] The headings (other than the page title) should be active. Instead of `Configuring GDK`, say something like `Configure GDK`.
    - [ ] Any task steps should be written as a numbered list.
    - [ ] If the content still needs to be edited for topic types, you can create a follow-up issue with the ~docs-technical-debt label.
- [ ] Review by assigned maintainer, who can always request/require the reviews above. Maintainer's review can occur before or after a technical writer review.

## Related issues
