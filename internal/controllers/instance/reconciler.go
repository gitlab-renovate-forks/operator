package instance

import (
	"context"
	"fmt"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// +kubebuilder:rbac:groups=gitlab.com,resources=instances,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=instances/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=instances/finalizers,verbs=update
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=databasemigrations,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=jobprocessors,verbs=get;list;watch;create;update;patch;delete

type Reconciler struct{}

func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.Instance) (ctrl.Result, error) {
	logger := ctrl.Log.
		WithName("controllers.instance").
		WithValues(
			"resource", fmt.Sprintf("%s/%s", obj.Namespace, obj.Name),
			"generation", obj.Generation,
			"resourceVersion", obj.ResourceVersion,
		)

	rtCtx, err := framework.NewRuntimeContext(ctx, framework.WithLogrLogger(logger))
	if err != nil {
		return framework.Expand(err)
	}

	err = NewController(obj.DeepCopy()).Execute(rtCtx)

	return framework.Expand(err)
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	b := ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.Instance{}).
		Owns(&v2alpha2.DatabaseMigration{}).
		WithEventFilter(predicate.Or(predicate.GenerationChangedPredicate{}, predicate.LabelChangedPredicate{}, predicate.AnnotationChangedPredicate{}))

	return b.Complete(reconcile.AsReconciler(mgr.GetClient(), r))
}
