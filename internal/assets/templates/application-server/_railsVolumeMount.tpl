{{- $railsCfgEntries := list "gitlab.yml" "database.yml" -}}

{{- with .ApplicationConfig.Spec.Redis -}}
{{- $railsCfgEntries = concat $railsCfgEntries (mapRedisConfigFileNames . | values) -}}
{{- end -}}

{{- if .ApplicationConfig.Spec.ClickHouse -}}
{{- $railsCfgEntries = append $railsCfgEntries "click_house.yml" -}}
{{- end -}}

- name: rails-config
  mountPath: {{ .Settings.Rails.HomeDir }}/INSTALLATION_TYPE
  subPath: INSTALLATION_TYPE

- name: rails-config
  mountPath: {{ $.Settings.Rails.ConfigDir }}/initializers/smtp_settings.rb
  subPath: smtp_settings.rb

{{- $ := . -}}
{{- range $railsCfgEntries  }}
- name: rails-config
  mountPath: {{ $.Settings.Rails.ConfigDir }}/{{ . }}
  subPath: {{ . }}
{{- end }}

- name: rails-secrets
  mountPath: {{ .Settings.Rails.ConfigDir }}/secrets.yml
  subPath: secrets.yml
