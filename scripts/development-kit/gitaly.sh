#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

prepare() {
    echo_stage "Prepare Gitaly"

    kubectl -n $(namespace) create secret generic "gitaly-secret" --from-literal="token"=$(gen_random 64)

    echo_stage "Use gitaly-secret"
}

install() {
    echo_stage "Install Gitaly"

    # prompt to input the application server service name
    app_server_name=$(read_input_with_default "Enter the application server name" "$(release)")

    # remove trailing newline characters

    sed -e "s/##APPLICATION_NAME##/$app_server_name/g" -e "s/##NAMESPACE##/$(namespace)/g" $SCRIPT_DIR/manifests/gitaly.yaml | kubectl apply -n $(namespace) -f -
}

uninstall() {
    echo_stage "Uninstall Gitaly"

    kubectl -n $(namespace) delete secret gitaly-secret --ignore-not-found=true
    kubectl -n $(namespace) delete -f $SCRIPT_DIR/manifests/gitaly.yaml --ignore-not-found=true
}

usage() {
    echo "Usage: $0 [prepare|install|uninstall]"
    echo "Note:"
    echo "  Run prepare before installing the CR."
    exit 1
}

case "$1" in
prepare)
    prepare
    ;;
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
