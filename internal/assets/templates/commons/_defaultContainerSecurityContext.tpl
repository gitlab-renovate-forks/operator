{{/*
Prints out the default security context for Pod containers.

Expects a context with the following keys:

  - UserID: Integer value for RunAsUser as expected in SecurityContext.

These are the default values that are defined in application-specific settings,
for example `.Settings.Rails`.
*/}}
securityContext:
  runAsUser: {{ .UserID }}
  allowPrivilegeEscalation: false
  runAsNonRoot: true
  capabilities:
    drop:
      - ALL