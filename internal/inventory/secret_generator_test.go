package inventory

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
)

var _ = Describe("Inventory", Ordered, func() {
	var (
		fakeScheme   *runtime.Scheme
		fakeClient   client.WithWatch
		mockGen      *mockGenerator
		mockResource *corev1.Pod
		mockSecret   *corev1.Secret
		rtCtx        *framework.RuntimeContext
	)

	BeforeAll(func() {
		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))

		mockResource = &corev1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-resource",
				Namespace: "default",
			},
		}

		mockSecret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-secret",
				Namespace: "default",
			},
			Type: corev1.SecretTypeOpaque,
			Data: map[string][]byte{
				"key": []byte("test-secret-value"),
			},
		}

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(mockResource, mockSecret).
			Build()

		mockGen = &mockGenerator{
			payload: secret.Payload{"key": []byte("value")},
		}
	})

	BeforeEach(func() {
		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
		rtCtx.State.Store("Resource", mockResource)

	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	It("expands the Name and Namespace", func() {
		inventory := &SecretGenerator{
			Name:       "{{ .Resource.Name }}-secret",
			Namespace:  "{{ .Resource.Namespace }}",
			SecretType: corev1.SecretTypeOpaque,
			Generators: []secret.Generator{mockGen},
		}

		secrets, err := inventory.Get(rtCtx)

		Expect(err).NotTo(HaveOccurred())
		Expect(secrets).To(HaveLen(1))
		Expect(secrets[0].Name).To(Equal("test-resource-secret"))
		Expect(secrets[0].Namespace).To(Equal("default"))
		Expect(secrets[0].Type).To(Equal(corev1.SecretTypeOpaque))
		Expect(secrets[0].Data).To(HaveKeyWithValue("key", []byte("value")))
	})

	It("returns an error when expansion of Name or Namespace fails", func() {
		inventory := &SecretGenerator{
			Name:       "{{ .Resource.Name }}-secret",
			Namespace:  "{{ Invalid }}",
			Generators: []secret.Generator{mockGen},
		}

		secrets, err := inventory.Get(rtCtx)

		Expect(err).To(HaveOccurred())
		Expect(secrets).To(BeEmpty())
	})

	It("returns an error when generator fails", func() {
		inventory := &SecretGenerator{
			Name:      "test-secret",
			Namespace: "default",
			Generators: []secret.Generator{&mockGenerator{
				shouldFail: true,
			}},
		}

		secrets, err := inventory.Get(rtCtx)

		Expect(err).To(HaveOccurred())
		Expect(secrets).To(BeEmpty())
	})

	When("GenerateIfNeeded is true", func() {
		It("returns a Secret with generated content when it does not exist", func() {
			inventory := &SecretGenerator{
				Name:             "test-new-secret",
				Namespace:        "default",
				SecretType:       corev1.SecretTypeOpaque,
				Generators:       []secret.Generator{mockGen},
				GenerateIfNeeded: true,
			}

			secrets, err := inventory.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(secrets).To(HaveLen(1))
			Expect(secrets[0].Name).To(Equal("test-new-secret"))
			Expect(secrets[0].Namespace).To(Equal("default"))
			Expect(secrets[0].Data).To(HaveKeyWithValue("key", []byte("value")))
		})

		It("skips generation and returns the existing Secret when it exists", func() {
			inventory := &SecretGenerator{
				Name:             "test-secret",
				Namespace:        "default",
				SecretType:       corev1.SecretTypeOpaque,
				Generators:       []secret.Generator{mockGen},
				GenerateIfNeeded: true,
			}

			secrets, err := inventory.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(secrets).To(HaveLen(1))
			Expect(secrets[0].Name).To(Equal("test-secret"))
			Expect(secrets[0].Namespace).To(Equal("default"))
			Expect(secrets[0].Data).To(HaveKeyWithValue("key", []byte("test-secret-value")))
		})
	})

	When("GenerateIfNeeded is false", func() {
		It("returns a Secret with generated content even if it exists", func() {
			inventory := &SecretGenerator{
				Name:             "test-secret",
				Namespace:        "default",
				SecretType:       corev1.SecretTypeOpaque,
				Generators:       []secret.Generator{mockGen},
				GenerateIfNeeded: false,
			}

			secrets, err := inventory.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(secrets).To(HaveLen(1))
			Expect(secrets[0].Name).To(Equal("test-secret"))
			Expect(secrets[0].Namespace).To(Equal("default"))
			Expect(secrets[0].Data).To(HaveKeyWithValue("key", []byte("value")))
		})
	})
})

type mockGenerator struct {
	payload    secret.Payload
	shouldFail bool
}

func (m *mockGenerator) Generate() (secret.Payload, error) {
	if m.shouldFail {
		return nil, fmt.Errorf("mock error")
	}

	return m.payload, nil
}
