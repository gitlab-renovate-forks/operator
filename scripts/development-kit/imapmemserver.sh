#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    echo_stage "Installing Imapmemserver"
    read -r -d '' command <<-EOF || true
    apt-get update && apt-get install -y netcat-traditional
    go install github.com/emersion/go-imap/v2/cmd/imapmemserver@latest
    /go/bin/imapmemserver --listen=0.0.0.0:143 --username=inbox@example.com --password=password --debug --insecure-auth
EOF
  
    kubectl -n $(namespace) create secret generic mailroom-inbox-authentication \
        --from-literal password="password"
  
    kubectl run imapmemserver -n $(namespace) --image=golang \
        --port=143 \
        --restart='Always' \
        --expose \
        --command -- sh -c "$command"
}

mail() {
    read -r -d '' email <<-EOF || true
    From: inbox@example.com
    To: inbox+test@example.com
    Subject: Test

    Hello, World!

EOF
    kubectl exec -n $(namespace) imapmemserver -it -- bash -c '{ sleep 1; echo "a1 LOGIN inbox@example.com password"; sleep 1; echo "a2 SELECT \"INBOX\""; sleep 1; echo "a3 APPEND \"INBOX\" {${#email}}"; sleep 1; echo $email; sleep 1; echo "a4 LOGOUT"; sleep 1; } | nc 0.0.0.0 143'
}

uninstall() {
    echo_stage "Uninstalling Imapmemserver"
    kubectl delete -n $(namespace) secret --wait --ignore-not-found mailroom-inbox-authentication
    kubectl delete -n $(namespace) pod --wait --ignore-not-found imapmemserver
    kubectl delete -n $(namespace) service --wait --ignore-not-found imapmemserver
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    exit 1
}

case "$1" in
install)
    install
    ;;
mail)
    mail
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
