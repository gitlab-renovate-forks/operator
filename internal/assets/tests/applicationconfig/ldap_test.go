package applicationconfig

import (
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/ptr"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _ldap.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/ldap.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleLDAP, "ldap/simple.yaml"),
	)
})

var (
	simpleLDAP = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				LDAP: &v2alpha2.ApplicationLDAPSettings{
					PreventSignIn: false,
					Servers: []v2alpha2.LDAPServer{
						{
							Name:       "main",
							Label:      "LDAP Primary",
							URL:        "ldap://ldap.example.com:398",
							Encryption: ptr.To(v2alpha2.LDAPEncryptionPlain),
							Authentication: &v2alpha2.ServiceAuthentication{
								Basic: refSecret("default", "main"),
							},
							UID:    "uid",
							BindDN: "cn=admin,dc=example,dc=com",
							Settings: map[string]string{
								"base": "dc=example,dc=com",
							},
							Timeout: &metav1.Duration{Duration: time.Minute},
						},
						{
							Name:       "secondary",
							Label:      "LDAP Secondary",
							URL:        "ldaps://ldap.example.com:636",
							Encryption: ptr.To(v2alpha2.LDAPEncryptionSimpleTLS),
							Authentication: &v2alpha2.ServiceAuthentication{
								Basic: refSecret("default", "secondary"),
							},
							UID:    "uid",
							BindDN: "cn=admin,dc=example,dc=com",
							Settings: map[string]string{
								"smartcardAuth":        "required",
								"smartcardADCertField": "extendedAttribute1",
								"activeDirectory":      "true",
								"syncSshKeys":          "true",
							},
							Attributes: map[string]string{
								"username": "uid,userid",
							},
						},
					},
				},
			},
		},
		"LDAPSecrets": map[string]*corev1.Secret{
			"main": makeSecret("default", "main",
				"password", "password",
			),
			"secondary": makeSecret("default", "secondary",
				"password", "password",
			),
		},

		"LDAPEndpoints": map[string]apiutils.ServiceEndpoint{
			"main": {
				Scheme: "ldap",
				Host:   "ldap.example.com",
				Port:   398,
			},
			"secondary": {
				Scheme: "ldaps",
				Host:   "ldap.example.com",
				Port:   636,
			},
		},
	}
)
