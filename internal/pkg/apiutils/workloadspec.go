package apiutils

import (
	"encoding/json"
	"fmt"
	"strings"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/strategicpatch"
	"sigs.k8s.io/yaml"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

// ApplyWorkloadSpec applies the provided WorkloadSpec to a PodTemplateSpec.
//
// This function modifies the given PodTemplateSpec by incorporating settings
// defined in the WorkloadSpec. It uses struct tags to generate a strategic
// merge patch, which is then applied to the PodTemplateSpec.
//
// The strategic merge patch allows for intelligent merging of partial
// specifications, respecting the semantics of specific Kubernetes object
// fields.
func ApplyWorkloadSpec(spec *v2alpha2.WorkloadSpec, podTemplate *corev1.PodTemplateSpec) error {
	if spec == nil || podTemplate == nil {
		return nil
	}

	if spec.ImageSource != nil {
		podTemplate.Spec.ImagePullSecrets = utils.MergeSlices(
			podTemplate.Spec.ImagePullSecrets, spec.ImageSource.PullSecrets,
			func(secret corev1.LocalObjectReference) string { return secret.Name },
		)
	}

	if userPodTemplate := spec.PodTemplate; userPodTemplate != nil {
		if md := userPodTemplate.Metadata; md != nil {
			applyPodMetadata(podTemplate, md)
		}

		if spec := userPodTemplate.Spec; spec != nil {
			if err := applyPodSpec(podTemplate, spec); err != nil {
				return err
			}
		}
	}

	return applyContainerSpecs(podTemplate, spec.ManagedContainers)
}

// ApplyWorkloadSpecToYAML applies a WorkloadSpec to a YAML-formatted PodTemplateSpec.
// It is intended for use in templates, where the PodTemplateSpec will be piped
// in as a string.
//
// This function takes a string representation of PodTemplateSpec in YAML
// format. It unmarshals the YAML into a PodTemplateSpec, applies the WorkloadSpec
// to it, and returns the modified PodTemplateSpec.
func ApplyWorkloadSpecToYAML(spec *v2alpha2.WorkloadSpec, podTemplate string) (*corev1.PodTemplateSpec, error) {
	podTmpl := &corev1.PodTemplateSpec{}
	if err := yaml.Unmarshal([]byte(podTemplate), podTmpl); err != nil {
		return nil, fmt.Errorf("failed to unmarshal PodTemplateSpec: %w", err)
	}

	if err := ApplyWorkloadSpec(spec, podTmpl); err != nil {
		return nil, fmt.Errorf("failed to apply PodTemplateSpec: %w", err)
	}

	return podTmpl, nil
}

func applyContainerSpecs(podTemplateSpec *corev1.PodTemplateSpec, containerSpec []v2alpha2.ManagedContainerSpec) error {
	for _, spec := range containerSpec {
		for i, c := range podTemplateSpec.Spec.InitContainers {
			if isManagedContainer(c.Name, spec.Name) {
				if err := applyContainerSpec(&c, &spec); err != nil {
					return err
				}

				podTemplateSpec.Spec.InitContainers[i] = c

				break
			}
		}

		for i, c := range podTemplateSpec.Spec.Containers {
			if isManagedContainer(c.Name, spec.Name) {
				if err := applyContainerSpec(&c, &spec); err != nil {
					return err
				}

				podTemplateSpec.Spec.Containers[i] = c

				break
			}
		}
	}

	return nil
}

func applyPodMetadata(podTemplateSpec *corev1.PodTemplateSpec, metadata *v2alpha2.PodMetadata) {
	podTemplateSpec.ObjectMeta.Labels = utils.MergeMap(podTemplateSpec.ObjectMeta.Labels, metadata.Labels)
	podTemplateSpec.ObjectMeta.Annotations = utils.MergeMap(podTemplateSpec.ObjectMeta.Annotations, metadata.Annotations)
}

func applyPodSpec(podTemplateSpec *corev1.PodTemplateSpec, spec *v2alpha2.RestrictedPodSpec) error {
	podSpec := podTemplateSpec.Spec

	// user provided volumes and volume mounts will override the ones in the pod spec if they have the same name.
	podSpec.Volumes = utils.MergeSlices(
		podSpec.Volumes, spec.ExtraVolumes,
		func(v corev1.Volume) string { return v.Name },
	)

	for i, c := range podSpec.InitContainers {
		c.VolumeMounts = utils.MergeSlices(
			c.VolumeMounts, spec.ExtraVolumeMounts,
			func(vm corev1.VolumeMount) string { return vm.MountPath },
		)
		podSpec.InitContainers[i] = c
	}

	for i, c := range podSpec.Containers {
		c.VolumeMounts = utils.MergeSlices(
			c.VolumeMounts, spec.ExtraVolumeMounts,
			func(vm corev1.VolumeMount) string { return vm.MountPath },
		)
		podSpec.Containers[i] = c
	}

	// extra containers, they won't be patched by the image source.
	// user provided containers will override the ones in the pod spec if they have the same name.
	podSpec.InitContainers = utils.MergeSlices(
		podSpec.InitContainers, spec.ExtraInitContainers,
		func(c corev1.Container) string { return c.Name },
	)
	podSpec.Containers = utils.MergeSlices(
		podSpec.Containers, spec.ExtraContainers,
		func(c corev1.Container) string { return c.Name },
	)

	// strategic merge patch
	// we just happen to have the same strategy for applying the spec to our deployment
	current, _ := json.Marshal(podSpec)
	patch, _ := json.Marshal(spec)
	out, err := strategicpatch.StrategicMergePatch(current, patch, v2alpha2.RestrictedPodSpec{})

	if err != nil {
		// something went wrong with the definition of the patch
		return err
	}

	if err := json.Unmarshal(out, &podSpec); err != nil {
		// something went wrong with the application of the patch
		return err
	}

	podTemplateSpec.Spec = podSpec

	return nil
}

func applyContainerSpec(c *corev1.Container, spec *v2alpha2.ManagedContainerSpec) error {
	// strategic merge patch
	current, _ := json.Marshal(c)
	patch, _ := json.Marshal(spec)
	out, err := strategicpatch.StrategicMergePatch(current, patch, v2alpha2.ManagedContainerSpec{})

	if err != nil {
		return err
	}

	if err := json.Unmarshal(out, c); err != nil {
		return err
	}

	return nil
}

func isManagedContainer(internalName, specName string) bool {
	return strings.EqualFold(internalName, specName)
}
