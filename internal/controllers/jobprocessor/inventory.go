package jobprocessor

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	framework "gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
)

func (c *Controller) workloadInventory(fingerprint string) framework.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"JobProcessor":      c.jobProcessor,
		"ApplicationConfig": c.appConfig,
		"Fingerprint":       fingerprint,
	}, c.appConfigResources.Resources)

	return framework.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "jobprocessor/deployment.yaml",
			Values:  values,
		},
		framework.WithControllerReference(c.jobProcessor))
}

func (c *Controller) configSecretsInventory() framework.Inventory[client.Object] {
	values := utils.MergeMap(map[string]any{
		"JobProcessor":      c.jobProcessor,
		"ApplicationConfig": c.appConfig,
	}, c.appConfigResources.Resources)

	return framework.Decorate(
		&inventory.TemplatedObjects{
			Pattern: "jobprocessor/config/*.yaml",
			Values:  values,
		},
		framework.WithControllerReference(c.jobProcessor))
}
