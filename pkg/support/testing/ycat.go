// This code is modified from : https://github.com/goccy/go-yaml/blob/df5e06fe582756efad49b1e11948635c811c1a16/cmd/ycat/ycat.go
//
// MIT License
//
// Copyright (c) 2019 Masaaki Goshima
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package testing

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/fatih/color"
	"github.com/goccy/go-yaml/lexer"
	"github.com/goccy/go-yaml/printer"
	"gopkg.in/yaml.v3"
)

const escape = "\x1b"

func colorFormat(attr color.Attribute) string {
	return fmt.Sprintf("%s[%dm", escape, attr)
}

func ycat(in string, lineNum bool) string {
	var p printer.Printer

	in, err := prettifyYaml(in)
	if err != nil {
		panic(err)
	}

	tokens := lexer.Tokenize(in)
	p.LineNumber = lineNum
	p.LineNumberFormat = func(num int) string {
		fn := color.New(color.Bold, color.FgHiWhite).SprintFunc()
		return fn(fmt.Sprintf("%2d | ", num))
	}

	p.Bool = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiMagenta),
			Suffix: colorFormat(color.Reset),
		}
	}
	p.Number = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiMagenta),
			Suffix: colorFormat(color.Reset),
		}
	}
	p.MapKey = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiCyan),
			Suffix: colorFormat(color.Reset),
		}
	}
	p.Anchor = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiYellow),
			Suffix: colorFormat(color.Reset),
		}
	}
	p.Alias = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiYellow),
			Suffix: colorFormat(color.Reset),
		}
	}
	p.String = func() *printer.Property {
		return &printer.Property{
			Prefix: colorFormat(color.FgHiGreen),
			Suffix: colorFormat(color.Reset),
		}
	}

	return p.PrintTokens(tokens)
}

func prettifyYaml(input string) (string, error) {
	var node yaml.Node

	if err := yaml.Unmarshal([]byte(input), &node); err != nil {
		return "", err
	}

	buf := &strings.Builder{}

	prettifyString(&node)

	yamlEncoder := yaml.NewEncoder(buf)
	yamlEncoder.SetIndent(2)

	if err := yamlEncoder.Encode(&node); err != nil {
		return "", err
	}

	return buf.String(), nil
}

// prettifyString converts long string values in the YAML node to multiline strings.
func prettifyString(node *yaml.Node) {
	if node.Kind == yaml.ScalarNode && strings.Contains(node.Value, "\n") && node.Style != yaml.LiteralStyle {
		node.Style = yaml.LiteralStyle
		node.Value = removeTrailingSpaces(node.Value)
	}

	for _, child := range node.Content {
		prettifyString(child)
	}
}

func removeTrailingSpaces(s string) string {
	lines := strings.Split(s, "\n")

	for i, line := range lines {
		lines[i] = strings.TrimRightFunc(line, unicode.IsSpace)
	}

	return strings.Join(lines, "\n")
}
