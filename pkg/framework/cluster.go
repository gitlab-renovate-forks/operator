package framework

import (
	"path"
	"sync"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/cluster"
	"sigs.k8s.io/controller-runtime/pkg/manager"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/tools/record"
)

var (
	// Cluster interface of the manager.
	Cluster cluster.Cluster

	// Client is interface of the manager. It is retrieved from Cluster.
	Client client.Client

	// Scheme of the manager. It is retrieved from Cluster.
	Scheme *runtime.Scheme

	// ServerVersion is the server version information.
	ServerVersion *version.Info

	// KubeVersion is the Kubernetes version of the cluster. For more
	// details refer to ServerVersion.
	KubeVersion string

	// APIResources is a list of API resources supported by the cluster.
	APIResources []string
)

// EventRecorder returns an EventRecorder for the given name. It caches
// recorders for reuse. If a recorder with the given name exists, it will return
// it, otherwise, it will create a new one.
func EventRecorder(name string) record.EventRecorder {
	if recorder, exists := eventRecorders.Load(name); exists {
		return recorder.(record.EventRecorder)
	}

	recorder := Cluster.GetEventRecorderFor(name)
	actualRecorder, _ := eventRecorders.LoadOrStore(name, recorder)

	return actualRecorder.(record.EventRecorder)
}

// UseManager initializes the cluster interface with the given manager.
//
// IMPORTANT: Call this method only once in the main function before creating
// any controllers.
func UseManager(m manager.Manager) error {
	Cluster = m
	Client = m.GetClient()
	Scheme = m.GetScheme()

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(m.GetConfig())
	if err != nil {
		return err
	}

	if err := initServerVersion(discoveryClient); err != nil {
		return err
	}

	if err := initAPIResources(discoveryClient, false); err != nil {
		return err
	}

	return nil
}

func initServerVersion(discoveryClient discovery.DiscoveryInterface) error {
	ServerVersion, err := discoveryClient.ServerVersion()
	if err != nil {
		return err
	}

	KubeVersion = ServerVersion.GitVersion

	return nil
}

func initAPIResources(discoveryClient discovery.DiscoveryInterface, includeGroups bool) error {
	groups, resources, err := discoveryClient.ServerGroupsAndResources()
	if err != nil {
		return err
	}

	apiResourceMap := map[string]struct{}{}

	if includeGroups {
		for _, g := range groups {
			for _, gv := range g.Versions {
				apiResourceMap[gv.GroupVersion] = struct{}{}
			}
		}
	}

	for _, r := range resources {
		for _, rl := range r.APIResources {
			gvr := path.Join(r.GroupVersion, rl.Kind)
			apiResourceMap[gvr] = struct{}{}
		}
	}

	APIResources = []string{}
	for k := range apiResourceMap {
		APIResources = append(APIResources, k)
	}

	return nil
}

var (
	eventRecorders sync.Map
)
