package controllerutils

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// GetObjects retrieves objects of type T with the given names.
func DeleteObjects[T client.Object](rtCtx *framework.RuntimeContext, objects []T, opts ...TaskOption) ([]T, error) {
	t := &DeleteObjectsTask[T]{
		Objects: objects,
	}
	t.ApplyOptions(opts...)

	return t.Objects, t.Execute(rtCtx)
}

type DeleteObjectsTask[T client.Object] struct {
	Register    framework.Register
	Objects     []T
	IgnoreError IgnoreErrorFunc
}

func (t *DeleteObjectsTask[T]) ApplyOptions(opts ...TaskOption) {
	for _, o := range opts {
		switch o := o.(type) {
		case IgnoreErrorFunc:
			t.IgnoreError = o
		}
	}
}

func (t *DeleteObjectsTask[T]) Execute(rtCtx *framework.RuntimeContext) error {
	_, err := framework.MiddlewareChain{t, &t.Register}.Handle(rtCtx, nil, nil)

	return err
}

func (t *DeleteObjectsTask[T]) Handle(rtCtx *framework.RuntimeContext, value any, err error) (any, error) {
	if err != nil {
		return nil, err
	}

	for _, obj := range t.Objects {
		if err := framework.Client.Delete(rtCtx, obj); err != nil {
			if t.IgnoreError != nil && t.IgnoreError(err) {
				continue
			}

			return nil, err
		}
	}

	return t.Objects, nil
}
