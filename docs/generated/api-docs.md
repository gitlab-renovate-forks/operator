---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!---
This document is auto generated using elastic/crd-ref-docs.

https://github.com/elastic/crd-ref-docs

Use the Makefile target `api-docs` to update this file.
Do not change the content manually!
-->

<!-- markdownlint-disable -->

# API Reference

## Packages
- [gitlab.com/v2alpha2](#gitlabcomv2alpha2)


## gitlab.com/v2alpha2


### Resource Types
- [ApplicationConfig](#applicationconfig)
- [ApplicationConfigList](#applicationconfiglist)
- [ApplicationServer](#applicationserver)
- [ApplicationServerList](#applicationserverlist)
- [DatabaseMigration](#databasemigration)
- [DatabaseMigrationList](#databasemigrationlist)
- [Instance](#instance)
- [InstanceList](#instancelist)
- [JobProcessor](#jobprocessor)
- [JobProcessorList](#jobprocessorlist)
- [KubernetesAgentServer](#kubernetesagentserver)
- [KubernetesAgentServerList](#kubernetesagentserverlist)
- [SSHServer](#sshserver)
- [SSHServerList](#sshserverlist)



#### ApplicationCiscoDuoSettings



Settings for integrating with Cisco Duo one-time password authenticator.

See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `host` _string_ | Host and port of Cisco Duo instance. |  | Pattern: `^([\w.]*:?\d{0,5})$` <br /> |  |
| `integrationKey` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | Integration key of Cisco Duo instance. |  |  |  |
| `secretKey` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | Secret key of Cisco Duo instance. |  |  |  |


#### ApplicationConfig



ApplicationConfig contains GitLab Rails application configuration. It plays
an essential role in configuring a GitLab instance and is required for
running some of the key components of GitLab, in particular any Rail-based
workload such as Puma and Sidekiq.

An ApplicationConfig can be shared between multiple components. Generally, a
GitLab instance only has one ApplicationConfig, but this is not a requirement
that is enforced, and in rare cases an instance may have multiple
"non-conflicting" ApplicationConfigs.

ApplicationConfig offers a wide range of configuration items, including:

  - External URL and host names
  - Database and object store backends
  - Incoming and outgoing email settings
  - Various authentication providers
  - GitLab Rails application settings
  - Integrations with other GitLab and third-party components

As a rule of thumb you can assume that any configuration that is located in
GitLab Rails `config` directory is available through the ApplicationConfig.



_Appears in:_
- [ApplicationConfigList](#applicationconfiglist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `ApplicationConfig` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[ApplicationConfigSpec](#applicationconfigspec)_ | Specification of the desired GitLab Rails application configuration. |  |  |  |
| `status` _[ApplicationConfigStatus](#applicationconfigstatus)_ | Summary the most recently observed status of the ApplicationConfig. |  |  |  |


#### ApplicationConfigList



A collection of ApplicationConfigs.





| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `ApplicationConfigList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[ApplicationConfig](#applicationconfig) array_ | The list of ApplicationConfigs. |  |  |  |


#### ApplicationConfigSpec



The specification of the desired GitLab Rails application configuration.



_Appears in:_
- [ApplicationConfig](#applicationconfig)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `externalUrl` _string_ | The URL that GitLab instance is reachable at. It must be a valid HTTP(S)<br />URL without a relative path. |  | Pattern: `^https?:\/\/(([a-zA-Z0-9][-a-zA-Z0-9]*\.)+[a-zA-Z]{2,}&#124;\d{1,3}(\.\d{1,3}){3})(:\d+)?$` <br /> | https://gitlab.example.com<br />http://192.168.1.2:8000 |
| `cdnHost` _string_ | The URL of CDN or a host that is used for delivering static Rails assets. |  | Format: uri <br /> |  |
| `sshHost` _string_ | The host for Git access to GitLab repositories over SSH.<br />Use this if your SSH host is different from the host of the external URL. |  | Pattern: `^(([a-zA-Z0-9][-a-zA-Z0-9]*\.)+[a-zA-Z]{2,}&#124;\d{1,3}(\.\d{1,3}){3})(:\d+)?$` <br /> | ssh.example.com<br />192.168.1.2:2222 |
| `maxRequestDuration` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The maximum time Puma can spend on processing a request.<br />This needs to be smaller than the Puma worker timeout. It defaults to<br />95% of the Puma worker timeout. |  | Format: duration <br /> |  |
| `contentSecurityPolicy` _[ContentSecurityPolicy](#contentsecuritypolicy)_ | The configuration for Content Security Policy (CSP) headers.<br />See: https://guides.rubyonrails.org/security.html#content-security-policy |  |  |  |
| `allowedHosts` _string array_ | A list of hosts that are accepted to send requests to the instance.<br />Use this to prevent GitLab application from accepting a `Host` headers<br />other than what is intended. It is recommended for defense in depth<br />against potential HTTP Host header attacks. |  | items:Pattern: ^(\.?[a-zA-Z0-9][-a-zA-Z0-9.]*[a-zA-Z0-9]&#124;\d{1,3}(\.\d{1,3}){3}(\/\d{1,2})?)$ <br /> | example.com<br />192.168.1.2<br />10.0.0.0/24 |
| `trustedProxies` _string array_ | A list of trusted proxies. Customize if your instance is behind a reverse<br />proxy that runs on a different network.<br />Add the IP address for your reverse proxies to the list, otherwise users<br />will appear signed in from that address. |  | items:Format: cidr <br /> |  |
| `timezone` _string_ | The default time zone of GitLab Rails application.<br />Use the format of IANA Time Zone Database standard. |  |  | America/New_York |
| `outgoingEmail` _[ApplicationOutgoingEmail](#applicationoutgoingemail)_ | Configuration for outgoing emails from GitLab application. When disabled,<br />GitLab will not send any emails. |  |  |  |
| `settings` _object (keys:string, values:string)_ | General GitLab Rails application settings. The following options are<br />supported:<br />  - `defaultCanCreateGroup`: Allow users to create groups by default.<br />  - `usernameChangingEnabled`: Allow users to change their username and<br />    namespace.<br />	 - `defaultTheme`: Default theme identifier:<br />    - `1`: Indigo (the default theme)<br />    - `2`: Gray<br />    - `3`: Light Gray<br />    - `4`: Blue<br />    - `5`: Green<br />    - `6`: Light Indigo<br />    - `7`: Light Blue<br />    - `8`: Light Green<br />    - `9`: Red<br />    - `10`: Light Red<br />    - `11`: Dark Mode (alpha)<br />  - `customHtmlHeaderTags`: Custom HTML header tags, for example EU cookie<br />    consent.<br />    Tip: You must add the externals source to the CSP as well, typically<br />    with the `script_src` and `style_src`.<br />  - `issueClosingPattern`: If a commit message matches this regular<br />    expression, all issues referenced from the matched text will be<br />    closed. This happens when the commit is pushed or merged into the<br />    default branch of a project.<br />    When not specified the default issue closing pattern will be used.<br />  - `impersonationEnabled`: Allow administrators to be impersonate users.<br />    See: https://docs.gitlab.com/administration/admin_area/#user-impersonation<br />  - `disableAnimations`: Disable jQuery and CSS animations.<br />Note that the options are string-valued. You must use a string value. For<br />example use `"true"` instead of `true` or `"1"` instead of `1`. |  |  |  |
| `defaultProjectsFeatures` _object (keys:string, values:boolean)_ | Default features of GitLab projects. The following options are supported:<br />  - `issues`: Enable "Issue" boards.<br />  - `mergeRequests`: Enable "Merge Requests".<br />  - `wiki`: Enable project "Wiki" pages.<br />  - `snippets`: Enable code "Snippets" sharing.<br />  - `builds`: Enable project "CI/CD" for build, test, and deploy.<br />  - `containerRegistry`: Enable "Container Registry" to store Docker images.<br />All of theses options are enabled by default.<br />See: https://docs.gitlab.com/user/project/settings/ |  |  |  |
| `webhookTimeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The duration to wait for the HTTP response after sending a webhook HTTP<br />POST request. |  | Format: duration <br /> |  |
| `graphqlTimeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The duration that the application has to complete a GraphQL request.<br />We suggest this value to be higher than the database timeout value<br />and lower than the Puma worker timeout. |  | Format: duration <br /> |  |
| `applicationSettingsCache` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The duration of expiry for application settings cache. You can increase<br />this value to have more delay between application setting changes and<br />when users notice those changes in the application.<br />We recommend setting a non-zero value. Setting it to `0`` causes the<br />application settings to load for every request. This causes extra load<br />for Redis and PostgreSQL. |  | Format: duration <br /> |  |
| `incomingEmail` _[ApplicationIncomingEmail](#applicationincomingemail)_ | Configuration for incoming emails to GitLab application. When enabled,<br />several features will be available to users:<br />  - Reply by email: Comment on issues and merge requests by replying to<br />    notification email.<br />  - New issue by email: Create a new issue by sending an email to a<br />    user-specific email address.<br />  - New merge request by email: Create a new merge request by sending an<br />    email to a user-specific email address.<br />See: https://docs.gitlab.com/administration/incoming_email/ |  |  |  |
| `serviceDeskEmail` _[ApplicationIncomingEmail](#applicationincomingemail)_ | Configuration for incoming emails for Service Desk function of GitLab<br />application. When enabled, the application provide email support to your<br />customers through GitLab.<br />See: https://docs.gitlab.com/user/project/service_desk/ |  |  |  |
| `objectStorage` _[ApplicationObjectStorage](#applicationobjectstorage)_ | Configuration for storing various application data on object storage. |  |  |  |
| `mattermost` _[ServiceEndpoint](#serviceendpoint)_ | The endpoint for Mattermost. Must be defined for enabling "Mattermost"<br />button. |  |  |  |
| `gravatarUrl` _string_ | URL template for Gravatar.<br />The URL can have placeholders. Possible placeholders are:<br />	- `%\{hash\}`<br />	- `%\{size\}`<br />	- `%\{email\}`<br />	- `%\{username\}`<br />See: https://docs.gitlab.com/administration/libravatar/ |  | Format: uri-template <br /> |  |
| `cronJobs` _[JobSchedule](#jobschedule) array_ | Auxiliary jobs, executed periodically to perform various tasks, such as<br />self-heal and do external synchronizations. |  |  |  |
| `eeCronJobs` _[JobSchedule](#jobschedule) array_ | Auxiliary jobs, executed periodically to perform various tasks for GitLab<br />EE instances. These jobs are automatically enabled for an EE installation,<br />and ignored for a CE installation. |  |  |  |
| `sentry` _[SentrySupport](#sentrysupport)_ | Sentry settings for error reporting and logging. |  |  |  |
| `ldap` _[ApplicationLDAPSettings](#applicationldapsettings)_ | Settings for LDAP login providers.<br />See: https://docs.gitlab.com/administration/auth/ldap/ |  |  |  |
| `omniAuth` _[ApplicationOmniAuthSettings](#applicationomniauthsettings)_ | Settings for OmniAuth authentication providers.<br />For the list of available providers refer to GitLab documentation.<br />See: https://docs.gitlab.com/integration/omniauth/ |  |  |  |
| `smartCard` _[ApplicationSmartCardSettings](#applicationsmartcardsettings)_ | Settings for smart card authentication.<br />See: https://docs.gitlab.com/administration/auth/smartcard/ |  |  |  |
| `kerberos` _[ApplicationKerberosSettings](#applicationkerberossettings)_ | Settings for Kerberos authentication.<br />See: https://docs.gitlab.com/integration/kerberos/ |  |  |  |
| `fortiAuthenticator` _[ApplicationFortiAuthenticatorSettings](#applicationfortiauthenticatorsettings)_ | Settings for integrating with FortiAuthenticator one-time password<br />authenticator.<br />See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/ |  |  |  |
| `ciscoDuo` _[ApplicationCiscoDuoSettings](#applicationciscoduosettings)_ | Settings for integrating with Cisco Duo one-time password authenticator.<br />See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/ |  |  |  |
| `suggestedReviewersToken` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | The token for verifying access to GitLab internal API for "Suggested Reviewers"<br />service. |  |  |  |
| `zoektCredentials` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ | Login credentials to authenticate to a Zoekt server.<br />A Kubernetes Secret with basic authentication type (`kubernetes.io/basic-auth`)<br />is expected. |  |  |  |
| `prometheus` _[ServiceEndpoint](#serviceendpoint)_ | The endpoint for Prometheus server. |  |  |  |
| `shutdownBlackout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The interval to block health check, but continue accepting application<br />requests. This allows load balancer to notice service being shutdown and<br />not to interrupt any of the clients. |  | Format: duration <br /> |  |
| `customization` _object (keys:string, values:string)_ | Additional customization. These are passed as "extra" configuration to<br />GitLab Rails application. |  |  |  |
| `rackAttackGitBasicAuth` _[ApplicationRackAttackGitBasicAuth](#applicationrackattackgitbasicauth)_ | Configure Rack attack IP banning for Git basic authentication.<br />Note that this feature is not enabled when not configured. |  |  |  |
| `repositories` _[ApplicationRepository](#applicationrepository) array_ | Gitaly server endpoints. Each endpoint must have a unique name.<br />Each endpoint can be a Gitaly storage or a Gitaly "virtual" storage. For<br />more details refer to Gitaly Cluster documentation.<br />See: https://docs.gitlab.com/administration/gitaly/ |  | MinItems: 1 <br /> |  |
| `postgresql` _[PostgreSQL](#postgresql) array_ | PostgreSQL database endpoints. Each endpoint must have a unique name.<br />GitLab supports multiple database endpoints. The purpose of the database<br />is defined by its name. For example "main" and "ci" are two predefined<br />names that GitLab Rails application uses for its main and CI databases.<br />See: https://docs.gitlab.com/development/database/multiple_databases/ |  | MinItems: 1 <br /> |  |
| `redis` _[Redis](#redis) array_ | Redis endpoints. Each endpoint must have a unique name.<br />GitLab supports splitting several of the resource intensive Redis<br />operations across multiple Redis instances. In this scenario, the name<br />of the Redis endpoint must match the name of the persistence class, for<br />example, "cache", "sessions", or "queues".<br />See: https://docs.gitlab.com/administration/redis/replication_and_failover/ |  | MinItems: 1 <br /> |  |
| `clickhouse` _[ClickHouse](#clickhouse) array_ | ClickHouse database endpoints. Each endpoint must have a unique name.<br />See: https://docs.gitlab.com/integration/clickhouse/ |  |  |  |
| `registry` _[ApplicationContainerRegistry](#applicationcontainerregistry)_ | Settings of the GitLab container registry integration. |  |  |  |
| `kas` _[ApplicationKubernetesAgentServer](#applicationkubernetesagentserver)_ | Settings of the GitLab Kubernetes Agent Server (KAS) integration. |  |  |  |
| `experimental` _[Experimental](#experimental)_ | Configuration for experimental features. Do not use in production.<br />Check the documentation for the list of available features. |  |  |  |


#### ApplicationConfigStatus



Summary the most recently observed status of the ApplicationConfig.



_Appears in:_
- [ApplicationConfig](#applicationconfig)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `sharedObjects` _[SharedObjectReference](#sharedobjectreference) array_ | Kubernetes objects that are associated to the application configuration<br />and shared with other GitLab or third-party components:<br />  - `RailsSecrets`: A Kubernetes Secret that contains _all_ of the<br />    generated secret values of GitLab Rails application. Most of these<br />    secret are used application configuration files. |  |  |  |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ | Represents the latest available observations of the current state. |  |  |  |


#### ApplicationContainerRegistry



Settings of the GitLab container registry integration.



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `service` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ | A reference to a container registry service within the Kubernetes cluster.<br />This _must_ be a GitLab "ContainerRegistry" custom resource. |  |  |  |
| `external` _[ApplicationExternalContainerRegistry](#applicationexternalcontainerregistry)_ | Details of an external container registry service, either within or<br />outside the Kubernetes cluster. |  |  |  |


#### ApplicationExternalContainerRegistry



Specification of an external container registry provider. An external
provider can be within or outside the Kubernetes cluster.



_Appears in:_
- [ApplicationContainerRegistry](#applicationcontainerregistry)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `url` _string_ | The public-facing external URL of the container registry. |  | Format: uri <br /> |  |
| `internalUrl` _string_ | The URL of the internal container registry API that GitLab backend uses<br />for direct access. |  | Format: uri <br /> |  |


#### ApplicationExternalKAS



Specification of an external Kubernetes Agent Server (KAS) provider. An
external provider can be within or outside the Kubernetes cluster.



_Appears in:_
- [ApplicationKubernetesAgentServer](#applicationkubernetesagentserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `url` _string_ | The public-facing URL to the external KAS API that Kubernetes agents use. |  | Format: uri <br /> |  |
| `internalUrl` _string_ | The URL to the internal KAS API that GitLab backend uses. |  | Format: uri <br /> |  |
| `proxyUrl` _string_ | The URL for the Kubernetes API proxy that GitLab users use. |  | Format: uri <br /> |  |


#### ApplicationFortiAuthenticatorSettings



Settings for integrating with FortiAuthenticator one-time password
authenticator.

See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `host` _string_ | Host and port of FortiAuthenticator instance. |  | Pattern: `^([\w.]*:?\d{0,5})$` <br /> |  |
| `credentials` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ | Username and password of FortiAuthenticator instance.<br />A Kubernetes Secret with basic authentication type (`kubernetes.io/basic-auth`)<br />is expected. |  |  |  |


#### ApplicationInboxProvider



Configuration of the receiving method for incoming emails.



_Appears in:_
- [ApplicationIncomingEmail](#applicationincomingemail)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `imap` _[IMAPServer](#imapserver)_ | IMAP email server settings. |  |  |  |
| `microsoftGraph` _[MicrosoftGraphMailer](#microsoftgraphmailer)_ | Delivery of emails using Microsoft Graph API with OAuth 2.0 credentials<br />flow. |  |  |  |
| `pollInterval` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The polling interval for incoming emails. Only MicrosoftGraph provider<br />uses this option to pull incoming emails from server. |  | Format: duration <br /> |  |


#### ApplicationIncomingEmail



Configuration for incoming emails to GitLab application.



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `address` _string_ | The email address including the `%\{key\}` placeholder that will be<br />replaced to reference the item being replied to.<br />The placeholder can be omitted but if present, it must appear in the<br />"user" segment of the email address (before `@`).<br />Note that a placeholder is _required_ for the Service Desk feature to<br />work. |  | Format: email <br /> |  |
| `mailbox` _string_ | The name of the mailbox where incoming email will end up, for example<br />"Inbox". |  |  |  |
| `deleteAfterDelivery` _boolean_ | Delete incoming emails after they are delivered.<br />If you are using Microsoft Graph instead of IMAP, set this to `false` to<br />retain messages in the mailbox since deleted messages are auto-expunged<br />after some time. |  |  |  |
| `expungeDeleted` _boolean_ | Whether to expunge (permanently remove) messages from the mailbox when<br />they are marked as deleted after delivery.<br />Only applies to IMAP servers. Microsoft Graph will auto-expunge deleted<br />messages. |  |  |  |
| `inbox` _[ApplicationInboxProvider](#applicationinboxprovider)_ | Configure the incoming email receiving method. Depending the selected<br />method the configuration will be different. |  |  |  |
| `deliveryMethod` _[IncomingEmailDeliveryMethod](#incomingemaildeliverymethod)_ | Defines how email content is delivered to GitLab Rails application:<br />  - Sidekiq: Email content is pushed directly to Sidekiq. The job is then<br />    picked up by Sidekiq.<br />  - Postback: A webhook is triggered to send a HTTP POST request to Rails<br />    application. The content is embedded into the request body. |  | Enum: [Sidekiq Postback] <br /> |  |
| `arbitrationMethod` _[IncomingEmailArbitrationMethod](#incomingemailarbitrationmethod)_ | The arbitration method for incoming emails. Only Redis is supported. |  | Enum: [Redis] <br /> |  |


#### ApplicationKerberosSettings



Settings for Kerberos authentication.

See: https://docs.gitlab.com/integration/kerberos/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `keytab` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `servicePrincipalName` _string_ |  |  | Format: email <br /> |  |
| `dedicatedPort` _[KerberosDedicatedPort](#kerberosdedicatedport)_ | Offer Kerberos ticket-based authentication on a different port while the<br />standard port offers only basic authentication. |  |  |  |
| `simpleLdapLinkingAllowedRealms` _string array_ | Kerberos realms (or domains) that are allowed to automatically link LDAP<br />identities.<br />By default, GitLab accepts a realm that matches the domain derived from<br />the LDAP `base` DN. For example, `ou=users,dc=example,dc=com` would allow<br />users with a realm matching `example.com`. |  | items:Format: hostname <br /> |  |


#### ApplicationKubernetesAgentServer



Settings of the GitLab Kubernetes Agent Server (KAS) integration.



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `service` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ | A reference to a KAS service within the Kubernetes cluster. This _must_<br />be a GitLab "KubernetesAgentServer" custom resource. |  |  |  |
| `external` _[ApplicationExternalKAS](#applicationexternalkas)_ | Details of an external KAS service, either within or outside the<br />Kubernetes cluster. |  |  |  |


#### ApplicationLDAPSettings



Settings for LDAP login providers.

See: https://docs.gitlab.com/administration/auth/ldap/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `preventSignIn` _boolean_ | Prevent sign-in with LDAP through the web UI. This does not disable using<br />LDAP credentials for Git access. |  |  |  |
| `servers` _[LDAPServer](#ldapserver) array_ | List of LDAP servers. Each server must have a unique name.<br />GitLab EE supports connecting to multiple LDAP<br />servers. |  | MinItems: 1 <br /> |  |


#### ApplicationObject



Details of how and where to store application data in an object store.

Specify the type of application data with "usage" field. To associate the
application object to a store, use the "storeName" field.



_Appears in:_
- [ApplicationObjectStorage](#applicationobjectstorage)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `storeName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `bucketName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `proxyDownload` _boolean_ |  |  |  |  |
| `options` _object (keys:string, values:string)_ |  |  |  |  |
| `usage` _[ObjectStoreUsage](#objectstoreusage)_ | The type of data in application object. The following values are available:<br /> - Artifacts: Build artifacts.<br /> - ExternalDiffs: Merge request external `diff`.<br /> - LFS: Git LFS objects.<br /> - Uploads: User uploads, such as attachments and avatars.<br /> - Packages: Package repository assets, such as Maven and NPM.<br /> - DependencyProxy: Cached container images for dependency proxy.<br /> - TerraformState: Terraform state.<br /> - CISecureFiles: Secure files for CI. |  | Enum: [Artifacts ExternalDiffs LFS Uploads Packages DependencyProxy TerraformState CISecureFiles] <br /> |  |


#### ApplicationObjectStorage



Configuration for storing various application data on object storage.

For configuring object storage:

  - Define "stores". Each store is the settings of an object store provider
    and is identified with a unique name. The name is used to associate the
    store to application data.

  - Define "objects" and associate them to stores. An application "object"
    type is defined by its "usage", for example for "artifacts" or "uploads".

See: https://docs.gitlab.com/administration/object_storage/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `stores` _[ObjectStore](#objectstore) array_ | A list of object store providers. Each provider must have a unique name<br />to be referenced with.<br />Depending on the selected provider (for example S3 or GCS), the settings<br />are different. |  | MinItems: 1 <br /> |  |
| `objects` _[ApplicationObject](#applicationobject) array_ | A list of application objects. Each object must be associated with a<br />"store", using the store name. The type of data for the object is defined<br />by its "usage". |  |  |  |


#### ApplicationOmniAuthProvider



Configuration for an OmniAuth provider.



_Appears in:_
- [ApplicationOmniAuthSettings](#applicationomniauthsettings)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `name` _string_ | Name of the provider. It must be unique among all OmniAuth providers. The<br />name is used to identify providers in OmniAuth settings. |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `label` _string_ | A human-friendly name for the OmniAuth provider. This will appear on<br />GitLab sign-in page. |  |  |  |
| `options` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | The reference to a Kubernetes secret that contains the provider's<br />configuration. The configuration must be formatted in YAML.<br />For the details of the configuration format, see the provider's<br />documentation.<br />See: https://docs.gitlab.com/integration/omniauth/#supported-providers |  |  |  |


#### ApplicationOmniAuthSettings



Settings for OmniAuth authentication providers.

For the list of available providers refer to GitLab documentation.

See: https://docs.gitlab.com/integration/omniauth/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `settings` _object (keys:string, values:string)_ | OmniAuth settings. The following options are supported:<br />  - `autoSignInWithProvider`: Set it `true` to automatically sign in with<br />    a specific provider without showing GitLab's sign-in page.<br />  - `syncProfileFromProvider`: A comma-separated list of providers to<br />    sync the user profile from, every time the user logs in. When<br />	   authenticating using LDAP, the user's email is always synced.<br />    You can set this to `true` to specify all providers.<br />  - `syncProfileAttributes`: A comma-separated list of attributes to sync<br />    from the providers above. Available options are "name", "email" and<br />	   "location". This consequently will make the selected attributes<br />    read-only. You can set this to `true` to specify all attributes.<br />  - `allowSingleSignOn`: A comma-separated list of providers to allow<br />    users to login without having a user account first. User accounts<br />    will be created automatically when authentication was successful. You<br />    can set this to `true` to specify all providers.<br />  - `blockAutoCreatedUsers`: Locks down auto-created users until they<br />    have been cleared by an administrator.<br />  - `autoLinkLDAPUser`: Look up new users in LDAP servers. If a match is<br />    found (with the same `uid`), automatically link the OmniAuth identity<br />    with the LDAP account.<br />  - `autoLinkSAMLUser`: Allow users with existing accounts to login and<br />    auto link their account via SAML login, without having to do a manual<br />    login first and manually add SAML.<br />  - `samlMessageMaxByteSize`: Allows larger SAML messages to be received.<br />    It is a numeric value in bytes. Too high limits exposes instance to<br />    decompression DDoS attack type.<br />  - `autoLinkUser`: Allow users with the existing accounts to sign in and<br />    automatically link their accounts via OmniAuth login, without having<br />    to do a manual login first and manually add them. It links on email.<br />	   Specify the allowed providers with in comma-separated list to enable<br />    this feature. You can set this to `true` to specify all providers.<br />  - `externalProviders`: A comma-separated list of providers that are<br />    designated as external so that all users creating accounts via these<br />    providers will not be able to have access to internal projects.<br />  - `allowBypassTwoFactor`: A comma-separated list of providers that are<br />    allowed to bypass GitLab's two-factor authentication. You can set<br />    this to `true` to specify all providers. This option should only be<br />    configured for providers which already have two facto authentication.<br />    This settings dose not apply to SAML.<br />Note that the options are string-valued. You must use a string value. For<br />example use `"true"` instead of `true` or `"1"` instead of `1`. |  |  |  |
| `providers` _[ApplicationOmniAuthProvider](#applicationomniauthprovider) array_ | List of OmniAuth providers. Each provider must have a unique name. The<br />name is used to identify providers in OmniAuth settings. |  |  |  |


#### ApplicationOutboxProvider



Configuration of the sending method for outgoing emails.



_Appears in:_
- [ApplicationOutgoingEmail](#applicationoutgoingemail)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `smtp` _[SMTPServer](#smtpserver)_ | SMTP email server settings. |  |  |  |
| `microsoftGraph` _[MicrosoftGraphMailer](#microsoftgraphmailer)_ | Delivery of emails using Microsoft Graph API with OAuth 2.0 credentials<br />flow. |  |  |  |


#### ApplicationOutgoingEmail



Configuration for outgoing emails from GitLab application.



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `from` _string_ | Address used in the "From" field of outgoing emails. |  | Format: email <br /> |  |
| `displayName` _string_ | The display name used in the "From" field of outgoing emails. |  |  |  |
| `replyTo` _string_ | Address used in the "Reply-To" field of outgoing emails. |  | Format: email <br /> |  |
| `subjectPrefix` _string_ | The prefix that is prepended to the subject of outgoing emails. |  |  |  |
| `smime` _[CertificateBundle](#certificatebundle)_ | Set S/MIME signing certificate and key for outgoing emails. Note that<br />S/MIME public certificate (and CA certificate, when specified) will be<br />attached to the signed outgoing messages. |  |  |  |
| `outbox` _[ApplicationOutboxProvider](#applicationoutboxprovider)_ | Configure the outgoing email sending method. Depending the selected<br />method the configuration will be different. |  |  |  |


#### ApplicationRackAttackGitBasicAuth



Configure Rack attack IP banning for Git basic authentication.



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `ipWhitelist` _string array_ | Whitelist requests from the specified IPs with incorrect headers. Useful<br />for whitelisting frontend proxies. |  | items:Format: cidr <br /> |  |
| `maxRetry` _integer_ | Limit the number of Git HTTP authentication attempts per IP. |  |  |  |
| `findTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Reset the authentication attempt counter per IP after the specified<br />duration. |  | Format: duration <br /> |  |
| `banTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Ban an IP for the specified duration if they exceed the retry limit. |  | Format: duration <br /> |  |


#### ApplicationRepository



Specification of a Gitaly server provider. An provider must have a unique
name, have an endpoint and authentication details. The endpoint can can be a
Gitaly storage or a "virtual" storage. Note that Gitaly servers only support
Token authentication.

See: https://docs.gitlab.com/administration/gitaly/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `name` _string_ | Name of the Gitaly server. It must be unique among all Gitaly servers. |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |


#### ApplicationServer







_Appears in:_
- [ApplicationServerList](#applicationserverlist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `ApplicationServer` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[ApplicationServerSpec](#applicationserverspec)_ |  |  |  |  |
| `status` _[ApplicationServerStatus](#applicationserverstatus)_ |  |  |  |  |


#### ApplicationServerList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `ApplicationServerList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[ApplicationServer](#applicationserver) array_ |  |  |  |  |


#### ApplicationServerSpec







_Appears in:_
- [ApplicationServer](#applicationserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  | Pattern: `^([1-9]\d?)\.(0&#124;[1-9]\d?)\.(0&#124;[1-9]\d?)$` <br /> |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |
| `replicas` _integer_ |  |  | Minimum: 0 <br /> |  |
| `paused` _boolean_ |  |  |  |  |
| `applicationConfigRef` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ |  |  |  |  |
| `puma` _[PumaSpec](#pumaspec)_ |  |  |  |  |
| `workhorse` _[WorkhorseSpec](#workhorsespec)_ |  |  |  |  |
| `uploadVolume` _[EmptyDirVolumeSource](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#emptydirvolumesource-v1-core)_ |  |  |  |  |
| `tracing` _[TracingSupport](#tracingsupport)_ |  |  |  |  |
| `workload` _[WorkloadSpec](#workloadspec)_ |  |  |  |  |


#### ApplicationServerStatus







_Appears in:_
- [ApplicationServer](#applicationserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  |  |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |
| `replicas` _integer_ |  |  |  |  |
| `selector` _string_ |  |  |  |  |
| `sharedObjects` _[SharedObjectReference](#sharedobjectreference) array_ |  |  |  |  |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ |  |  |  |  |


#### ApplicationSmartCardSettings



Settings for smart card authentication.

See: https://docs.gitlab.com/administration/auth/smartcard/



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `caCertificate` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | CA certificate bundle of smart cards. |  |  |  |
| `host` _string_ | Host and port where the client-side certificate is requested. |  | Pattern: `^([\w.]*:?\d{0,5})$` <br /> | smartcard.example.com:8080 |
| `requiredForGitAccess` _boolean_ | Browser session with smart card sign-in is required for Git access. |  |  |  |
| `sanExtensions` _boolean_ | Use X.509 SAN extensions certificates to identify GitLab users.<br />Add a `subjectAltName` to your certificates like `email:user`. |  |  |  |


#### CertificateBundle







_Appears in:_
- [ApplicationOutgoingEmail](#applicationoutgoingemail)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `credentials` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ |  |  |  |  |
| `caCertificate` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### ClickHouse







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `name` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `databaseName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |


#### ContentSecurityPolicy



ContentSecurityPolicy is the configuration for Content Security Policy
(CSP) headers.

See https://guides.rubyonrails.org/security.html#content-security-policy



_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `reportOnly` _boolean_ | ReportOnly set the CSP header to only report the violations. |  |  |  |
| `directives` _object (keys:string, values:string)_ | Directives defines the CSP directives.<br />See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy#directives |  |  |  |


#### DatabaseMigration







_Appears in:_
- [DatabaseMigrationList](#databasemigrationlist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `DatabaseMigration` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[DatabaseMigrationSpec](#databasemigrationspec)_ |  |  |  |  |
| `status` _[DatabaseMigrationStatus](#databasemigrationstatus)_ |  |  |  |  |


#### DatabaseMigrationList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `DatabaseMigrationList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[DatabaseMigration](#databasemigration) array_ |  |  |  |  |


#### DatabaseMigrationSpec







_Appears in:_
- [DatabaseMigration](#databasemigration)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ | Version is the target GitLab version the database will be upgraded to. |  | Required: {} <br /> |  |
| `edition` _[Edition](#edition)_ | Edition specifies if the enterprise or community edition of GitLab is used.<br />Defaults to enterprise edition (EE). |  | Enum: [EE CE] <br /> |  |
| `applicationConfigRef` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ | ApplicationConfigRef references the ApplicationConfigRef which points to the database to be migrated. |  | Required: {} <br /> |  |
| `isUpgrade` _boolean_ | IsUpgrade specifies if the database migration is an upgrade.<br />If so, it will skip root password provisioning. |  |  |  |
| `skipPostMigrations` _boolean_ | SkipPostMigrations allow to skip post deployment migrations for zero downtime upgrades:<br />  https://docs.gitlab.com/ee/update/zero_downtime.html#requirements-and-considerations. |  |  |  |
| `initialRootPassword` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | InitialRootPassword is a reference to the Secret with the initial root password.<br />This will only be applied if the database is being initialized the first time and will be ignored otherwise. |  |  |  |
| `license` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | License is a reference to the Secret of the GitLab license.<br />This will only be applied if the database is being initialized the first time and will be ignored otherwise. |  |  |  |
| `activeDeadline` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | ActiveDeadline is the duration in which the database migration should complete.<br />Defaults to 1 hour. |  |  |  |
| `backoffLimit` _integer_ | BackoffLimit is the number of retries before considering the database migration as failed.<br />Defaults to 6. |  | Minimum: 0 <br /> |  |
| `workload` _[WorkloadSpec](#workloadspec)_ |  |  |  |  |


#### DatabaseMigrationStatus







_Appears in:_
- [DatabaseMigration](#databasemigration)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ |  |  |  |  |
| `startTime` _[Time](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#time-v1-meta)_ | StartTime is the time the controller started the database migrations. |  |  |  |
| `completionTime` _[Time](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#time-v1-meta)_ | CompletionTime is the time the migrations completed.<br />The completion time is set when the migrations finish successfully. |  |  |  |


#### Edition

_Underlying type:_ _string_



_Validation:_
- Enum: [EE CE]

_Appears in:_
- [ApplicationServerSpec](#applicationserverspec)
- [ApplicationServerStatus](#applicationserverstatus)
- [DatabaseMigrationSpec](#databasemigrationspec)
- [InstanceSpec](#instancespec)
- [InstanceStatus](#instancestatus)
- [JobProcessorSpec](#jobprocessorspec)
- [JobProcessorStatus](#jobprocessorstatus)



#### Experimental







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)



#### ExternalServiceEndpoint

_Underlying type:_ _[struct{ProviderName *string "json:\"providerName,omitempty\""; Host string "json:\"host\""; Port int32 "json:\"port,omitempty\""}](#struct{providername-*string-"json:\"providername,omitempty\"";-host-string-"json:\"host\"";-port-int32-"json:\"port,omitempty\""})_





_Appears in:_
- [ServiceEndpoint](#serviceendpoint)



#### GCSObjectStoreConnection







_Appears in:_
- [ObjectStore](#objectstore)
- [ObjectStoreProvider](#objectstoreprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `project` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `credentials` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### GSSAPISupport







_Appears in:_
- [ApplicationKerberosSettings](#applicationkerberossettings)
- [GitLabSSHDConfig](#gitlabsshdconfig)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `keytab` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `servicePrincipalName` _string_ |  |  | Format: email <br /> |  |


#### GitLabSSHDConfig



GitLabSSHDConfig contains configuration options for the GitLab SSHD daemon.

See the [Shell example config] for more information.

[Shell example config]: https://gitlab.com/gitlab-org/gitlab-shell/-/blob/main/config.yml.example



_Appears in:_
- [SSHServerDaemon](#sshserverdaemon)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `hostKeys` _[LocalObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#localobjectreference-v1-core)_ | The name of the Kubernetes secret to grab the SSH host keys from.<br />The keys in the secret must start with the key names `ssh_host_` in order to be used by GitLab Shell.<br />If not provided, the operator will generate its own host keys.<br />An example layout of the secret:<br /> - ssh_host_rsa_key<br /> - ssh_host_rsa_key.pub<br /> - ssh_host_ecdsa_key<br /> - ssh_host_ecdsa_key.pub<br /> - ssh_host_ed25519_key<br /> - ssh_host_ed25519_key.pub |  |  |  |
| `clientAliveInterval` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 0. |  | Format: duration <br /> |  |
| `loginGraceTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 60. |  | Format: duration <br /> |  |
| `metrics` _[MetricsSupport](#metricssupport)_ |  |  |  |  |
| `concurrentSessionsLimit` _integer_ |  |  |  |  |
| `proxyProtocol` _boolean_ | If using Traefik or other proxy frontend, see https://doc.traefik.io/traefik/routing/services/#proxy-protocol. |  |  |  |
| `proxyPolicy` _string_ | PROXY protocol policy ("Use", "Require", "Reject", "Ignore"), "use" is the default value. |  | Enum: [Use Require Reject Ignore] <br /> |  |
| `proxyHeaderTimeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 500. |  | Format: duration <br /> |  |
| `gracePeriod` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  | Format: duration <br /> |  |
| `ciphers` _string array_ |  |  | items:Pattern: ^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$ <br /> |  |
| `kexAlgorithms` _string array_ |  |  | items:Pattern: ^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$ <br /> |  |
| `macs` _string array_ |  |  | items:Pattern: ^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$ <br /> |  |
| `publicKeyAlgorithms` _string array_ |  |  | items:Pattern: ^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$ <br /> |  |
| `gssapi` _[GSSAPISupport](#gssapisupport)_ |  |  |  |  |


#### IMAPServer







_Appears in:_
- [ApplicationInboxProvider](#applicationinboxprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `tls` _[TLSClientSupport](#tlsclientsupport)_ |  |  |  |  |
| `settings` _object (keys:string, values:string)_ | SMTP settings. Supported keys:<br />  - startTLS<br />  - idleTimeout |  |  |  |


#### ImageFlavor

_Underlying type:_ _string_





_Appears in:_
- [ImageSource](#imagesource)



#### ImageSource







_Appears in:_
- [WorkloadSpec](#workloadspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `registry` _string_ |  |  |  |  |
| `repository` _string_ |  |  |  |  |
| `flavor` _[ImageFlavor](#imageflavor)_ |  |  |  |  |
| `pullSecrets` _[LocalObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#localobjectreference-v1-core) array_ |  |  |  |  |


#### IncomingEmailArbitrationMethod

_Underlying type:_ _string_



_Validation:_
- Enum: [Redis]

_Appears in:_
- [ApplicationIncomingEmail](#applicationincomingemail)



#### IncomingEmailDeliveryMethod

_Underlying type:_ _string_



_Validation:_
- Enum: [Sidekiq Postback]

_Appears in:_
- [ApplicationIncomingEmail](#applicationincomingemail)



#### Instance







_Appears in:_
- [InstanceList](#instancelist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `Instance` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[InstanceSpec](#instancespec)_ |  |  |  |  |
| `status` _[InstanceStatus](#instancestatus)_ |  |  |  |  |


#### InstanceList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `InstanceList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[Instance](#instance) array_ |  |  |  |  |


#### InstanceSpec







_Appears in:_
- [Instance](#instance)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  | Pattern: `^([1-9]\d?)\.(0&#124;[1-9]\d?)\.(0&#124;[1-9]\d?)$` <br /> |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |


#### InstanceStatus







_Appears in:_
- [Instance](#instance)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  |  |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ |  |  |  |  |


#### JobProcessor







_Appears in:_
- [JobProcessorList](#jobprocessorlist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `JobProcessor` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[JobProcessorSpec](#jobprocessorspec)_ |  |  |  |  |
| `status` _[JobProcessorStatus](#jobprocessorstatus)_ |  |  |  |  |


#### JobProcessorList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `JobProcessorList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[JobProcessor](#jobprocessor) array_ |  |  |  |  |


#### JobProcessorSpec







_Appears in:_
- [JobProcessor](#jobprocessor)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  | Pattern: `^([1-9]\d?)\.(0&#124;[1-9]\d?)\.(0&#124;[1-9]\d?)$` <br /> |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |
| `replicas` _integer_ |  |  | Minimum: 0 <br /> |  |
| `paused` _boolean_ |  |  |  |  |
| `applicationConfigRef` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ |  |  |  |  |
| `puma` _[SidekiqSpec](#sidekiqspec)_ |  |  |  |  |
| `tracing` _[TracingSupport](#tracingsupport)_ |  |  |  |  |
| `workload` _[WorkloadSpec](#workloadspec)_ |  |  |  |  |


#### JobProcessorStatus







_Appears in:_
- [JobProcessor](#jobprocessor)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  |  |  |
| `edition` _[Edition](#edition)_ |  |  | Enum: [EE CE] <br /> |  |
| `replicas` _integer_ |  |  |  |  |
| `selector` _string_ |  |  |  |  |
| `sharedObjects` _[SharedObjectReference](#sharedobjectreference) array_ |  |  |  |  |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ |  |  |  |  |


#### JobSchedule







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `name` _string_ |  |  | Pattern: `^[a-z]+(_[a-z]+)*$` <br /> |  |
| `schedule` _string_ |  |  | Pattern: `^(((\d+,)+\d+&#124;(\d+(\/&#124;-)\d+)&#124;\d+&#124;\*) ?){5,7}$` <br /> | 15,30 * * * *<br />0 2 * * 1-5<br />0 0-23/2 * * * |


#### KASApplicationServerProvider







_Appears in:_
- [KubernetesAgentServerSpec](#kubernetesagentserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `service` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ |  |  |  |  |
| `external` _[KASExternalApplicationServer](#kasexternalapplicationserver)_ |  |  |  |  |


#### KASExternalApplicationServer







_Appears in:_
- [KASApplicationServerProvider](#kasapplicationserverprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `externalUrl` _string_ | ExternalURL is the external GitLab/ApplicationServer URL. |  | Required: {} <br /> | https://gitlab.example.com |
| `internalUrl` _string_ | InternalURL is the interhal GitLab/ApplicationServer URL pointing to workhorse. |  | Required: {} <br /> | http://gitlab-application-server.svc.cluster.local:8181 |


#### KerberosDedicatedPort



Git before 2.4 does not fall back to Basic authentication if `negotiate`
fails. To support both `basic` and `negotiate` methods with older versions of
Git, configure the your frontend proxy on an extra port, for example `8443`,
and use the following configuration to dedicate it to Kerberos authentication.



_Appears in:_
- [ApplicationKerberosSettings](#applicationkerberossettings)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `https` _boolean_ | If the Kerberos dedicated port uses HTTPS. |  |  |  |
| `port` _integer_ | The Kerberos dedicated port number. |  |  |  |


#### KubernetesAgentServer







_Appears in:_
- [KubernetesAgentServerList](#kubernetesagentserverlist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `KubernetesAgentServer` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[KubernetesAgentServerSpec](#kubernetesagentserverspec)_ |  |  |  |  |
| `status` _[KubernetesAgentServerStatus](#kubernetesagentserverstatus)_ |  |  |  |  |


#### KubernetesAgentServerList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `KubernetesAgentServerList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[KubernetesAgentServer](#kubernetesagentserver) array_ |  |  |  |  |


#### KubernetesAgentServerSpec







_Appears in:_
- [KubernetesAgentServer](#kubernetesagentserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ | Version is the target version of the KAS. This should usually match the GitLab version. |  | Pattern: `^([1-9]\d?)\.(0&#124;[1-9]\d?)\.(0&#124;[1-9]\d?)$` <br />Required: {} <br /> |  |
| `externalUrl` _string_ | ExternalURL is the URL KAS will be available as. |  | Required: {} <br /> | https://kas.example.com |
| `applicationServer` _[KASApplicationServerProvider](#kasapplicationserverprovider)_ |  |  | Required: {} <br /> |  |
| `redis` _[Redis](#redis)_ |  |  | Required: {} <br /> |  |
| `workload` _[WorkloadSpec](#workloadspec)_ |  |  |  |  |
| `replicas` _integer_ | Number of desired pods. Defaults to 1 if not specified. |  |  |  |
| `paused` _boolean_ | Paused will stop the rollout of new KAS Pods. |  |  |  |


#### KubernetesAgentServerStatus







_Appears in:_
- [KubernetesAgentServer](#kubernetesagentserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `replicas` _integer_ |  |  |  |  |
| `selector` _string_ |  |  |  |  |
| `sharedObjects` _[SharedObjectReference](#sharedobjectreference) array_ |  |  |  |  |


#### LDAPEncryption

_Underlying type:_ _string_



_Validation:_
- Enum: [Plain StartTLS SimpleTLS]

_Appears in:_
- [LDAPServer](#ldapserver)



#### LDAPServer







_Appears in:_
- [ApplicationLDAPSettings](#applicationldapsettings)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `name` _string_ | The internal name of the LDAP server. It must be unique among other LDAP<br />servers. |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `label` _string_ | A human-friendly name for the LDAP server. |  |  |  |
| `url` _string_ | The URL of the LDAP server. This URL must have `ldap or `ldaps` scheme.<br />When the port is not specified, the default port for the scheme is used. |  | Format: uri <br /> |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ | The authentication method for the LDAP server. |  |  |  |
| `encryption` _[LDAPEncryption](#ldapencryption)_ | Encryption method for the LDAP server.<br />The following values are available:<br />  - Plain: No encryption.<br />  - StartTLS: Use StartTLS to encrypt the LDAP connection.<br />  - SimpleTLS: Use SimpleTLS to encrypt the LDAP connection. |  | Enum: [Plain StartTLS SimpleTLS] <br /> |  |
| `tls` _[TLSClientSupport](#tlsclientsupport)_ | Cline-side TLS settings of the LDAP server, including CA certificate and<br />TLS options. |  |  |  |
| `timeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | The timeout for LDAP queries. This helps to avoid blocking a request if<br />the LDAP server becomes unresponsive. |  | Format: duration <br /> |  |
| `uid` _string_ | The user's LDAP login. This should be the attribute, not the value that<br />maps to `uid`. |  |  |  |
| `bindDN` _string_ | The full DN of the user that is binding to the LDAP server. |  |  |  |
| `bindPassword` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ | The password of the user that is binding to the LDAP server. |  |  |  |
| `settings` _object (keys:string, values:string)_ | LDAP server settings. The following options are supported:<br />  - `smartcardAuth`: Set to `true` to enable smart card authentication<br />    against the LDAP server.<br />  - `activeDirectory`: Set to `true`, to indicate the LDAP server is an<br />    ActiveDirectory (AD). For non-AD servers it skips the AD specific<br />    queries.<br />  - `smartcardADCertField`: If both `smartcardAuth` and `activeDirectory`<br />	   are is `true`, use this setting to authenticate users based on their<br />    smart card certificates in AD.<br />  - `smartcardADCertFormat`: If both `smartcardAuth` and `activeDirectory`<br />	   are is `true`, search the `smartcardADCertField` for certificate data<br />    stored in this format.<br />  - `allowUsernameOrEmailLogin`: If `true`, GitLab will ignore everything<br />    after the first '@' in the username submitted by the user on login.<br />    If you are using `userPrincipalName` as the `uid` for AD you need to<br />    disable this setting, because the `userPrincipalName` contains '@'.<br />  - `blockAutoCreatedUsers`: To maintain tight control over the number of<br />    active users on your instance, set this to `true` to keep new users<br />    blocked until they have been cleared by the administrator.<br />  - `base`: The base DN to search for users.<br />  - `userFilter`: The filter for user search. Uses RFC 4515 format. Note<br />    that GitLab does not support "omniauth-ldap" custom filter syntax.<br />  - `groupBase`: The base to search for groups.<br />  - `adminGroup`: LDAP group of users that GitLab administrators.<br />  - `externalGroups`: LDAP group of users that are marked as external<br />    users in GitLab.<br />  - `syncSSHKeys`: Name of attribute which holds a SSH public key of the<br />	   user. When not specified, SSH key synchronization is disabled.<br />  - `lowerCaseUsernames`: If `true`, GitLab will lower case the LDAP<br />    usernames.<br />Note that the options are string-valued. You must use a string value. For<br />example use `"true"` instead of `true` or `"1"` instead of `1`. |  |  |  |
| `attributes` _object (keys:string, values:string)_ | Mapping for LDAP attributes. GitLab application uses the mappings to<br />create an account for LDAP users.<br />The specified attributes can either be a single attribute name, for<br />example `mail`, or a comma-separated list of attribute names to try in<br />order, for example `mail,email`,<br />Note that the user's LDAP login will always be the attribute specified in<br />`uid` above.<br />The following attributes are supported:<br />  - `username`: The username will be used in paths for the user's own<br />    projects and when mentioning them with (like `@username`). If the<br />    attribute specified for `username` contains an email address, the<br />    GitLab username will be the part of the email address before the<br />    '@'. Example: `uid,userid,sAMAccountName`.<br />  - `email`: The primary email address of the user.<br />    Example: `mail,email,userPrincipalName`.<br />  - `name`: If no full name could be found at the attribute specified for<br />    `name`, the full name is determined using the attributes specified<br />    for first name and last name. Example: `cn`.<br />  - `firstName`: First name of the user. Example: `givenName`.<br />  - `lastName`: Last name of the user. Example: `sn`. |  |  |  |


#### LogFormat

_Underlying type:_ _string_



_Validation:_
- Enum: [Text JSON Structured]

_Appears in:_
- [SSHServerSpec](#sshserverspec)
- [SidekiqSettings](#sidekiqsettings)
- [WorkhorseSettings](#workhorsesettings)



#### LogLevel

_Underlying type:_ _string_



_Validation:_
- Enum: [Trace Debug Info Warn Error Fatal]

_Appears in:_
- [SSHServerSpec](#sshserverspec)



#### ManagedContainerSpec







_Appears in:_
- [WorkloadSpec](#workloadspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `name` _string_ |  |  |  |  |
| `image` _string_ |  |  |  |  |
| `envFrom` _[EnvFromSource](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#envfromsource-v1-core) array_ |  |  |  |  |
| `env` _[EnvVar](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#envvar-v1-core) array_ |  |  |  |  |
| `resources` _[ResourceRequirements](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#resourcerequirements-v1-core)_ |  |  |  |  |
| `livenessProbe` _[Probe](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#probe-v1-core)_ |  |  |  |  |
| `readinessProbe` _[Probe](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#probe-v1-core)_ |  |  |  |  |
| `startupProbe` _[Probe](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#probe-v1-core)_ |  |  |  |  |
| `imagePullPolicy` _[PullPolicy](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#pullpolicy-v1-core)_ |  |  |  |  |
| `securityContext` _[SecurityContext](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#securitycontext-v1-core)_ |  |  |  |  |


#### MetricsSupport







_Appears in:_
- [GitLabSSHDConfig](#gitlabsshdconfig)
- [PumaSpec](#pumaspec)
- [SidekiqSpec](#sidekiqspec)
- [WorkhorseSpec](#workhorsespec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `disabled` _boolean_ |  |  |  |  |
| `tls` _[TLSServerSupport](#tlsserversupport)_ |  |  |  |  |


#### MicrosoftGraphMailer







_Appears in:_
- [ApplicationInboxProvider](#applicationinboxprovider)
- [ApplicationOutboxProvider](#applicationoutboxprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `tenant` _string_ |  |  | Format: uuid <br /> |  |
| `userId` _string_ |  |  | Format: uuid <br /> |  |
| `clientId` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `clientSecret` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `endpoint` _string_ |  |  | Format: uri <br /> |  |
| `azureADEndpoint` _string_ |  |  | Format: uri <br /> |  |


#### ObjectStore







_Appears in:_
- [ApplicationObjectStorage](#applicationobjectstorage)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `s3` _[S3ObjectStoreConnection](#s3objectstoreconnection)_ |  |  |  |  |
| `gcs` _[GCSObjectStoreConnection](#gcsobjectstoreconnection)_ |  |  |  |  |
| `name` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |


#### ObjectStoreDestination







_Appears in:_
- [ApplicationObject](#applicationobject)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `storeName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `bucketName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `proxyDownload` _boolean_ |  |  |  |  |
| `options` _object (keys:string, values:string)_ |  |  |  |  |


#### ObjectStoreProvider







_Appears in:_
- [ObjectStore](#objectstore)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `s3` _[S3ObjectStoreConnection](#s3objectstoreconnection)_ |  |  |  |  |
| `gcs` _[GCSObjectStoreConnection](#gcsobjectstoreconnection)_ |  |  |  |  |


#### ObjectStoreUsage

_Underlying type:_ _string_

The type of data in application object.

_Validation:_
- Enum: [Artifacts ExternalDiffs LFS Uploads Packages DependencyProxy TerraformState CISecureFiles]

_Appears in:_
- [ApplicationObject](#applicationobject)



#### OpenSSHConfig



OpenSSHConfig contains extra configuration options for the OpenSSH daemon.



_Appears in:_
- [SSHServerDaemon](#sshserverdaemon)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `hostKeys` _[LocalObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#localobjectreference-v1-core)_ | The name of the Kubernetes secret to grab the SSH host keys from.<br />The keys in the secret must start with the key names `ssh_host_` in order to be used by GitLab Shell.<br />If not provided, the operator will generate its own host keys.<br />An example layout of the secret:<br /> - ssh_host_rsa_key<br /> - ssh_host_rsa_key.pub<br /> - ssh_host_ecdsa_key<br /> - ssh_host_ecdsa_key.pub<br /> - ssh_host_ed25519_key<br /> - ssh_host_ed25519_key.pub |  |  |  |
| `clientAliveInterval` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 0. |  | Format: duration <br /> |  |
| `loginGraceTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 60. |  | Format: duration <br /> |  |
| `maxStartups` _string_ |  |  | Pattern: `^\d+:\d+:\d+$` <br /> | 10:30:100 |
| `extraConfig` _[ConfigMapKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#configmapkeyselector-v1-core)_ |  |  |  |  |


#### PodDisruptionBudget







_Appears in:_
- [WorkloadSpec](#workloadspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `minAvailable` _[IntOrString](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#intorstring-intstr-util)_ |  |  |  |  |
| `maxUnavailable` _[IntOrString](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#intorstring-intstr-util)_ |  |  |  |  |
| `unhealthyPodEvictionPolicy` _[UnhealthyPodEvictionPolicyType](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#unhealthypodevictionpolicytype-v1-policy)_ |  |  |  |  |


#### PodMetadata







_Appears in:_
- [PodTemplate](#podtemplate)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `labels` _object (keys:string, values:string)_ |  |  |  |  |
| `annotations` _object (keys:string, values:string)_ |  |  |  |  |


#### PodTemplate







_Appears in:_
- [WorkloadSpec](#workloadspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `metadata` _[PodMetadata](#podmetadata)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[RestrictedPodSpec](#restrictedpodspec)_ |  |  |  |  |


#### PostgreSQL







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `name` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `username` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `databaseName` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `settings` _object (keys:string, values:string)_ |  |  |  |  |
| `tls` _[TLSClientSupport](#tlsclientsupport)_ |  |  |  |  |


#### PumaSettings







_Appears in:_
- [PumaSpec](#pumaspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `enableBootsnap` _boolean_ |  |  |  |  |
| `minThreads` _integer_ |  |  | Minimum: 0 <br /> |  |
| `maxThreads` _integer_ |  |  | Minimum: 0 <br /> |  |
| `workerProcesses` _integer_ |  |  | Minimum: 0 <br /> |  |
| `workerTimeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `workerMaxMemory` _[Quantity](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#quantity-resource-api)_ |  |  |  |  |
| `disableWorkerKiller` _boolean_ |  |  |  |  |


#### PumaSpec







_Appears in:_
- [ApplicationServerSpec](#applicationserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `metrics` _[MetricsSupport](#metricssupport)_ |  |  |  |  |
| `settings` _[PumaSettings](#pumasettings)_ |  |  |  |  |
| `tls` _[TLSServerSupport](#tlsserversupport)_ |  |  |  |  |


#### Redis







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)
- [KubernetesAgentServerSpec](#kubernetesagentserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `name` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `settings` _object (keys:string, values:string)_ |  |  |  |  |
| `tls` _[TLSClientSupport](#tlsclientsupport)_ |  |  |  |  |


#### RestrictedPodSpec



RestrictedPodSpec is a subset of the PodSpec that can be used to customize the pod.
Provided fields will override the default ones populated by the operator.



_Appears in:_
- [PodTemplate](#podtemplate)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `extraVolumes` _[Volume](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#volume-v1-core) array_ | ExtraVolumes is a list of additional volumes to be added to the pod.<br />Provided volumes will override the ones in the pod if they have the same name. |  |  |  |
| `extraVolumeMounts` _[VolumeMount](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#volumemount-v1-core) array_ | ExtraVolumeMounts is a list of additional volume mounts to be added to the pod.<br />Same as volumes, provided volume mounts will override the ones in the pod if they have the same mount path. |  |  |  |
| `extraInitContainers` _[Container](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#container-v1-core) array_ | ExtraInitContainers is a list of additional init containers to be added to the pod.<br />Provided init containers will override the ones in the pod if they have the same name. |  |  |  |
| `extraContainers` _[Container](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#container-v1-core) array_ | ExtraContainers is a list of additional containers to be added to the pod.<br />Provided containers will override the ones in the pod if they have the same name. |  |  |  |
| `restartPolicy` _[RestartPolicy](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#restartpolicy-v1-core)_ |  |  |  |  |
| `terminationGracePeriodSeconds` _integer_ |  |  |  |  |
| `activeDeadlineSeconds` _integer_ |  |  |  |  |
| `dnsPolicy` _[DNSPolicy](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#dnspolicy-v1-core)_ |  |  |  |  |
| `nodeSelector` _object (keys:string, values:string)_ |  |  |  |  |
| `serviceAccountName` _string_ |  |  |  |  |
| `automountServiceAccountToken` _boolean_ |  |  |  |  |
| `nodeName` _string_ |  |  |  |  |
| `shareProcessNamespace` _boolean_ |  |  |  |  |
| `securityContext` _[PodSecurityContext](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#podsecuritycontext-v1-core)_ |  |  |  |  |
| `affinity` _[Affinity](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#affinity-v1-core)_ |  |  |  |  |
| `schedulerName` _string_ |  |  |  |  |
| `tolerations` _[Toleration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#toleration-v1-core) array_ |  |  |  |  |
| `hostAliases` _[HostAlias](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#hostalias-v1-core) array_ |  |  |  |  |
| `priorityClassName` _string_ |  |  |  |  |
| `dnsConfig` _[PodDNSConfig](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#poddnsconfig-v1-core)_ |  |  |  |  |
| `runtimeClassName` _string_ |  |  |  |  |
| `overhead` _[ResourceList](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#resourcelist-v1-core)_ |  |  |  |  |
| `topologySpreadConstraints` _[TopologySpreadConstraint](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#topologyspreadconstraint-v1-core) array_ |  |  |  |  |
| `resourceClaims` _[PodResourceClaim](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#podresourceclaim-v1-core) array_ |  |  |  |  |


#### S3ObjectStoreConnection







_Appears in:_
- [ObjectStore](#objectstore)
- [ObjectStoreProvider](#objectstoreprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `accessKeyId` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `accessKeySecret` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |
| `region` _string_ |  |  | Pattern: `^[a-zA-Z_#][a-zA-Z0-9_#-]*$` <br /> |  |
| `signatureVersion` _string_ |  |  | Enum: [V4 V2] <br /> |  |
| `host` _string_ |  |  | Pattern: `^(([a-zA-Z0-9][-a-zA-Z0-9]*\.)+[a-zA-Z]{2,}&#124;\d{1,3}(\.\d{1,3}){3})(:\d+)?$` <br /> | s3.example.com<br />192.168.1.2:9000 |
| `endpoint` _string_ |  |  | Format: uri <br /> |  |
| `pathStyle` _boolean_ |  |  |  |  |


#### SMTPServer







_Appears in:_
- [ApplicationOutboxProvider](#applicationoutboxprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |
| `tls` _[TLSClientSupport](#tlsclientsupport)_ |  |  |  |  |
| `pool` _boolean_ |  |  |  |  |
| `settings` _object (keys:string, values:string)_ | SMTP settings. Supported keys:<br />  - domain<br />  - autoStartTLS<br />  - opensslVerifyMode<br />  - openTimeout<br />  - readTimeout<br />  - authentication |  |  |  |


#### SSHDApplicationServerProvider







_Appears in:_
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `service` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ |  |  |  |  |
| `external` _[SSHDExternalApplicationServer](#sshdexternalapplicationserver)_ |  |  |  |  |


#### SSHDExternalApplicationServer







_Appears in:_
- [SSHDApplicationServerProvider](#sshdapplicationserverprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `externalUrl` _string_ |  |  | Format: uri <br /> |  |
| `internalUrl` _string_ |  |  | Format: uri <br /> |  |
| `apiToken` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### SSHDaemonConfig



Common SSH daemon options for all SSH daemon types.



_Appears in:_
- [GitLabSSHDConfig](#gitlabsshdconfig)
- [OpenSSHConfig](#opensshconfig)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `hostKeys` _[LocalObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#localobjectreference-v1-core)_ | The name of the Kubernetes secret to grab the SSH host keys from.<br />The keys in the secret must start with the key names `ssh_host_` in order to be used by GitLab Shell.<br />If not provided, the operator will generate its own host keys.<br />An example layout of the secret:<br /> - ssh_host_rsa_key<br /> - ssh_host_rsa_key.pub<br /> - ssh_host_ecdsa_key<br /> - ssh_host_ecdsa_key.pub<br /> - ssh_host_ed25519_key<br /> - ssh_host_ed25519_key.pub |  |  |  |
| `clientAliveInterval` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 0. |  | Format: duration <br /> |  |
| `loginGraceTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ | Defaults to 60. |  | Format: duration <br /> |  |


#### SSHServer







_Appears in:_
- [SSHServerList](#sshserverlist)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `SSHServer` | | | |
| `metadata` _[ObjectMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `spec` _[SSHServerSpec](#sshserverspec)_ |  |  |  |  |
| `status` _[SSHServerStatus](#sshserverstatus)_ |  |  |  |  |


#### SSHServerDaemon



SSHServerDaemon contains configuration options for the specified SSH daemon.



_Appears in:_
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `openSSH` _[OpenSSHConfig](#opensshconfig)_ | Settings for OpenSSH. Used when DaemonType is OpenSSH. |  |  |  |
| `gitlabSSHD` _[GitLabSSHDConfig](#gitlabsshdconfig)_ | Settings for GitLab SSHD. Used when DaemonType is GitLabSSHD. |  |  |  |


#### SSHServerLFS







_Appears in:_
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `pureSSHProtocol` _boolean_ |  |  |  |  |


#### SSHServerList









| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `apiVersion` _string_ | `gitlab.com/v2alpha2` | | | |
| `kind` _string_ | `SSHServerList` | | | |
| `metadata` _[ListMeta](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#listmeta-v1-meta)_ | Refer to Kubernetes API documentation for fields of `metadata`. |  |  |  |
| `items` _[SSHServer](#sshserver) array_ |  |  |  |  |


#### SSHServerPAT







_Appears in:_
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `allowedScopes` _string array_ |  |  | items:Pattern: ^[a-zA-Z][a-zA-Z0-9_]*$ <br /> |  |


#### SSHServerSpec







_Appears in:_
- [SSHServer](#sshserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `replicas` _integer_ |  |  | Minimum: 0 <br /> |  |
| `paused` _boolean_ |  |  |  |  |
| `version` _string_ |  |  | Pattern: `^([1-9]\d?)\.(0&#124;[1-9]\d?)\.(0&#124;[1-9]\d?)$` <br /> |  |
| `applicationServer` _[SSHDApplicationServerProvider](#sshdapplicationserverprovider)_ | ApplicationServer references the ApplicationConfig which populates the internal API secret. |  |  |  |
| `daemon` _[SSHServerDaemon](#sshserverdaemon)_ | Daemon contains configuration options for the specified SSH daemon. |  |  |  |
| `lfs` _[SSHServerLFS](#sshserverlfs)_ |  |  |  |  |
| `pat` _[SSHServerPAT](#sshserverpat)_ |  |  |  |  |
| `logFormat` _[LogFormat](#logformat)_ | Defaults to JSON. Available values: ("JSON", "Text") |  | Enum: [Text JSON Structured] <br /> |  |
| `logLevel` _[LogLevel](#loglevel)_ |  |  | Enum: [Trace Debug Info Warn Error Fatal] <br /> |  |
| `tracing` _[TracingSupport](#tracingsupport)_ |  |  |  |  |
| `workload` _[WorkloadSpec](#workloadspec)_ |  |  |  |  |


#### SSHServerStatus







_Appears in:_
- [SSHServer](#sshserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `version` _string_ |  |  |  |  |
| `replicas` _integer_ |  |  |  |  |
| `selector` _string_ |  |  |  |  |
| `sharedObjects` _[SharedObjectReference](#sharedobjectreference) array_ |  |  |  |  |
| `conditions` _[Condition](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#condition-v1-meta) array_ |  |  |  |  |


#### SentrySupport







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `dsn` _string_ |  |  |  |  |
| `environment` _string_ |  |  |  |  |
| `clientSideDSN` _string_ |  |  |  |  |


#### ServiceAuthentication







_Appears in:_
- [ApplicationRepository](#applicationrepository)
- [ClickHouse](#clickhouse)
- [IMAPServer](#imapserver)
- [LDAPServer](#ldapserver)
- [PostgreSQL](#postgresql)
- [Redis](#redis)
- [SMTPServer](#smtpserver)
- [ServiceProvider](#serviceprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `basic` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ |  |  |  |  |
| `tls` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ |  |  |  |  |
| `token` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### ServiceEndpoint







_Appears in:_
- [ApplicationConfigSpec](#applicationconfigspec)
- [ApplicationRepository](#applicationrepository)
- [ClickHouse](#clickhouse)
- [IMAPServer](#imapserver)
- [PostgreSQL](#postgresql)
- [Redis](#redis)
- [SMTPServer](#smtpserver)
- [ServiceProvider](#serviceprovider)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `service` _[ObjectReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#objectreference-v1-core)_ | Service is a reference to the provider service.<br />It could be a Controller, a Service.<br />Currently we support:<br /> - Service<br />The controller will derive the TLS configuration on a best-effort basis.<br />If the TLS configuration is not derived, please use the [TLS] field. |  |  |  |
| `external` _[ExternalServiceEndpoint](#externalserviceendpoint)_ | External is a reference to the external service.<br />Use [TLS] to enable the TLS settings. |  |  |  |
| `tls` _boolean_ |  |  |  |  |


#### ServiceProvider







_Appears in:_
- [ApplicationRepository](#applicationrepository)
- [ClickHouse](#clickhouse)
- [IMAPServer](#imapserver)
- [PostgreSQL](#postgresql)
- [Redis](#redis)
- [SMTPServer](#smtpserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `endpoint` _[ServiceEndpoint](#serviceendpoint)_ |  |  |  |  |
| `authentication` _[ServiceAuthentication](#serviceauthentication)_ |  |  |  |  |


#### SharedObjectReference







_Appears in:_
- [ApplicationConfigStatus](#applicationconfigstatus)
- [ApplicationServerStatus](#applicationserverstatus)
- [JobProcessorStatus](#jobprocessorstatus)
- [KubernetesAgentServerStatus](#kubernetesagentserverstatus)
- [SSHServerStatus](#sshserverstatus)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `name` _string_ |  |  |  |  |
| `namespace` _string_ |  |  |  |  |
| `apiVersion` _string_ |  |  |  |  |
| `kind` _string_ |  |  |  |  |
| `usage` _string_ |  |  |  |  |


#### SidekiqMemoryKiller







_Appears in:_
- [SidekiqSpec](#sidekiqspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `daemonMode` _boolean_ |  |  |  |  |
| `checkInterval` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `maxRSS` _[Quantity](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#quantity-resource-api)_ |  |  |  |  |
| `hardLimitRSS` _[Quantity](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#quantity-resource-api)_ |  |  |  |  |
| `shutdownWait` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `graceTime` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |


#### SidekiqRoutingRule







_Appears in:_
- [SidekiqSpec](#sidekiqspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `query` _string_ |  |  |  |  |
| `queue` _string_ |  |  |  |  |


#### SidekiqSettings







_Appears in:_
- [SidekiqSpec](#sidekiqspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `enableBootsnap` _boolean_ |  |  |  |  |
| `concurrency` _integer_ |  |  | Minimum: 0 <br /> |  |
| `timeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `logFormat` _[LogFormat](#logformat)_ |  |  | Enum: [Text JSON Structured] <br /> |  |
| `queues` _string array_ |  |  |  |  |


#### SidekiqSpec







_Appears in:_
- [JobProcessorSpec](#jobprocessorspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `metrics` _[MetricsSupport](#metricssupport)_ |  |  |  |  |
| `settings` _[SidekiqSettings](#sidekiqsettings)_ |  |  |  |  |
| `memoryKiller` _[SidekiqMemoryKiller](#sidekiqmemorykiller)_ |  |  |  |  |
| `routingRules` _[SidekiqRoutingRule](#sidekiqroutingrule) array_ |  |  |  |  |


#### TLSClientSupport







_Appears in:_
- [IMAPServer](#imapserver)
- [LDAPServer](#ldapserver)
- [PostgreSQL](#postgresql)
- [Redis](#redis)
- [SMTPServer](#smtpserver)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `minVersion` _string_ |  |  | Pattern: `^TLSv1(\.[0-3])?$` <br /> |  |
| `maxVersion` _string_ |  |  | Pattern: `^TLSv1(\.[0-3])?$` <br /> |  |
| `ciphers` _string_ |  |  | Pattern: `^(!?[\w-]+)(?::!?[\w-]+)*$` <br /> |  |
| `caCertificate` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### TLSServerSupport







_Appears in:_
- [MetricsSupport](#metricssupport)
- [PumaSpec](#pumaspec)
- [WorkhorseSpec](#workhorsespec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `minVersion` _string_ |  |  | Pattern: `^TLSv1(\.[0-3])?$` <br /> |  |
| `maxVersion` _string_ |  |  | Pattern: `^TLSv1(\.[0-3])?$` <br /> |  |
| `ciphers` _string_ |  |  | Pattern: `^(!?[\w-]+)(?::!?[\w-]+)*$` <br /> |  |
| `credentials` _[SecretReference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretreference-v1-core)_ |  |  |  |  |
| `caCertificate` _[SecretKeySelector](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#secretkeyselector-v1-core)_ |  |  |  |  |


#### TracingSupport







_Appears in:_
- [ApplicationServerSpec](#applicationserverspec)
- [JobProcessorSpec](#jobprocessorspec)
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `connectionString` _string_ |  |  |  |  |
| `urlTemplate` _string_ |  |  |  |  |


#### WorkhorseSettings







_Appears in:_
- [WorkhorseSpec](#workhorsespec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `logFormat` _[LogFormat](#logformat)_ |  |  | Enum: [Text JSON Structured] <br /> |  |
| `sentryDSN` _string_ |  |  | Format: uri <br /> |  |
| `shutdownTimeout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `shutdownBlackout` _[Duration](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#duration-v1-meta)_ |  |  |  |  |
| `trustedCIDRsForPropagation` _string array_ |  |  |  |  |
| `trustedCIDRsForXForwardedFor` _string array_ |  |  |  |  |
| `scalerMaxProcesses` _integer_ |  |  | Minimum: 0 <br /> |  |
| `scalerMaxFileSize` _[Quantity](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#quantity-resource-api)_ |  |  |  |  |


#### WorkhorseSpec







_Appears in:_
- [ApplicationServerSpec](#applicationserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `metrics` _[MetricsSupport](#metricssupport)_ |  |  |  |  |
| `settings` _[WorkhorseSettings](#workhorsesettings)_ |  |  |  |  |
| `redisName` _string_ |  |  |  |  |
| `objectStoreName` _string_ |  |  |  |  |
| `tls` _[TLSServerSupport](#tlsserversupport)_ |  |  |  |  |


#### WorkloadSpec







_Appears in:_
- [ApplicationServerSpec](#applicationserverspec)
- [DatabaseMigrationSpec](#databasemigrationspec)
- [JobProcessorSpec](#jobprocessorspec)
- [KubernetesAgentServerSpec](#kubernetesagentserverspec)
- [SSHServerSpec](#sshserverspec)

| Field | Description | Default | Validation | Example |
| --- | --- | --- | --- | --- |
| `imageSource` _[ImageSource](#imagesource)_ |  |  |  |  |
| `podTemplate` _[PodTemplate](#podtemplate)_ |  |  |  |  |
| `podDisruptionBudget` _[PodDisruptionBudget](#poddisruptionbudget)_ |  |  |  |  |
| `managedContainers` _[ManagedContainerSpec](#managedcontainerspec) array_ |  |  |  |  |




<!-- markdownlint-enable -->