package api

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("ApplicationConfig", func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		appConfig  *v2alpha2.ApplicationConfig
		service    *corev1.Service
		secret     *corev1.Secret
	)

	fakeScheme = runtime.NewScheme()
	utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

	BeforeEach(func() {
		service = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-postgres",
				Namespace: "default",
			},
			Spec: corev1.ServiceSpec{
				Ports: []corev1.ServicePort{
					{Name: "postgresql", Port: 5432},
				},
			},
		}

		secret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-zoekt-secret",
				Namespace: "default",
			},
			Data: map[string][]byte{
				"username": []byte("test-user"),
				"password": []byte("test-password"),
			},
		}

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(service, secret).
			Build()

		framework.Client = fakeClient
		framework.Scheme = fakeScheme
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	Context("Consume", func() {
		It("resolves references and reports missing elements", func() {
			appConfig = &v2alpha2.ApplicationConfig{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-app-config",
					Namespace: "default",
				},
				Spec: v2alpha2.ApplicationConfigSpec{
					PostgreSQL: []v2alpha2.PostgreSQL{
						{
							Name: "db1",
							ServiceProvider: v2alpha2.ServiceProvider{
								Endpoint: v2alpha2.ServiceEndpoint{
									Service: &corev1.ObjectReference{
										Name:      "test-postgres",
										Namespace: "default",
									},
								},
								Authentication: v2alpha2.ServiceAuthentication{
									Basic: &corev1.SecretReference{
										Name: "test-postgres-secret",
									},
								},
							},
						},
					},
					Prometheus: &v2alpha2.ServiceEndpoint{
						Service: &corev1.ObjectReference{
							Name:      "test-prometheus",
							Namespace: "default",
						},
					},
					ZoektCredentials: &corev1.SecretReference{
						Name:      "test-zoekt-secret",
						Namespace: "default",
					},
				},
			}

			cfg := NewApplicationConfig(
				ApplicationConfigPostgreSQL,
				ApplicationConfigPrometheus,
				ApplicationConfigZoekt,
			)

			err := cfg.Consume(context.Background(), appConfig)
			Expect(err).NotTo(HaveOccurred())

			Expect(cfg.PostgreSQL).NotTo(BeNil())
			Expect(cfg.PostgreSQL.All).To(HaveLen(1))
			Expect(cfg.PostgreSQL.All["db1"]).NotTo(BeNil())
			Expect(cfg.PostgreSQL.All["db1"].Endpoint.Host).To(Equal("test-postgres.default.svc.cluster.local"))

			Expect(cfg.Prometheus).NotTo(BeNil())
			Expect(cfg.Prometheus.Host).To(BeEmpty())

			Expect(cfg.ZoektCredentials).NotTo(BeNil())
			Expect(cfg.ZoektCredentials["username"]).To(Equal("test-user"))
			Expect(cfg.ZoektCredentials["password"]).To(Equal("test-password"))

			Expect(cfg.Ready()).To(BeFalse())
			Expect(cfg.Missing()).To(ContainElements(
				"v1/Secret[default/test-postgres-secret]",
				"v1/Service[default/test-prometheus]",
			))
		})
	})
})
