package applicationserver

import (
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/appconfigutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
)

const (
	internalSecretsSuffix = "-app-server-internal" //nolint:gosec
	appSecretNameSuffix   = "-app-secrets"
	appServerNameSuffix   = "-app-server"
)

func (c *Controller) seSharedObject(rtCtx *framework.RuntimeContext, usage string, target client.Object) error {
	sharedObject := apiutils.SharedObjectReferenceForObject(usage, target)

	return controllerutils.SetStatusSharedObject(rtCtx, c.appServer, &c.appServer.Status.SharedObjects, sharedObject)
}

func (c *Controller) setStatusCondition(rtCtx *framework.RuntimeContext, condition metav1.Condition) error {
	return controllerutils.SetStatusCondition(rtCtx, c.appServer, &c.appServer.Status.Conditions, condition)
}

func (c *Controller) clearStatusCondition(rtCtx *framework.RuntimeContext, condType string) error {
	return controllerutils.ClearStatusCondition(rtCtx, c.appServer, &c.appServer.Status.Conditions, condType)
}

func (c *Controller) appConfigRef() (*types.NamespacedName, error) {
	return appconfigutils.GetApplicationConfigRef(c.appServer.Spec.ApplicationConfigRef, c.appServer)
}

func (c *Controller) getReferencedResources(rtCtx *framework.RuntimeContext) error {
	c.appConfigResources = appconfigutils.NewApplicationConfigDereferencer(c.appConfig, appconfigutils.IncludeAllResources...)
	if err := c.appConfigResources.Dereference(rtCtx); err != nil {
		return err
	}

	return nil
}

func (c *Controller) allResourcesReady() framework.ConditionFunc {
	return func(rtCtx *framework.RuntimeContext) (bool, error) {
		return c.appConfigResources.Ready(), nil
	}
}

func (c *Controller) getDeployment(rtCtx *framework.RuntimeContext) (*appsv1.Deployment, error) {
	key := types.NamespacedName{
		Namespace: c.appServer.Namespace,
		Name:      c.appServer.Name + appServerNameSuffix,
	}
	deployment := &appsv1.Deployment{}

	if err := framework.Client.Get(rtCtx, key, deployment); err != nil {
		return nil, err
	}

	return deployment, nil
}
