{{/*
Prints out the default resource requirements for Kubernetes workload.

Expects a context with the following keys:

  - ResourceRequests: Resource requests in the form of `corev1.ResourceList`.
  - ResourceLimits: Resource limits in the form of `corev1.ResourceList`.

These are the default values that are defined in application-specific settings,
for example `.Settings.Rails`.
*/}}
resources:
  requests: {{ .ResourceRequests | toJson }}
  limits: {{ .ResourceLimits | toJson }}