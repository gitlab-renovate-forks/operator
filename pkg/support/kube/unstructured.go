package kube

import (
	"fmt"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// Reads a typed object from an unstructured object.
func FromUnstructured(u *unstructured.Unstructured, obj client.Object) error {
	return runtime.DefaultUnstructuredConverter.FromUnstructured(u.Object, obj)
}

// Reads a typed object from an unstructured object using the given scheme.
func ConvertUnstructuredWithScheme(u *unstructured.Unstructured, scheme *runtime.Scheme) (client.Object, error) {
	gvk := u.GetObjectKind().GroupVersionKind()

	rtObj, err := scheme.New(gvk)
	if err != nil {
		return nil, fmt.Errorf("failed to create runtime.Object for %q: %w", gvk, err)
	}

	if obj, isClientObj := rtObj.(client.Object); !isClientObj {
		return nil, fmt.Errorf("failed to convert unstructured.Unstructured object to client.Object. %T is not supported", rtObj)
	} else {
		err = FromUnstructured(u, obj)

		return obj, err
	}
}

// Reads a typed object from an unstructured object and returns it.
func ConvertUnstructured(u *unstructured.Unstructured) (client.Object, error) {
	return ConvertUnstructuredWithScheme(u, framework.Scheme)
}
