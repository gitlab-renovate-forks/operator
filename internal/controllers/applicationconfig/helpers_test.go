package applicationconfig

import (
	"errors"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	// We use apimachinery YAML parser for test. The implementation
	// uses gopkg.in/yaml.v3 for formatting and marshaling YAML.
	"k8s.io/apimachinery/pkg/util/yaml"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
)

var _ = Describe("formatRailsSecrets", func() {
	Context("when generators succeed", func() {
		It("should aggregate secrets and format them correctly", func() {
			generators := []secret.Generator{
				&mockGenerator{
					payload: secret.Payload{
						"secret_key_base": []byte("test-secret-key"),
					},
					err: nil,
				},
				&mockGenerator{
					payload: secret.Payload{
						"active_record_encryption_primary_key":       []byte("test-encryption-primary-key"),
						"active_record_encryption_deterministic_key": []byte("test-encryption-deterministic-key"),
					},
					err: nil,
				},
				&mockGenerator{
					payload: secret.Payload{
						"openid_connect_signing_key": []byte(mockRSAPrivateKey),
					},
					err: nil,
				},
			}

			result, err := shapeRailsSecrets(
				generators, railsSecretKey, railsEnvName).Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(HaveKey(railsSecretKey))

			var railsSecretData map[string]any
			err = yaml.Unmarshal(result[railsSecretKey], &railsSecretData)

			Expect(err).NotTo(HaveOccurred())
			Expect(railsSecretData).To(HaveKey(railsEnvName))

			envSecrets := railsSecretData[railsEnvName].(map[string]any)
			Expect(envSecrets).To(HaveKeyWithValue("secret_key_base", "test-secret-key"))
			Expect(envSecrets).To(HaveKeyWithValue("active_record_encryption_primary_key", []any{"test-encryption-primary-key"}))
			Expect(envSecrets).To(HaveKeyWithValue("active_record_encryption_deterministic_key", []any{"test-encryption-deterministic-key"}))
			Expect(envSecrets).To(HaveKeyWithValue("openid_connect_signing_key", mockRSAPrivateKey))
		})
	})

	Context("when a generator fails", func() {
		It("should return the error", func() {
			expectedErr := errors.New("generator error")

			generators := []secret.Generator{
				&mockGenerator{
					payload: nil,
					err:     expectedErr,
				},
			}

			result, err := shapeRailsSecrets(
				generators, railsSecretKey, railsEnvName).Generate()

			Expect(err).To(MatchError(expectedErr))
			Expect(result).To(BeNil())
		})
	})

	Context("with empty generators list", func() {
		It("should generate valid YAML with empty secrets", func() {
			generators := []secret.Generator{}

			result, err := shapeRailsSecrets(
				generators, railsSecretKey, railsEnvName).Generate()

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(HaveKey(railsSecretKey))

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(HaveKey(railsSecretKey))

			var railsSecretData map[string]any
			err = yaml.Unmarshal(result[railsSecretKey], &railsSecretData)

			Expect(err).NotTo(HaveOccurred())
			Expect(railsSecretData).To(HaveKey(railsEnvName))

			envSecrets := railsSecretData[railsEnvName].(map[string]any)
			Expect(envSecrets).To(BeEmpty())
		})
	})
})

type mockGenerator struct {
	payload secret.Payload
	err     error
}

func (m *mockGenerator) Generate() (secret.Payload, error) {
	return m.payload, m.err
}

const (
	railsSecretKey    = "secrets.yaml"
	railsEnvName      = "production"
	mockRSAPrivateKey = `-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCtwUbWXzbI8+QF
R7xmvz0S81SzhHLNa4Jsjcf18JhYcQWqD5xeO0HRxz1gczNSxNVX8PrNw4HfaaRE
WkNCXFj1CQws9eBIAnNoOOiSIwcMqpYfiI2wQChoOWCV7kn2gYtMJFpclpcyLoVf
-----END PRIVATE KEY-----`
)
