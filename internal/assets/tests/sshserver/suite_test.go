package gitlabshell

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"unicode"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gopkg.in/yaml.v3"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestApplicationConfigTemplates(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: ApplicationConfig Templates")
}

func readYAMLData(data []byte) map[string]any {
	m := map[string]any{}

	err := yaml.Unmarshal(data, &m)

	Expect(err).NotTo(HaveOccurred())

	return m
}

func readYAMLSource(source string) map[string]any {
	f, err := os.Open(fmt.Sprintf("testdata/%s", source))
	Expect(err).NotTo(HaveOccurred())

	m := map[string]any{}

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&m)

	Expect(err).NotTo(HaveOccurred())

	return m
}

func readLiteralSource(source string) string {
	f, err := os.Open(fmt.Sprintf("testdata/%s", source))
	Expect(err).NotTo(HaveOccurred())

	b := make([]byte, 1024)
	n, err := f.Read(b)
	Expect(err).NotTo(HaveOccurred())

	return string(b[:n])
}

func selectConfigMap(name string, key string) *corev1.ConfigMapKeySelector {
	return &corev1.ConfigMapKeySelector{
		LocalObjectReference: corev1.LocalObjectReference{
			Name: name,
		},
		Key: key,
	}
}

func makeConfigMap(namespace, name string, kvs ...string) *corev1.ConfigMap {
	dataMap := map[string]string{}
	for i := 0; i < len(kvs); i += 2 {
		dataMap[kvs[i]] = kvs[i+1]
	}

	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Data: dataMap,
	}
}

// normalizeString is a helper function to normalize rendered strings.
// It removes empty lines and trailing whitespaces.
func normalizeString(s string) string {
	lines := strings.Split(s, "\n")

	output := make([]string, 0, len(lines))

	for i, line := range lines {
		lines[i] = strings.TrimRightFunc(line, unicode.IsSpace)
		if lines[i] != "" {
			output = append(output, lines[i])
		}
	}

	return strings.Join(output, "\n")
}
