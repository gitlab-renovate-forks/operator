{{- $spec := .ApplicationServer.Spec -}}

image: {{ getImageName $spec.Workload.ImageSource "Workhorse" $spec.Version $spec.Edition }}
ports:
  - name: workhorse
    containerPort: {{ .Settings.Rails.Workhorse.Port }}
  {{- if not $spec.Workhorse.Metrics.Disabled }}    
  - name: metrics
    containerPort: {{ .Settings.Rails.Workhorse.MetricsPort }}
  {{- end }}
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: GITLAB_WORKHORSE_SECRET_FILE
    value: {{ .Settings.Secrets.MountPath }}/workhorse-secret
  - name: GITLAB_WORKHORSE_AUTH_BACKEND
    value: http://localhost:{{ .Settings.Rails.Puma.Port }}
  - name: GITLAB_WORKHORSE_LISTEN_PORT
    value: {{ .Settings.Rails.Workhorse.Port }}
  - name: GITLAB_WORKHORSE_LOG_FORMAT
    value: {{ $spec.Workhorse.Settings.LogFormat | toString | default .Settings.Rails.Workhorse.LogFormat | lower }}
  - name: SHUTDOWN_BLACKOUT_SECONDS
    value: {{ $spec.Workhorse.Settings.ShutdownBlackout | default .Settings.Rails.Workhorse.ShutdownBlackout | toSeconds }}
  {{- if $spec.Workhorse.Settings.SentryDSN }}
  - name: GITLAB_WORKHORSE_SENTRY_DSN
    value: {{ $spec.Workhorse.Settings.SentryDSN }}
  {{- end }}
  {{- include "commons/_tracingVariables.tpl" $spec | nindent 2 }}
volumeMounts:
  {{- include "application-server/_sharedVolumeMount.tpl" . | nindent 2 }}
  - name: workhorse-config
    mountPath: {{ $.Settings.Rails.ConfigDir }}/workhorse-config.toml
    subPath: workhorse.toml
lifecycle:
  preStop:
    exec:
      command: ["/bin/bash", "-c", "sleep ${SHUTDOWN_BLACKOUT_SECONDS}"]
livenessProbe:
  exec:
    command: ["/scripts/healthcheck"]
  initialDelaySeconds: 20
  periodSeconds: 60
  timeoutSeconds: 30
  successThreshold: 1
  failureThreshold: 3
readinessProbe:
  exec:
    command: ["/scripts/healthcheck"]
  initialDelaySeconds: 0
  periodSeconds: 10
  timeoutSeconds: 2
  successThreshold: 1
  failureThreshold: 3

{{- include "commons/_defaultResources.tpl" .Settings.Rails }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}