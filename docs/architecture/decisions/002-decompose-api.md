# Decompose Kubernetes API

**Date**: 2024-09-26

**Status**: Accepted

## Context

The `v2alpha1` API provides an all-in-one Kubernetes custom resource to manage
GitLab instances. The design challenges and requirements of a decomposed API are
outlined in [API development document](../../development/api.md#decomposition).

## Decision

1. In API version `v2alpha2`, the all-in-one GitLab custom resource of API
   version `v2alpha1` is decomposed into multiple custom resources, most notably:

   - `ApplicationConfig` which contains GitLab Rails application configuration
     and secrets, most of which are located in [`config`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/config)
     directory of GitLab Rails application. It is shared between Puma, Sidekiq,
     database migrations, and other Rails-based components. Note that it is the
     responsibility of the referencing component to render the content of the
     resource it into a usable application configuration.

   - `ApplicationServer` describes the [_workload_](https://kubernetes.io/docs/concepts/workloads/)
     of GitLab Rails application server that runs Puma and Workhorse. It uses
     `ApplicationConfig` to configure Puma.

   - `JobProcessor` describes the workload for background job processing of
     GitLab Rails application that Sidekiq runs. It uses `ApplicationConfig` to
     configure Sidekiq.

   - `DatabaseMigration` describes the workload for running database migrations
     of GitLab Rails application. It uses `ApplicationConfig` to configure the
     database migrations.

   - `Instance` manages the lifecycle of a GitLab instance and encompasses its
     various components. It provides shared information such as version and
     edition of GitLab and orchestrates install and upgrade processes.

   - Container registry and other components such GitLab Shell and Kubernetes
     Agent Server (KAS) have their own individual resources. For brevity, in
     this document we only refer to `ContainerRegistry` but the same pattern can
     be applied to other similar components.

1. Each custom resource is completely self-contained and has its own controller,
   sub-resources, and events. It may depend on other custom resources to
   function but its controller manages its full lifecycle.

## Consequences

- `GitLab` custom resource `v2alpha1` is split into multiple custom resources
  with their own controllers.
- API `v2alpha1` and its controller will not implement new features, and will be
  removed as soon as the decomposed API and controllers of `v2alpha2` offer the
  similar features.

## Appendix

### Interdependencies of resources

```mermaid
classDiagram
  ApplicationConfig --> ContainerRegistry : containerRegistryRef
  ApplicationServer --> ApplicationConfig : appConfigRef
  JobProcessor --> ApplicationConfig : appConfigRef
  DatabaseMigration --> ApplicationConfig : appConfigRef
  ContainerRegistry --> ApplicationServer : authEndpointRef
```

Notes:

1. All controllers are aware of interdependencies of underlying resources. As
   such, they:
   - Await resources that are created by other controllers and handle errors
     that are due to unfulfilled resource promises.
   - Prioritize the creation of resources that other controllers may depend on.

1. Rails-based components such as `ApplicationServer`, `JobProcessor`, and
   `DatabaseMigration` directly reference `ApplicationConfig` via `appConfigRef`
   attribute. However, their controllers must render the Rails application
   configuration based on their requirements. For example, while using the same
   `ApplicationConfig`, they can have different [`gitlab.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/gitlab.yml.example).

1. Controller of `ApplicationConfig` is responsible for:
   - Validating the configuration.
   - Generating Rails application secrets, for example [`secrets.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/initializers/01_secret_token.rb),
     when needed.
   - Updating the status and condition of the resource.

1. Controller of a typical Rails-based component:
   - Dereferences `ApplicationConfig` and waits for it to become _ready_.
   - Renders Rails application configuration.
   - Dereferences transient dependencies of `ApplicationConfig`, for example
     other Secrets, and waits for them to become available.
   - Provisions Rails application workload.
   - Watch for changes to associated `ApplicationConfig` resource and reconcile
     when needed.

#### Instance resource

```mermaid
classDiagram
  ApplicationServer o.. Instance : labels{instance}
  JobProcessor o.. Instance : labels{instance}
  DatabaseMigration o.. Instance : labels{instance}
  ContainerRegistry o.. Instance : labels{instance}
```

Notes:

1. Controller of `Instance` manages the lifecycle of a GitLab instance and goes
   beyond individual components. For example, the controller:
   - Runs workflow for [zero-downtime upgrade](https://docs.gitlab.com/ee/update/zero_downtime.html).
     This workflow involves multiple Rails-based components.
   - Creates `DatabaseMigration` resources when a database migration is needed.
   - Updates the version of `ApplicationServer`, `JobProcessor`, and other
     Rails-based components.
   - Manages the upgrade cycle of non-Rails components like `ContainerRegistry`.

1. An `Instance` is capable of supporting vertical scaling of a GitLab instance
   and can manage multiple `ApplicationServer` or `JobProcessor` instances.
