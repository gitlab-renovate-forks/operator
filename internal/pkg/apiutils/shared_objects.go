package apiutils

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// SharedObjectReferenceForObjectRef creates a SharedObjectReference for the
// given usage and Kubernetes object.
func SharedObjectReferenceForObject(usage string, object client.Object) v2alpha2.SharedObjectReference {
	// client.Object usually does not have GVK set, so we need to get it from the object / scheme.
	gvk := getGvk(object)

	return v2alpha2.SharedObjectReference{
		Usage:      usage,
		Name:       object.GetName(),
		Namespace:  object.GetNamespace(),
		APIVersion: gvk.GroupVersion().String(),
		Kind:       gvk.Kind,
	}
}

func getGvk(obj client.Object) schema.GroupVersionKind {
	gvk := obj.GetObjectKind().GroupVersionKind()
	if gvk.Empty() {
		// try get gvk
		gvks, _, err := framework.Scheme.ObjectKinds(obj)
		if err == nil && len(gvks) > 0 {
			gvk = gvks[0]
		}
	}

	return gvk
}

// SharedObjectReferenceForObjectKey creates a SharedObjectReference for the
// given usage and Kubernetes object key. It does not include the APIVersion or
// Kind.
func SharedObjectReferenceForObjectKey(usage string, objectKey client.ObjectKey) v2alpha2.SharedObjectReference {
	return v2alpha2.SharedObjectReference{
		Usage:     usage,
		Name:      objectKey.Name,
		Namespace: objectKey.Namespace,
	}
}

// SharedObjectReferenceForObjectRef creates a SharedObjectReference for the
// given usage and Kubernetes object reference.
func SharedObjectReferenceForObjectRef(usage string, objectRef corev1.ObjectReference) v2alpha2.SharedObjectReference {
	return v2alpha2.SharedObjectReference{
		Usage:      usage,
		Name:       objectRef.Name,
		Namespace:  objectRef.Namespace,
		APIVersion: objectRef.APIVersion,
		Kind:       objectRef.Kind,
	}
}

// SetStatusSharedObject sets the corresponding shared object reference in the
// list of shared objects references. It returns true if the shared object
// reference was changed, false otherwise.
//
// If a shared object reference with the same usage already exists, all fields
// of the existing reference are updated with the new values. Otherwise a new
// shared object reference is created and added to the list.
func SetStatusSharedObject(sharedObjects *[]v2alpha2.SharedObjectReference, newSharedObject v2alpha2.SharedObjectReference) bool {
	if sharedObjects == nil {
		return false
	}

	existingSharedObject := FindStatusSharedObject(*sharedObjects, newSharedObject.Usage)
	if existingSharedObject == nil {
		*sharedObjects = append(*sharedObjects, newSharedObject)

		return true
	}

	changed := false

	changed = compareAndSet(&existingSharedObject.Name, newSharedObject.Name) || changed
	changed = compareAndSet(&existingSharedObject.Namespace, newSharedObject.Namespace) || changed
	changed = compareAndSet(&existingSharedObject.APIVersion, newSharedObject.APIVersion) || changed
	changed = compareAndSet(&existingSharedObject.Kind, newSharedObject.Kind) || changed

	return changed
}

// RemoveStatusSharedObject removes the shared object reference with the given
// usage from the list of shared objects references. It returns true if the
// shared object reference was removed, false otherwise.
func RemoveStatusSharedObject(sharedObjects *[]v2alpha2.SharedObjectReference, usage string) bool {
	if sharedObjects == nil || len(*sharedObjects) == 0 {
		return false
	}

	newSharedObjects := make([]v2alpha2.SharedObjectReference, 0, len(*sharedObjects)-1)
	for i := range *sharedObjects {
		if (*sharedObjects)[i].Usage != usage {
			newSharedObjects = append(newSharedObjects, (*sharedObjects)[i])
		}
	}

	if len(newSharedObjects) == len(*sharedObjects) {
		return false
	} else {
		*sharedObjects = newSharedObjects

		return true
	}
}

// FindStatusSharedObject returns the shared object reference with the given
// usage. If no shared object reference with the given usage exists, nil is
// returned.
func FindStatusSharedObject(sharedObjects []v2alpha2.SharedObjectReference, usage string) *v2alpha2.SharedObjectReference {
	for i := range sharedObjects {
		if sharedObjects[i].Usage == usage {
			return &sharedObjects[i]
		}
	}

	return nil
}

func compareAndSet(oldValue *string, newValue string) bool {
	if *oldValue != newValue {
		*oldValue = newValue

		return true
	}

	return false
}
