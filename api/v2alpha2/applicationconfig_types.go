package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	/* Condition Types */
	ApplicationConfigReadyCondition = "ApplicationConfigReady"

	/* Condition Reasons */
	PendingRailsSecretsReason   = "PendingRailsSecrets"
	RailsSecretsAvailableReason = "RailsSecretsAvailable"

	/* Shared Object Usages */
	RailsSecretsObjectUsage = "RailsSecrets"
)

// ApplicationConfig contains GitLab Rails application configuration. It plays
// an essential role in configuring a GitLab instance and is required for
// running some of the key components of GitLab, in particular any Rail-based
// workload such as Puma and Sidekiq.
//
// An ApplicationConfig can be shared between multiple components. Generally, a
// GitLab instance only has one ApplicationConfig, but this is not a requirement
// that is enforced, and in rare cases an instance may have multiple
// "non-conflicting" ApplicationConfigs.
//
// ApplicationConfig offers a wide range of configuration items, including:
//
//   - External URL and host names
//   - Database and object store backends
//   - Incoming and outgoing email settings
//   - Various authentication providers
//   - GitLab Rails application settings
//   - Integrations with other GitLab and third-party components
//
// As a rule of thumb you can assume that any configuration that is located in
// GitLab Rails `config` directory is available through the ApplicationConfig.
//
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=appcfg
// +kubebuilder:printcolumn:name="ExternalURL",type=string,JSONPath=`.spec.externalUrl`
type ApplicationConfig struct {
	metav1.TypeMeta `json:",inline"`

	// Standard object metadata.
	// See: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired GitLab Rails application configuration.
	Spec ApplicationConfigSpec `json:"spec,omitempty"`

	// Summary the most recently observed status of the ApplicationConfig.
	Status ApplicationConfigStatus `json:"status,omitempty"`
}

// A collection of ApplicationConfigs.
//
// +kubebuilder:object:root=true
type ApplicationConfigList struct {
	metav1.TypeMeta `json:",inline"`

	// Standard object metadata.
	// See: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
	metav1.ListMeta `json:"metadata,omitempty"`

	// The list of ApplicationConfigs.
	Items []ApplicationConfig `json:"items"`
}

// The specification of the desired GitLab Rails application configuration.
//
// +kubebuilder:validation:XValidation:rule="!has(self.customization) || self.customization.all(key, key.matches('^[a-z]+(?:[A-Z][a-z]*)*$'))",fieldPath=".customization"
type ApplicationConfigSpec struct {
	// The URL that GitLab instance is reachable at. It must be a valid HTTP(S)
	// URL without a relative path.
	//
	// +required
	// +kubebuilder:validation:Pattern="^https?:\\/\\/(([a-zA-Z0-9][-a-zA-Z0-9]*\\.)+[a-zA-Z]{2,}|\\d{1,3}(\\.\\d{1,3}){3})(:\\d+)?$"
	// +kubebuilder:example="https://gitlab.example.com"
	// +kubebuilder:example="http://192.168.1.2:8000"
	ExternalURL string `json:"externalUrl"`

	// The URL of CDN or a host that is used for delivering static Rails assets.
	//
	// +optional
	// +kubebuilder:validation:Format=uri
	CDNHost *string `json:"cdnHost,omitempty"`

	// The host for Git access to GitLab repositories over SSH.
	//
	// Use this if your SSH host is different from the host of the external URL.
	//
	// +optional
	// +kubebuilder:validation:Pattern="^(([a-zA-Z0-9][-a-zA-Z0-9]*\\.)+[a-zA-Z]{2,}|\\d{1,3}(\\.\\d{1,3}){3})(:\\d+)?$"
	// +kubebuilder:example="ssh.example.com"
	// +kubebuilder:example="192.168.1.2:2222"
	SSHHost *string `json:"sshHost,omitempty"`

	// The maximum time Puma can spend on processing a request.
	//
	// This needs to be smaller than the Puma worker timeout. It defaults to
	// 95% of the Puma worker timeout.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	MaxRequestDuration *metav1.Duration `json:"maxRequestDuration,omitempty"`

	// The configuration for Content Security Policy (CSP) headers.
	//
	// See: https://guides.rubyonrails.org/security.html#content-security-policy
	//
	// +optional
	ContentSecurityPolicy *ContentSecurityPolicy `json:"contentSecurityPolicy,omitempty"`

	// A list of hosts that are accepted to send requests to the instance.
	//
	// Use this to prevent GitLab application from accepting a `Host` headers
	// other than what is intended. It is recommended for defense in depth
	// against potential HTTP Host header attacks.
	//
	// +optional
	// +kubebuilder:validation:items:Pattern="^(\\.?[a-zA-Z0-9][-a-zA-Z0-9.]*[a-zA-Z0-9]|\\d{1,3}(\\.\\d{1,3}){3}(\\/\\d{1,2})?)$"
	// +kubebuilder:example="example.com" 
	// +kubebuilder:example="192.168.1.2"
	// +kubebuilder:example="10.0.0.0/24" 
	AllowedHosts []string `json:"allowedHosts,omitempty"`

	// A list of trusted proxies. Customize if your instance is behind a reverse
	// proxy that runs on a different network.
	//
	// Add the IP address for your reverse proxies to the list, otherwise users
	// will appear signed in from that address.
	//
	// +optional
	// +kubebuilder:validation:items:Format=cidr
	TrustedProxies []string `json:"trustedProxies,omitempty"`

	// The default time zone of GitLab Rails application.
	//
	// Use the format of IANA Time Zone Database standard.
	//
	// +optional
	// +kubebuilder:example="America/New_York" 
	Timezone *string `json:"timezone,omitempty"`

	// Configuration for outgoing emails from GitLab application. When disabled,
	// GitLab will not send any emails.
	//
	// +optional
	OutgoingEmail *ApplicationOutgoingEmail `json:"outgoingEmail,omitempty"`

	// General GitLab Rails application settings. The following options are
	// supported:
	//
	//   - `defaultCanCreateGroup`: Allow users to create groups by default.
	//
	//   - `usernameChangingEnabled`: Allow users to change their username and
	//     namespace.
	//
	//	 - `defaultTheme`: Default theme identifier:
	//
	//     - `1`: Indigo (the default theme)
	//     - `2`: Gray
	//     - `3`: Light Gray
	//     - `4`: Blue
	//     - `5`: Green
	//     - `6`: Light Indigo
	//     - `7`: Light Blue
	//     - `8`: Light Green
	//     - `9`: Red
	//     - `10`: Light Red
	//     - `11`: Dark Mode (alpha)
	//
	//   - `customHtmlHeaderTags`: Custom HTML header tags, for example EU cookie
	//     consent.
	//
	//     Tip: You must add the externals source to the CSP as well, typically
	//     with the `script_src` and `style_src`.
	//
	//   - `issueClosingPattern`: If a commit message matches this regular
	//     expression, all issues referenced from the matched text will be
	//     closed. This happens when the commit is pushed or merged into the
	//     default branch of a project.
	//
	//     When not specified the default issue closing pattern will be used.
	//
	//   - `impersonationEnabled`: Allow administrators to be impersonate users.
	//
	//     See: https://docs.gitlab.com/administration/admin_area/#user-impersonation
	//
	//   - `disableAnimations`: Disable jQuery and CSS animations.
	//
	// Note that the options are string-valued. You must use a string value. For
	// example use `"true"` instead of `true` or `"1"` instead of `1`.
	//
	// +optional
	Settings map[string]string `json:"settings,omitempty"`

	// Default features of GitLab projects. The following options are supported:
	//
	//   - `issues`: Enable "Issue" boards.
	//   - `mergeRequests`: Enable "Merge Requests".
	//   - `wiki`: Enable project "Wiki" pages.
	//   - `snippets`: Enable code "Snippets" sharing.
	//   - `builds`: Enable project "CI/CD" for build, test, and deploy.
	//   - `containerRegistry`: Enable "Container Registry" to store Docker images.
	//
	// All of theses options are enabled by default.
	//
	// See: https://docs.gitlab.com/user/project/settings/
	//
	// +optional
	DefaultProjectsFeatures map[string]bool `json:"defaultProjectsFeatures,omitempty"`

	// The duration to wait for the HTTP response after sending a webhook HTTP
	// POST request.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	WebhookTimeout *metav1.Duration `json:"webhookTimeout,omitempty"`

	// The duration that the application has to complete a GraphQL request.
	//
	// We suggest this value to be higher than the database timeout value
	// and lower than the Puma worker timeout.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	GraphQLTimeout *metav1.Duration `json:"graphqlTimeout,omitempty"`

	// The duration of expiry for application settings cache. You can increase
	// this value to have more delay between application setting changes and
	// when users notice those changes in the application.
	//
	// We recommend setting a non-zero value. Setting it to `0`` causes the
	// application settings to load for every request. This causes extra load
	// for Redis and PostgreSQL.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	ApplicationSettingsCache *metav1.Duration `json:"applicationSettingsCache,omitempty"`

	// Configuration for incoming emails to GitLab application. When enabled,
	// several features will be available to users:
	//
	//   - Reply by email: Comment on issues and merge requests by replying to
	//     notification email.
	//
	//   - New issue by email: Create a new issue by sending an email to a
	//     user-specific email address.
	//
	//   - New merge request by email: Create a new merge request by sending an
	//     email to a user-specific email address.
	//
	// See: https://docs.gitlab.com/administration/incoming_email/
	//
	// +optional
	IncomingEmail *ApplicationIncomingEmail `json:"incomingEmail,omitempty"`

	// Configuration for incoming emails for Service Desk function of GitLab
	// application. When enabled, the application provide email support to your
	// customers through GitLab.
	//
	// See: https://docs.gitlab.com/user/project/service_desk/
	//
	// +optional
	ServiceDeskEmail *ApplicationIncomingEmail `json:"serviceDeskEmail,omitempty"`

	// Configuration for storing various application data on object storage.
	//
	// +required
	ObjectStorage ApplicationObjectStorage `json:"objectStorage"`

	// The endpoint for Mattermost. Must be defined for enabling "Mattermost"
	// button.
	//
	// +optional
	Mattermost *ServiceEndpoint `json:"mattermost,omitempty"`

	// URL template for Gravatar.
	//
	// The URL can have placeholders. Possible placeholders are:
	//
	// 	- `%{hash}`
	// 	- `%{size}`
	// 	- `%{email}`
	// 	- `%{username}`
	//
	// See: https://docs.gitlab.com/administration/libravatar/
	//
	// +optional
	// +kubebuilder:validation:Format=uri-template
	GravatarUrl *string `json:"gravatarUrl,omitempty"`

	// Auxiliary jobs, executed periodically to perform various tasks, such as
	// self-heal and do external synchronizations.
	//
	// +optional
	CronJobs []JobSchedule `json:"cronJobs,omitempty"`

	// Auxiliary jobs, executed periodically to perform various tasks for GitLab
	// EE instances. These jobs are automatically enabled for an EE installation,
	// and ignored for a CE installation.
	//
	// +optional
	EECronJobs []JobSchedule `json:"eeCronJobs,omitempty"`

	// Sentry settings for error reporting and logging.
	//
	// +optional
	Sentry *SentrySupport `json:"sentry,omitempty"`

	// Settings for LDAP login providers.
	//
	// See: https://docs.gitlab.com/administration/auth/ldap/
	//
	// +optional
	LDAP *ApplicationLDAPSettings `json:"ldap,omitempty"`

	// Settings for OmniAuth authentication providers.
	//
	// For the list of available providers refer to GitLab documentation.
	//
	// See: https://docs.gitlab.com/integration/omniauth/
	//
	// +optional
	OmniAuth *ApplicationOmniAuthSettings `json:"omniAuth,omitempty"`

	// Settings for smart card authentication.
	//
	// See: https://docs.gitlab.com/administration/auth/smartcard/
	//
	// +optional
	SmartCard *ApplicationSmartCardSettings `json:"smartCard,omitempty"`

	// Settings for Kerberos authentication.
	//
	// See: https://docs.gitlab.com/integration/kerberos/
	//
	// +optional
	Kerberos *ApplicationKerberosSettings `json:"kerberos,omitempty"`

	// Settings for integrating with FortiAuthenticator one-time password
	// authenticator.
	//
	// See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/
	//
	// +optional
	FortiAuthenticator *ApplicationFortiAuthenticatorSettings `json:"fortiAuthenticator,omitempty"`

	// Settings for integrating with Cisco Duo one-time password authenticator.
	//
	// See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/
	//
	// +optional
	CiscoDuo *ApplicationCiscoDuoSettings `json:"ciscoDuo,omitempty"`

	// The token for verifying access to GitLab internal API for "Suggested Reviewers"
	// service.
	//
	// +optional
	SuggestedReviewersToken *corev1.SecretKeySelector `json:"suggestedReviewersToken,omitempty"`

	// Login credentials to authenticate to a Zoekt server.
	//
	// A Kubernetes Secret with basic authentication type (`kubernetes.io/basic-auth`)
	// is expected.
	//
	// +optional
	ZoektCredentials *corev1.SecretReference `json:"zoektCredentials,omitempty"`

	// The endpoint for Prometheus server.
	//
	// +optional
	Prometheus *ServiceEndpoint `json:"prometheus,omitempty"`

	// The interval to block health check, but continue accepting application
	// requests. This allows load balancer to notice service being shutdown and
	// not to interrupt any of the clients.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	ShutdownBlackout *metav1.Duration `json:"shutdownBlackout,omitempty"`

	// Additional customization. These are passed as "extra" configuration to
	// GitLab Rails application.
	//
	// +optional
	Customization map[string]string `json:"customization,omitempty"`

	// Configure Rack attack IP banning for Git basic authentication.
	//
	// Note that this feature is not enabled when not configured.
	//
	// +optional
	RackAttackGitBasicAuth *ApplicationRackAttackGitBasicAuth `json:"rackAttackGitBasicAuth,omitempty"`

	// Gitaly server endpoints. Each endpoint must have a unique name.
	//
	// Each endpoint can be a Gitaly storage or a Gitaly "virtual" storage. For
	// more details refer to Gitaly Cluster documentation.
	//
	// See: https://docs.gitlab.com/administration/gitaly/
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +kubebuilder:validation:MinItems=1
	Repositories []ApplicationRepository `json:"repositories" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// PostgreSQL database endpoints. Each endpoint must have a unique name.
	//
	// GitLab supports multiple database endpoints. The purpose of the database
	// is defined by its name. For example "main" and "ci" are two predefined
	// names that GitLab Rails application uses for its main and CI databases.
	//
	// See: https://docs.gitlab.com/development/database/multiple_databases/
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +kubebuilder:validation:MinItems=1
	PostgreSQL []PostgreSQL `json:"postgresql" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// Redis endpoints. Each endpoint must have a unique name.
	//
	// GitLab supports splitting several of the resource intensive Redis
	// operations across multiple Redis instances. In this scenario, the name
	// of the Redis endpoint must match the name of the persistence class, for
	// example, "cache", "sessions", or "queues".
	//
	// See: https://docs.gitlab.com/administration/redis/replication_and_failover/
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +kubebuilder:validation:MinItems=1
	Redis []Redis `json:"redis" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// ClickHouse database endpoints. Each endpoint must have a unique name.
	//
	// See: https://docs.gitlab.com/integration/clickhouse/
	//
	// +optional
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	ClickHouse []ClickHouse `json:"clickhouse,omitempty" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// Settings of the GitLab container registry integration.
	//
	// +optional
	ContainerRegistry *ApplicationContainerRegistry `json:"registry,omitempty"`

	// Settings of the GitLab Kubernetes Agent Server (KAS) integration.
	//
	// +optional
	KAS *ApplicationKubernetesAgentServer `json:"kas,omitempty"`

	// Configuration for experimental features. Do not use in production.
	//
	// Check the documentation for the list of available features.
	//
	// +optional
	// +kubebuilder:pruning:PreserveUnknownFields
	Experimental *Experimental `json:"experimental,omitempty"`
}

// Summary the most recently observed status of the ApplicationConfig.
type ApplicationConfigStatus struct {
	// Kubernetes objects that are associated to the application configuration
	// and shared with other GitLab or third-party components:
	//
	//   - `RailsSecrets`: A Kubernetes Secret that contains _all_ of the
	//     generated secret values of GitLab Rails application. Most of these
	//     secret are used application configuration files.
	//
	// +optional
	// +patchMergeKey=usage
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	SharedObjects []SharedObjectReference `json:"sharedObjects,omitempty" patchStrategy:"merge" patchMergeKey:"usage"`

	// Represents the latest available observations of the current state.
	//
	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

// ContentSecurityPolicy is the configuration for Content Security Policy
// (CSP) headers.
//
// See https://guides.rubyonrails.org/security.html#content-security-policy
//
// +structType=atomic
// +kubebuilder:validation:XValidation:rule="!has(self.directives) || self.directives.all(key, key.matches('^[a-z]+(-[a-z]+)*$'))",fieldPath=".directives"
type ContentSecurityPolicy struct {
	// ReportOnly set the CSP header to only report the violations.
	//
	// +optional
	ReportOnly bool `json:"reportOnly,omitempty"`

	// Directives defines the CSP directives.
	//
	// See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy#directives
	//
	// +optional
	// +mapType=atomic
	Directives map[string]string `json:"directives"`
}

// Configuration for outgoing emails from GitLab application.
type ApplicationOutgoingEmail struct {
	// Address used in the "From" field of outgoing emails.
	//
	// +optional
	// +kubebuilder:validation:Format=email
	From string `json:"from,omitempty"`

	// The display name used in the "From" field of outgoing emails.
	//
	// +optional
	DisplayName string `json:"displayName,omitempty"`

	// Address used in the "Reply-To" field of outgoing emails.
	//
	// +optional
	// +kubebuilder:validation:Format=email
	ReplyTo string `json:"replyTo,omitempty"`

	// The prefix that is prepended to the subject of outgoing emails.
	//
	// +optional
	SubjectPrefix string `json:"subjectPrefix,omitempty"`

	// Set S/MIME signing certificate and key for outgoing emails. Note that
	// S/MIME public certificate (and CA certificate, when specified) will be
	// attached to the signed outgoing messages.
	//
	// +optional
	SMIME *CertificateBundle `json:"smime,omitempty"`

	// Configure the outgoing email sending method. Depending the selected
	// method the configuration will be different.
	//
	// +required
	Outbox ApplicationOutboxProvider `json:"outbox"`
}

// Configuration of the sending method for outgoing emails.
type ApplicationOutboxProvider struct {
	// SMTP email server settings.
	//
	// +optional
	SMTP *SMTPServer `json:"smtp,omitempty"`

	// Delivery of emails using Microsoft Graph API with OAuth 2.0 credentials
	// flow.
	//
	// +optional
	MicrosoftGraph *MicrosoftGraphMailer `json:"microsoftGraph,omitempty"`
}

// Configuration for incoming emails to GitLab application.
type ApplicationIncomingEmail struct {
	// The email address including the `%{key}` placeholder that will be
	// replaced to reference the item being replied to.
	//
	// The placeholder can be omitted but if present, it must appear in the
	// "user" segment of the email address (before `@`).
	//
	// Note that a placeholder is _required_ for the Service Desk feature to
	// work.
	//
	// +required
	// +kubebuilder:validation:Format=email
	Address string `json:"address"`

	// The name of the mailbox where incoming email will end up, for example
	// "Inbox".
	//
	// +optional
	Mailbox string `json:"mailbox,omitempty"`

	// Delete incoming emails after they are delivered.
	//
	// If you are using Microsoft Graph instead of IMAP, set this to `false` to
	// retain messages in the mailbox since deleted messages are auto-expunged
	// after some time.
	//
	// +optional
	DeleteAfterDelivery *bool `json:"deleteAfterDelivery,omitempty"`

	// Whether to expunge (permanently remove) messages from the mailbox when
	// they are marked as deleted after delivery.
	//
	// Only applies to IMAP servers. Microsoft Graph will auto-expunge deleted
	// messages.
	//
	// +optional
	ExpungeDeleted bool `json:"expungeDeleted,omitempty"`

	// Configure the incoming email receiving method. Depending the selected
	// method the configuration will be different.
	//
	// +required
	Inbox ApplicationInboxProvider `json:"inbox"`

	// Defines how email content is delivered to GitLab Rails application:
	//
	//   - Sidekiq: Email content is pushed directly to Sidekiq. The job is then
	//     picked up by Sidekiq.
	//
	//   - Postback: A webhook is triggered to send a HTTP POST request to Rails
	//     application. The content is embedded into the request body.
	//
	// +optional
	DeliveryMethod *IncomingEmailDeliveryMethod `json:"deliveryMethod,omitempty"`

	// The arbitration method for incoming emails. Only Redis is supported.
	//
	// +optional
	ArbitrationMethod *IncomingEmailArbitrationMethod `json:"arbitrationMethod,omitempty"`
}

// Configuration of the receiving method for incoming emails.
type ApplicationInboxProvider struct {
	// IMAP email server settings.
	//
	// +optional
	IMAP *IMAPServer `json:"imap,omitempty"`

	// Delivery of emails using Microsoft Graph API with OAuth 2.0 credentials
	// flow.
	//
	// +optional
	MicrosoftGraph *MicrosoftGraphMailer `json:"microsoftGraph,omitempty"`

	// The polling interval for incoming emails. Only MicrosoftGraph provider
	// uses this option to pull incoming emails from server.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	PollInterval *metav1.Duration `json:"pollInterval,omitempty"`
}

// Configuration for storing various application data on object storage.
//
// For configuring object storage:
//
//   - Define "stores". Each store is the settings of an object store provider
//     and is identified with a unique name. The name is used to associate the
//     store to application data.
//
//   - Define "objects" and associate them to stores. An application "object"
//     type is defined by its "usage", for example for "artifacts" or "uploads".
//
// See: https://docs.gitlab.com/administration/object_storage/
type ApplicationObjectStorage struct {
	// A list of object store providers. Each provider must have a unique name
	// to be referenced with.
	//
	// Depending on the selected provider (for example S3 or GCS), the settings
	// are different.
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +kubebuilder:validation:MinItems=1
	Stores []ObjectStore `json:"stores" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`

	// A list of application objects. Each object must be associated with a
	// "store", using the store name. The type of data for the object is defined
	// by its "usage".
	//
	// +required
	// +patchMergeKey=usage
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=usage
	Objects []ApplicationObject `json:"objects" patchStrategy:"merge,retainKeys" patchMergeKey:"usage"`
}

// Details of how and where to store application data in an object store.
//
// Specify the type of application data with "usage" field. To associate the
// application object to a store, use the "storeName" field.
type ApplicationObject struct {
	ObjectStoreDestination `json:",inline"`

	// The type of data in application object. The following values are available:
	//
	//  - Artifacts: Build artifacts.
	//  - ExternalDiffs: Merge request external `diff`.
	//  - LFS: Git LFS objects.
	//  - Uploads: User uploads, such as attachments and avatars.
	//  - Packages: Package repository assets, such as Maven and NPM.
	//  - DependencyProxy: Cached container images for dependency proxy.
	//  - TerraformState: Terraform state.
	//  - CISecureFiles: Secure files for CI.
	//
	// +required
	Usage ObjectStoreUsage `json:"usage"`
}

// The type of data in application object.
//
// +kubebuilder:validation:Enum=Artifacts;ExternalDiffs;LFS;Uploads;Packages;DependencyProxy;TerraformState;CISecureFiles
type ObjectStoreUsage string

const (
	ObjectStoreUsageArtifacts       ObjectStoreUsage = "Artifacts"
	ObjectStoreUsageExternalDiffs   ObjectStoreUsage = "ExternalDiffs"
	ObjectStoreUsageLFS             ObjectStoreUsage = "LFS"
	ObjectStoreUsageUploads         ObjectStoreUsage = "Uploads"
	ObjectStoreUsagePackages        ObjectStoreUsage = "Packages"
	ObjectStoreUsageDependencyProxy ObjectStoreUsage = "DependencyProxy"
	ObjectStoreUsageTerraformState  ObjectStoreUsage = "TerraformState"
	ObjectStoreUsageCISecureFiles   ObjectStoreUsage = "CISecureFiles"
)

// Settings for LDAP login providers.
//
// See: https://docs.gitlab.com/administration/auth/ldap/
type ApplicationLDAPSettings struct {
	// Prevent sign-in with LDAP through the web UI. This does not disable using
	// LDAP credentials for Git access.
	//
	// +optional
	PreventSignIn bool `json:"preventSignIn,omitempty"`

	// List of LDAP servers. Each server must have a unique name.
	//
	// GitLab EE supports connecting to multiple LDAP
	// servers.
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	// +kubebuilder:validation:MinItems=1
	Servers []LDAPServer `json:"servers" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`
}

// +kubebuilder:validation:Enum=Plain;StartTLS;SimpleTLS
type LDAPEncryption string

const (
	LDAPEncryptionPlain     LDAPEncryption = "Plain"
	LDAPEncryptionStartTLS  LDAPEncryption = "StartTLS"
	LDAPEncryptionSimpleTLS LDAPEncryption = "SimpleTLS"
)

// +kubebuilder:validation:XValidation:rule="has(self.authentication.basic) || has(self.authentication.tls)",message="either basic or tls authentication method is required"
type LDAPServer struct {
	// The internal name of the LDAP server. It must be unique among other LDAP
	// servers.
	//
	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`

	// A human-friendly name for the LDAP server.
	//
	// +optional
	Label string `json:"label,omitempty"`

	// The URL of the LDAP server. This URL must have `ldap or `ldaps` scheme.
	//
	// When the port is not specified, the default port for the scheme is used.
	//
	// +required
	// +kubebuilder:validation:Format=uri
	URL string `json:"url"`

	// The authentication method for the LDAP server.
	//
	// +optional
	Authentication *ServiceAuthentication `json:"authentication,omitempty"`

	// Encryption method for the LDAP server.
	//
	// The following values are available:
	//
	//   - Plain: No encryption.
	//   - StartTLS: Use StartTLS to encrypt the LDAP connection.
	//   - SimpleTLS: Use SimpleTLS to encrypt the LDAP connection.
	//
	// +optional
	Encryption *LDAPEncryption `json:"encryption,omitempty"`

	// Cline-side TLS settings of the LDAP server, including CA certificate and
	// TLS options.
	//
	// +optional
	TLS *TLSClientSupport `json:"tls,omitempty"`

	// The timeout for LDAP queries. This helps to avoid blocking a request if
	// the LDAP server becomes unresponsive.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	Timeout *metav1.Duration `json:"timeout,omitempty"`

	// The user's LDAP login. This should be the attribute, not the value that
	// maps to `uid`.
	//
	// +required
	UID string `json:"uid"`

	// The full DN of the user that is binding to the LDAP server.
	//
	// +required
	BindDN string `json:"bindDN"`

	// The password of the user that is binding to the LDAP server.
	//
	// +optional
	BindPassword *corev1.SecretKeySelector `json:"bindPassword,omitempty"`

	// LDAP server settings. The following options are supported:
	//
	//   - `smartcardAuth`: Set to `true` to enable smart card authentication
	//     against the LDAP server.
	//
	//   - `activeDirectory`: Set to `true`, to indicate the LDAP server is an
	//     ActiveDirectory (AD). For non-AD servers it skips the AD specific
	//     queries.
	//
	//   - `smartcardADCertField`: If both `smartcardAuth` and `activeDirectory`
	// 	   are is `true`, use this setting to authenticate users based on their
	//     smart card certificates in AD.
	//
	//   - `smartcardADCertFormat`: If both `smartcardAuth` and `activeDirectory`
	// 	   are is `true`, search the `smartcardADCertField` for certificate data
	//     stored in this format.
	//
	//   - `allowUsernameOrEmailLogin`: If `true`, GitLab will ignore everything
	//     after the first '@' in the username submitted by the user on login.
	//     If you are using `userPrincipalName` as the `uid` for AD you need to
	//     disable this setting, because the `userPrincipalName` contains '@'.
	//
	//   - `blockAutoCreatedUsers`: To maintain tight control over the number of
	//     active users on your instance, set this to `true` to keep new users
	//     blocked until they have been cleared by the administrator.
	//
	//   - `base`: The base DN to search for users.
	//
	//   - `userFilter`: The filter for user search. Uses RFC 4515 format. Note
	//     that GitLab does not support "omniauth-ldap" custom filter syntax.
	//
	//   - `groupBase`: The base to search for groups.
	//
	//   - `adminGroup`: LDAP group of users that GitLab administrators.
	//
	//   - `externalGroups`: LDAP group of users that are marked as external
	//     users in GitLab.
	//
	//   - `syncSSHKeys`: Name of attribute which holds a SSH public key of the
	// 	   user. When not specified, SSH key synchronization is disabled.
	//
	//   - `lowerCaseUsernames`: If `true`, GitLab will lower case the LDAP
	//     usernames.
	//
	// Note that the options are string-valued. You must use a string value. For
	// example use `"true"` instead of `true` or `"1"` instead of `1`.
	//
	// +optional
	Settings map[string]string `json:"settings,omitempty"`

	// Mapping for LDAP attributes. GitLab application uses the mappings to
	// create an account for LDAP users.
	//
	// The specified attributes can either be a single attribute name, for
	// example `mail`, or a comma-separated list of attribute names to try in
	// order, for example `mail,email`,
	//
	// Note that the user's LDAP login will always be the attribute specified in
	// `uid` above.
	//
	// The following attributes are supported:
	//
	//   - `username`: The username will be used in paths for the user's own
	//     projects and when mentioning them with (like `@username`). If the
	//     attribute specified for `username` contains an email address, the
	//     GitLab username will be the part of the email address before the
	//     '@'. Example: `uid,userid,sAMAccountName`.
	//
	//   - `email`: The primary email address of the user.
	//     Example: `mail,email,userPrincipalName`.
	//
	//   - `name`: If no full name could be found at the attribute specified for
	//     `name`, the full name is determined using the attributes specified
	//     for first name and last name. Example: `cn`.
	//
	//   - `firstName`: First name of the user. Example: `givenName`.
	//
	//   - `lastName`: Last name of the user. Example: `sn`.
	//
	// +optional
	Attributes map[string]string `json:"attributes,omitempty"`
}

// Settings for smart card authentication.
//
// See: https://docs.gitlab.com/administration/auth/smartcard/
type ApplicationSmartCardSettings struct {
	// CA certificate bundle of smart cards.
	//
	// +required
	CACertificate corev1.SecretKeySelector `json:"caCertificate,omitempty"`

	// Host and port where the client-side certificate is requested.
	//
	// +kubebuilder:validation:Pattern=`^([\w.]*:?\d{0,5})$`
	// +kubebuilder:example="smartcard.example.com:8080"
	// +required
	Host string `json:"host,omitempty"`

	// Browser session with smart card sign-in is required for Git access.
	//
	// +optional
	RequiredForGitAccess bool `json:"requiredForGitAccess,omitempty"`

	// Use X.509 SAN extensions certificates to identify GitLab users.
	// Add a `subjectAltName` to your certificates like `email:user`.
	//
	// +optional
	SANExtensions bool `json:"sanExtensions,omitempty"`
}

// Settings for Kerberos authentication.
//
// See: https://docs.gitlab.com/integration/kerberos/
type ApplicationKerberosSettings struct {
	GSSAPISupport `json:",inline"`

	// Offer Kerberos ticket-based authentication on a different port while the
	// standard port offers only basic authentication.
	//
	// +optional
	DedicatedPort *KerberosDedicatedPort `json:"dedicatedPort,omitempty"`

	// Kerberos realms (or domains) that are allowed to automatically link LDAP
	// identities.
	//
	// By default, GitLab accepts a realm that matches the domain derived from
	// the LDAP `base` DN. For example, `ou=users,dc=example,dc=com` would allow
	// users with a realm matching `example.com`.
	//
	// +optional
	// +kubebuilder:validation:items:Format=hostname
	SimpleLDAPLinkingAllowedRealms []string `json:"simpleLdapLinkingAllowedRealms,omitempty"`
}

// Git before 2.4 does not fall back to Basic authentication if `negotiate`
// fails. To support both `basic` and `negotiate` methods with older versions of
// Git, configure the your frontend proxy on an extra port, for example `8443`,
// and use the following configuration to dedicate it to Kerberos authentication.
type KerberosDedicatedPort struct {
	// If the Kerberos dedicated port uses HTTPS.
	//
	// +required
	HTTPS bool `json:"https"`

	// The Kerberos dedicated port number.
	//
	// +optional
	Port int32 `json:"port,omitempty"`
}

// Settings for OmniAuth authentication providers.
//
// For the list of available providers refer to GitLab documentation.
//
// See: https://docs.gitlab.com/integration/omniauth/
type ApplicationOmniAuthSettings struct {
	// OmniAuth settings. The following options are supported:
	//
	//   - `autoSignInWithProvider`: Set it `true` to automatically sign in with
	//     a specific provider without showing GitLab's sign-in page.
	//
	//   - `syncProfileFromProvider`: A comma-separated list of providers to
	//     sync the user profile from, every time the user logs in. When
	// 	   authenticating using LDAP, the user's email is always synced.
	//     You can set this to `true` to specify all providers.
	//
	//   - `syncProfileAttributes`: A comma-separated list of attributes to sync
	//     from the providers above. Available options are "name", "email" and
	// 	   "location". This consequently will make the selected attributes
	//     read-only. You can set this to `true` to specify all attributes.
	//
	//   - `allowSingleSignOn`: A comma-separated list of providers to allow
	//     users to login without having a user account first. User accounts
	//     will be created automatically when authentication was successful. You
	//     can set this to `true` to specify all providers.
	//
	//   - `blockAutoCreatedUsers`: Locks down auto-created users until they
	//     have been cleared by an administrator.
	//
	//   - `autoLinkLDAPUser`: Look up new users in LDAP servers. If a match is
	//     found (with the same `uid`), automatically link the OmniAuth identity
	//     with the LDAP account.
	//
	//   - `autoLinkSAMLUser`: Allow users with existing accounts to login and
	//     auto link their account via SAML login, without having to do a manual
	//     login first and manually add SAML.
	//
	//   - `samlMessageMaxByteSize`: Allows larger SAML messages to be received.
	//     It is a numeric value in bytes. Too high limits exposes instance to
	//     decompression DDoS attack type.
	//
	//   - `autoLinkUser`: Allow users with the existing accounts to sign in and
	//     automatically link their accounts via OmniAuth login, without having
	//     to do a manual login first and manually add them. It links on email.
	//	   Specify the allowed providers with in comma-separated list to enable
	//     this feature. You can set this to `true` to specify all providers.
	//
	//   - `externalProviders`: A comma-separated list of providers that are
	//     designated as external so that all users creating accounts via these
	//     providers will not be able to have access to internal projects.
	//
	//   - `allowBypassTwoFactor`: A comma-separated list of providers that are
	//     allowed to bypass GitLab's two-factor authentication. You can set
	//     this to `true` to specify all providers. This option should only be
	//     configured for providers which already have two facto authentication.
	//     This settings dose not apply to SAML.
	//
	// Note that the options are string-valued. You must use a string value. For
	// example use `"true"` instead of `true` or `"1"` instead of `1`.
	//
	// +optional
	Settings map[string]string `json:"settings,omitempty"`

	// List of OmniAuth providers. Each provider must have a unique name. The
	// name is used to identify providers in OmniAuth settings.
	//
	// +required
	// +patchMergeKey=name
	// +patchStrategy=merge,retainKeys
	// +listType=map
	// +listMapKey=name
	Providers []ApplicationOmniAuthProvider `json:"providers" patchStrategy:"merge,retainKeys" patchMergeKey:"name"`
}

// Configuration for an OmniAuth provider.
type ApplicationOmniAuthProvider struct {
	// Name of the provider. It must be unique among all OmniAuth providers. The
	// name is used to identify providers in OmniAuth settings.
	//
	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`

	// A human-friendly name for the OmniAuth provider. This will appear on
	// GitLab sign-in page.
	//
	// +optional
	Label string `json:"label,omitempty"`

	// The reference to a Kubernetes secret that contains the provider's
	// configuration. The configuration must be formatted in YAML.
	// For the details of the configuration format, see the provider's
	// documentation.
	//
	// See: https://docs.gitlab.com/integration/omniauth/#supported-providers
	//
	// +required
	Options corev1.SecretKeySelector `json:"options"`
}

// Settings for integrating with FortiAuthenticator one-time password
// authenticator.
//
// See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/
type ApplicationFortiAuthenticatorSettings struct {
	// Host and port of FortiAuthenticator instance.
	//
	// +required
	// +kubebuilder:validation:Pattern=`^([\w.]*:?\d{0,5})$`
	Host string `json:"host"`

	// Username and password of FortiAuthenticator instance.
	//
	// A Kubernetes Secret with basic authentication type (`kubernetes.io/basic-auth`)
	// is expected.
	//
	// +required
	Credentials corev1.SecretReference `json:"credentials,omitempty"`
}

// Settings for integrating with Cisco Duo one-time password authenticator.
//
// See: https://docs.gitlab.com/user/profile/account/two_factor_authentication/
type ApplicationCiscoDuoSettings struct {
	// Host and port of Cisco Duo instance.
	//
	// +required
	// +kubebuilder:validation:Pattern=`^([\w.]*:?\d{0,5})$`
	Host string `json:"host"`

	// Integration key of Cisco Duo instance.
	//
	// +required
	IntegrationKey corev1.SecretKeySelector `json:"integrationKey"`

	// Secret key of Cisco Duo instance.
	//
	// +required
	SecretKey corev1.SecretKeySelector `json:"secretKey"`
}

// Configure Rack attack IP banning for Git basic authentication.
type ApplicationRackAttackGitBasicAuth struct {
	// Whitelist requests from the specified IPs with incorrect headers. Useful
	// for whitelisting frontend proxies.
	//
	// +optinal
	// +listType=set
	// +kubebuilder:validation:items:Format=cidr
	IPWhitelist []string `json:"ipWhitelist,omitempty"`

	// Limit the number of Git HTTP authentication attempts per IP.
	//
	// +optional
	MaxRetry *int32 `json:"maxRetry,omitempty"`

	// Reset the authentication attempt counter per IP after the specified
	// duration.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	FindTime *metav1.Duration `json:"findTime,omitempty"`

	// Ban an IP for the specified duration if they exceed the retry limit.
	//
	// +optional
	// +kubebuilder:validation:Format=duration
	BanTime *metav1.Duration `json:"banTime,omitempty"`
}

// Specification of a Gitaly server provider. An provider must have a unique
// name, have an endpoint and authentication details. The endpoint can can be a
// Gitaly storage or a "virtual" storage. Note that Gitaly servers only support
// Token authentication.
//
// See: https://docs.gitlab.com/administration/gitaly/
//
// +kubebuilder:validation:XValidation:rule="has(self.authentication.token)",message="token authentication method is required"
type ApplicationRepository struct {
	ServiceProvider `json:",inline"`

	// Name of the Gitaly server. It must be unique among all Gitaly servers.
	//
	// +required
	// +kubebuilder:validation:Pattern="^[a-zA-Z_#][a-zA-Z0-9_#-]*$"
	Name string `json:"name"`
}

// Settings of the GitLab container registry integration.
type ApplicationContainerRegistry struct {
	// A reference to a container registry service within the Kubernetes cluster.
	// This _must_ be a GitLab "ContainerRegistry" custom resource.
	//
	// +optional
	Service *corev1.ObjectReference `json:"service,omitempty"`

	// Details of an external container registry service, either within or
	// outside the Kubernetes cluster.
	//
	// +optional
	External *ApplicationExternalContainerRegistry `json:"external,omitempty"`
}

// Specification of an external container registry provider. An external
// provider can be within or outside the Kubernetes cluster.
type ApplicationExternalContainerRegistry struct {
	// The public-facing external URL of the container registry.
	//
	// +required
	// +kubebuilder:validation:Format=uri
	URL string `json:"url"`

	// The URL of the internal container registry API that GitLab backend uses
	// for direct access.
	//
	// +optional
	// +kubebuilder:validation:Format=uri
	InternalURL *string `json:"internalUrl,omitempty"`
}

// Settings of the GitLab Kubernetes Agent Server (KAS) integration.
type ApplicationKubernetesAgentServer struct {
	// A reference to a KAS service within the Kubernetes cluster. This _must_
	// be a GitLab "KubernetesAgentServer" custom resource.
	//
	// +optional
	Service *corev1.ObjectReference `json:"service,omitempty"`

	// Details of an external KAS service, either within or outside the
	// Kubernetes cluster.
	//
	// +optional
	External *ApplicationExternalKAS `json:"external,omitempty"`
}

// Specification of an external Kubernetes Agent Server (KAS) provider. An
// external provider can be within or outside the Kubernetes cluster.
type ApplicationExternalKAS struct {
	// The public-facing URL to the external KAS API that Kubernetes agents use.
	//
	// +required
	// +kubebuilder:validation:Format=uri
	URL string `json:"url"`

	// The URL to the internal KAS API that GitLab backend uses.
	//
	// +required
	// +kubebuilder:validation:Format=uri
	InternalURL string `json:"internalUrl"`

	// The URL for the Kubernetes API proxy that GitLab users use.
	//
	// +optional
	// +kubebuilder:validation:Format=uri
	ProxyURL *string `json:"proxyUrl,omitempty"`
}

func init() {
	SchemeBuilder.Register(&ApplicationConfig{}, &ApplicationConfigList{})
}
