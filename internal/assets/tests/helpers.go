package tests

import (
	"fmt"
	"os"

	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

func ReadObject(scenario, source string, decoder runtime.Decoder) client.Object {
	f, err := os.Open(fmt.Sprintf("testdata/%s/%s", scenario, source))
	Expect(err).NotTo(HaveOccurred())

	objList, err := kube.ReadObjects(f, decoder)
	Expect(err).NotTo(HaveOccurred())

	if len(objList) == 0 {
		return nil
	} else {
		return objList[0]
	}
}
