package applicationconfig

import (
	"context"
	"errors"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

var _ = Describe("ApplicationConfigController", Ordered, func() {
	var (
		fakeScheme      *runtime.Scheme
		fakeClient      client.WithWatch
		mockRailsSecret *corev1.Secret
		appConfig       *v2alpha2.ApplicationConfig
		rtCtx           *framework.RuntimeContext
	)

	BeforeAll(func() {
		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
		utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))
	})

	BeforeEach(func() {
		appConfig = &v2alpha2.ApplicationConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test",
				Namespace: "default",
				Annotations: map[string]string{
					"gitlab.io/test": "test",
				},
				Labels: map[string]string{
					"gitlab.io/instance": "test",
				},
			},
		}

		framework.Scheme = fakeScheme

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	Context("when Rails secret does not exist", func() {
		BeforeEach(func() {
			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(appConfig).
				WithStatusSubresource(appConfig).
				Build()

			framework.Client = fakeClient
		})

		It("should create Rails secrets and update status", func() {
			err := NewController(appConfig).Execute(rtCtx)
			Expect(err).NotTo(HaveOccurred())

			// Verify secret
			railsSecret := &corev1.Secret{}

			err = fakeClient.Get(rtCtx, client.ObjectKey{
				Name:      appConfig.Name + "-rails-secrets",
				Namespace: appConfig.Namespace}, railsSecret)
			Expect(err).NotTo(HaveOccurred())

			Expect(railsSecret.Type).To(Equal(corev1.SecretTypeOpaque))
			Expect(railsSecret.Annotations).To(Equal(appConfig.Annotations))
			Expect(railsSecret.Labels).To(Equal(appConfig.Labels))

			Expect(railsSecret.Data).To(HaveLen(1))
			Expect(railsSecret.Data).To(HaveKey(railsSecretYamlName))

			// Verify status conditions
			Expect(appConfig.Status.Conditions).To(HaveLen(1))
			Expect(appConfig.Status.Conditions[0].Type).To(Equal(v2alpha2.ApplicationConfigReadyCondition))
			Expect(appConfig.Status.Conditions[0].Status).To(Equal(metav1.ConditionTrue))
			Expect(appConfig.Status.Conditions[0].Reason).To(Equal(v2alpha2.RailsSecretsAvailableReason))

			// Verify status shared objects
			Expect(appConfig.Status.SharedObjects).To(HaveLen(1))
			Expect(appConfig.Status.SharedObjects[0].Name).To(Equal(railsSecret.Name))
			Expect(appConfig.Status.SharedObjects[0].Usage).To(Equal(v2alpha2.RailsSecretsObjectUsage))
		})
	})

	Context("when Rails secret exists", func() {
		BeforeEach(func() {
			mockRailsSecret = &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      appConfig.Name + "-rails-secrets",
					Namespace: "default",
				},
				Type: corev1.SecretTypeOpaque,
			}

			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(appConfig, mockRailsSecret).
				WithStatusSubresource(appConfig).
				Build()

			framework.Client = fakeClient
		})

		It("should not create Rails secrets but locate and reference it", func() {
			err := NewController(appConfig).Execute(rtCtx)
			Expect(err).NotTo(HaveOccurred())

			// Ensure the Secret is not touched
			railsSecret := &corev1.Secret{}

			err = fakeClient.Get(rtCtx, client.ObjectKey{
				Name:      appConfig.Name + "-rails-secrets",
				Namespace: appConfig.Namespace}, railsSecret)
			Expect(err).NotTo(HaveOccurred())

			Expect(railsSecret.Type).To(Equal(corev1.SecretTypeOpaque))
			Expect(railsSecret.Annotations).To(BeNil())
			Expect(railsSecret.Labels).To(BeNil())

			// Verify status
			Expect(appConfig.Status.Conditions).To(HaveLen(1))
			Expect(appConfig.Status.Conditions[0].Type).To(Equal(v2alpha2.ApplicationConfigReadyCondition))
			Expect(appConfig.Status.Conditions[0].Status).To(Equal(metav1.ConditionTrue))

			// Verify status shared objects
			Expect(appConfig.Status.SharedObjects).To(HaveLen(1))
			Expect(appConfig.Status.SharedObjects[0].Name).To(Equal(railsSecret.Name))
			Expect(appConfig.Status.SharedObjects[0].Usage).To(Equal(v2alpha2.RailsSecretsObjectUsage))
		})
	})

	Context("when Rails secret creation fails", func() {
		var (
			appConfigController *Controller
		)

		BeforeEach(func() {
			appConfigController = NewController(appConfig)

			// Make generateRailsSecrets fail
			appConfigController.workflow[1] = framework.TaskFunc(func(_ *framework.RuntimeContext) error {
				return errors.New("create rails secrets error")
			})

			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(appConfig).
				WithStatusSubresource(appConfig).
				Build()

			framework.Client = fakeClient
		})

		It("should fail the workflow and update the status", func() {
			err := appConfigController.Execute(rtCtx)
			Expect(err).To(MatchError("create rails secrets error"))

			// Verify status conditions
			Expect(appConfig.Status.Conditions).To(HaveLen(1))
			Expect(appConfig.Status.Conditions[0].Type).To(Equal(v2alpha2.ApplicationConfigReadyCondition))
			Expect(appConfig.Status.Conditions[0].Status).To(Equal(metav1.ConditionFalse))
			Expect(appConfig.Status.Conditions[0].Reason).To(Equal(v2alpha2.PendingRailsSecretsReason))

			// Verify status shared objects
			Expect(appConfig.Status.SharedObjects).To(HaveLen(0))
		})
	})
})
