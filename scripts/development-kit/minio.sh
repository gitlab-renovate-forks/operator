#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    echo_stage "Installing MiniO"

    kubectl -n $(namespace) create secret generic "minio-accesskey" --from-literal="ACCESSKEY_ID"=$(gen_random 16) --from-literal="ACCESSKEY_SECRET"=$(gen_random 32)

    helm install minio minio \
        --repo https://charts.bitnami.com/bitnami \
        -n $(namespace) \
        --create-namespace \
        --version 14.6.32 -f "$SCRIPT_DIR/manifests/values.minio.yaml"
}

uninstall() {
    echo_stage "Uninstalling MiniO"

    helm uninstall minio -n $(namespace) || true

    kubectl -n $(namespace) delete secret minio-accesskey --ignore-not-found=true
    kubectl -n $(namespace) delete job minio-provisioning --ignore-not-found=true
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
