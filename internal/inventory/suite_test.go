package inventory

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestGitLabOperatorInventory(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: Inventory")
}
