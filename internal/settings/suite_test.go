package settings

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestSettings(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: Settings")
}

var _ = AfterEach(func() {
	configFile = ""
})
