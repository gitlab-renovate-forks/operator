package jobprocessor

import (
	"context"
	"fmt"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// +kubebuilder:rbac:groups=gitlab.com,resources=jobprocessors,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=jobprocessors/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=jobprocessorss/finalizers,verbs=update
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete

type Reconciler struct{}

// Reconcile runs in the reconciliation loop for the JobProcessor resource.
func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.JobProcessor) (ctrl.Result, error) {
	logger := ctrl.Log.
		WithName("controllers.jobprocessor").
		WithValues(
			"resource", fmt.Sprintf("%s/%s", obj.Namespace, obj.Name),
			"generation", obj.Generation,
			"resourceVersion", obj.ResourceVersion,
		)

	rtCtx, err := framework.NewRuntimeContext(ctx, framework.WithLogrLogger(logger))
	if err != nil {
		return framework.Expand(err)
	}

	err = NewController(obj.DeepCopy()).Execute(rtCtx)

	return framework.Expand(err)
}

func listJobProccesorsForApplicationConfig(ctx context.Context, appConfig client.Object) []reconcile.Request {
	// NOTE:
	// This mapping is prone to the issue that is described in:
	//
	//   - https://github.com/kubernetes-sigs/controller-runtime/issues/1996.
	//
	// Since the handler.MapFunc does not return an error, any possible error
	// is ignored and does not lead to request requeue. As a result, the
	// controller fails to detect the changes in ApplicationConfig resources and
	// will not reconcile its changes.
	//
	// This is a known issue with controller-runtime and other controllers are
	// prone to it, for example:
	//
	//   - https://github.com/Kong/kubernetes-ingress-controller/blob/main/internal/controllers/gateway/gateway_controller.go#L280
	//
	// This needs to be revisited again once there is a viable solution for it
	// is available.
	result := []reconcile.Request{}

	jobProcs := &v2alpha2.JobProcessorList{}
	listOpts := &client.ListOptions{Namespace: appConfig.GetNamespace()}

	if err := framework.Client.List(ctx, jobProcs, listOpts); err != nil {
		return result
	}

	for _, jobProc := range jobProcs.Items {
		if jobProc.Spec.ApplicationConfigRef.Name == appConfig.GetName() {
			result = append(result, reconcile.Request{
				NamespacedName: types.NamespacedName{
					Namespace: jobProc.GetNamespace(),
					Name:      jobProc.GetName(),
				}})
		}
	}

	return result
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	b := ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.JobProcessor{}).
		Watches(
			&v2alpha2.ApplicationConfig{},
			handler.EnqueueRequestsFromMapFunc(listJobProccesorsForApplicationConfig)).
		Owns(&corev1.Secret{}).
		Owns(&appsv1.Deployment{}).
		WithEventFilter(predicate.Or(predicate.GenerationChangedPredicate{}, predicate.LabelChangedPredicate{}, predicate.AnnotationChangedPredicate{}))

	return b.Complete(reconcile.AsReconciler(mgr.GetClient(), r))
}
