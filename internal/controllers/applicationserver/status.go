package applicationserver

import (
	"fmt"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func (c *Controller) isPaused() bool {
	cond := meta.FindStatusCondition(c.appServer.Status.Conditions, v2alpha2.ApplicationServerPausedCondition)

	return cond != nil && cond.Status == metav1.ConditionTrue
}

func (c *Controller) updateStatus(rtCtx *framework.RuntimeContext) error {
	return controllerutils.PatchStatus(rtCtx, c.appServer, func() bool {
		if c.isPaused() || c.appServer.Spec.Paused {
			return false
		}

		changed := c.appServer.Status.Version != c.appServer.Spec.Version ||
			c.appServer.Status.Edition != c.appServer.Spec.Edition

		c.appServer.Status.Version = c.appServer.Spec.Version
		c.appServer.Status.Edition = c.appServer.Spec.Edition

		return changed
	})
}

func (c *Controller) checkPausedCondition(rtCtx *framework.RuntimeContext) error {
	paused := c.isPaused()

	if c.appServer.Spec.Paused && !paused {
		return c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.ApplicationServerPausedCondition,
			Status:  metav1.ConditionTrue,
			Reason:  v2alpha2.WorkloadPausedReason,
			Message: "Application workload is paused",
		})
	} else if !c.appServer.Spec.Paused && paused {
		return c.setStatusCondition(rtCtx, metav1.Condition{
			Type:    v2alpha2.ApplicationServerPausedCondition,
			Status:  metav1.ConditionFalse,
			Reason:  v2alpha2.WorkloadResumedReason,
			Message: "Application workload is resumed",
		})
	}

	return nil
}

func (c *Controller) checkReadyCondition(rtCtx *framework.RuntimeContext) error {
	rtCtx.Logger.Debug("Checking application server readiness")

	deployment, err := c.getDeployment(rtCtx)
	if err != nil {
		return fmt.Errorf("failed to get Deployment: %w", err)
	}

	if err := controllerutils.WaitForDeploymentReady(rtCtx, deployment, Settings.Rails.Puma.StatusCheckDelay); err != nil {
		return err
	}

	err = c.setStatusCondition(rtCtx, metav1.Condition{
		Type:    v2alpha2.ApplicationServerReadyCondition,
		Status:  metav1.ConditionTrue,
		Reason:  v2alpha2.WorkloadAvailableReason,
		Message: "Application workload is available",
	})
	if err != nil {
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	return nil
}
