## 0.2.0 (2025-02-19)

### changed (1 change)

- [Update to Go 1.24](https://gitlab.com/gitlab-org/cloud-native/operator/-/commit/ab4cef8c075fb7d67842d4c868671650b5c89cd9) ([merge request](https://gitlab.com/gitlab-org/cloud-native/operator/-/merge_requests/209))

## 0.1.1 (2025-02-06)

No changes.
