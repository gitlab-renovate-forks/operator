package template

import (
	"io/fs"
	"testing"
	"testing/fstest"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var testData fs.FS
var testEngine *Engine

func TestGitLabOperatorSupportTemplates(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator Support: Templates")
}

var _ = BeforeSuite(func() {
	testData = loadTestData()
	testEngine = New(testData)

	Expect(testEngine).NotTo(BeNil())
	Expect(testEngine.Load()).NotTo(HaveOccurred())
})

func loadTestData() fs.FS {
	return &fstest.MapFS{
		"hello.txt": &fstest.MapFile{
			Data: []byte("Hello, {{ .Name }}!"),
		},
		"_partial.txt": &fstest.MapFile{
			Data: []byte("This is a partial template"),
		},
		"configMap.yaml": &fstest.MapFile{
			Data: []byte(`
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{.Name}}
data:
{{- range $key, $value := .Data }}
  {{ $key }}: {{ $value }}
{{- end }}
`),
		},
	}
}
