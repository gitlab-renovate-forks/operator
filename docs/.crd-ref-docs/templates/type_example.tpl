{{- define "type_example" -}}
{{- with (index .Markers "kubebuilder:example" ) -}}
  {{- $exampleValues := list }}
  {{- range $ex := . }}
    {{- $exampleValues = append $exampleValues $ex.Value }}
  {{- end }}
  {{- $exampleValues | join "<br />" }}
{{- end -}}
{{- end -}}
