{{- $edition := .ApplicationServer.Spec.Edition | default .Settings.Rails.Edition -}}
{{- $appName := printf "%s-%s" .ApplicationServer.Namespace .ApplicationServer.Name -}}
{{- $appInfo := dict
      "Name" $appName
      "Version" (printf "%s-%s" .ApplicationServer.Spec.Version $edition | lower)
      "Component" "ApplicationServer"
      "Role" "ApplicationServer"
-}}

namespace: {{ .ApplicationServer.Namespace }}
labels:
  {{- with .ApplicationServer.Labels }}
  {{-   . | toYaml | nindent 2 }}
  {{- end }}
  {{- include "commons/_commonLabels.tpl" $appInfo | nindent 2 }}
  gitlab.io/app-server: {{ $appName }}
  gitlab.io/edition: {{ $edition }}
annotations:
  {{- with .ApplicationServer.Annotations }}
  {{-   . | toYaml | nindent 2 }}
  {{- end }}
