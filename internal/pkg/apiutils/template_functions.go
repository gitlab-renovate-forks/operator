package apiutils

import (
	"text/template"

	corev1 "k8s.io/api/core/v1"
)

func FuncMap() template.FuncMap {
	return funcMap
}

var (
	funcMap = template.FuncMap{
		"patchWorkloadSpec":       ApplyWorkloadSpecToYAML,
		"getImageName":            GetImageName,
		"findStatusSharedObjects": FindStatusSharedObject,
		"getSecretValue":          getSecretValue,
		"getConfigMapValue":       getConfigMapValue,
	}
)

func getSecretValue(secret *corev1.Secret, keys ...string) string {
	if secret == nil {
		return ""
	}

	for _, key := range keys {
		if value, hasKey := secret.Data[key]; hasKey {
			return string(value)
		}

		// NOTE: This does not work with live Secrets. It is only used for testing.
		if value, hasKey := secret.StringData[key]; hasKey {
			return value
		}
	}

	return ""
}

func getConfigMapValue(secret *corev1.ConfigMap, keys ...string) string {
	if secret == nil {
		return ""
	}

	for _, key := range keys {
		if value, hasKey := secret.Data[key]; hasKey {
			return value
		}

		if value, hasKey := secret.BinaryData[key]; hasKey {
			return string(value)
		}
	}

	return ""
}
