package secret

import (
	"bytes"
	"encoding/pem"
	"fmt"

	"golang.org/x/crypto/ssh"
)

// SSHGenerator generates SSH key pairs.
type SSHGenerator struct {
	Algorithm    Algorithm
	Size         int
	AddPublicKey bool
}

// Generate validates the parameters and generates a public/private key pair.
func (g *SSHGenerator) Generate() (Payload, error) {
	if err := validateAlgorithmSize(g.Algorithm, g.Size); err != nil {
		return nil, err
	}

	priv, err := GeneratePrivateKey(g.Algorithm, g.Size)
	if err != nil {
		return nil, err
	}

	privPem, err := ssh.MarshalPrivateKey(priv, "")
	if err != nil {
		return nil, err
	}

	var privOut bytes.Buffer
	if err = pem.Encode(&privOut, privPem); err != nil {
		return nil, fmt.Errorf("failed to encode X509 key in PEM format: %w", err)
	}

	result := Payload{SSHPrivateKey: privOut.Bytes()}

	if g.AddPublicKey {
		pub, err := ssh.NewPublicKey(GetPublicKey(priv))
		if err != nil {
			return nil, err
		}

		pubBytes := ssh.MarshalAuthorizedKey(pub)
		result[SSHPublicKey] = pubBytes
	}

	return result, nil
}
