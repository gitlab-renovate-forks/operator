package internal

import (
	"cmp"
	"slices"
)

// Return a list of unique elements in `s`.
func Unique[S ~[]E, E cmp.Ordered](s S) S {
	if len(s) == 0 {
		return s
	}

	slices.Sort(s)

	return slices.Compact(s)
}

// Check if `q` is a subset of `s`.
func Subset[S ~[]E, E comparable](s, q S) bool {
	for _, e := range q {
		if !slices.Contains(s, e) {
			return false
		}
	}

	return true
}
