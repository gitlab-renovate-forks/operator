package appconfigutils

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func GetApplicationConfigRef(ref corev1.ObjectReference, source client.Object) (*types.NamespacedName, error) {
	if ref.APIVersion != "" && ref.APIVersion != v2alpha2.GroupVersion.String() {
		return nil, fmt.Errorf("unsupported group and version for ApplicationConfig reference: %s", ref.APIVersion)
	}

	if ref.Kind != "" && ref.Kind != "ApplicationConfig" {
		return nil, fmt.Errorf("unsupported kind for ApplicationConfig reference: %s", ref.Kind)
	}

	if ref.Namespace != "" && ref.Namespace != source.GetNamespace() {
		return nil, fmt.Errorf("expected ApplicationConfig to be in the same namespace: %s", ref.Namespace)
	}

	name := types.NamespacedName{
		Name:      ref.Name,
		Namespace: source.GetNamespace(),
	}

	return &name, nil
}

func AppConfigReady(appConfig *v2alpha2.ApplicationConfig) framework.ConditionFunc {
	return func(rtCtx *framework.RuntimeContext) (bool, error) {
		readyCondition := meta.FindStatusCondition(appConfig.Status.Conditions, v2alpha2.ApplicationConfigReadyCondition)

		return readyCondition != nil && readyCondition.Status == metav1.ConditionTrue, nil
	}
}

func RailsSecretRef(appConfig *v2alpha2.ApplicationConfig) (*types.NamespacedName, error) {
	sharedObjRef := apiutils.FindStatusSharedObject(appConfig.Status.SharedObjects, v2alpha2.RailsSecretsObjectUsage)
	if sharedObjRef == nil {
		return nil, fmt.Errorf("rails secrets is not specified: %s", client.ObjectKeyFromObject(appConfig))
	}

	return &types.NamespacedName{
		Name:      sharedObjRef.Name,
		Namespace: sharedObjRef.Namespace,
	}, nil
}
