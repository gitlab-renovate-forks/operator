package secret

type SSHHostKeysGenerator struct {
	Algorithms []Algorithm
}

func (g *SSHHostKeysGenerator) Generate() (Payload, error) {
	result := Payload{}

	for _, algorithm := range g.Algorithms {
		sshGen := SSHGenerator{
			Algorithm:    algorithm,
			Size:         algorithmSizes[algorithm],
			AddPublicKey: true,
		}

		hostKey, err := sshGen.Generate()
		if err != nil {
			return nil, err
		}

		keyRef := string(algorithm)
		result[keyRef] = hostKey[SSHPrivateKey]
		result[keyRef+".pub"] = hostKey[SSHPublicKey]
	}

	return result, nil
}

var (
	algorithmSizes = map[Algorithm]int{
		RSA:     3072,
		ECDSA:   256,
		ED25519: 256,
	}
)
