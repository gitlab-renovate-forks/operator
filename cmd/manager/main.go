package main

import (
	"crypto/tls"
	"fmt"
	"os"

	"runtime/debug"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/metrics/filters"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/applicationconfig"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/applicationserver"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/databasemigration"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/instance"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/jobprocessor"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/kubernetesagentserver"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/controllers/sshserver"

	//+kubebuilder:scaffold:imports

	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var (
	// Version is a human readable version set as ldflag on build.
	Version string = "unknown"

	Settings settings.Config = settings.Get()
)

func main() {
	pflag.Parse()

	if err := viper.BindPFlags(pflag.CommandLine); err != nil {
		panic(err)
	}

	if err := configureLogger(); err != nil {
		panic(err)
	}

	logBuildInfo()

	mgr, err := configureManager()
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	registerControllers(mgr)

	setupLog.Info("starting manager", "version", Version)

	if err := startManager(mgr); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

//nolint:wsl
func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(monitoringv1.AddToScheme(scheme))
	utilruntime.Must(v2alpha2.AddToScheme(scheme))

	//+kubebuilder:scaffold:scheme
}

//nolint:wsl
func registerControllers(mgr ctrl.Manager) {
	var err error

	if err = (&applicationconfig.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApplicationConfig")
		os.Exit(1)
	}

	if err = (&applicationserver.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApplicationServer")
		os.Exit(1)
	}

	if err = (&jobprocessor.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "JobProcessor")
		os.Exit(1)
	}

	if err = (&databasemigration.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "DatabaseMigration")
		os.Exit(1)
	}

	if err = (&instance.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Instance")
		os.Exit(1)
	}

	if err = (&sshserver.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "SSHServer")
		os.Exit(1)
	}

	if err = (&kubernetesagentserver.Reconciler{}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "KubernetesAgentServer")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder
}

func configureLogger() error {
	zapOpts, err := Settings.Log.ZapOptions()
	if err != nil {
		return err
	}

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(zapOpts)))

	return nil
}

func configureManager() (ctrl.Manager, error) {
	tlsOpts := []func(*tls.Config){}

	if !Settings.Runtime.HTTP2 {
		disableHTTP2 := func(c *tls.Config) { c.NextProtos = []string{"http/1.1"} }
		tlsOpts = append(tlsOpts, disableHTTP2)
	}

	webhookServer := webhook.NewServer(webhook.Options{
		TLSOpts: tlsOpts,
	})

	metricsServerOptions := metricsserver.Options{
		BindAddress:   Settings.Runtime.MetricsAddr,
		SecureServing: Settings.Runtime.MetricsSecure,
		TLSOpts:       tlsOpts,
	}

	if Settings.Runtime.MetricsSecure {
		metricsServerOptions.FilterProvider = filters.WithAuthenticationAndAuthorization
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		Metrics:                metricsServerOptions,
		WebhookServer:          webhookServer,
		HealthProbeBindAddress: Settings.Runtime.HealthProbeAddr,
		LeaderElection:         Settings.Runtime.LeaderElection,
		LeaderElectionID:       Settings.Runtime.LeaderElectionID,
	})

	if err != nil {
		return nil, fmt.Errorf("unable to create manager: %w", err)
	}

	if err := framework.UseManager(mgr); err != nil {
		return nil, fmt.Errorf("unable to initialize controller framework: %w", err)
	}

	return mgr, nil
}

func startManager(mgr ctrl.Manager) error {
	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		return fmt.Errorf("unable to set up health check: %w", err)
	}

	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		return fmt.Errorf("unable to set up ready check: %w", err)
	}

	return mgr.Start(ctrl.SetupSignalHandler())
}

func logBuildInfo() {
	if info, ok := debug.ReadBuildInfo(); !ok {
		setupLog.Info("could not read build info")
	} else {
		infoargs := []any{"version", Version, "goversion", info.GoVersion}

		for _, setting := range info.Settings {
			if key := setting.Key; key == "vcs.revision" || key == "vcs.time" {
				infoargs = append(infoargs, key, setting.Value)
			}
		}

		setupLog.Info("build info", infoargs...)
	}
}
