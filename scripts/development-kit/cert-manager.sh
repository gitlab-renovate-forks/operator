#!/usr/bin/env bash

SCRIPT_DIR=$(dirname "$0")

source $SCRIPT_DIR/lib/helper.sh

install() {
    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.1/cert-manager.yaml
    echo_stage "Waiting for cert-manager to be ready"
    kubectl wait --for=condition=established crd/certificates.cert-manager.io --timeout=5m

}

uninstall() {
    kubectl delete -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.1/cert-manager.yaml --ignore-not-found=true
}

usage() {
    echo "Usage: $0 [install|uninstall]"
    exit 1
}

case "$1" in
install)
    install
    ;;
uninstall)
    uninstall
    ;;
*)
    usage
    ;;
esac
