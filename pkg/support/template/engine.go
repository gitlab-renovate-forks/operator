package template

import (
	"bytes"
	"errors"
	"fmt"
	"io/fs"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/Masterminds/sprig/v3"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/kube"
)

// DefaultEngine is the default template engine. Use [SetupDefaultEngine] to
// initialize and configure it.
var DefaultEngine *Engine

// Initializes the default template engine with the given root directory and
// options.
//
// Usage Notes:
// This function is not thread-safe and does not panic. Use it only once in your
// application initialization. If the application depends on template engine,
// any error returned by this function must terminate the application.
func SetupDefaultEngine(root fs.FS, opts ...EngineOption) error {
	var err error
	DefaultEngine, err = Load(root, opts...)

	return err
}

// Engine provides a template engine for rendering and parsing Kubernetes object
// templates. At initialization time, the Engine traverses the root directory
// looking for templates.
//
// The templates can be identified by their full path relative to the template
// root directory. For example, in the following directory structure:
//
//		root
//		└── component-1
//		    ├── module-1
//		    │   └── hello.txt
//		    └── module-2
//		        └── hello.txt
//
//	 There are two templates:
//
//		  - component-1/module-1/hello.txt
//		  - component-1/module-2/hello.txt
//
// There are two different methods to use templates. While `Render` loads and
// renders the templates into [bytes.Buffer], `Parse` decodes the rendered
// templates and returns the result as a list of Kubernetes objects. You can
// customize the parse function with the `Decoder`.
//
// Both `Render` and `Parse` accept a glob pattern to identify the templates to
// render or parse. For example, `component-1/module-1/*.yaml`.
//
// In addition to its built-in functions, it accepts a list of functions that
// will be available to all templates.
type Engine struct {
	Functions template.FuncMap
	Decoder   runtime.Decoder

	isLoaded      bool
	rootDir       fs.FS
	rootTemplate  *template.Template
	childrenNames []string
	decoderToUse  runtime.Decoder
}

// EngineOption is a configuration option for the template engine.
type EngineOption = func(*Engine)

// WithDecoder configures the template engine to use the given decoder.
func WithDecoder(decoder *runtime.Decoder) EngineOption {
	return func(e *Engine) {
		e.Decoder = *decoder
	}
}

// WithFunctions configures the template engine to use the given functions.
func WithFunctions(functions ...template.FuncMap) EngineOption {
	return func(e *Engine) {
		for _, f := range functions {
			for k, v := range f {
				e.Functions[k] = v
			}
		}
	}
}

// New creates a new template engine with the given root directory. It accepts
// [io/fs.FS] which is compatible with both [os.DirFS] and [embed.FS].
func New(root fs.FS) *Engine {
	rootTpl := template.New("")
	rootTpl.Option(defaultTemplateOptions...)

	return &Engine{
		childrenNames: []string{},
		rootDir:       root,
		rootTemplate:  rootTpl,
		Functions:     template.FuncMap{},
	}
}

// Load creates a new template engine with the given root directory and options,
// and loads it. It is a shorthand function for `New` and `Load`.
func Load(root fs.FS, opts ...EngineOption) (*Engine, error) {
	e := New(root)

	for _, opt := range opts {
		opt(e)
	}

	err := e.Load()

	return e, err
}

// Load traverses the root directory looking for templates. It ignores any file
// or directory that starts with `.`. After successful loading, the templates
// cannot be changed and the engine cannot be reloaded.
//
// Built-in and user-defined functions are added at this stage. Once loaded, the
// functions cannot be changed.
func (e *Engine) Load() error {
	if e.rootTemplate == nil || e.childrenNames == nil {
		return errors.New("not initialized")
	}

	if e.isLoaded {
		return nil
	}

	e.addFunctions()

	err := fs.WalkDir(e.rootDir, ".", e.processDirEntry)
	if err != nil {
		return err
	}

	e.isLoaded = true

	return nil
}

// Render locates the templates matching the given glob pattern and renders them
// into [bytes.Buffer] using the given data. It considers any template that
// starts with `_` as a partial template and does not render it.
//
// The key of the returned map is the template name and the value is the rendered
// template.
func (e *Engine) Render(pattern string, data map[string]any) (map[string]*bytes.Buffer, error) {
	result := map[string]*bytes.Buffer{}

	regexPattern, err := globToRegexp(pattern)
	if err != nil {
		return result, err
	}

	for _, child := range e.childrenNames {
		// Do not render partial templates.
		if strings.HasPrefix(filepath.Base(child), "_") {
			continue
		}

		// Do not render templates that do not match the pattern.
		if !regexPattern.MatchString(child) {
			continue
		}

		// Rendering the text template.
		var output bytes.Buffer
		if err := e.rootTemplate.ExecuteTemplate(&output, child, data); err != nil {
			return result, err
		}

		result[child] = &output
	}

	return result, nil
}

// Parse locates the templates matching the given glob pattern and decodes the
// rendered templates into a list of Kubernetes objects using the given data.
// It considers any template that starts with `_` as a partial template and does
// not render it.
//
// You can customize the `Decoder` to control how the objects are parsed from
// the rendered templates. Note that a template can contain more than one object,
// for example divided by YAML separator. The built-in decoder is capable of
// handling this and parse more that one object.
func (e *Engine) Parse(pattern string, data map[string]any) ([]client.Object, error) {
	result := []client.Object{}

	sources, err := e.Render(pattern, data)
	if err != nil {
		return result, err
	}

	e.decoderToUse = e.Decoder
	if e.decoderToUse == nil {
		// Use unstructured types when decoder is not specified.
		e.decoderToUse = unstructured.UnstructuredJSONScheme
	}

	for path, buf := range sources {
		// A source can contain more than one object.
		objects, err := e.readObjects(path, buf)
		if err != nil {
			return result, err
		}

		result = append(result, objects...)
	}

	return result, nil
}

func (e *Engine) addFunctions() {
	funcMap := sprig.TxtFuncMap()

	for name, fnc := range builtinFuncMap {
		funcMap[name] = fnc
	}

	includeTracker := map[string]any{}
	funcMap["include"] = generateInclude(e.rootTemplate, includeTracker)

	for name, fnc := range e.Functions {
		funcMap[name] = fnc
	}

	e.rootTemplate.Funcs(funcMap)
}

func (e *Engine) processDirEntry(path string, entry fs.DirEntry, err error) error {
	if err != nil {
		return err
	}

	if entry.IsDir() && entry.Name() == "." {
		return nil
	}

	// Skip directories that start with a dot.
	if entry.IsDir() && strings.HasPrefix(entry.Name(), ".") {
		return fs.SkipDir
	}

	// Load regular files that do not start with dot.
	if entry.Type().IsRegular() && !strings.HasPrefix(entry.Name(), ".") {
		buf, err := fs.ReadFile(e.rootDir, path)
		if err != nil {
			return fmt.Errorf("failed to read template %#q: %w", path, getCause(err))
		}

		_, err = e.rootTemplate.New(path).Parse(string(buf))
		if err != nil {
			return fmt.Errorf("failed to parse template %#q: %w", path, getCause(err))
		}

		e.childrenNames = append(e.childrenNames, path)
	}

	return nil
}

func (e *Engine) readObjects(source string, b *bytes.Buffer) ([]client.Object, error) {
	result, err := kube.ReadObjects(b, e.decoderToUse)
	if err != nil {
		return result, fmt.Errorf("error parsing %s: %w", source, err)
	}

	return result, nil
}

func getCause(err error) error {
	causeToUse := errors.Unwrap(err)
	if causeToUse == nil {
		causeToUse = err
	}

	return causeToUse
}

func globToRegexp(glob string) (*regexp.Regexp, error) {
	pattern := glob

	pattern = regexp.MustCompile(`\.`).ReplaceAllString(pattern, "\\.")
	pattern = regexp.MustCompile(`\*\*$`).ReplaceAllString(pattern, "(.+)")
	pattern = regexp.MustCompile(`(?:\*\*/|\*\*|\*)`).ReplaceAllStringFunc(pattern, func(s string) string {
		return globPatterns[s]
	})

	return regexp.Compile("^" + pattern + "$")
}

var (
	defaultTemplateOptions []string = []string{
		"missingkey=error",
	}

	globPatterns = map[string]string{
		"*":   "([^/]+)",
		"**":  "(.+/)?([^/]+)",
		"**/": "(.+/)?",
	}
)
