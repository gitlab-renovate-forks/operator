package v2alpha2

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	/* Condition Types */
	UpgradeCompletedCondition   = "UpgradeCompleted"
	UpgradeProgressingCondition = "UpgradeProgressing"

	/* Condition Reasons */
	FailedUpgradeInitializationReason      = "UpgradeInitializationFailed"
	NoUpgradeAvailableReason               = "UpgradeNotAvailable"
	UpgradeProgressingReason               = "UpgradeProgressing"
	UpgradeTargetReachedReason             = "UpgradeTargetReached"
	IntermediateUpgradeTargetReachedReason = "IntermediateUpgradeTargetReached"
	UpgradeFailedReason                    = "UpgradeFailed"
)

// +kubebuilder:object:root=true
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=gl
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=`.spec.version`
// +kubebuilder:printcolumn:name="Edition",type=string,JSONPath=`.spec.edition`
type Instance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   InstanceSpec   `json:"spec,omitempty"`
	Status InstanceStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true
type InstanceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []Instance `json:"items"`
}

type InstanceSpec struct {
	// +required
	// +kubebuilder:validation:Pattern=^([1-9]\d?)\.(0|[1-9]\d?)\.(0|[1-9]\d?)$
	Version string `json:"version"`

	// +optional
	Edition Edition `json:"edition,omitempty"`
}

type InstanceStatus struct {
	// +optional
	Version string `json:"version,omitempty"`

	// +optional
	Edition Edition `json:"edition,omitempty"`

	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

func init() {
	SchemeBuilder.Register(&Instance{}, &InstanceList{})
}
