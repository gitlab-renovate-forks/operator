package main

import (
	"context"
	"fmt"
	"log/slog"

	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions"
	structuralschema "k8s.io/apiextensions-apiserver/pkg/apiserver/schema"
	"k8s.io/apiextensions-apiserver/pkg/apiserver/schema/cel"
	"k8s.io/apiextensions-apiserver/pkg/apiserver/validation"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/validation/field"
	celconfig "k8s.io/apiserver/pkg/apis/cel"
)

type ValidatorCollection map[schema.GroupVersionKind]*validator

func NewValidatorCollection(crdPath string) (ValidatorCollection, error) {
	crds, err := loadCRD(crdPath)
	if err != nil {
		return nil, err
	}

	validators := make(ValidatorCollection)

	for _, crd := range crds {
		vs, err := newValidators(crd)
		if err != nil {
			return nil, err
		}

		for _, v := range vs {
			slog.Debug("Validator added", "gvk", v.gvk.String())
			validators[v.gvk] = v
		}
	}

	return validators, nil
}

func (vc ValidatorCollection) ValidateCustomResource(cr *unstructured.Unstructured) field.ErrorList {
	v, ok := vc[cr.GroupVersionKind()]
	if !ok {
		slog.Error("no validator found", "group", cr.GetAPIVersion(), "kind", cr.GetKind())

		return field.ErrorList{
			field.InternalError(field.NewPath("."), fmt.Errorf("no validator found for %s/%s", cr.GetAPIVersion(), cr.GetKind())),
		}
	}

	return v.validateCustomResource(cr)
}

func (vc ValidatorCollection) ValidateCustomResourceUpdate(old, new *unstructured.Unstructured) field.ErrorList {
	v, ok := vc[old.GroupVersionKind()]
	if !ok {
		slog.Error("no validator found", "group", new.GetAPIVersion(), "kind", new.GetKind())

		return field.ErrorList{
			field.InternalError(field.NewPath("."), fmt.Errorf("no validator found for %s/%s", new.GetAPIVersion(), new.GetKind())),
		}
	}

	return v.validateCustomResourceUpdate(old, new)
}

type validator struct {
	gvk schema.GroupVersionKind

	schemaValidator validation.SchemaValidator
	celValidator    *cel.Validator
	structural      *structuralschema.Structural
}

func newValidators(crd *apiextensions.CustomResourceDefinition) ([]*validator, error) {
	logger := slog.Default().With("crd", crd.Name)
	results := make([]*validator, 0, len(crd.Spec.Versions))
	topScheme := crd.Spec.Validation.OpenAPIV3Schema

	// Handling multiple versions: https://pkg.go.dev/k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1#CustomResourceDefinitionSpec
	for _, version := range crd.Spec.Versions {
		scheme := topScheme
		if version.Schema != nil && version.Schema.OpenAPIV3Schema != nil {
			scheme = version.Schema.OpenAPIV3Schema
		}

		schemaValidator, _, err := validation.NewSchemaValidator(scheme)
		if err != nil {
			logger.Error("failed to create schema validator", "version", version.Name, "error", err)
			return nil, fmt.Errorf("failed to create schema validator for %s: %w", crd.Name, err)
		}

		structural, err := structuralschema.NewStructural(scheme)
		if err != nil {
			logger.Error("failed to create structural schema", "version", version.Name, "error", err)
			return nil, fmt.Errorf("failed to create structural schema for %s: %w", crd.Name, err)
		}

		celValidator := cel.NewValidator(structural, true, celconfig.PerCallLimit)

		v := &validator{
			gvk:             schema.GroupVersionKind{Group: crd.Spec.Group, Kind: crd.Spec.Names.Kind, Version: version.Name},
			schemaValidator: schemaValidator,
			celValidator:    celValidator,
			structural:      structural,
		}

		results = append(results, v)
	}

	return results, nil
}

func (v *validator) validateCustomResource(cr *unstructured.Unstructured) field.ErrorList {
	errs := validation.ValidateCustomResource(nil, cr.UnstructuredContent(), v.schemaValidator)
	celErrs, _ := v.celValidator.Validate(context.TODO(), nil, v.structural, cr.UnstructuredContent(), nil, celconfig.RuntimeCELCostBudget)

	return append(errs, celErrs...)
}

func (v *validator) validateCustomResourceUpdate(old, new *unstructured.Unstructured) field.ErrorList {
	if old.GroupVersionKind() != new.GroupVersionKind() {
		slog.Error("old and new objects have different GroupVersionKind", "old", old.GroupVersionKind(), "new", new.GroupVersionKind())

		return field.ErrorList{
			field.InternalError(field.NewPath("."), fmt.Errorf("old and new objects have different GroupVersionKind")),
		}
	}

	if old.GetName() != new.GetName() {
		slog.Error("old and new objects have different names", "old", old.GetName(), "new", new.GetName())

		return field.ErrorList{
			field.InternalError(field.NewPath(".metadata.name"), fmt.Errorf("old and new objects have different names")),
		}
	}

	if old.GetNamespace() != new.GetNamespace() {
		slog.Error("old and new objects have different namespaces", "old", old.GetNamespace(), "new", new.GetNamespace())

		return field.ErrorList{
			field.InternalError(field.NewPath(".metadata.namespace"), fmt.Errorf("old and new objects have different namespaces")),
		}
	}

	errs := validation.ValidateCustomResourceUpdate(nil, new.UnstructuredContent(), old.UnstructuredContent(), v.schemaValidator)
	celErrs, _ := v.celValidator.Validate(context.TODO(), nil, v.structural, new.UnstructuredContent(), old.UnstructuredContent(), celconfig.RuntimeCELCostBudget)

	return append(errs, celErrs...)
}
