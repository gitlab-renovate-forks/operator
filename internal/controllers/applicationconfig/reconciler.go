package applicationconfig

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// +kubebuilder:rbac:groups=gitlab.com,resources=applicationconfigs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationconfigs/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=gitlab.com,resources=applicationconfigs/finalizers,verbs=update
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete

type Reconciler struct{}

// Reconcile runs in the reconciliation loop for the ApplicationConfig resource.
func (r *Reconciler) Reconcile(ctx context.Context, obj *v2alpha2.ApplicationConfig) (ctrl.Result, error) {
	rtCtx, err := framework.NewRuntimeContext(ctx)
	if err != nil {
		return framework.Expand(err)
	}

	err = NewController(obj.DeepCopy()).Execute(rtCtx)

	return framework.Expand(err)
}

// SetupWithManager sets up the controller with the Manager.
func (r *Reconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v2alpha2.ApplicationConfig{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(reconcile.AsReconciler(mgr.GetClient(), r))
}
