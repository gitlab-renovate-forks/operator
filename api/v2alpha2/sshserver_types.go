package v2alpha2

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:object:root=true
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:resource:shortName=sshsrv
// +kubebuilder:printcolumn:name="Version",type="string",JSONPath=".spec.version"
// +kubebuilder:printcolumn:name="Available",type="string",JSONPath=".status.replicas"
type SSHServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SSHServerSpec   `json:"spec,omitempty"`
	Status SSHServerStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true
type SSHServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []SSHServer `json:"items"`
}

type SSHServerSpec struct {
	// +optional
	// +kubebuilder:validation:Minimum=0
	Replicas *int32 `json:"replicas,omitempty"`

	// +optional
	Paused bool `json:"paused,omitempty"`

	// +required
	// +kubebuilder:validation:Pattern="^([1-9]\\d?)\\.(0|[1-9]\\d?)\\.(0|[1-9]\\d?)$"
	Version string `json:"version"`

	// ApplicationServer references the ApplicationConfig which populates the internal API secret.
	// +required
	ApplicationServer SSHDApplicationServerProvider `json:"applicationServer"`

	// Daemon contains configuration options for the specified SSH daemon.
	// +optional
	Daemon SSHServerDaemon `json:"daemon,omitempty"`

	// +optional
	LFS SSHServerLFS `json:"lfs,omitempty"`

	// +optional
	PAT *SSHServerPAT `json:"pat,omitempty"`

	// Defaults to JSON. Available values: ("JSON", "Text")
	// +optional
	LogFormat LogFormat `json:"logFormat,omitempty"`

	// +optional
	LogLevel LogLevel `json:"logLevel,omitempty"`

	// +optional
	Tracing *TracingSupport `json:"tracing,omitempty"`

	// +optional
	Workload WorkloadSpec `json:"workload,omitempty"`
}

type SSHServerStatus struct {
	// +optional
	Version string `json:"version,omitempty"`

	// +optional
	Replicas int32 `json:"replicas,omitempty"`

	// +optional
	Selector string `json:"selector,omitempty"`

	// +optional
	// +patchMergeKey=usage
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=name
	SharedObjects []SharedObjectReference `json:"sharedObjects,omitempty" patchStrategy:"merge" patchMergeKey:"usage"`

	// +optional
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

// +kubebuilder:validation:XValidation:rule="has(self.service) || has(self.external)",message="either service or external application server provider must be set"
type SSHDApplicationServerProvider struct {
	// +optional
	Service *corev1.ObjectReference `json:"service,omitempty"`

	// +optional
	External *SSHDExternalApplicationServer `json:"external,omitempty"`
}

type SSHDExternalApplicationServer struct {
	// +required
	// +kubebuilder:validation:Format=uri
	ExternalURL string `json:"externalUrl"`

	// +required
	// +kubebuilder:validation:Format=uri
	InternalURL string `json:"internalUrl"`

	// +required
	APIToken corev1.SecretKeySelector `json:"apiToken"`
}

// SSHServerDaemon contains configuration options for the specified SSH daemon.
// +kubebuilder:validation:XValidation:rule="has(self.openSSH) || has(self.gitlabSSHD)",message="either openSSH or gitlabSSHD daemon must be set"
type SSHServerDaemon struct {
	// Settings for OpenSSH. Used when DaemonType is OpenSSH.
	// +optional
	OpenSSH *OpenSSHConfig `json:"openSSH,omitempty"`

	// Settings for GitLab SSHD. Used when DaemonType is GitLabSSHD.
	// +optional
	GitLabSSHD *GitLabSSHDConfig `json:"gitlabSSHD,omitempty"`
}

// Common SSH daemon options for all SSH daemon types.
type SSHDaemonConfig struct {
	// The name of the Kubernetes secret to grab the SSH host keys from.
	// The keys in the secret must start with the key names `ssh_host_` in order to be used by GitLab Shell.
	// If not provided, the operator will generate its own host keys.
	//
	// An example layout of the secret:
	//  - ssh_host_rsa_key
	//  - ssh_host_rsa_key.pub
	//  - ssh_host_ecdsa_key
	//  - ssh_host_ecdsa_key.pub
	//  - ssh_host_ed25519_key
	//  - ssh_host_ed25519_key.pub
	// +optional
	HostKeys *corev1.LocalObjectReference `json:"hostKeys,omitempty"`

	// Defaults to 0.
	// +optional
	// +kubebuilder:validation:Format=duration
	ClientAliveInterval *metav1.Duration `json:"clientAliveInterval,omitempty"`

	// Defaults to 60.
	// +optional
	// +kubebuilder:validation:Format=duration
	LoginGraceTime *metav1.Duration `json:"loginGraceTime,omitempty"`
}

// OpenSSHConfig contains extra configuration options for the OpenSSH daemon.
type OpenSSHConfig struct {
	SSHDaemonConfig `json:",inline"`

	// +optional
	// +kubebuilder:validation:Pattern="^\\d+:\\d+:\\d+$"
	// +kubebuilder:example="10:30:100"
	MaxStartups string `json:"maxStartups,omitempty"`

	// +optional
	ExtraConfig *corev1.ConfigMapKeySelector `json:"extraConfig,omitempty"`
}

// GitLabSSHDConfig contains configuration options for the GitLab SSHD daemon.
//
// See the [Shell example config] for more information.
//
// [Shell example config]: https://gitlab.com/gitlab-org/gitlab-shell/-/blob/main/config.yml.example
type GitLabSSHDConfig struct {
	SSHDaemonConfig `json:",inline"`

	// +optional
	Metrics MetricsSupport `json:"metrics"`

	// +optional
	ConcurrentSessionsLimit *int64 `json:"concurrentSessionsLimit,omitempty"`

	// If using Traefik or other proxy frontend, see https://doc.traefik.io/traefik/routing/services/#proxy-protocol.
	// +optional
	ProxyProtocol bool `json:"proxyProtocol,omitempty"`

	// PROXY protocol policy ("Use", "Require", "Reject", "Ignore"), "use" is the default value.
	// +kubebuilder:validation:Enum=Use;Require;Reject;Ignore
	// +optional
	ProxyPolicy string `json:"proxyPolicy,omitempty"`

	// Defaults to 500.
	// +optional
	// +kubebuilder:validation:Format=duration
	ProxyHeaderTimeout *metav1.Duration `json:"proxyHeaderTimeout,omitempty"`

	// +optional
	// +kubebuilder:validation:Format=duration
	GracePeriod *metav1.Duration `json:"gracePeriod,omitempty"`

	// +optional
	// +kubebuilder:validation:items:Pattern="^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$"
	Ciphers []string `json:"ciphers,omitempty"`

	// +optional
	// +kubebuilder:validation:items:Pattern="^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$"
	KexAlgorithms []string `json:"kexAlgorithms,omitempty"`

	// +optional
	// +kubebuilder:validation:items:Pattern="^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$"
	MACs []string `json:"macs,omitempty"`

	// +optional
	// +kubebuilder:validation:items:Pattern="^[a-zA-Z0-9-]+(?:@[a-zA-Z0-9.-]+)?$"
	PublicKeyAlgorithms []string `json:"publicKeyAlgorithms,omitempty"`

	// +optional
	GSSAPI *GSSAPISupport `json:"gssapi,omitempty"`
}

type SSHServerLFS struct {
	// +optional
	PureSSHProtocol bool `json:"pureSSHProtocol,omitempty"`
}

type SSHServerPAT struct {
	// +required
	// +kubebuilder:validation:items:Pattern="^[a-zA-Z][a-zA-Z0-9_]*$"
	AllowedScopes []string `json:"allowedScopes,omitempty"`
}

func init() {
	SchemeBuilder.Register(&SSHServer{}, &SSHServerList{})
}
