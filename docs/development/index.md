---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GitLab Operator V2 Development Kit

This development kit is designed to help you get started with the GitLab
Operator V2. It provides a set of scripts to build, test, and deploy the
operator.

## Prerequisites

- [Go](https://golang.org/doc/install) 1.22 or later
- [jq](https://stedolan.github.io/jq/download/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/) or a Kubernetes cluster
- [helm](https://helm.sh/docs/intro/install/)

## Quick Start

By default, the development kit uses `helm` to deploy stateful components
in the same namespace as the operator and the GitLab instance.

It uses the following default values:

- Namespace: `gitlab-operator-v2-system`
- Release Name: `gitlab-operator-v2`
- External SSH Port: `32022`
  - To change it, edit the `gitlab.yaml` and set `EXTERNAL_SSH_PORT`
    when executing [`nginx.sh`](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/scripts/development-kit/nginx.sh).
- External URL: `http://example.com`

_This quick start guide does not cover customizations._

### 1. Prepare a Kubernetes cluster (Skip if you have an existing cluster)

You can use `kind` with the default [`kind-conf.yaml`](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/scripts/development-kit/manifests/kind-conf.yaml)
to create a Kubernetes cluster:

```bash
kind create cluster --config scripts/development-kit/manifests/kind-conf.yaml
```

### 2. Install ingress-nginx controller

The `nginx.sh` script currently only supports local `kind`, `orbstack` clusters,
and GKE clusters.

Ensure that you have set the kubectl context to the desired cluster.

```bash
# With gke, you can specify the load balancer IP
export LOAD_BALANCER_IP=34.85.93.236
# Customize the external SSH port if needed, default is 32022, remember
# to update the gitlab.yaml as well
export EXTERNAL_SSH_PORT=32022
./scripts/development-kit/nginx.sh install
```

#### For other clusters

If your cluster comes with an ingress-nginx controller, you may need to follow
the guide to enable TCP proxy for SSH:
<https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/>

Or you can install it using the following commands:

```bash
export EXTERNAL_SSH_PORT=32022
export namespace=gitlab-operator-v2-system
export release=gitlab-operator-v2

# By default it will create a LoadBalancer service
helm upgrade --install ingress-nginx ingress-nginx \
    --repo https://kubernetes.github.io/ingress-nginx \
    --namespace ingress-nginx --create-namespace \
    --set tcp\.${EXTERNAL_SSH_PORT}=${namespace}/${release}-gitlab-shell:22

# If you need the NodePort service
helm upgrade --install ingress-nginx ingress-nginx \
    --repo https://kubernetes.github.io/ingress-nginx \
    --namespace ingress-nginx --create-namespace \
    --set tcp\.${EXTERNAL_SSH_PORT}=${namespace}/${release}-gitlab-shell:22 \
    --set controller.hostPort.enabled=true \
    --set controller.service.type=NodePort

# Annotate the ingress class as default
kubectl annotate ingressclasses nginx \
    'ingressclass.kubernetes.io/is-default-class'='true'
```

### 3. Install stateful components

The operator does not manage the stateful components, so you must install
them manually, or use external services.

Scripts are provided to quickly install individual components:

```bash
# Create the namespace first
kubectl create namespace gitlab-operator-v2-system

# Install the MinIO
./scripts/development-kit/minio.sh install

# Install the PostgreSQL
./scripts/development-kit/postgresql.sh install

# If you want to use the Valkey
./scripts/development-kit/valkey.sh install
# If you want to use the Redis
# ./scripts/development-kit/redis.sh install

# Must prepare Gitaly before installing GitLab first
./scripts/development-kit/gitaly.sh prepare
# Configure the GitLab CR using the generated Gitaly secret

# Install the Imapmemserver
# If you want to enable the Mailroom component the default
# devkit GitLab CR requires an IMAP server to start.
./scripts/development-kit/imapmemserver.sh install
```

### 4. Build and deploy the operator

#### Deploy from a branch

```bash
# Branch Tag
export TAG='main-staging'

make build deploy

# Check GitLab CRD and the Operator's controller-manager logs
kubectl api-resources | grep gitlab

stern -n gitlab-operator-v2-system -c manager controller-manager
```

#### Deploy the local changes

**Note:** If you are not using `kind` or `orbstack`, you must load
the image into your cluster.

First, change the `imagePullPolicy` to `IfNotPresent` in the [manager.yaml](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/3caf355f2182ea870fbb8e934d4ba4cf6b614c8c/config/manager/manager.yaml#L74)
file.

```bash
# Just a random image name
export IMG='operator:local'

make docker-build

# if you are using orbstack, you don't need to load the image

# if you are using kind
kind load docker-image ${IMG}

# deploy the operator and the CRD
make deploy

# Or delete the old pod
kubectl delete pod \
    -l control-plane=controller-manager \
    -n gitlab-operator-v2-system
```

### 5. Create a GitLab instance

**Note 1:** If the external SSH port have been changed, remember to edit
the [gitlab.yaml](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/scripts/development-kit/manifests/gitlab.yaml)
as well.

**Note 2:** If stateful components are installed individually, you must
prepare Gitaly before installing GitLab, and then install Gitaly.

```bash
kubectl apply -f scripts/development-kit/manifests/gitlabinstance.yaml \
    -n gitlab-operator-v2-system

kubectl apply -f scripts/development-kit/manifests/gitlab.yaml \
    -n gitlab-operator-v2-system

# If stateful components are installed individually, wait a few
# seconds for the operator to generate secrets, and run
./scripts/development-kit/gitaly.sh install

# Check Operator's controller-manager logs
stern -n gitlab-operator-v2-system -c manager controller-manager

# wait a few minutes for the application server to get ready
kubectl rollout status deployments \
    -n gitlab-operator-v2-system \
    -l app.kubernetes.io/name=operator-v2-dev-kit \
    --timeout=300s

# then it should be accessible from the ingress
kubectl get ingress -n gitlab-operator-v2-system

# or the service
kubectl port-forward services/operator-v2-dev-kit-application-server \
    8181:8181 -n gitlab-operator-v2-system

# use the initial root password to login
kubectl -n gitlab-operator-v2-system get secret \
    operator-v2-dev-kit-initial-root-password \
    -o jsonpath="{.data.password}" | base64 -d
```
