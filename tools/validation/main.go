package main

import (
	"fmt"
	"log/slog"
	"os"
	"strings"

	"github.com/spf13/cobra"
	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apimachinery/pkg/runtime"
	sjson "k8s.io/apimachinery/pkg/runtime/serializer/json"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
)

var (
	crdPath  string
	logLevel string
)

var (
	scheme  = runtime.NewScheme()
	decoder *sjson.Serializer
)

var rootCmd = &cobra.Command{
	Use:     "validation",
	Short:   "validation is a validation tool for Kubernetes CRDs",
	Example: "  validation -c config/crd -l debug create custom-resource.yaml",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		switch strings.ToLower(logLevel) {
		case "debug":
			slog.SetLogLoggerLevel(slog.LevelDebug)
		case "info":
			slog.SetLogLoggerLevel(slog.LevelInfo)
		case "warn":
			slog.SetLogLoggerLevel(slog.LevelWarn)
		case "error":
			slog.SetLogLoggerLevel(slog.LevelError)
		default:
			return fmt.Errorf("invalid log level: %s", logLevel)
		}

		return nil
	},
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&crdPath, "crd", "c", "config/crd", "Path to crd's kustomization.yaml")
	rootCmd.PersistentFlags().StringVarP(&logLevel, "log-level", "l", "info", "Log level")

	rootCmd.AddCommand(createCmd)
	rootCmd.AddCommand(updateCmd)

	utilruntime.Must(apiextensions.AddToScheme(scheme))
	utilruntime.Must(apiextensionsv1.AddToScheme(scheme))
	decoder = sjson.NewSerializerWithOptions(
		sjson.DefaultMetaFactory, scheme, scheme, sjson.SerializerOptions{Yaml: true, Pretty: true, Strict: true},
	)
}

func main() {
	if rootCmd.Execute() != nil {
		os.Exit(1)
	}
}
