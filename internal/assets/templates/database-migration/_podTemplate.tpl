{{- $spec := .DatabaseMigration.Spec -}}

{{- $railsSecretRef := findStatusSharedObjects .ApplicationConfig.Status.SharedObjects "RailsSecret" -}}
{{- $railsSecretName :=  empty $railsSecretRef | ternary (printf "%s-rails-secrets" .ApplicationConfig.Name) (digStruct $railsSecretRef "Name") -}}
{{- $initRootPasswordSecretName := empty $spec.InitialRootPassword | ternary (printf "%s-%s-root-password" .DatabaseMigration.Name $spec.Version) (digStruct $spec.InitialRootPassword "Name") -}}

metadata:
  {{- with .DatabaseMigration.Labels }}
  labels:
    {{ . | toYaml | nindent 4 }}
  {{- end }}
  {{- with .DatabaseMigration.Annotations }}
  annotations:
    {{ . | toYaml | nindent 4 }}
  {{- end }}
spec:
  automountServiceAccountToken: false
  {{- include "commons/_defaultPodSecurityContext.tpl" .Settings.Rails | nindent 2 }}
  restartPolicy: OnFailure
  terminationGracePeriodSeconds: 30
  containers:
    - name: migration
      {{- include "database-migration/_migration.tpl" . | nindent 6 }}
  volumes:
    - name: migration-config
      secret:
        secretName: {{ .DatabaseMigration.Name }}-migration-config
    - name: rails-secrets
      secret:
        secretName: {{ $railsSecretName }}
    {{- if not $spec.IsUpgrade }}
    - name: initial-password
      secret:
        secretName: {{ $initRootPasswordSecretName }}
    {{- end }}
    {{- if $spec.License }}
    - name: license
      secret:
        secretName: {{ $spec.License.Name }}
    {{- end }}
    - name: secrets
      projected:
        defaultMode: 0400
        sources:
          # - secret:
        {{- $tlsVolumes := populateTLSSupport .DatabaseMigration .ApplicationConfig -}}
        {{- range $tlsPath, $tlsSupport := $tlsVolumes }}
        {{-   include "commons/_tlsSupportProjection.tpl" (dict "TLS" $tlsSupport "Path" $tlsPath) | nindent 10 }}
        {{- end }}
