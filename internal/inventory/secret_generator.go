package inventory

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var _ inventory.Inventory[*corev1.Secret] = &SecretGenerator{}

// SecretGenerator generates a Kubernetes Secret with a the given parameters and
// secret generators.
//
// When GenerateIfNeeded is true, the secret will be generated if it does not
// exist.
type SecretGenerator struct {
	Name             string
	Namespace        string
	SecretType       corev1.SecretType
	Generators       []secret.Generator
	GenerateIfNeeded bool
}

func (i *SecretGenerator) Get(rtCtx *framework.RuntimeContext) ([]*corev1.Secret, error) {
	qName, err := utils.ExpandStrings(rtCtx, i.Name, i.Namespace)
	if err != nil {
		// This is most likely a template error. Terminate the loop and do not
		// retry.
		return []*corev1.Secret{}, framework.Terminate(err)
	}

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      qName[0],
			Namespace: qName[1],
		},
		Type: i.SecretType,
	}

	if i.GenerateIfNeeded {
		if err := framework.Client.Get(rtCtx, client.ObjectKeyFromObject(secret), secret); err == nil {
			return []*corev1.Secret{secret}, nil
		} else if !errors.IsNotFound(err) {
			return []*corev1.Secret{}, err
		}
	}

	for _, g := range i.Generators {
		payload, err := g.Generate()
		if err != nil {
			return []*corev1.Secret{}, err
		}

		payload.CopyInto(secret)
	}

	return []*corev1.Secret{secret}, nil
}
