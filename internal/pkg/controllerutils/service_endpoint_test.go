package controllerutils

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var _ = Describe("DereferenceServiceProvider", Ordered, func() {
	var (
		fakeScheme *runtime.Scheme
		fakeClient client.WithWatch
		rtCtx      *framework.RuntimeContext

		svcPostgreSQL, svcRedis *corev1.Service
	)

	BeforeAll(func() {
		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	})

	BeforeEach(func() {
		svcPostgreSQL = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-postgresql",
				Namespace: "default",
			},
			Spec: corev1.ServiceSpec{
				Ports: []corev1.ServicePort{
					{
						Name: "tcp-postgresql",
						Port: 6543,
					},
				},
			},
		}

		svcRedis = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-redis",
				Namespace: "default",
			},
			Spec: corev1.ServiceSpec{
				Ports: []corev1.ServicePort{
					{
						Name: "unpredictable",
						Port: 6379,
					},
				},
			},
		}

		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(svcPostgreSQL, svcRedis).
			Build()

		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	DescribeTable("External provider scenarios",
		func(provider v2alpha2.ServiceEndpoint, endpointType apiutils.ServiceEndpointType, expected apiutils.ServiceEndpoint) {
			endpoint, err := DereferenceServiceProvider(rtCtx, provider, endpointType)

			Expect(err).NotTo(HaveOccurred())
			Expect(endpoint).To(Equal(expected))
		},
		Entry("External PostgreSQL with specified port and TLS enabled",
			v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "postgres.example.com",
					Port: 6543,
				},
				TLS: utils.Ptr[bool](true),
			},
			apiutils.PostgreSQLServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "postgresql",
				Host:   "postgres.example.com",
				Port:   6543,
				UseTLS: true,
			},
		),
		Entry("External Redis with TLS enabled and default port",
			v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "redis.example.com",
				},
				TLS: utils.Ptr[bool](true),
			},
			apiutils.RedisServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "rediss",
				Host:   "redis.example.com",
				Port:   6379,
				UseTLS: true,
			},
		),
		Entry("External Redis with specified port and TLS disabled",
			v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "redis.example.com",
					Port: 6380,
				},
				TLS: utils.Ptr[bool](false),
			},
			apiutils.RedisServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "redis",
				Host:   "redis.example.com",
				Port:   6380,
				UseTLS: false,
			},
		),
		Entry("External PostgreSQL with TLS disabled and default port",
			v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "postgres.example.com",
				},
				TLS: utils.Ptr[bool](false),
			},
			apiutils.PostgreSQLServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "postgresql",
				Host:   "postgres.example.com",
				Port:   5432,
				UseTLS: false,
			},
		),
		Entry("External PostgreSQL with TLS unspecified",
			v2alpha2.ServiceEndpoint{
				External: &v2alpha2.ExternalServiceEndpoint{
					Host: "postgres.example.com",
				},
			},
			apiutils.PostgreSQLServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "postgresql",
				Host:   "postgres.example.com",
				Port:   5432,
				UseTLS: false,
			},
		),
		Entry("In-cluster PostgreSQL service with discoverable port",
			v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-postgresql",
					Namespace: "default",
				},
			},
			apiutils.PostgreSQLServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "postgresql",
				Host:   "test-postgresql.default.svc.cluster.local",
				Port:   6543,
				UseTLS: false,
			},
		),
		Entry("In-cluster PostgreSQL service with TLS enabled",
			v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-postgresql",
					Namespace: "default",
				},
				TLS: utils.Ptr[bool](true),
			},
			apiutils.PostgreSQLServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "postgresql",
				Host:   "test-postgresql.default.svc.cluster.local",
				Port:   6543,
				UseTLS: true,
			},
		),
		Entry("In-cluster Redis service with unrecognizable port",
			v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-redis",
					Namespace: "default",
				},
			},
			apiutils.RedisServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "redis",
				Host:   "test-redis.default.svc.cluster.local",
				Port:   6379,
				UseTLS: false,
			},
		),
		Entry("In-cluster Redis service with TLS enabled",
			v2alpha2.ServiceEndpoint{
				Service: &corev1.ObjectReference{
					Name:      "test-redis",
					Namespace: "default",
				},
				TLS: utils.Ptr[bool](true),
			},
			apiutils.RedisServiceEndpoint,
			apiutils.ServiceEndpoint{
				Scheme: "rediss",
				Host:   "test-redis.default.svc.cluster.local",
				Port:   6379,
				UseTLS: true,
			},
		),
	)

	Context("error cases", func() {
		It("returns error for ambiguous provider", func() {
			provider := v2alpha2.ServiceEndpoint{}
			_, err := DereferenceServiceProvider(rtCtx, provider, apiutils.PostgreSQLServiceEndpoint)
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(Equal("ambagious ServiceProvider for PostgreSQL"))
		})
	})
})
