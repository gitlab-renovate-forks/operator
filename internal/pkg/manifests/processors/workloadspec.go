package processors

import (
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
)

// WithWorkloadSpec is a function that applies a workload spec to a workload object.
// It is a no-op for objects that do not have a workload spec.
func WithWorkloadSpec(spec *v2alpha2.WorkloadSpec) func(client.Object) error {
	return func(workload client.Object) error {
		switch x := workload.(type) {
		case *appsv1.Deployment:
			return apiutils.ApplyWorkloadSpec(spec, &x.Spec.Template)
		case *appsv1.StatefulSet:
			return apiutils.ApplyWorkloadSpec(spec, &x.Spec.Template)
		case *batchv1.Job:
			return apiutils.ApplyWorkloadSpec(spec, &x.Spec.Template)
		}

		return nil
	}
}
